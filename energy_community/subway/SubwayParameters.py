from datetime import datetime
import os
import pandas as pd
from buildingenergy.timemg import stringdate_to_datetime

import logging

from energy_community.core.simulation.parameters import BaseParameters
logger = logging.getLogger(__name__)

class SubwayParameters(BaseParameters):
    """Parameters of a simulation scenario related to an energy community near a subway station

    Args:
        Params (_type_): The Params class
    """

    def __init__(self, root_folder_name, community_folder_name, data_folder_name, configuration_file_name, simulation_id: int, print_flag : bool = False):
        """Initialization function for the SubwayParams class.

        Args:
            cfg_file_path (str): subway community configuration file
            sim_id (int): subway community scenario id
            print_flag (bool, optional): print messages during a simulation. Defaults to False.
        """
        super().__init__(root_folder_name, community_folder_name, data_folder_name, configuration_file_name, simulation_id, print_flag)
        
        flexibility_parameters = self.scenario_config["flexibility_period"]
        self.init_flexibility_period_parameters(flexibility_parameters)
        
        self.init_pv_parameters()
  


        '''
        start_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_1_start'][self.simulation_id].item())
        #self.flexibility_period_1_start = stringdate_to_datetime(start_date, date_format='%H:%M')
        self.flexibility_period_1_start = pd.to_datetime(start_date)
        #print(self.flexibility_period_1_start)
        
        end_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_1_end'][self.simulation_id].item())
        #self.flexibility_period_1_end = stringdate_to_datetime(end_date, date_format='%H:%M')
        self.flexibility_period_1_end = pd.to_datetime(end_date)

        start_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_2_start'][self.simulation_id].item())
        #self.flexibility_period_2_start = stringdate_to_datetime(start_date, date_format='%H:%M')
        self.flexibility_period_2_start = pd.to_datetime(start_date)
        
        end_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_2_end'][self.simulation_id].item())
        #self.flexibility_period_2_end = stringdate_to_datetime(end_date, date_format='%H:%M')
        self.flexibility_period_2_end = pd.to_datetime(end_date)
        '''
        
        logger.info('----------SIMULATION DESCRIPTION---------------')
        logger.info(f'- PV plant number of panels: {self.pv_plant_number_of_modules}')
        logger.info(f'- PV plant module power: {self.pv_plant_module_power}')
        logger.info(f'- Solar data location: {self.solar_location}')
        logger.info(f'- Simulation start date: {self.starting_date}')
        logger.info(f'- Simulation end date: {self.ending_date}')
        
        logger.info('----------Subway community specific parameters---------------')
        logger.info(f'- Flexibility period 1 between: {self.flexibility_period["morning"]["start"].hour} and {self.flexibility_period["morning"]["end"].hour}')
        logger.info(f'- Flexibility period 2 between: {self.flexibility_period["afternoon"]["start"].hour} and {self.flexibility_period["afternoon"]["end"].hour}')

    
    def init_pv_parameters(self):
        
        self.pv_plant_number_of_modules = self.solar_parameters["number_of_modules"]
        self.pv_plant_module_power = self.solar_parameters["module_power"]

    def init_flexibility_period_parameters(self, flexibility_parameters: dict):
        """Initializes the simulation parameters from the given dictionary.

        Args:
            simulation_parameters (dict): Dictionary containing the simulation parameters.
        """

        self.flexibility_period = {"morning": {}, "afternoon": {}}
        
        for period in ["morning", "afternoon"]:
            for coordinate in ["start", "end"]:
                self.flexibility_period[period][coordinate] = self.get_date_as_datetime_from_str(flexibility_parameters[period][coordinate], "%H:%M")

    
    
    def convert_string_to_list(self, string : str, separator: str):
        """Split a string into a list of elements based on the separator.

        Args:
            string (str): the target string
            separator (str): the separator used

        Returns:
            list[str]: a list of elements
        """
        li = list(string.split(separator))
        return li