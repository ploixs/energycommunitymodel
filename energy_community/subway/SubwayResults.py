
from datetime import tzinfo
import os
from typing import Dict, List, Tuple
from matplotlib import gridspec, pyplot as plt
import numpy as np
import pytz
from energy_community.core.data.collective_data import CollectiveDataTotal
from energy_community.core.data.member_data import MemberDataTotal
from energy_community.helpers import indicators
from energy_community.core.constants import FLEX_MAP, AsignType, DSType, DType, DayPeriods, FlexibilityCapacity, IndicatorsAll, IndicatorsInd, IndicatorsLP, IndicatorsLoad, MType, Rec, Stages, Strategy, Interval
from energy_community.helpers.indicators import NEEG, NEEG_per_day, Normalised_NEEG, avg_dissatisfaction_member, cost, effort, involvement_based_on_contribution, renunciation, savings_per_day, self_consumption, self_sufficiency, money_for_grid_energy
from energy_community.subway.SubwayParameters import SubwayParameters
from energy_community.subway.core import SubwayCommunity, SubwayCommunityMember


from energy_community.subway.profiles import OneTurnFullFlexibilitySubwayMember, OneTurnNoFlexibilitySubwayMember, OneTurnSemiFlexibleSubwayMember, OneTurnSubwayManager
import matplotlib.lines as mlines
from matplotlib import markers
from energy_community.helpers.Spec import Spec
from energy_community.helpers.Results import Results
import pandas as pd
import matplotlib.dates as mdates

from tzlocal import get_localzone 

import logging
logger = logging.getLogger(__name__)


class SubwayResults():
    """Results analysis for a community near a subway station"""
    
    def __init__(self, id: int, analysis_spec: Spec, results_folder_path: str):
        """Initialization function for the subway community results.

        Args:
            id (int): subway community scenario ID
            analysis_spec (Spec): plotting and writing specification
        """
        self.spec : Spec = analysis_spec
        self.scenario_id = id
        #self.local_tz = str(get_localzone())
        self.local_tz = 'Europe/Bucharest'
        
        #self.local_tz = 'utc'
        self.performances  = {} # performances at community level
        self.performances_each_cm = {} # performances at cm level
        self.cm_file_writing_data = {} # cm data to be written in files
        self.cm_file_writing_column_names = [] # cm data columns
        self.file_writing_data = [] # community level data to be written in files
        self.file_writing_column_names : List[str] = [] # community level data column names
        self.results_folder_path = os.path.join(results_folder_path, f"Scenario_{self.scenario_id}") # the folder path to the results of the respective simulation scenario
        
        self.hourly_com_results_full_path = os.path.join(self.results_folder_path, f"Scenario_{self.scenario_id}_com_hourly.csv")
        self.agent_results_full_path = os.path.join(self.results_folder_path, f"Scenario_{self.scenario_id}_agents.csv")
        self.hourly_cm_results_full_path = os.path.join(self.results_folder_path, f"Scenario_{self.scenario_id}_cm_hourly.csv")
        self.cm_profiles_path =  os.path.join(self.results_folder_path, f"Scenario_{self.scenario_id}_cm_profiles.csv")
        self.manager_profile_path =  os.path.join(self.results_folder_path, f"Scenario_{self.scenario_id}_manager_profile.csv")
        
        self.com_indicators_load_prod = {IndicatorsLP.SC: self_consumption, 
                                         IndicatorsLP.SS: self_sufficiency,
                                         IndicatorsLP.COST: cost,
                                         IndicatorsLP.NEEG_PER_DAY: NEEG_per_day,
                                         IndicatorsLP.NEEG: NEEG} # a mapping dict between indicators and corresponding functions from indicators.py

        self.REC_MAPPING = {str(Rec.RED): 3, str(Rec.WHITE):  0, str(Rec.GREEN): 5}
        self.ACT_MAPPING = {str(Rec.RED): 3, str(Rec.WHITE):  0, str(Rec.GREEN): 2.5}
        
        
        
        self.COM_INDICATORS_LOAD = {IndicatorsLoad.EFFORT: effort,
                                    IndicatorsLoad.RENUNCIATION: renunciation} # a mapping dict between load-specific indicators and corresponding functions from indicators.py
        
        self.MEMBER_MAP = {(str(Stages.ONE_TURN),str(FlexibilityCapacity.FULLY_FLEXIBLE)) : OneTurnFullFlexibilitySubwayMember,
                           (str(Stages.ONE_TURN),str(FlexibilityCapacity.SEMY_FLEXIBLE)) : OneTurnSemiFlexibleSubwayMember,
                           (str(Stages.ONE_TURN),str(FlexibilityCapacity.NO_FLEXBILITY)) : OneTurnNoFlexibilitySubwayMember} # a mapping between member prodile and corresponding class init
        
        self.params = SubwayParameters("energy_community", "subway", "data", "scenarios_strategy_comparison.yaml", self.scenario_id, print_flag=False)
  
        # Generate simulation results
        self.com_model = SubwayCommunity(self.params, self.results_folder_path) # STEP 1: generate the community model 
        self.set_community_manager_based_on_profile() #  STEP 2: assign the manager
        com_energy_models = self.get_com_energy_models() # STEP 3: get community energy models
        members = self.get_community_members_based_on_profiles() # STEP 4: init the members
        self.manager.update_manager(com_energy_models, members) # STEP 5: update the manger with the models and the members
        self.manager.set_representative_cms_and_cm_count() # STEP 6: filter the members and select only the representative ones
        cm_data_models = self.get_cm_data_models() # STEP 7: update only the representative members
        for _, members_with_same_cap in self.manager.cm_by_flex_and_pref_hr.items(): # STEP 8: ...
            for _, member in members_with_same_cap.items():
                member.update_cm(cm_data_models[member.unique_id]) # ... for the representative members, assign the simulated data
                member.set_df_for_plotting()
        
        self.manager.set_df_for_plotting()  # STEP 9: set the manager dataframe for plotting purposes
            
        self.set_performances_all_simulation() # STEP 10: calculte performances
        self.set_results_file_name() # STEP 11: set the file name for writing data
        self.set_file_writing_data_and_column_names()  # STEP 12: Set the data to be written in files
    
    def get_com_energy_models(self) -> Tuple[CollectiveDataTotal,CollectiveDataTotal, CollectiveDataTotal]:
        """Extracts the community profiles (simulated consumption, estimated consumption and estiamted production) from files.

        Returns:
            Tuple[CollectiveDataTotal,CollectiveDataTotal, CollectiveDataTotal]: simulated consumption, estimated consumption, estimated production
        """
        
        df = pd.read_csv(self.hourly_com_results_full_path)
        eu_tz = pytz.timezone(self.local_tz)
        self.timeframe_simulation = [pd.to_datetime(x).astimezone(eu_tz) for x in df['sim_time'].to_list()] # extract the simulation hourly time
        est_com_load_total_model = CollectiveDataTotal(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.COMMUNITY) # estimated consumption empty model
        sim_com_load_total_model = CollectiveDataTotal(DSType.SIMULATED, DType.CONSUMPTION, AsignType.COMMUNITY) # simulated consumption empty model
        est_com_prod_total_model = CollectiveDataTotal(DSType.ESTIMATED, DType.PRODUCTION, AsignType.COMMUNITY) # estimated production empty model

        est_com_load_total_model.data = {self.timeframe_simulation[i] : value for i,value in enumerate(df[f'{DSType.ESTIMATED}_{DType.CONSUMPTION}_{AsignType.COMMUNITY}_{Interval.SIMULATION}'].to_list()) }
        sim_com_load_total_model.data = {self.timeframe_simulation[i] : value for i,value in enumerate(df[f'{DSType.SIMULATED}_{DType.CONSUMPTION}_{AsignType.COMMUNITY}_{Interval.SIMULATION}'].to_list()) }
        est_com_prod_total_model.data = {self.timeframe_simulation[i] : value for i,value in enumerate(df[f'{DSType.ESTIMATED}_{DType.PRODUCTION}_{AsignType.COMMUNITY}_{Interval.SIMULATION}'].to_list()) }
        return sim_com_load_total_model, est_com_load_total_model, est_com_prod_total_model
        
    
    def set_performances_all_simulation(self):
        """Calculate the performances of the simualtion scenario """
        
        if len(self.manager.est_com_load_total.data) == 0 or len(self.manager.est_com_prod_total.data) == 0 or len(self.manager.sim_com_load_total.data) == 0:
            raise ValueError("Community hourly energy models not initialized!")
        if len(self.manager.cm_list) == 0:
            raise ValueError("CMs not initialized!")
        
        days_in_sim = len(self.com_model.start_of_day_end_of_day_index_map) # get the number of days

        for com_load in [self.manager.est_com_load_total, self.manager.sim_com_load_total]: # for each case: estimated and simulated consumption
            for indicator in self.com_indicators_load_prod:
                self.performances[(com_load.data_source_type, com_load.asignee_type, indicator)] = round(self.com_indicators_load_prod[indicator](list(com_load.data.values()), list(self.manager.est_com_prod_total.data.values())),2) # calculate the indicator based on the mapping self.com_indicators_load_prod
                logger.info(f"-- {com_load.data_source_type} {com_load.asignee_type} {indicator} : {self.performances[(com_load.data_source_type, com_load.asignee_type, indicator)]} ")
        for indicator in self.COM_INDICATORS_LOAD: # for each indicator based specifically on load: effort, renounciation
            self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, indicator)] = round(self.COM_INDICATORS_LOAD[indicator](list(self.manager.est_com_load_total.data.values()), list(self.manager.sim_com_load_total.data.values())),2)
        
        self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsAll.NORMALISED_NEEG)] = round(Normalised_NEEG(list(self.manager.est_com_load_total.data.values()), list(self.manager.sim_com_load_total.data.values()), list(self.manager.est_com_prod_total.data.values())),2)
        self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLP.DAILY_SAVINGS)] = round(savings_per_day(list(self.manager.est_com_load_total.data.values()), list(self.manager.sim_com_load_total.data.values()), list(self.manager.est_com_prod_total.data.values()), days_in_sim, len(self.manager.cm_list)),2)
        self.performances[(DSType.ESTIMATED, AsignType.COMMUNITY, IndicatorsLoad.MONEY_FOR_GRID_ENERGY)] = round(money_for_grid_energy(list(self.manager.est_com_load_total.data.values()), 1-self.performances[(DSType.ESTIMATED, AsignType.COMMUNITY, IndicatorsLP.SS)]),2)
        self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLoad.MONEY_FOR_GRID_ENERGY)] = round(money_for_grid_energy(list(self.manager.sim_com_load_total.data.values()), 1-self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLP.SS)]),2)

        if self.spec.calculate_individual_indicators:  # Calculate individual indicators (for each member)
            for flex_cap in self.manager.cm_by_flex_and_pref_hr:
                for cm in list(self.manager.cm_by_flex_and_pref_hr[flex_cap].values()): # for each representative member
                    self.performances_each_cm[cm.unique_id] = {}
                    
                    dif_rho_list = [(cm.est_member_load_total.data[time] / est_com_load) if est_com_load != 0 else 0 for time,est_com_load in self.manager.est_com_load_total.data.items() ] # Get differential share values
                    member_p_pv = [prod_val * dif_rho_list[i] for i, prod_val in enumerate(list(self.manager.est_com_load_total.data.values()))] # calculate the corresponding share of production for each member
                    
                    self.performances_each_cm[cm.unique_id][(DSType.SIMULATED, AsignType.MEMBER, IndicatorsLP.NEEG)] = NEEG(list(cm.sim_member_load_total.data.values()), member_p_pv)
                    self.performances_each_cm[cm.unique_id][(DSType.ESTIMATED, AsignType.MEMBER, IndicatorsInd.AVERAGE_ESTIMATED_LOAD_DAY)] = sum(list(cm.est_member_load_total.data.values())) * 24 / len(list(self.manager.est_com_prod_total.data.values())) / 1000
                    self.performances_each_cm[cm.unique_id][(DSType.ESTIMATED, AsignType.MEMBER, IndicatorsInd.DISSATISFACTION_PER_CM)] = avg_dissatisfaction_member(list(cm.sim_member_load_total.data.values()), list(cm.est_member_load_total.data.keys()), len(self.manager.flexibility_periods[0].get_hours()), cm.pref_hour[DayPeriods.MORNING]) # the estimated dissatisfaction by the manager, with 8 hrs duration of work
                    self.performances_each_cm[cm.unique_id][(DSType.SIMULATED, AsignType.MEMBER, IndicatorsInd.DISSATISFACTION_PER_CM)] = avg_dissatisfaction_member(list(cm.sim_member_load_total.data.values()), list(cm.est_member_load_total.data.keys()), len(self.manager.flexibility_periods[0].get_hours()), cm.pref_hour[DayPeriods.MORNING], cm.pref_duration) # the hidden, real, dissatisfaction
                    self.performances_each_cm[cm.unique_id][(DSType.SIMULATED, AsignType.MEMBER, IndicatorsInd.FAV_COMMUTING_HOUR)] = cm.pref_hour[DayPeriods.MORNING]
                    self.performances_each_cm[cm.unique_id][(DSType.SIMULATED, AsignType.MEMBER, IndicatorsInd.FAV_DURATION)] = cm.pref_duration
                    
            self.cm_file_writing_column_names += [f'cm neeg',f'average exp load per day',f'avg est dissatisfaction',f'avg sim dissatisfaction',f'favourite hour', 'favourite duration']
        
        sim_dis_values = []
        est_dis_values = []
        cm_count_total = 0
        for flex_cap in self.manager.cm_by_flex_and_pref_hr:
                for pref_hour, cm in self.manager.cm_by_flex_and_pref_hr[flex_cap].items(): # for each representative member
                    sim_dis_value = self.performances_each_cm[cm.unique_id][(DSType.SIMULATED, AsignType.MEMBER, IndicatorsInd.DISSATISFACTION_PER_CM)] # get hidden dis of the rep member
                    est_dis_value = self.performances_each_cm[cm.unique_id][(DSType.ESTIMATED, AsignType.MEMBER, IndicatorsInd.DISSATISFACTION_PER_CM)] # get the estimated dis of the rep member
                    sim_dis_values += [sim_dis_value] * self.manager.cm_count[flex_cap][pref_hour] # multiply the dis of one rep member with the cm count from that specific category
                    est_dis_values += [est_dis_value] * self.manager.cm_count[flex_cap][pref_hour] # multiply the dis of one rep member with the cm count from that specific category
                    cm_count_total += self.manager.cm_count[flex_cap][pref_hour] # count the members from that specific category
        
        self.performances[(DSType.ESTIMATED, AsignType.COMMUNITY, IndicatorsAll.AVERAGE_DISSATISFACTION)] = round(sum(est_dis_values) / cm_count_total,2) # calculate the average estimated dissatisfaction by the manager   
        self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsAll.AVERAGE_DISSATISFACTION)] = round(sum(sim_dis_values) / cm_count_total,2) # calculate the average simulated hidden dissatisfaction  
        
        print("- Simulation performances: ")
        for key, value in self.performances.items(): # print community performances
            print(f"-- {[perf for perf in key]} : {value}")
       
        if self.spec.print_member_performances:
            print("-- Community Member Performances: ") 
            for cm_id in self.performances_each_cm: 
                        for key, value in self.performances.items(): # print member performances
                            print(f"-- CM {cm_id} : {[perf for perf in key]} : {value}")
        
        
    def set_community_manager_based_on_profile(self):
        """Asign the community manager based on the profile read from the csv results"""
        
        if self.manager_profile_path == "":
            raise ValueError("Manager profile path not set!")
        
        df = pd.read_csv(self.manager_profile_path) # extract the profile from the csv results file
        
        if df['stages'].item() == str(Stages.ONE_TURN):
            if df['strategy'].item() == str(Strategy.MULTI_OBJECTIVE): # if we have the multi-objective strategy, we also read the optimisation problem weights
                self.manager = OneTurnSubwayManager(10000,self.com_model, df['target_of_recommendations'].item(),  df['expectancy_development'].item(),Strategy.MULTI_OBJECTIVE, df['privacy_level'].item(), df['no_alert_threshold'].item(),  df['weight_neeg'].item(), df['weight_dis'].item())
            else: # otherwise, we assign the weights to 0
                self.manager = OneTurnSubwayManager(10000,self.com_model, df['target_of_recommendations'].item(),  df['expectancy_development'].item(),Strategy.INFORMATIVE, df['privacy_level'].item(), df['no_alert_threshold'].item(), 0, 0)
        
        self.com_model.community_manager = self.manager
        
        
    def get_community_members_based_on_profiles(self) -> List[SubwayCommunityMember]:
        """Initiate and assign community members based on the profiles read from the csv results

        Raises:
            ValueError: If the manager profiles is not set, raise an error

        Returns:
            List[SubwayCommunityMember]: a list of the community members
        """
        
        members = []
        if self.cm_profiles_path == "":
            raise ValueError("Member profile path not set!")
        
        df = pd.read_csv(self.cm_profiles_path)
        for _, row in df.iterrows(): # for each profile
            new_cm = self.MEMBER_MAP[(row['stages'], row['flexibility_capacity'])](row['unique_id'], self.com_model, None, row['preferred_hour']) # use self.member_map to initiate empty members
            members.append(new_cm) 
        return members
    
    
    def get_cm_data_models(self) -> Dict[int, Tuple[MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal]]:
        """Return the data models for each representative cm

        Raises:
            ValueError: if the manager cm_list is empty, raise an error
            ValueError: if no models are extracted, raise an error

        Returns:
            Dict[int, Tuple[MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal]]: a dictionary with cm id as key and a tuple as value, with the structure: (estimated cm load, simulated cm load, simulated cm actions, simulated cm recommendations)
        """
        
        if len(self.manager.cm_list) == 0:
            raise ValueError("No member id list available, cannot read data from file!")
        cm_data_models : Dict[int, Tuple[MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal]] = {}
        
        df = pd.read_csv(self.hourly_cm_results_full_path) # read the results for all members
        eu_tz = pytz.timezone(self.local_tz)
        time_frame = [pd.to_datetime(x).astimezone(eu_tz) for x in df[f'sim_time'].to_list()] # convert the time to the local time zone
        
        for flex in self.manager.cm_by_flex_and_pref_hr: 
            for _,cm in self.manager.cm_by_flex_and_pref_hr[flex].items(): # for each presentative member - flex capacity and preferred hour
                cm_data_models[cm.unique_id] = (MemberDataTotal(DSType.ESTIMATED, DType.CONSUMPTION),  
                                                MemberDataTotal(DSType.SIMULATED, DType.CONSUMPTION), 
                                                MemberDataTotal(DSType.SIMULATED, DType.ACTIONS),
                                                MemberDataTotal(DSType.SIMULATED, DType.RECOMMENDATIONS)) # create empty data models
                for data_model in cm_data_models[cm.unique_id]: # update the models according to the df
                    data_model.data = { time_frame[i] : value for i,value in enumerate(df[f'{cm.unique_id}_{cm.type}_{data_model.data_source_type}_{data_model.data_type}_{data_model.asignee_type}_{data_model.interval}'].to_list())} # get the model parameters from each column for the respective member and connect it with the corresponding time key
        if len(cm_data_models) == 0:
            raise ValueError("Data models could not be set!")
        return cm_data_models
        
        
    def set_file_writing_data_and_column_names(self):
        """Get the data to be written in files in a suitable format (along with the desired column names)"""
        
        self.file_writing_data.append(self.results_plot_file_name) # first colum is the scenario description
        self.file_writing_column_names.append('Scenario')
        self.file_writing_data.append(len(self.manager.cm_list)) # second column is the total member count
        self.file_writing_column_names.append('Member count')

        for key, value in self.performances.items(): # for each community performances
            self.file_writing_data.append(value) # get the value
            self.file_writing_column_names.append(f'{key[0]} {key[1]} {key[2]}') # ... the colum  name is composed of: DSType, AsignType and Indicator name
        
        for flex in self.manager.cm_by_flex_and_pref_hr: 
            for cm in list(self.manager.cm_by_flex_and_pref_hr[flex].values()): # for each representative member
                self.cm_file_writing_data[cm.unique_id] = [] # we empty the buffer for the performances
    
        if self.spec.calculate_individual_indicators:
            for flex in self.manager.cm_by_flex_and_pref_hr:
                for cm in list(self.manager.cm_by_flex_and_pref_hr[flex].values()): # for each representative member
                    for _, performance in self.performances_each_cm[cm.unique_id].items(): # for each performance of each representative member
                        self.cm_file_writing_data[cm.unique_id].append(performance) # add the performance to the cm coresponding buffer
                        
        
    def set_results_file_name(self):
        """Asign the path ccording to the most relevant scenario parameters"""
        
        path = f'Scenario_{self.scenario_id}_PV_{self.params.pv_plant_number_of_modules}_{self.params.pv_plant_module_power}_Manager_{self.manager.manager_stages}_{self.manager.target_of_recommendations}_{self.manager.expectancy_development}_{self.manager.strategy}_{self.manager.privacy_level}_{self.manager.no_alert_threshold}_wn_{self.manager.weight_neeg}_wd_{self.manager.weight_dis}' # include all relevant parameters
        self.results_plot_file_name = path
        
    
    def plot_power_profile_comparison_general(self, plt, index: int):
        """Plot the community power profiles"""
        
        if self.manager.plotting_df is None:
            raise ValueError("Plotting df not set yet!")
        plt.plot(self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]][f'{DSType.ESTIMATED}_{DType.CONSUMPTION}'] / 1000, color='blue', linestyle='dashed', label='$p^{estimated}_{commuting,Load}$') # the estimated community consumption profile
        plt.plot(self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]][f'{DSType.SIMULATED}_{DType.CONSUMPTION}'] / 1000, color='blue', label='$p^{simulated}_{commuting,Load}$') # the simulated community consumption profile
        plt.plot(self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]][f'{DSType.ESTIMATED}_{DType.PRODUCTION}'] / 1000, color='red', linestyle='dashed', label='$p^{simulated}_{PV}$') # the estimated community PV profile
        
        
    def plot_period_vertical_lines_flexibility_periods(self, plt, index: int):
        """Plot vertical lines at each flexibility hour start/end"""
        
        if self.manager.plotting_df is None:
            raise ValueError("Plotting df not set yet!")
        
        df_temp = self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]] # get the plotting df for given interval index
        flex_times = []
        for date in df_temp.index: # for each date 
            for period in self.manager.flexibility_periods: 
                if date.hour == period.hour_min or date.hour == period.hour_max: # type: ignore ;if the hour of the date is a flexibility hour start/end
                    flex_times.append(date) # append the date
        flex_times = pd.DatetimeIndex(flex_times) # convert the list to a datetime index
        
        plt.vlines(flex_times, ymin=0, ymax=plt.gca().get_ylim()[1], linestyles='dashed', colors='orange', label='Flexibility \n period ')  # plot a vertical line at each flex time
  
        
    def set_plot_properties_actions_comparison(self, plt, index : int, flex_type: FlexibilityCapacity):
        """Assign plot properties to actions/recommendations subplot"""
        
        delta = self.spec.plt_dt[index][1] - self.spec.plt_dt[index][0] # get the difference in time between the plotting interval bounds
        plt.grid(True)

        if delta.days > 2 and delta.days < 10: # if we have a small number of days
            plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=4)) # we modify the time axis to show ticks every 4 hours
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:00'))
        else:
            plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=1)) # we modify the time axis to show ticks every 1 hour
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%H'))
        plt.xlabel('Time', fontsize=self.spec.axis_fs)
        
        plt.title(f"Recommendations given to the {FLEX_MAP[flex_type]} members", fontsize=self.spec.title_fs) # we assign the title with the given specification format
        plt.yticks([10, 25, 40, 55]) # for each preferred hour group, we assign a tick number
        plt.ylim(3.5, 65) # the limits are asssigned to include the arrows
        plt.gca().set_yticklabels(['Group 7', 'Group 8', 'Group 9','Group 10']) # we always know we have these commuting groups

        plt.xticks(fontsize=self.spec.axis_fs)
        plt.yticks(fontsize=self.spec.axis_fs)
        plt.xticks(rotation=90)
        
        legend_elements = [mlines.Line2D([], [], color='blue', marker=markers.CARETUP, linestyle='None', markersize=7, label='Recommendation'), # type: ignore # we create a custom symbol for the recommendation
                           mlines.Line2D([], [], color='green', marker=markers.CARETUP, linestyle='None', markersize=7, label='Group action')] # type: ignore # ... and for the action
        plt.legend(handles=legend_elements, framealpha=0.8, fontsize=self.spec.legend_fs, loc='center left', bbox_to_anchor=(1, 0.5))
        #plt.legend(framealpha=0.8, fontsize=self.spec.legend_fs, loc='center left', bbox_to_anchor=(1, 0.5))
        
    
    def set_plot_properties_power_profile_comparison_general(self, plt, index):
        """Assign plot properties to power profile comparison subplot"""
        
        if self.manager.strategy == Strategy.MULTI_OBJECTIVE:
            plt.title(fr"Community power profile comparison between {self.spec.plt_dt_str[index][0]} and {self.spec.plt_dt_str[index][1]} with $w_{{NEEG}}$ = {self.manager.weight_neeg} and $w_{{DIS}}$ = {self.manager.weight_dis}", fontsize=self.spec.title_fs)
        else:
            plt.title(fr"Community power profile comparison between {self.spec.plt_dt_str[index][0]} and {self.spec.plt_dt_str[index][1]}", fontsize=self.spec.title_fs)
        
        plt.ylabel('Power [kW]', fontsize=self.spec.axis_fs) # the axis are shared with the action/recommendation subplot
        plt.xlabel('Time', fontsize=self.spec.axis_fs) 
        plt.gca().xaxis_date(tz='UTC')
        plt.xticks(rotation=90)
        plt.legend(framealpha=0.8, fontsize=self.spec.legend_fs, loc='center left', bbox_to_anchor=(1, 0.5)) # plot the legend at the right, outside the plot
        
        
    def plot_actions_comparison_for_cm_type_by_pref_hour(self, plt, index: int, flex_type: FlexibilityCapacity):
        """Subplot for action/recommendations comparison"""
        
        pref_hour_map = {7: 10, 8: 25, 9: 40, 10: 55} # a map between preferred hours and plot assigned numbers

        for pref_hour,cm in self.manager.cm_by_flex_and_pref_hr[flex_type].items(): # for each preferred hour group
            if cm.plotting_df is None: # check if the plotting_df is assigned
                raise ValueError("Plotting df for first cm not set!")
            
            df_temp = cm.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]].copy() # get the plotting_df for the plotting interval
            recs = [self.REC_MAPPING[x] for x in df_temp[f'{DSType.SIMULATED}_{DType.RECOMMENDATIONS}']] # map the recommendation to a value suitable for the axis
  
            lowlimits = [True if x == 5 else False for x in recs] # the magnitude is set to be plotted if we have a GREEN recommendation (0.5*10 == 5)
            x = list(df_temp.index) # the x is the time
            y = [pref_hour_map[cm.pref_hour[DayPeriods.MORNING]]] * len(df_temp.index) # y is the value accoridng to the pref_hour_map for the respecitve hour
            plt.errorbar(x, y, yerr=recs, uplims=False, lolims=lowlimits, label=f'Recommended for group {pref_hour} to commute', color='blue') # plot arrows for the recommendations if the magnitude is set to true
            if self.spec.plot_real_behaviour_of_first_agent_flag: 
                acts = [self.ACT_MAPPING[x] for x in df_temp[f'{DSType.SIMULATED}_{DType.ACTIONS}']] # map the recommendation to a value suitable for the axis
                lowlimits2 = [True if x == 2.5 else False for x in acts]  # set the magnitude to true if we have a GREEN action; 
                plt.errorbar(x, y, yerr=acts, uplims=False, lolims=lowlimits2, label=f'Group {pref_hour} has commuted', color='green') # plot arrows for the actions if the magnitude is set to true
                

    def plot_hourly_evolution_comparison_specific_months_and_days_subway_community(self, vertical_axes_flex_periods: bool = True, save_figures_flag: bool = True, use_tex_flag: bool = False):
        """Plot hourly comparison for the plotting intervals set in the specification

        Args:
            vertical_axes_flex_periods (bool, optional): Plot vertical axes for flexibility periods. Defaults to True.
            save_figures_flag (bool, optional): Save figures in the results folder. Defaults to True.
            use_tex_flag (bool, optional): Use LaTeX in plots. Defaults to False.
        """
        
        plt.rcParams['timezone']='UTC' # the time axis must be in UTC; plotting_df time frame should be in UTC, too
        if save_figures_flag:
            if use_tex_flag:
                plt.rc('text', usetex=True) # use Latex if we have figures
                plt.style.use(['science']) # type: ignore
            plt.grid(True)

        plt.figure(figsize=(30, 20))
            
        flex_types_in_sim = list(self.manager.cm_by_flex_and_pref_hr) # get the flexibility capacities in the scenario

        if len(flex_types_in_sim) > 1: # if we have more then one flexibility capacity
            specific_plots_grid_spaces = len(flex_types_in_sim) # we have a subplot space for each flexibility capacity
        else:
            specific_plots_grid_spaces = 1
        
        grid_row_length = (1 + specific_plots_grid_spaces) * len(self.spec.plot_intervals) # the total number of spaces in the grid - we account for the power profile comparison, actions comparison and plotting intervals
        gs = gridspec.GridSpec(grid_row_length, 1) # create the grid with one column and grid_row_length rows
        grid_index, interval_index = 0, 0 # grid index is the place in the grid, interval index is the current plotting interval

        while interval_index < len(self.spec.plot_intervals):
            ax1 = plt.subplot(gs[grid_index, 0]) # the first subplot is the power profile comparison
            self.plot_power_profile_comparison_general(plt, interval_index) 
            if vertical_axes_flex_periods:
                self.plot_period_vertical_lines_flexibility_periods(plt, interval_index)
            self.set_plot_properties_power_profile_comparison_general(plt, interval_index)
            grid_index += 1 # get to the next grid position
            category_index = 0 # ... but is the same flexibilty capacity

            if len(self.manager.cm_by_flex_and_pref_hr) > 1: # if we have more then one flexibility capacity
                while category_index < len(self.manager.cm_by_flex_and_pref_hr):
                    flex_type = list(self.manager.cm_by_flex_and_pref_hr)[category_index] # get the flexibility type based on the current cat index
                    plt.subplot(gs[grid_index, 0], sharex=ax1) # share the axes between the first subplot and this one
                    self.plot_actions_comparison_for_cm_type_by_pref_hour(plt, interval_index, flex_type ) # plot the comparison between actions and recommendations for representative members
                    self.set_plot_properties_actions_comparison(plt, interval_index, flex_type)
                    grid_index += 1
                    category_index += 1
            else:
                plt.subplot(gs[grid_index, 0], sharex=ax1) # share the axes between the first subplot and this one
                self.plot_actions_comparison_for_cm_type_by_pref_hour(plt, interval_index, list(self.manager.cm_by_flex_and_pref_hr)[0] ) # plot the comparison between actions and recommendations for representative members
                self.set_plot_properties_actions_comparison(plt, interval_index, list(self.manager.cm_by_flex_and_pref_hr)[0])
                grid_index += 1

            interval_index += 1 # get to the next plotting interval

        f = plt.gcf()
        if len(flex_types_in_sim) == 1: # if we have only one flex capacity
            plt.subplots_adjust(wspace=0.1, hspace=0.6)
            f.set_size_inches(10, 5) if len(self.spec.plot_intervals) == 1 else f # set the figure size accoring to the number of plotting intervals
            f.set_size_inches(15, 10) if len(self.spec.plot_intervals) == 2 else f
            f.set_size_inches(20, 15) if len(self.spec.plot_intervals) == 3 else f
            f.tight_layout()
            
        if len(flex_types_in_sim) == 2:
            plt.subplots_adjust(wspace=0.1, hspace=1)
            f.set_size_inches(20, 25)
        if len(flex_types_in_sim) == 3:
            f.set_size_inches(20, 30)
            plt.subplots_adjust(wspace=0.1, hspace=1)

        logger.info(f"Plotted monthly comparison - Scenario {self.results_plot_file_name}")

        if save_figures_flag:
            path = os.path.join(self.results_folder_path, f"Figure_Scenario_{self.scenario_id}_analysis.pdf") 
            f.savefig(path, bbox_inches='tight')
        else:
            plt.show()
          