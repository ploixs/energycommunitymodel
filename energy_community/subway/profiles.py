from datetime import datetime, timedelta
from energy_community.core.constants import AsignType, DSType, DType, DayPeriods, ExpectancyDev, FlexibilityCapacity, PrivacyLevel, Stages, Strategy, TargetOfRec
from energy_community.core.data.member_data import MemberDataTotal
from energy_community.subway.core import SubwayCommunity, SubwayCommunityManager, SubwayCommunityMember
from energy_community.core.constants import Availability, MType, MemberActionIntensity, Rec, Stages, Willigness



class OneTurnSubwayManager(SubwayCommunityManager):

    def __init__(self, unique_id, model: SubwayCommunity, target_of_recommendations: TargetOfRec, expectancy_development: ExpectancyDev, strategy: Strategy, privacy_level: PrivacyLevel, no_alert_threshold: float, weight_neeg: float, weight_dis: float):
        super().__init__(unique_id, model, Stages.ONE_TURN, target_of_recommendations, expectancy_development, strategy, privacy_level, no_alert_threshold, weight_neeg, weight_dis)

    def first_stage(self):
        #if self.com_model.current_hour_index == self.com_model.start_of_day_hour_indexes[0]:
        if self.com_model.current_hour_index == list(self.com_model.start_of_day_end_of_day_index_map)[0]:
            self.set_estimated_production_for_community_in_Bucharest()
            for hour in set([cm.pref_hour[DayPeriods.MORNING] for cm in self.cm_list]):
                self.rec_manager_rec_total_by_preferred_hour[hour] = MemberDataTotal(DSType.REQUESTED, DType.RECOMMENDATIONS, AsignType.CATEGORY)
            
        
        #if self.com_model.current_hour_index in self.com_model.start_of_day_hour_indexes and self.cm_list:
        if self.com_model.current_hour_index in self.com_model.start_of_day_end_of_day_index_map and self.cm_list:
            for agent in self.cm_list:
                agent.init_cm_day() # we initialize est_member_load_day for each member
            self.init_manager() # calculate est_com_load_day for the recommendations
            #self.set_member_requested_consumption_levels() # we need est laod day of each member, est com load day and est prod day
            if self.strategy == Strategy.DUMMY:
                self.set_recommendations_current_day_dummy_approach()
                self.set_recommended_hours_to_commute()
            if self.strategy == Strategy.INFORMATIVE:
                self.set_recommendations_informative_strategy_current_day()
                self.set_recommended_hours_to_commute()
                self.communicate_actions_to_cms()
            if self.strategy == Strategy.MULTI_OBJECTIVE:
                self.set_multi_objective_recommendation()
                self.set_recommended_hours_to_commute_multi_objective_strategy()
                self.communicate_actions_to_cms_multi_objective_strategy()
            
            

    def second_stage(self):

        if self.com_model.current_hour_index in list(self.com_model.start_of_day_end_of_day_index_map.values()) and self.cm_list:
            self.record_data_members_day()
            self.record_data_community_day()
        if self.com_model.current_hour_index == list(self.com_model.start_of_day_end_of_day_index_map.values())[-1]:  # in the last day of the simulation, write data to files
            self.write_cm_hourly_results_simulation_to_csv()
            self.write_com_hourly_results_simulation_to_csv()
            self.write_cm_profiles_to_csv()
            self.write_manager_profile_to_csv()
            #self.locator.write_info_file()
            
class OneTurnFullFlexibilitySubwayMember(SubwayCommunityMember):
    """one stage, realistic intensity, no model for willingness (ideal agent), always available"""

    def __init__(self, unique_id: int, model, mean_values_icp_df, pref_hour: int):
        super().__init__(FlexibilityCapacity.FULLY_FLEXIBLE, unique_id, model, 
                         MType.IDEAL, Stages.ONE_TURN, MemberActionIntensity.REALISTIC, Willigness.FIXED, Availability.DAY, 
                         mean_values_icp_df, pref_hour)

    def first_stage(self):
        
        current_time = self.com_model.tf_total[self.com_model.current_hour_index]
        decision = 0
        if current_time.hour == 0 and self.com_model.time_and_day_names_map[current_time] not in ['Sunday', 'Saturday']:
            if self.est_times_to_commute_day is None:
                raise ValueError("self.est_times_to_commute_day not set!")
            decision = self.cm_type_decision_map[self.type]
            if decision == 1: # Agent doesn't follow the recommendation
                self.sim_times_to_commute_day = self.est_times_to_commute_day
                
                for i,time in enumerate(self.est_times_to_commute_day):
                    self.sim_member_actions_day.data[time] = Rec.GREEN
                    self.sim_member_load_day.data[time] = self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][i]
            if decision == 0: # Agent follows the recommendation
                #if len(self.rec_hours_to_go_to_work) > 0 and len(self.rec_hours_to_return_from_work) > 0:
                self.set_sim_hours_to_commute_based_on_recommendations_fully_flexible()
                if self.sim_times_to_commute_day is None:
                    raise ValueError("self.sim_times_to_commute_day not set!")

                for i,time in enumerate(self.sim_times_to_commute_day):
                    if self.sim_times_to_commute_day[i] == current_time: # if there is no hour to commute in the morning
                        self.sim_member_actions_day.data[self.est_times_to_commute_day[i]] = Rec.RED
                        #self.sim_member_load_day.data[self.est_times_to_commute_day[i]] = self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][i]
                    else:
                        self.sim_member_actions_day.data[time] = Rec.GREEN
                        self.sim_member_load_day.data[time] = self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][i]

    def second_stage(self):
        pass
    
    
class OneTurnNoFlexibilitySubwayMember(SubwayCommunityMember):
    """one stage, realistic intensity, no model for willingness (ideal agent), always available"""

    def __init__(self, 
                 unique_id: int, model,
                 mean_values_icp_df, pref_hour: int):
        super().__init__(FlexibilityCapacity.NO_FLEXBILITY, unique_id, model, MType.IDEAL, Stages.ONE_TURN, MemberActionIntensity.REALISTIC, Willigness.FIXED, Availability.DAY, mean_values_icp_df, pref_hour)

    def first_stage(self):
 
        current_time = self.com_model.tf_total[self.com_model.current_hour_index]

        if ((self.com_model.community_manager.flexibility_periods[0].check_if_in_period(current_time.hour) or 
             self.com_model.community_manager.flexibility_periods[1].check_if_in_period(current_time.hour)) and
             self.com_model.time_and_day_names_map[current_time] not in ['Sunday', 'Saturday']):
            
            if self.est_times_to_commute_day is None:
                raise ValueError("self.est_times_to_commute_day not set!")
            
            for i,time in enumerate(self.est_times_to_commute_day):
                self.sim_member_actions_day.data[time] = Rec.GREEN
                self.sim_member_load_day.data[time] = self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][i]
                    
    def second_stage(self):
        pass
    
    
class OneTurnSemiFlexibleSubwayMember(SubwayCommunityMember):
    """one stage, realistic intensity, no model for willingness (ideal agent), always available"""

    def __init__(self, unique_id: int, model, mean_values_icp_df, pref_hour: int):
        super().__init__(FlexibilityCapacity.SEMY_FLEXIBLE, unique_id, model, MType.IDEAL, Stages.ONE_TURN, MemberActionIntensity.REALISTIC, Willigness.FIXED, Availability.DAY, mean_values_icp_df, pref_hour)

    def first_stage(self):
        
        current_time = self.com_model.tf_total[self.com_model.current_hour_index]
        decision = 0
        if current_time.hour == 0 and self.com_model.time_and_day_names_map[current_time] not in ['Sunday', 'Saturday']:
            decision = self.cm_type_decision_map[self.type]
            if decision == 1: # Agent doesn't follow the recommendation
                self.sim_times_to_commute_day = self.est_times_to_commute_day
                if self.est_times_to_commute_day is None:
                    raise ValueError("self.est_times_to_commute_day not set!")
                for i,time in enumerate(self.est_times_to_commute_day):
                    self.sim_member_actions_day.data[time] = Rec.GREEN
                    self.sim_member_load_day.data[time] = self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][i]
            if decision == 0: # Agent follows the recommendation
                if len(self.rec_hours_to_go_to_work) > 0 and len(self.rec_hours_to_return_from_work) > 0:
                    self.set_sim_hours_to_commute_based_on_recommendations_semi_flexible()
                    if self.sim_times_to_commute_day is None:
                        raise ValueError("self.sim_times_to_commute_day not set!")
                    for i,time in enumerate(self.sim_times_to_commute_day):
                        self.sim_member_actions_day.data[time] = Rec.GREEN
                        self.sim_member_load_day.data[time] = self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][i]
                else:
                    time_to_commute_morning : datetime = self.com_model.current_time.replace(hour=self.pref_hour[DayPeriods.MORNING])
                    #self.sim_times_to_commute_day = (time_to_commute_morning,time_to_commute_morning + timedelta(hours=8))
                    self.sim_times_to_commute_day = (time_to_commute_morning,time_to_commute_morning + timedelta(hours=self.pref_duration))
                    for i,time in enumerate(self.sim_times_to_commute_day):
                        self.sim_member_actions_day.data[time] = Rec.GREEN
                        self.sim_member_load_day.data[time] = self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][i]

        '''
        if ((self.com_model.community_manager.flexibility_periods[0].check_if_in_period(current_time.hour) or self.com_model.community_manager.flexibility_periods[1].check_if_in_period(current_time.hour)) and
                self.com_model.time_and_day_names_map[current_time] not in ['Sunday', 'Saturday']):
                        
            if self.est_times_to_commute_day is None:
                raise ValueError("self.est_times_to_commute_day not set!")
            
            if self.sim_times_to_commute_day is None:
                raise ValueError("self.sim_times_to_commute_day not set!")

            if decision == 1: # Agent doesn't follow the recommendation
                if current_time.hour == self.est_times_to_commute_day[0]:
                    self.record_cm_data_current_hour(Rec.GREEN,  self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][0]) # type: ignore
                elif current_time.hour == self.est_times_to_commute_day[1]:
                    self.record_cm_data_current_hour(Rec.GREEN, self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][1]) # type: ignore
                else:
                    self.record_cm_data_current_hour(Rec.WHITE, 0)
                    
            if decision == 0: # Agent follows the recommendation
                if current_time.hour == self.sim_times_to_commute_day[0]: # If the current hour we have to go to work, then we consume energy
                    self.record_cm_data_current_hour(Rec.GREEN, self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][0]) # type: ignore
                elif current_time.hour == self.sim_times_to_commute_day[1]: # If the current hour we have to return from work, then we consume energy
                    self.record_cm_data_current_hour(Rec.GREEN, self.est_mean_values_df[list(self.com_model.tf_day.values())[0]][1]) # type: ignore
                elif current_time.hour == self.est_times_to_commute_day[0]:
                    self.record_cm_data_current_hour(Rec.WHITE, 0) 
        '''
    def second_stage(self):
        pass
            
            

        