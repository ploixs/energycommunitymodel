from datetime import datetime, timedelta
import os
import random
import sys
from typing import Any, Dict, List, Tuple, Union
import numpy as np
#from pydantic import ColorError

from buildingenergy import timemg
from buildingenergy import solar
from buildingenergy.openweather import OpenWeatherMapJsonReader
from energy_community.core.data.collective_data import CollectiveDataDay, CollectiveDataTotal
from energy_community.core.data.member_data import MemberDataDay, MemberDataTotal
from energy_community.core.entities.community import EnergyCommunityModel
from energy_community.core.entities.manager import CommunityManager
from energy_community.core.entities.member import CommunityMember
from energy_community.core.simulation.Period import Period
import pandas as pd
from mesa.time import StagedActivation
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import pulp
from pulp import PULP_CBC_CMD
from tzlocal import get_localzone 
from energy_community.core.constants import FLEX_MAP, AsignType, Availability, DSType, DType, FlexibilityCapacity, Interval, MType, MemberActionIntensity, PType, Rec, Stages, Strategy, Willigness, DayPeriods
import logging

from energy_community.subway.SubwayParameters import SubwayParameters

logger = logging.getLogger(__name__)

class SubwayCommunity(EnergyCommunityModel):
    """ The subway community model

    Args:
        EnergyCommunityModel (_type_): inherits the EnergyCommunityModel
    """

    def __init__(self, sim_params: SubwayParameters, results_folder_path: str):
        """Constructor for the SubwayCommunityModel

        Args:
            sim_params (Params): subway station simulation parameters
        """
        super().__init__(sim_params, results_folder_path)

        #self.timezone = str(get_localzone()) # get local timezone
        self.timezone = 'Europe/Bucharest'
        self.schedule = StagedActivation(self, ['first_stage', 'second_stage']) # the schedule is customly set - see MESA documentation
        self._community_manager : Union['SubwayCommunityManager',None] = None
        if not os.path.exists(self.results_folder_path):
            os.mkdir(self.results_folder_path)
        
    @property
    def community_manager(self) -> 'SubwayCommunityManager':
        if self._community_manager is None:
            raise ValueError("Communtiy manager not set!")
        return self._community_manager
    
    @community_manager.setter
    def community_manager(self, community_manager : 'SubwayCommunityManager'):
        """Sets the community manager according to the specification in the scenario_description file.

        Args:
            community_manager (SubwayCommunityManager): the manager to be assigned

        Raises:
            ValueError: if the manager is not seet in the process, raise an error.
        """
        
        self._community_manager = community_manager
        if self._community_manager is None:
            raise ValueError("Community manager not set!")
        self.schedule.add(self._community_manager)      
        
    def add_new_community_members(self, subway_community_members: List['SubwayCommunityMember']):
        """Adds new community members to the model.

        Args:
            subway_community_members (List[&#39;SubwayCommunityMember&#39;]): the list of subway community members to be added

        Raises:
            ValueError: if the manager is not assigned, raise an error.
        """
        if self.community_manager is None:
            raise ValueError("Community manager not assigned yet!")
        for cm in subway_community_members:
            self.schedule.add(cm)
            self.community_manager.cm_list.append(cm)
            if self.sim_params.print_member_info:
                print(f"Agent {cm.unique_id} added to the community as a {cm.type} agent")
    
    def step(self):
        """Advances the model by one step."""
        
        self.current_time = self.tf_total[self.current_hour_index]

        if self.current_hour_index in self.start_of_day_end_of_day_index_map: # if it's the beginning of the day
            self.current_hour_index_day_relative = 0
            self.set_timeframe_current_day() # set the time frame current day

        if self.sim_params.print_member_info:
            logger.info(f"Sim dt: {self.tf_total[self.current_hour_index]}, sim h index: {self.current_hour_index}, sim h day-relative index: {self.current_hour_index_day_relative}")

        self.schedule.step()

        self.current_hour_index += 1  # Update time
        self.current_hour_index_day_relative += 1 
        
        
    def plot_results_in_browser(self):
        """After a simulation, plot a comparison between the results in the memory.

        Raises:
            ValueError: if the manager is not set, raise an error.
        """

        if self.community_manager is None:
            raise ValueError("Manager not set!")
        
        fig = make_subplots(rows=2, cols=1, shared_xaxes=True)
            
        fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=list(self.community_manager.est_com_prod_total.data.values()), name='estimated PV production', marker_color='red'), row=1, col=1) # plot the production
        fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=list(self.community_manager.sim_com_load_total.data.values()), name='real community consumption', marker_color='green'), row=1, col=1) # plot the simulated consumption
        fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=list(self.community_manager.est_com_load_total.data.values()), name='estimated community consumption', marker_color='blue'), row=1, col=1) # plot the estimated consumption

        pref_hours = list(self.community_manager.rec_manager_rec_total_by_preferred_hour.keys()) # get the preferred hours
        
        colors = {Rec.RED: 'red', Rec.WHITE: 'white', Rec.GREEN: 'green'} # a map between recommendation codes and the assigned colors
        legend_map = {'red': 'recommendation not to commute', 'white': 'no flexibility period', 'green': 'recommendation to commute'}
        
        if self.community_manager.strategy == Strategy.MULTI_OBJECTIVE:
            for i in range(2, 6): #
                for color_code, plot_color in colors.items(): # for each color code
                    fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=[color_code.value if c == color_code else None for i,c in self.community_manager.rec_manager_rec_total_by_preferred_hour[pref_hours[i-2]].data.items()],name=f'Group {pref_hours[i-2]} {legend_map[plot_color]}', mode='markers', marker_color=plot_color, marker_symbol=i-2, marker_size=10, showlegend=True), row=2, col=1) # plot the recommendations
        else:
            for color_code, plot_color in colors.items(): # for each color code
                fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=[color_code.value if c == color_code else None for i,c in self.community_manager.req_manager_rec_total.data.items()], mode='markers',  marker_color=plot_color, marker_size=20, showlegend=True), row=2, col=1) # plot the recommendations

        flexibility_cap_types = set(cm.flexibility_capacity for cm in self.community_manager.cm_list) # get the flexibility capacities in the communitye
        fig.update_layout(title=f"Subway community with {','.join([FLEX_MAP[x] for x in flexibility_cap_types])} members", yaxis=dict(title='Power [W]'), yaxis2=dict(type='category', title='Recommendation type')) # set the plot properties

        fig.show()

class SubwayCommunityManager(CommunityManager):
    """Community Automatic Manager Class implementation. 

    Args:
        CommunityManager (_type_): Inherits the Community Manager abstract class.
    """

    def __init__(self, unique_id, model, manager_stages, target_of_recommendations, expectancy_development, strategy, privacy_level, no_alert_threshold, weight_neeg = 0.0, weight_dis = 0.0):
        """_summary_

        Args:
            unique_id (int): community member id
            model (EnergyCommunityModel): the community model
            manager_stages (Stages): one turn / two turns
            target_of_recommendations (TargetOfRec): everyone/groups
            expectancy_development (ExpectancyDev): basic/adaptive
            strategy (Strategy):informative, etc
            privacy_level (PrivacyLevel): private/not private
            no_alert_threshold (float): a comfort threshold for members
            weight_neeg (float, optional): the weight for the optimisation problem assigned to the NEEG. Defaults to 0.0.
            weight_dis (float, optional): the weight for the optimisation problem assigned to the dissatisfaction. Defaults to 0.0.
        """
        super().__init__(unique_id, model, manager_stages, target_of_recommendations, expectancy_development, strategy, privacy_level, no_alert_threshold)
        
        self._cm_list : List[SubwayCommunityMember] = [] # the list of community members
        self.cm_by_flex_and_pref_hr : Dict[ FlexibilityCapacity, Dict[int, SubwayCommunityMember]] = {} # representative members for each flex group, for each hour
        self.cm_count : Dict[ FlexibilityCapacity, Dict[int, int]] = {} # the count of cms from each category, for each flex capacity
        self._com_model : SubwayCommunity = model
        self.weight_neeg = weight_neeg
        self.weight_dis = weight_dis
        
        self.sim_com_load_day, self.sim_com_load_total = CollectiveDataDay(DSType.SIMULATED, DType.CONSUMPTION, AsignType.COMMUNITY), CollectiveDataTotal(DSType.SIMULATED, DType.CONSUMPTION, AsignType.COMMUNITY) # simulated community consumption profiles
        self.est_com_load_day, self.est_com_load_total = CollectiveDataDay(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.COMMUNITY), CollectiveDataTotal(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.COMMUNITY) # estimated community consumption profiles
        self.est_com_prod_day, self.est_com_prod_total = CollectiveDataDay(DSType.ESTIMATED, DType.PRODUCTION, AsignType.COMMUNITY), CollectiveDataTotal(DSType.ESTIMATED, DType.PRODUCTION, AsignType.COMMUNITY) # estimated community production profiles

        self.est_collective_icp : Dict[ int, Dict[DayPeriods, float]] = {} # estimated total ICP gouped by morning and afternoon and then by pref hour (going or returning)
        self.cm_est_load_day_models : Dict[DayPeriods,  Dict[int, List[MemberDataDay]]]  = {} # estimated load for models grouped by period and then by pref hour - going or returning
   
        self.req_manager_rec_day, self.req_manager_rec_total = MemberDataDay(DSType.REQUESTED, DType.RECOMMENDATIONS, AsignType.MANAGER), MemberDataTotal(DSType.REQUESTED,DType.RECOMMENDATIONS, AsignType.MANAGER) # recommendations assigned to the member for the current day
        
        self.rec_manager_rec_day_by_preferred_hour : Dict[int, MemberDataDay] = {} # requested manager recommendations for the day, grouped by the preferred hour
        self.rec_manager_rec_total_by_preferred_hour : Dict [int, MemberDataTotal]= {}  # requested manager recommendations for the simulation, grouped by the preferred hour
        
        self.rec_hours_to_go_to_work : List[int] = [] # recommended hours to go to work
        self.rec_hours_to_return_from_work  : List[int] = [] # recommended hours to return from work
        
        self.rec_hours_to_go_to_work_group = {} # recommended hours to go to work, grouped by pref hour
        self.rec_hours_to_return_from_work_group = {}  # recommended hours to return from work, grouped by pref hour

        flexibility_p1 = Period(PType.FLEXIBILITY, self.com_model.sim_params.flexibility_period["morning"]["start"].hour, self.com_model.sim_params.flexibility_period["morning"]["end"].hour) # type: ignore
        flexibility_p2 = Period(PType.FLEXIBILITY, self.com_model.sim_params.flexibility_period["afternoon"]["start"].hour, self.com_model.sim_params.flexibility_period["afternoon"]["end"].hour) # type: ignore
        self.flexibility_periods = (flexibility_p1, flexibility_p2) # flexibility periods
        
        self.commuting_periods = (Period(PType.COMMUTING, 7, 10), Period(PType.COMMUTING, 15, 19)) # commuting periods
        
        self.plotting_df : None | pd.DataFrame = None # the plotting dataframe
        
    @property
    def cm_list(self) -> List['SubwayCommunityMember']:
        """Returns the community member list.

        Returns:
            _type_: a list of community members assigned to the model.
        """
        return self._cm_list
    
    @cm_list.setter
    def cm_list(self, community_member_list: List['SubwayCommunityMember']):
        """Sets the cm list of the model with the given argument.

        Args:
            community_member_list (List[&#39;SubwayCommunityMember&#39;]): A list of subway community members.
        """
        self._cm_list = community_member_list
    
    def update_cm_list(self, community_member: 'SubwayCommunityMember'):
        """Updates the community member list with the subway community member given as argument.

        Args:
            community_member (SubwayCommunityMember): a subway community member
        """
        self._cm_list.append(community_member)
        
    @property
    def com_model(self) -> SubwayCommunity:
        """Returns the subway community model.

        Returns:
            SubwayCommunity: the subway community model to which the manager is assigned.
        """
        return self._com_model
    
    @com_model.setter
    def com_model(self, subway_community_model: SubwayCommunity):
        """Sets the community model of the manager.

        Args:
            subway_community_model (SubwayCommunity): A subway community model.
        """
        self._com_model = subway_community_model
    
    def update_manager(self, energy_models: Tuple[CollectiveDataTotal, CollectiveDataTotal, CollectiveDataTotal], members: List['SubwayCommunityMember']):
        """Updates the manager energy models.

        Args:
            energy_models (tuple[CollectiveDataTotal, CollectiveDataTotal, CollectiveDataTotal]): a tuple with the following configuration: (simulated consumption, estimated consumption, estimated production)
            members (list[&#39;SubwayCommunityMember&#39;]): a list of subway community members
        """
        self.sim_com_load_total = energy_models[0]
        self.est_com_load_total = energy_models[1]
        self.est_com_prod_total = energy_models[2]
        self.com_model.add_new_community_members(members)
        
    def set_member_requested_consumption_levels(self):
        pass
        
        
    def set_representative_cms_and_cm_count(self):
        """Select only the representative cms for each flexibility group and for each preferred hour.

        Raises:
            ValueError: If the cm_list is not set, raise an error.
        """
        
        if len(self.cm_list) == 0:
            raise ValueError("CM list not set!")
        pref_hours = set(cm.pref_hour[DayPeriods.MORNING] for cm in self.cm_list) # get the unique preferred hours
        flex_caps = set(cm.flexibility_capacity for cm in self.cm_list) # get the unique flexibility capacities
        for flex_cap in flex_caps:
            self.cm_by_flex_and_pref_hr[flex_cap] = {}
            self.cm_count[flex_cap] = {}

        for flex_cap in flex_caps:
            for hour in pref_hours:
                self.cm_count[flex_cap][hour] = 0 # reset the counter
                cm_found = False # flag to check if we found a representative member for a flex cap and hour
                for cm in self.cm_list:
                    if cm.pref_hour[DayPeriods.MORNING] == hour and cm.flexibility_capacity == flex_cap: 
                        if not cm_found:
                            self.cm_by_flex_and_pref_hr[flex_cap][hour] = cm # we found a representative member
                            cm_found = True # we worn't search for another rep member for this flex capacity and this hour
                        self.cm_count[flex_cap][hour] += 1 # count the member
                            

    def init_manager(self):
        """Initialize the manager at the beginning of each day"""
        
        self.est_com_prod_day.data = {val : self.est_com_prod_total.data[val] for _,val in self.com_model.tf_day.items() } # sets the production for the current day
        member_est_load_day_models = [member.est_member_load_day for member in self.cm_list] # get estimated consumption for each member
        self.est_com_load_day.update_data_with_other_models(member_est_load_day_models) # update the community estimated consumption
        
        if self.strategy == Strategy.MULTI_OBJECTIVE:
            for period in DayPeriods:
                self.cm_est_load_day_models[period] = {}
                for member in set(self.cm_list):
                    self.cm_est_load_day_models[period][member.pref_hour[period]] = []
            for member in self.cm_list:
                for period in DayPeriods:
                    self.cm_est_load_day_models[period][member.pref_hour[period]].append(member.est_member_load_day) # we get the est member load day models for each cateogyr of members by the returning hour
            
            collective_load = {}
            for period in DayPeriods:
                #self.est_collective_icp[period] = {}
                collective_load[period] = { pref_hour : CollectiveDataDay(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.CATEGORY) for pref_hour in self.cm_est_load_day_models[period]}
                #for hour in self.cm_est_load_day_models[period]:
                    #self.est_collective_icp[period][hour] = { pref_hour : CollectiveDataDay(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.CATEGORY) for pref_hour in self.cm_est_load_day_models_by_pref_going_hour}
            # we develop a model for the collectiv estimated consumption for each category
            
            #collective_load = { pref_hour : CollectiveDataDay(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.CATEGORY) for pref_hour in self.cm_est_load_day_models_by_pref_going_hour}
            #collective_load_by_pref_returning_hour = { pref_hour : CollectiveDataDay(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.CATEGORY) for pref_hour in self.cm_est_load_day_models_by_pref_returning_hour}
            #for hour, model in collective_load_by_pref_going_hour.items():
            #    model.update_data_with_other_models(self.cm_est_load_day_models_by_pref_going_hour[hour])
            for period in DayPeriods:
                for hour, model in collective_load[period].items():
                    model.update_data_with_other_models(self.cm_est_load_day_models[period][hour])
            
            #for period in DayPeriods:
            for pref_hour in collective_load[DayPeriods.MORNING]: # for each pref hour group
                self.est_collective_icp[pref_hour] = {} # initialize dict for pref hour group in the morning
                self.est_collective_icp[pref_hour + 8] = {} # initialize dict for pref hour group in the afternoon
                self.est_collective_icp[pref_hour + 8][DayPeriods.AFTERNOON] = 0 # for each pref hour group in the afternoon, initialize with 0 because we will make a sum
                for time, value in collective_load[DayPeriods.MORNING][pref_hour].data.items(): # for each time in the collective est model data
                    if value > 0 and time.hour < 13: 
                        self.est_collective_icp[pref_hour][DayPeriods.MORNING] = value # if the members have consumed in the morning, account for the value
                    if value > 0 and time.hour > 13: 
                        self.est_collective_icp[pref_hour + 8][DayPeriods.AFTERNOON] += value # if the members have consumed in the afternoon, regardless the hour, account for the value

            # we get the estimated consumption for the morning and the afternoon - this consumption is time independent, since mean ICP is computed for the whole flexibility period
            #for key, est_load_model in collective_load_by_pref_hour.items():
            #    self.est_collective_icp_by_pref_hour[key] = tuple([x for hour, x in enumerate(list(est_load_model.data.values())) if x > 0]) # type: ignore


    def set_recommended_hours_to_commute_multi_objective_strategy(self):
        """ Sets the recommended hours to commute (morning, afternoon) according to the results of the multi-objective recommendation strategy.

        Raises:
            ValueError: if the recommendations are not set as a result of the ptimisation problem, raise an error.
        """
        
        if len(self.rec_manager_rec_day_by_preferred_hour) == 0:
            raise ValueError("Recomendations current day not set!")
        
        if self.com_model.time_and_day_names_map[self.com_model.current_time] in ['Sunday', 'Saturday']: # if we are during the weekend
            for hour in list(self.rec_manager_rec_day_by_preferred_hour.keys()):
                self.rec_hours_to_go_to_work_group[hour] = [] # there are no hours to commute in the morning
                self.rec_hours_to_return_from_work_group[hour] = [] # ... or in the afternoon
            return
        
        times_f1 = [self.com_model.current_time.replace(hour=x) for x in self.flexibility_periods[0].get_hours()] # get the flexibility times for the morning
        times_f2 = [self.com_model.current_time.replace(hour=x) for x in self.flexibility_periods[1].get_hours()] # get the flexibility times for the afternoon
        
        f_data = {} 
        for hour in list(self.rec_manager_rec_day_by_preferred_hour.keys()): # for each preferred hour group
            rec_data = self.rec_manager_rec_day_by_preferred_hour[hour].data # get the recommendations for the preferred hour group
            f_data[hour] = { key : value  for key, value in rec_data.items() if key in (times_f1 + times_f2) and value == Rec.GREEN} # get the times when the recommendation is GREEN

            if len(f_data[hour]) == 0: # if there is no recommendation to commute
                self.rec_hours_to_go_to_work_group[hour] = [] # there are no hours to commute in the morning
                self.rec_hours_to_return_from_work_group[hour] = [] # ... or in the afternoon
            else:
                self.rec_hours_to_go_to_work_group[hour] = [time.hour for time in f_data[hour] if time.hour < 13] # get the hour for the morning
                self.rec_hours_to_return_from_work_group[hour] = [time.hour for time in f_data[hour] if time.hour > 13] # get the hour for the afternoon
    
    
    def set_recommended_hours_to_commute(self):
        """Sets the recommended hours to commute according to the informative strategy.

        Raises:
            ValueError: if the recommendatios are not set, raise an error.
        """
        if len(self.req_manager_rec_day.data) == 0:
            raise ValueError("Recomendations current day not set!")
        
        if self.com_model.time_and_day_names_map[self.com_model.current_time] in ['Sunday', 'Saturday']: # if we are during the weekend
            self.rec_hours_to_go_to_work = [] # there are no hours to commute in the morning
            self.rec_hours_to_return_from_work = [] # ... or in the afternoon
            return
        
        times_f1 = [self.com_model.current_time.replace(hour=x) for x in self.flexibility_periods[0].get_hours()] # get the flexibility times for the morning
        times_f2 = [self.com_model.current_time.replace(hour=x) for x in self.flexibility_periods[1].get_hours()] # get the flexibility times for the afternoon

        f_data = { key : value  for key, value in self.req_manager_rec_day.data.items() if key in (times_f1 + times_f2) and value == Rec.GREEN} # get the times when the recommendation is GREEN
        
        if len(f_data) == 0: # if there is no recommendation to commute
            self.rec_hours_to_go_to_work = [] # there are no hours to commute in the morning
            self.rec_hours_to_return_from_work = [] # ... or in the afternoon
            return
        
        self.rec_hours_to_go_to_work = [time.hour for time in f_data if time.hour < 13] # get the hour for the morning
        self.rec_hours_to_return_from_work = [time.hour for time in f_data if time.hour > 13] # get the hour for the afternoon
    
    
    def communicate_actions_to_cms_multi_objective_strategy(self):
        """Communicate recommended hours to commute to each member, according ot their group.

        Raises:
            ValueError: if cm_list is empty, no members to communicate with.
        """
        if len(self.cm_list) == 0:
            raise ValueError("No community members to commuicate with!")

        for member in self.cm_list:
            member.rec_hours_to_go_to_work = self.rec_hours_to_go_to_work_group[member.pref_hour[DayPeriods.MORNING]] # get the recommended hours to commute for the respective group
            member.rec_hours_to_return_from_work = self.rec_hours_to_return_from_work_group[member.pref_hour[DayPeriods.MORNING]] # get the recommended hours to commute for the respective group
            member.sim_member_rec_day.data = self.rec_manager_rec_day_by_preferred_hour[member.pref_hour[DayPeriods.MORNING]].data # get the recommendations
    
    
    def communicate_actions_to_cms(self):
        """Communicate recommended hours to commute to each member.

        Raises:
            ValueError: if cm_list is empty, no members to communicate with.
        """
        if len(self.cm_list) == 0:
            raise ValueError("No community members to commuicate with!")
        
        for member in self.cm_list:
            member.rec_hours_to_go_to_work = self.rec_hours_to_go_to_work
            member.rec_hours_to_return_from_work = self.rec_hours_to_return_from_work
            member.sim_member_rec_day.data = self.req_manager_rec_day.data
    
    
    def record_data_members_day(self):
        """Update member total data models, according to day data models."""
        
        for member in self.cm_list:
            member.sim_member_load_total.update_data_with_other_models(member.sim_member_load_day) 
            member.sim_member_actions_total.update_data_with_other_models(member.sim_member_actions_day)
            member.est_member_actions_total.update_data_with_other_models(member.est_member_actions_day)
            member.sim_member_rec_total.update_data_with_other_models(member.sim_member_rec_day)
            member.est_member_load_total.update_data_with_other_models(member.est_member_load_day) 
    
    def record_data_community_day(self):
        """Update community total data models, according to day data models."""
        
        if self.strategy == Strategy.INFORMATIVE:
            self.req_manager_rec_total.update_data_with_other_models(self.req_manager_rec_day) # update with recommendations for the current day
        if self.strategy == Strategy.MULTI_OBJECTIVE:
            for hour in self.cm_est_load_day_models[DayPeriods.MORNING]:
                self.rec_manager_rec_total_by_preferred_hour[hour].update_data_with_other_models(self.rec_manager_rec_day_by_preferred_hour[hour]) # update with recommendations for the current day, for each group
                
        self.sim_com_load_day.update_data_with_other_models([member.sim_member_load_day for member in self.cm_list]) # get the simulated consumption for the day
        self.sim_com_load_total.update_data_with_other_models(self.sim_com_load_day) # update the total simulated consumption
        self.est_com_load_day.update_data_with_other_models([member.est_member_load_day for member in self.cm_list]) # get the estimated consumption for the day
        self.est_com_load_total.update_data_with_other_models(self.est_com_load_day) # update the total estimated consumption
        
        
    def write_cm_hourly_results_simulation_to_csv(self):
        """Write cm data models to csv"""
        
        hourly_data = {}    
        hourly_data['sim_time'] = list(self.com_model.tf_total.values()) # get the time frame
        for member in self.cm_list:
            data_models  = [member.est_member_load_total, member.sim_member_load_total, member.sim_member_rec_total, member.sim_member_actions_total] # get the data models
            for model in data_models:
                hourly_data[f'{member.unique_id}_{member.type}_{model.data_source_type}_{model.data_type}_{model.asignee_type}_{model.interval}'] = list(model.data.values()) # assign the data models to a representative key

        df = pd.DataFrame(hourly_data) 
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_cm_hourly.csv"))
        
              
    def write_com_hourly_results_simulation_to_csv(self):  
        """Write community data models to csv"""
         
        hourly_data = {}     
        hourly_data['sim_time'] = list(self.com_model.tf_total.values()) # get the time frame
        energy_models  = [self.est_com_load_total, self.sim_com_load_total, self.est_com_prod_total] # get the data models
        for model in energy_models:
            hourly_data[f'{model.data_source_type}_{model.data_type}_{model.asignee_type}_{model.interval}'] = list(model.data.values()) # assign the data models to a representative key
        df = pd.DataFrame(hourly_data)
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_com_hourly.csv"))
        
    def write_cm_profiles_to_csv(self):
        """Get a description of each member profile in the community and write them to csv for analysis."""
        
        temp_dict = {}
        temp_dict['unique_id'] = [member.unique_id for member in self.cm_list]
        temp_dict['type'] = [member.type for member in self.cm_list]
        temp_dict['flexibility_capacity'] = [member.flexibility_capacity for member in self.cm_list]
        temp_dict['preferred_hour'] = [member.pref_hour[DayPeriods.MORNING] for member in self.cm_list]
        temp_dict['stages'] = [member.member_stages for member in self.cm_list]
        temp_dict['action_intensity'] = [member.member_action_intensity for member in self.cm_list]
        temp_dict['willigness_type'] = [member.willigness_type for member in self.cm_list]
        temp_dict['availability'] = [member.availability for member in self.cm_list]
        df = pd.DataFrame(temp_dict)
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_cm_profiles.csv"))
        
    def write_manager_profile_to_csv(self):
        """Get a description of the manager profile in the community and write it to csv for analysis."""
         
        temp_dict = {}
        temp_dict['stages'] = self.manager_stages
        temp_dict['target_of_recommendations'] = self.target_of_recommendations
        temp_dict['expectancy_development'] = self.expectancy_development
        temp_dict['strategy'] = self.strategy
        temp_dict['privacy_level'] = self.privacy_level
        temp_dict['no_alert_threshold'] = self.no_alert_threshold
        if self.strategy == Strategy.MULTI_OBJECTIVE:
            temp_dict['weight_neeg'] = self.weight_neeg
            temp_dict['weight_dis'] = self.weight_dis
        df = pd.DataFrame(temp_dict, index=[0])
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_manager_profile.csv"))
    
    def write_info_file(self):  
        info_file_path = os.path.join("energycommunity","subway","results", f'Scenario_{self.com_model.sim_params.simulation_id}_info.csv')
        params = ['PV plant number of modules','PV plant module power', 'Estimation System','Solar location','Starting date','Ending date','Flexibility period 1 starting hour', 'Flexibility period 1 ending hour', 'Flexibility period 2 starting hour', 'Flexibility period 2 ending hour']
        values = [self.com_model.sim_params.pv_plant_number_of_modules, self.com_model.sim_params.pv_plant_module_power, #type: ignore
                  self.com_model.sim_params.estimation_system, self.com_model.sim_params.solar_location,   #type: ignore
                  self.com_model.sim_params.starting_date, self.com_model.sim_params.ending_date, self.com_model.sim_params.flexibility_period["morning"]["start"], self.com_model.sim_params.flexibility_period["morning"]["end"], # type: ignore
                  self.com_model.sim_params.flexibility_period["afternoon"]["start"], self.com_model.sim_params.flexibility_period_["afternoon"]["end"]] # type: ignore

        df = pd.DataFrame({'Parameters':params, 'Values': values})
        df.to_csv(info_file_path)
    
            
    def set_df_for_plotting(self):
        """Sets a dataframe for plotting in the analysis.

        Raises:
            ValueError: if the estimated consumption of the community is not set, raise an error.
            ValueError: if the simulated consumption of the community is not set, raise an error.
            ValueError: if the estimated production of the community is not set, raise an error.
        """
        if len(self.est_com_load_total.data) == 0:
            raise ValueError("Est community load data not set!")
        if len(self.sim_com_load_total.data) == 0:
            raise ValueError("Sim community load data not set!")
        if len(self.est_com_prod_total.data) == 0:
            raise ValueError("Est com prod data not set!")
        time_frame = [pd.Timestamp(time).tz_localize(None) for time in list(self.com_model.tf_total.values())]

        df = pd.DataFrame({f'{DSType.ESTIMATED}_{DType.CONSUMPTION}': list(self.est_com_load_total.data.values()), f'{DSType.ESTIMATED}_{DType.PRODUCTION}': list(self.est_com_prod_total.data.values()), f'{DSType.SIMULATED}_{DType.CONSUMPTION}': list(self.sim_com_load_total.data.values())}, index=time_frame) 
        self.plotting_df = df


    def set_recommendations_current_day_dummy_approach(self):
        """Sets the recommendations for the current day for the dummy approach"""
        
        self.req_manager_rec_day.data = {time : Rec.WHITE for index, time in self.com_model.tf_day.items()}
        
        
    def set_estimated_production_for_community_in_Bucharest(self):
        """Sets the estimated production for Bucharest weather data."""
        
        #weather_file_path = os.path.join("energycommunity","subway","data", 'bucharest-unirii.json') # get the weather data file path
        solar_model = self.get_solar_model(self.com_model.sim_params.weather_file_path) # create a solar model 
        phisun_predicted = solar_model.solar_irradiations(slope_in_deg=60, exposure_in_deg=0)['total'] # get the solar radiation
        n = self.com_model.sim_params.pv_plant_number_of_modules #type:ignore
        p_m = self.com_model.sim_params.pv_plant_module_power #type:ignore
        predicted_supplied_powers = [P * 0.75 * n * p_m / 1000 for P in phisun_predicted] # convert to PV production
        predicted_supplied_powers.pop() # exclude the last value since the simulation time frame does not include the right boundary
        self.est_com_prod_total.data = {time : predicted_supplied_powers[i] for i,time in self.com_model.tf_total.items() } # assign the production data to the manager

    def get_solar_model(self, weather_file_path):
        """Generates a solar model according to the weather_file_path.

        Args:
            weather_file_path (_type_): a string to a JSON openweather data file.

        Returns:
            _type_: a solar model developed in the buildingenergy project.
        """
        starting_str_date = timemg.datetime_to_stringdate(self.com_model.sim_params.starting_date, date_format='%d/%m/%Y %H:%M:%S')
        ending_str_date = timemg.datetime_to_stringdate(self.com_model.sim_params.ending_date, date_format='%d/%m/%Y %H:%M:%S')

        
        site_weather_data = OpenWeatherMapJsonReader(weather_file_path, from_stringdate=starting_str_date, to_stringdate=ending_str_date, sea_level_in_meter=330, albedo=.1).site_weather_data # read the weather data

        solar_model = solar.SolarModel(site_weather_data=site_weather_data) # create the solar model
        return solar_model

    def set_recommendations_informative_strategy_current_day(self):
        """Sets the recommendations based on the informative strategy for the current day."""
        
        if self.com_model.sim_params.print_member_info:
            logger.info("SETTING RECOMMENDATIONS - INFORMATIVE STRATEGY")
        
        if self.com_model.time_and_day_names_map[self.com_model.current_time] in ['Sunday', 'Saturday']: # if we are during the weekend
            if self.com_model.sim_params.print_member_info:
                logger.info("No recommendations during weekend!")
            self.req_manager_rec_day.data = {self.com_model.tf_day[i] : Rec.WHITE for i in self.com_model.tf_day} # no recommendations
            return
        
        times_f1 = [self.com_model.current_time.replace(hour=x) for x in self.flexibility_periods[0].get_hours()] # get the flexibility hours morning
        times_f2 = [self.com_model.current_time.replace(hour=x) for x in self.flexibility_periods[1].get_hours()] # get the flexibility hours afternoon
        temp_data = {self.com_model.tf_day[key] : Rec.WHITE for key in self.com_model.tf_day}
        
        for time in times_f1 + times_f2: # for each time in the flexibility period
            if self.est_com_prod_day.data[time] > self.est_com_load_day.data[time] + self.no_alert_threshold: # type: ignore  
                temp_data[time] = Rec.GREEN
            elif self.est_com_load_day.data[time] > self.est_com_prod_day.data[time] + self.no_alert_threshold: # type: ignore  
                temp_data[time] = Rec.RED
        
        self.req_manager_rec_day.data = temp_data 
        
    def set_multi_objective_recommendation(self):
        """Sets the recommendations based on the multi-objective approach.

        Raises:
            ValueError: if the optimisation problem result is not optimal, raise an error.
        """
        if self.com_model.sim_params.print_member_info:
            logger.info("SETTING RECOMMENDATIONS MULTI-OBJECTIVE APPROACH")
        
        if self.com_model.time_and_day_names_map[self.com_model.current_time] in ['Sunday', 'Saturday']: # during the weekend
            if self.com_model.sim_params.print_member_info:
                logger.info("No recommendations during weekend!")
            #for pref_hour in list(self.cm_est_load_day_models_by_pref_going_hour.keys()):
            for pref_hour in list(self.cm_est_load_day_models[DayPeriods.MORNING].keys()):
                self.rec_manager_rec_day_by_preferred_hour[pref_hour] = MemberDataDay(DSType.REQUESTED, DType.RECOMMENDATIONS, AsignType.MANAGER) # create a data model
                self.rec_manager_rec_day_by_preferred_hour[pref_hour].data = {self.com_model.tf_day[i] : Rec.WHITE for i in self.com_model.tf_day} # ... adna assign only WHITE recommendation
            return
        
        flex_times_by_period : Dict[DayPeriods, list[datetime]] = {}
        for flex_time_index, period in enumerate(DayPeriods): # for each flexibility period
            flex_times_by_period[period] = [self.com_model.current_time.replace(hour=x) for x in self.flexibility_periods[flex_time_index].get_hours()] # get the times

        model = pulp.LpProblem("Problem_Optimal_Decision", pulp.LpMinimize)
        error_index : Dict[DayPeriods, List] = {}
        action_index : Dict[DayPeriods, List] = {}
        error_neeg : Dict[DayPeriods, Dict] = {}
        actions : Dict[DayPeriods, Dict] = {}
        error_dis : Dict[DayPeriods, Dict] = {}
        exp_pref_hours = {}
        exp_pref_hours[DayPeriods.MORNING] = list(set([member.pref_hour[DayPeriods.MORNING] for member in self.cm_list]))
        exp_pref_hours[DayPeriods.AFTERNOON] = [x + 8 for x in exp_pref_hours[DayPeriods.MORNING] ]
        
        #for flex_time_index, period in enumerate(['morning','afternoon']): # we generate the indexes for errors; the errors will be stored i dictionaries
        for flex_time_index, period in enumerate(DayPeriods): # we generate the indexes for errors; the errors will be stored i dictionaries
            error_index[period] = [index for index, x in enumerate(self.flexibility_periods[flex_time_index].get_hours())]
            action_index[period] = self.flexibility_periods[flex_time_index].get_hours()
        
        #for flex_time_index, period in enumerate(['morning','afternoon']):  # we generate the error and action dictionaries
        for flex_time_index, period in enumerate(DayPeriods):  # we generate the error and action dictionaries
            error_neeg[period] = pulp.LpVariable.dicts(f"e_{period}_period_at_hour", (i for i in error_index[period]), cat='Continuous') # a continous variable for the error
            error_dis[period] = {}
            actions[period] = {}
            #for pref_hour in self.cm_est_load_day_models_by_pref_going_hour:
            for pref_hour in exp_pref_hours[period]:
                actions[period][pref_hour] = pulp.LpVariable.dicts(f"x_{period}_period_for_group_with_pref_hour_{pref_hour}_at_hour", (i for i in action_index[period]), cat='Binary')
                error_dis[period][pref_hour] = pulp.LpVariable.dicts(f"e_dis_per_{period}_pref_hour_{pref_hour}", (i for i in error_index[period]), cat='Continuous')
    
        # Objective function
        #model += self.weight_neeg * pulp.lpSum([error_neeg["morning"][i] for i in error_index["morning"]] +  [error_neeg["afternoon"][i] for i in error_index["afternoon"]]) + \
        #         self.weight_dis * pulp.lpSum([error_dis["morning"][pref_hour][i] for pref_hour in self.cm_est_load_day_models_by_pref_going_hour for i in error_index["morning"]] +  [error_dis["afternoon"][pref_hour][i] for pref_hour in self.cm_est_load_day_models_by_pref_going_hour for i in error_index["afternoon"]])     
        model += self.weight_neeg * pulp.lpSum([error_neeg[DayPeriods.MORNING][i] for i in error_index[DayPeriods.MORNING]] +  [error_neeg[DayPeriods.AFTERNOON][i] for i in error_index[DayPeriods.AFTERNOON]]) + \
                 self.weight_dis * pulp.lpSum([error_dis[DayPeriods.MORNING][pref_hour][i] for pref_hour in exp_pref_hours[DayPeriods.MORNING] for i in error_index[DayPeriods.MORNING]] +  [error_dis[DayPeriods.AFTERNOON][pref_hour][i] for pref_hour in exp_pref_hours[DayPeriods.AFTERNOON] for i in error_index[DayPeriods.AFTERNOON]])
        #print(model)
        prod_by_flex_period : Dict[DayPeriods, Dict[datetime, Any]] = {} 
        #for flex_time_index, period in enumerate(['morning','afternoon']): # generate the production for flexibility periods
        for flex_time_index, period in enumerate(DayPeriods): # generate the production for flexibility periods
            #prod_by_flex_period[period] = {time: prod_value for time, prod_value in self.est_com_prod_total_by_days[self.com_model.current_time].items() if time in flex_times_by_period[period] }
            prod_by_flex_period[period] = {time: prod_value for time, prod_value in self.est_com_prod_day.data.items() if time in flex_times_by_period[period] }
        
        max_load = {}
        max_load[DayPeriods.MORNING] = 0
        max_load[DayPeriods.AFTERNOON] = 0
        for period in DayPeriods:
            for pref_hour in exp_pref_hours[period]:
                    max_load[period] += self.est_collective_icp[pref_hour][period]
                    #max_load[DayPeriods.AFTERNOON] += self.est_collective_icp[pref_hour + 8][DayPeriods.AFTERNOON]
        #for period_index, period in enumerate(['morning','afternoon']):
        for period in DayPeriods:
            #max_load = sum([x[period_index] for x in list(self.est_collective_icp_by_pref_going_hour.values())])
            
            # Liniarize the normalised NEEG abs value in the objective function
            for flex_time_index,time in enumerate(flex_times_by_period[period]): # for each time in the period
                potential_load = 0
                #for pref_hour in self.cm_est_load_day_models_by_pref_going_hour: # for each group defined by pref_hour
                for pref_hour in exp_pref_hours[period]: # for each group defined by pref_hour
                    #potential_load +=  actions[period][pref_hour][time.hour] * self.est_collective_icp_by_pref_going_hour[pref_hour][period_index] # calculate the potential estimated consumption, with decision variables for each group
                    potential_load +=  actions[period][pref_hour][time.hour] * self.est_collective_icp[pref_hour][period] # calculate the potential estimated consumption, with decision variables for each group

                model += (error_neeg[period][flex_time_index] + (potential_load - prod_by_flex_period[period][time]) / max([ prod_by_flex_period[period][time], max_load[period]]) ) >= 0 # we normalise the potential with the maximum load 
                model += (error_neeg[period][flex_time_index] - (potential_load - prod_by_flex_period[period][time]) / max([ prod_by_flex_period[period][time], max_load[period]]) ) >= 0 
            #for pref_hour in self.cm_est_load_day_models_by_pref_going_hour: 
        for period in DayPeriods:
            for pref_hour in exp_pref_hours[period]:
                model += (sum(list(actions[period][pref_hour].values()))) == 1 # during a period, a group defined by a pref hour should commute once
        
        for period in DayPeriods:    
            # Liniarize the normalised Dissatisfaction abs value in the objective function
            for time_index,time in enumerate(flex_times_by_period[period]): # for each hour in a period
                #for pref_hour in self.cm_est_load_day_models_by_pref_going_hour: # for each group defined by pref_hour
                for pref_hour in exp_pref_hours[period]: # for each group defined by pref_hour
                    potential_dis_by_pref_hour = (actions[period][pref_hour][time.hour] * time.hour - pref_hour) # what would be the dissatisfaction of members in the group of pref_hour if they would go to work at hour time.hour
                    #pref_ret_hour = pref_hour + 8
                    #if time.hour < 13:
                    #    potential_dis_by_pref_hour = (actions[period][pref_hour][time.hour] * time.hour - pref_hour)  # what would be the dissatisfaction of members in the group of pref_hour if they would go to work at hour time.hour
                    #else:
                    #    potential_dis_by_pref_hour = (actions[period][pref_hour][time.hour] * time.hour - pref_ret_hour) # what would be the dissatisfaction of members in the group of pref_hour if they would go to work at hour time.hour
                    model += (error_dis[period][pref_hour][time_index] + potential_dis_by_pref_hour /  len(flex_times_by_period[period]))   >= 0  # we normalise the potential dissatisfaction with the number of hours in a period
                    model += (error_dis[period][pref_hour][time_index] - potential_dis_by_pref_hour /   len(flex_times_by_period[period]))   >= 0
                    
        #print(model)
            
        status = model.solve(PULP_CBC_CMD(msg=False))
        
        if pulp.LpStatus[status] != 'Optimal':
            raise ValueError("optimal solution not found!")
        
        #KEEP FOR DEBUGGING/PRINTING PRUPOSES

        #for pref_hour in self.cm_est_load_day_models_by_pref_hour:
        #    print("NUMBER OF MEMBERS WITH THIS HOUR:", len(self.cm_est_load_day_models_by_pref_hour[pref_hour]))
        
        #for period_index, period in enumerate(['morning','afternoon']):
        #    #for pref_hour in self.cm_est_load_day_models_by_pref_going_hour:
        #    for pref_hour in exp_pref_hours[period]:
        #        print(f'ICP {period} for group {pref_hour}',self.est_collective_icp[pref_hour][period])
        #    print(f'PROD {period}',prod_by_flex_period[period].values())
        #    #for period in action_dictionary:
        #    for pref_hour in actions[period]:
        #        for hour in actions[period][pref_hour]:
        #            if actions[period][pref_hour][hour].varValue == 1:
        #                print(f'Action morning {period} for group {pref_hour} on hour {hour}:',actions[period][pref_hour][hour].varValue)
        
        for pref_hour in exp_pref_hours[DayPeriods.MORNING]: # for each group
            self.rec_manager_rec_day_by_preferred_hour[pref_hour] = MemberDataDay(DSType.REQUESTED, DType.RECOMMENDATIONS, AsignType.MANAGER) 
            temp_data = {self.com_model.tf_day[key] : Rec.WHITE for key in self.com_model.tf_day} # the initial recommendations are white
            
            for time in flex_times_by_period[DayPeriods.MORNING]: # in the morning, we refer to the actions defined at the same pref hour
                if actions[DayPeriods.MORNING][pref_hour][time.hour].varValue == 1: 
                    temp_data[time] = Rec.GREEN
                else:
                    temp_data[time] = Rec.RED
            for time in flex_times_by_period[DayPeriods.AFTERNOON]:
                if actions[DayPeriods.AFTERNOON][pref_hour + 8][time.hour].varValue == 1:  # in the afternoon, the pref hour is the initial pref_hour+8
                    temp_data[time] = Rec.GREEN
                else:
                    temp_data[time] = Rec.RED
       
            self.rec_manager_rec_day_by_preferred_hour[pref_hour].data = temp_data
                        

class SubwayCommunityMember(CommunityMember):
    """Community Member Class definition. """

    def __init__(self, flexibility_capacity : FlexibilityCapacity, unique_id, model: SubwayCommunity, agent_type: MType, member_stages: Stages, member_action_intensity: MemberActionIntensity, willigness_type: Willigness, availability: Availability, est_mean_values_df: Dict[datetime, Tuple[float,float]], pref_hour : int):
        super().__init__(unique_id, model, agent_type, member_stages, member_action_intensity, willigness_type, availability)
 
        self.com_model : SubwayCommunity = model 
        self.flexibility_capacity : FlexibilityCapacity = flexibility_capacity
        self.est_mean_values_df = est_mean_values_df
        
        self.est_member_load_total = MemberDataTotal(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.MEMBER)
        
        self.est_mean_values_icp_morning_current_day, self.est_mean_values_icp_afternoon_current_day  = 0, 0
        
        self.sim_times_to_commute_day : Tuple[datetime, datetime] | None = None
        self.est_times_to_commute_day : Tuple[datetime,datetime] | None = None
        
        self.rec_hours_to_go_to_work : List[int] = []
        self.rec_hours_to_return_from_work : List[int] = []

        self.pref_hour : Dict[DayPeriods, int] = {} # prefered hour by period: in the morning it's the prefered going hour, in the afternon it's the prefered returning hour
        self.pref_hour[DayPeriods.MORNING] = pref_hour
        self.pref_duration = 8
        self.pref_hour[DayPeriods.AFTERNOON] = pref_hour + self.pref_duration
        self.plotting_df : None | pd.DataFrame = None
         
        self.est_member_actions_day, self.est_member_actions_total  = MemberDataDay(DSType.ESTIMATED, DType.ACTIONS), MemberDataTotal(DSType.ESTIMATED, DType.ACTIONS)
        
        if len(self.com_model.tf_total) != 0:
            self.joining_date = list(self.com_model.tf_total.values())[0]
            self.leaving_date = list(self.com_model.tf_total.values())[-1] 

    def init_cm_day(self):
        """This function is executed at the start of each day"""

        self.sim_member_load_day.data = {time_value : 0 for time_index, time_value in self.com_model.tf_day.items()}
        self.sim_member_actions_day.data = {time_value : Rec.WHITE for time_index, time_value in self.com_model.tf_day.items()}
        self.sim_member_rec_day.data = {time_value :Rec.WHITE for time_index, time_value in self.com_model.tf_day.items()}
        self.set_est_hour_to_commute()
        self.set_est_member_consumption_current_day() # based on the hours to commute and the mean est icp in morning/afternoon


    def set_est_member_consumption_current_day(self):
        """Sets the est_consumption_current_day_list according to est_icp_current_day

        Raises:
            ValueError: if there are no estimated hours to commute for the current day, raise an error
            ValueError: if there are no ICP values for the current day, raise an error
        """
        
        if self.est_times_to_commute_day is None:
            raise ValueError("self.est_hours_to_commute_not_set!")

        if self.com_model.time_and_day_names_map[self.com_model.current_time] in ['Sunday', 'Saturday']:
            self.est_member_load_day.data = {time : 0 for i, time in self.com_model.tf_day.items()}
            self.est_member_actions_day.data = {time : Rec.WHITE for i, time in self.com_model.tf_day.items()}
            return
        
        temp_model = {time : 0.0 for i, time in self.com_model.tf_day.items()} 
        temp_actions_model = {time : Rec.WHITE for i,time in self.com_model.tf_day.items()}
        
        icp_vals = self.est_mean_values_df.get(self.com_model.current_time)
        
        if icp_vals is None:
            raise ValueError("ICP not found for current day!")
        
        for i, period in enumerate(['morning','afternoon']):
            
            icp_val = icp_vals[i]
            time = self.est_times_to_commute_day[i]
            
            #time_icp = { time.replace(hour=self.est_times_to_commute_day[i].hour) : value[i] for time, value in  self.est_mean_values_df.items() if time.date() == self.est_times_to_commute_day[i].date()}
            temp_model.update({time : icp_val }) # update the consumption and recommendations when the member should commute
            temp_actions_model.update({time : Rec.GREEN  } )

        self.est_member_load_day.data = temp_model  # type: ignore
        self.est_member_actions_day.data = temp_actions_model # type: ignore
        
    def set_est_hour_to_commute(self):
        """Sets the estimated commuting hours"""
            
        time_commute_morning : datetime = self.com_model.current_time.replace(hour=self.pref_hour[DayPeriods.MORNING])
        time_commute_afternoon = self.com_model.current_time.replace(hour=self.pref_hour[DayPeriods.AFTERNOON])
        self.est_times_to_commute_day = (time_commute_morning, time_commute_afternoon)


    def set_sim_hours_to_commute_based_on_recommendations_fully_flexible(self):
        if self.pref_hour is None:
            raise ValueError("self.preferred_hour not set!")
        time_to_pref_hour : List[int] = []
        
        if len(self.rec_hours_to_go_to_work) != 0:
            for hour in self.rec_hours_to_go_to_work:
                time_to_pref_hour.append(abs(hour - self.pref_hour[DayPeriods.MORNING]))  # we ge the time between recommended hours and the preferred hour
            index_of_min_time = time_to_pref_hour.index(min(time_to_pref_hour))
            hour_to_go_to_work = self.rec_hours_to_go_to_work[index_of_min_time] # the member goes to work at the hour that is closest to the preferred hour
            time_to_go_to_work = self.com_model.current_time.replace(hour=hour_to_go_to_work)
        else:
            time_to_go_to_work = self.com_model.current_time

        time_to_8_hrs_work_period = []
        if len(self.rec_hours_to_return_from_work) != 0:
            for hour in self.rec_hours_to_return_from_work:
                #time_to_8_hrs_work_period.append(abs(hour - (self.preferred_hour + 8) ))  # we ge the time between recommended hours and the time closest to fulfilling an 8 hour working period
                time_to_8_hrs_work_period.append(abs(hour - (self.pref_hour[DayPeriods.AFTERNOON]) ))  # we ge the time between recommended hours and the time closest to fulfilling the preferred duration
            index_of_min_time = time_to_8_hrs_work_period.index(min(time_to_8_hrs_work_period))
            hour_to_return_from_work = self.rec_hours_to_return_from_work[index_of_min_time] # the member goes to work at the hour that is closest to the 8 hr limit
            time_to_return_from_work = self.com_model.current_time.replace(hour=hour_to_return_from_work)
        else:
            time_to_return_from_work = self.com_model.current_time
        self.sim_times_to_commute_day = (time_to_go_to_work, time_to_return_from_work)
        
        
    def set_sim_hours_to_commute_based_on_recommendations_semi_flexible(self):
        if self.pref_hour is None:
            raise ValueError("self.preferred_hour not set!")
        time_to_pref_hour = [] 
        for hour in self.rec_hours_to_go_to_work:
            time_to_pref_hour.append(abs(hour - self.pref_hour[DayPeriods.MORNING]))  # we ge the time between recommended hours and the preferred hour
        index_of_min_time = time_to_pref_hour.index(min(time_to_pref_hour))
        hour_to_go_to_work = self.rec_hours_to_go_to_work[index_of_min_time] # the member goes to work at the hour that is closest to the preferred hour
        time_to_go_to_work : datetime = self.com_model.current_time.replace(hour=hour_to_go_to_work)

        #self.sim_times_to_commute_day = (time_to_go_to_work, time_to_go_to_work + timedelta(hours=8))
        self.sim_times_to_commute_day = (time_to_go_to_work, time_to_go_to_work + timedelta(hours=self.pref_duration))

    def set_df_for_plotting(self):
        if len(self.est_member_load_total.data) == 0:
            raise ValueError("Est member load data not set!")
        if len(self.sim_member_load_total.data) == 0:
            raise ValueError("Sim member load data not set!") 
        
        time_frame = [pd.Timestamp(time).tz_localize(None) for time in list(self.com_model.tf_total.values())] # the simulation time frame
        new_tf = [x.to_pydatetime() for x in time_frame] # convert it to timestamp
        df = pd.DataFrame({f'{DSType.ESTIMATED}_{DType.CONSUMPTION}': list(self.est_member_load_total.data.values()), f'{DSType.SIMULATED}_{DType.CONSUMPTION}': list(self.sim_member_load_total.data.values()),
                                         f'{DSType.SIMULATED}_{DType.ACTIONS}': list(self.sim_member_actions_total.data.values()), f'{DSType.SIMULATED}_{DType.RECOMMENDATIONS}': list(self.sim_member_rec_total.data.values())},
                                        index = new_tf)

        self.plotting_df = df
        