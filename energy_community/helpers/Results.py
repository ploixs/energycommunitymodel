import ast
import copy
import datetime
import os
from ssl import PEM_FOOTER
import sys
from datetime import timedelta
from math import sqrt
from statistics import stdev, mean
from typing import Dict, List, Union

import pandas as pd
import pytz


from energy_community.core.constants import AsignType, DSType, DType, IndicatorsAll, IndicatorsInd, IndicatorsLP, IndicatorsLoad, Interval, MType, Rec, Stages
from energy_community.helpers.time_management_functions import generate_start_and_end_dates_for_file_reading_writing

from energy_community.helpers.Spec import Spec
#from plotwriteapp.helpers import check_dominance, get_member_model_based_on_features
#from simulation.Configurator import Configuration
from energy_community.helpers import indicators
#from simulation.SubwayComResults import SubwayComResults
#from simulation.params import Parms, SubwayComParams
from energy_community.helpers.indicators import NEEG_per_day, avg_dissatisfaction_member, cost, effort, involvement_based_on_contribution, NEEG, renunciation, self_consumption, self_sufficiency





    

class Results:
    """ Simulation Results Class """

    def __init__(self, id, analysis_spec, timezone):
        results_folder_flag, results_file_paths_flag, com_models_flag, agent_results_df_flag, manager_archetype_flag = False, False, False, False, False
        
        self.hourly_com_results_full_path = ""
        self.agent_results_full_path = ""
        self.hourly_cm_results_full_path = ""
        self.cm_profiles_path = ""
        self.manager_profile_path = ""
        
        
        
        
        
        #self.est_com_load_total = EstimatedCommunityConsumptionTotal()
        #self.sim_com_load_total = SimulatedCommunityConsumptionTotal()
        #self.est_com_prod_total = EstimatedCommunityProductionTotal(None,None)
        #self.cm_models = {}
        self.timeframe_simulation : List[Union[pd.Timestamp, datetime.datetime]] = []
        self.local_tz = timezone
        
        #self.agents_results_df : pd.DataFrame | None = None
        self.results_plot_file_name = None
        #self.member_archetype = ""
        #self.manager_archetype = ""
        self.folder_path : str = ""
        self.description_indicator_plots = None

        #self.agent_types_in_simulation : List[MType] = []
        #self.agents_ids_by_type = {}
        #self.agents_id_list = []
        self.total_number_of_recommendations = None
        self.short_description = ""
        self.results_agents_by_type = {}

        self.file_writing_data = []
        self.file_writing_column_names : List[str] = []
        
        self.cm_file_writing_data = {}
        self.cm_file_writing_column_names = []
        
        self.spec : Spec = analysis_spec
        self.norm_neeg = None
        self.norm_rec_count_groups = None
        self.norm_rec_count_deviation = None
        
        self.performances  = {}
        self.performances_each_cm  = {}

        self.scenario_id = id
        
        
        

        #results_folder_flag = self.update_results_folder_path_generation_simulation_no_estimation() # Set folder path from which results will be extracted
        #if results_folder_flag:
        #    results_file_paths_flag  = self.update_simulation_results_file_paths() # Set the results file paths

        
        '''
        if results_file_paths_flag:
            com_models_flag = self.update_com_energy_models() # Extract results from csvs at community level
        if results_file_paths_flag:
            agent_results_df_flag = self.update_agents_results_df() # Extract results about members
        if agent_results_df_flag and self.agents_results_df is not None:
            self.days_count = self.agents_results_df.shape[0]  # Count the number of days
            self.set_agent_types_in_simulation()  # Determine agent categories from files
            self.set_agents_ids()  # Extract agent ids
        if agent_results_df_flag and results_file_paths_flag:
            self.set_cm_data_models() # Set consumption, actions and recommendations models
        manager_archetype_flag = self.update_manager_archetype()  # Determine the model of the manager
        self.set_member_archetype()  # Determine the model of each member
        if manager_archetype_flag:
            self.set_results_file_name_no_estimation()  # Determine plot file name
        if self.spec.calculate_recommendation_count:
            self.set_total_number_of_recommendations()  # TO IMPREOVE: Calculate the total number of recommendations given
        #self.initialize_variables_for_storing_results()
        
        if self.spec.calculate_deviation_from_white:
            self.set_recommendation_count_based_on_deviation()
        if self.spec.calculate_recommendation_count_grouping:
            # self.rec_count_groups = self.set_recommendation_count_based_on_grouping()
            self.norm_rec_count_groups = self.set_recommendation_count_based_on_grouping_by_stdev()
        if self.spec.calculate_recommendation_count_simple:
            self.rec_count_simple = self.set_recommendation_count_simple()
        
        self.set_performances_all_simulation()  # Calculate and set the performances for all simulation scenarios

        #self.set_normalised_neeg()

        self.set_file_writing_data_and_column_names()  # Set the data to be written in files
        self.set_short_description()  # Set a short description for each simulation scenario
        self.set_description_for_indicator_plots()  # Set description indicator plots

        self.binary_effort_pareto_values = None
        self.general_effort_pareto_value = None

        # Calculate the pareto values for plotting
        # self.set_pareto_values()

        # self.set_requested_community_consumption()
        '''
    

    


    def update_simulation_results_file_paths(self) -> bool:
        """ Add extensions to the folder path """
        
        self.hourly_com_results_full_path = os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_com_hourly.pickle")
        self.agent_results_full_path = os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_agents.csv")
        self.hourly_cm_results_full_path = os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_cm_hourly.pickle")
        self.cm_profiles_path =  os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_cm_profiles.csv")
        self.manager_profile_path =  os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_manager_profile.csv")
        
        if self.hourly_com_results_full_path == "":
            raise ValueError(f"Community results file path could not be set!")
        if self.hourly_cm_results_full_path == "":
            raise ValueError(f"Member results file path could not be set!")
        if self.agent_results_full_path == "":
            raise ValueError(f"Agent results file path could not be set!")
        if self.cm_profiles_path == "":
            raise ValueError(f"CM profiles path could not be set!")
        if self.manager_profile_path == "":
            raise ValueError(f"Manager profiles path could not be set!")
        return True


    def set_pareto_values(self):
        # non_dominated_values = get_pareto_front(scenario, 10)
        effort_list, neeg_list = [], []

        neeg_list = []
        for a_type in self.agent_types_in_simulation:
            for a_id in self.agents_ids_by_type[a_type]:
                # for i in range(len(self.performances['simulated']['neeg_' + a_type + '_' + str(a_id)])):
                neeg_list.append(self.performances['simulated']['neeg_' + a_type + '_' + str(a_id)])
                effort_list.append(
                    self.performances['simulated']['gen_effort_eq_sh_' + a_type + '_' + str(a_id)])

        temp_tuple_list = []
        for i in range(len(effort_list)):
            if effort_list[i] >= 0:
                temp_tuple_list.append((effort_list[i], neeg_list[i]))

        # sorted_data = sorted(temp_tuple_list, key=lambda tup: tup[1])
        non_dominated_values = []
        i = 0
        temp_tuple_list = sorted(temp_tuple_list, key=lambda tup: tup[0])

        while i < len(temp_tuple_list) and len(non_dominated_values) < 10:
            current_element = temp_tuple_list[i]
            non_dominated_values.append(current_element)
            for j in range(i + 1, len(temp_tuple_list)):
                next_element = temp_tuple_list[j]
                flag = 0
                if check_dominance(current_element[1], next_element[1]):
                    i = j
                    break

        non_dominated_values = sorted(non_dominated_values, key=lambda tup: tup[1])
        self.binary_effort_pareto_values = [x[0] for x in non_dominated_values]
        self.general_effort_pareto_value = [x[1] for x in non_dominated_values]

    def set_total_number_of_recommendations(self):
        if self.hourly_cm_results_df is None:
            raise ValueError("hourly_cm_results_df not initialized!")
        
        self.total_number_of_recommendations = 0
        for hour in self.hourly_cm_results_df['hour']:
            if 7 < hour < 23:
                self.total_number_of_recommendations += 1
        print("- Total number of recommendations given between 07:00 - 23:00 identified!")
        print("-- Recommendation count: ", self.total_number_of_recommendations)

    """
    def set_short_description(self):
        description = f"{self.params.manager_stages} {self.params.recommendation_strategy} manager, " \
                      fr"{self.params.expectancy_development_approach} expectancy and $\tau=${self.params.no_alert_threshold} W"
        self.short_description = description.replace("_", " ")
    
    def set_description_for_indicator_plots(self):
        description = ""
        description += self.params.manager_stages + " \n" + \
                       self.params.recommendation_strategy + " manager, \n" + \
                       self.params.expectancy_development_approach + " expectancy"
        self.description_indicator_plots = description
    """
    def update_results_folder_path_generation_simulation_no_estimation(self) -> bool:
        #root_folder = Configuration.get_results_config('results_folder')
        self.folder_path = os.path.join(root_folder, f'Simulation_{self.scenario_id}')
        return True
        
    def update_manager_archetype(self) -> bool:
        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'fixed' and \
                self.params.recommendation_strategy == 'coaching':
            self.manager_archetype = "one_turn_coaching_clusters_fixed_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'basic' and \
                self.params.recommendation_strategy == 'informative':
            self.manager_archetype = "one_turn_informative_everyone_basic_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'adaptive' and \
                self.params.recommendation_strategy == 'informative':
            self.manager_archetype = "one_turn_informative_everyone_adaptive_expectancy"

        if self.params.manager_stages == 'two_turns' and self.params.expectancy_development_approach == 'adaptive' and \
                self.params.recommendation_strategy == 'informative':
            self.manager_archetype = "two_turns_informative_everyone_adaptive_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'adaptive' and \
                self.params.recommendation_strategy == 'coaching':
            self.manager_archetype = "one_turn_coaching_everyone_adaptive_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'basic' and \
                self.params.recommendation_strategy == 'coaching':
            self.manager_archetype = "one_turn_coaching_everyone_basic_expectancy"

        if self.params.manager_stages == 'two_turns' and self.params.expectancy_development_approach == 'basic' and \
                self.params.recommendation_strategy == 'informative':
            self.manager_archetype = "two_turns_informative_everyone_basic_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'basic' and \
                self.params.recommendation_strategy == 'dummy':
            if self.params.subway_flag:
                self.manager_archetype = "one_turn_dummy_everyone_ss_basic_expectancy"
            else:
                self.manager_archetype = "one_turn_dummy_everyone_basic_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'basic' and \
                self.params.recommendation_strategy == 'dynamic':
            self.manager_archetype = "one_turn_dynamic_everyone_basic_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'basic' and \
                self.params.recommendation_strategy == 'relaxed_dynamic':
            self.manager_archetype = "one_turn_relaxed_dynamic_everyone_basic_expectancy"

        if self.params.manager_stages == 'one_turn' and self.params.expectancy_development_approach == 'basic' and \
                self.params.recommendation_strategy == 'performant_dynamic':
            self.manager_archetype = "one_turn_performant_dynamic_everyone_basic_expectancy"
        print("- Manager archetype successfully identified!")
        print("-- Manager archetype: ", self.manager_archetype)
        if self.manager_archetype == "":
            raise ValueError("Manager archetype could not be set!")
        return True

    def set_member_archetype(self):
        if self.params.member_action_intensity == 'fixed':
            self.member_archetype = "one_turn_fixed_intensity"
        if self.params.member_action_intensity == 'realistic' and self.params.member_stages == 'one_turn':
            self.member_archetype = "one_turn_realistic_intensity"
        if self.params.member_action_intensity == 'realistic' and self.params.member_stages == 'two_turns':
            self.member_archetype = "two_turns_realistic_intensity"
        print("- Member archetype successfully identified!")
        print("-- Member archetype: ", self.member_archetype)
        if self.member_archetype == "":
            raise ValueError("Member archetype could not be set!")




     
    def update_agents_results_df(self) -> bool:
        self.agents_results_df = pd.read_csv(self.agent_results_full_path)
        if self.agents_results_df is None:
            raise ValueError("agent_results_df could not be set!")
        return True

    def set_agent_types_in_simulation(self):
        if self.agents_results_df is None:
            raise ValueError("agents_results_df not initialized!")
        for agent_type in MType:
            for column_name in self.agents_results_df.columns:
                if agent_type in column_name and agent_type not in self.agent_types_in_simulation:
                    self.agent_types_in_simulation.append(agent_type)
        print("- Agent types in simulation identified! ")
        print("-- Agent types: ", self.agent_types_in_simulation)
        if len(self.agent_types_in_simulation) == 0:
            raise ValueError("Could not set member types in simulation!")

    def set_agents_initial_and_final_agents_data_all_simulation(self):
        if not self.agents_results_df:
            raise ValueError("agents_results_df not initialized!")
        agent_count = 0
        for agent_type in self.agent_types_in_simulation:
            wanted_col_name = f'{DSType.SIMULATED}_{agent_type}_agent_count_by_day'
            if wanted_col_name not in self.agents_results_df.columns:
                self.results_agents_by_type[agent_type]['initial_count'] = 0
                self.results_agents_by_type[agent_type]['final_count'] = 0
            else:
                self.results_agents_by_type[agent_type]['initial_count'] = self.agents_results_df[wanted_col_name].to_list()[3]
                self.results_agents_by_type[agent_type]['final_count'] = self.agents_results_df[wanted_col_name].to_list()[-1]
            agent_count += len(self.results_agents_by_type)
        self.final_no_agents = self.agents_results_df[f'{DSType.SIMULATED}_agent_count_by_day'].to_list()[-1]

    def set_agents_ids(self):
        if self.agents_results_df is None:
            raise ValueError("agents_results_df not initialized!")
        for agent_type in self.agent_types_in_simulation:
            id_list = ast.literal_eval(self.agents_results_df[f'{DSType.SIMULATED}_{agent_type}_agent_id_list'][0])
            self.agents_ids_by_type[agent_type] = id_list
            self.agents_id_list.extend(id_list)
        print("- Agent ids identified!")
        print("-- Agent ids: ", self.agents_id_list)
        print(f"-- We have {len(self.agents_id_list)} agents in simulation.")

    def set_recommendation_count_based_on_deviation(self):
        if not self.hourly_cm_results_df:
            raise ValueError("hourly_cm_results_df not initialized!")
        recommendations_count = 0
        recommendations_simulation = self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total']
        for recommendation in recommendations_simulation:
            if recommendation in [0.5, -0.5]:
                recommendations_count += 1
            if recommendation in [1, -1]:
                recommendations_count += 1.5 * 1.5
            if recommendation in [2, -2]:
                recommendations_count += 2 * 2
        # we norm to 2 because 2 is the miax value of standard dev
        result = sqrt(recommendations_count / len(self.hourly_cm_results_df)) / 2
        print("-- Deviation from white color: ", result)
        self.norm_rec_count_deviation = result

    def set_recommendation_count_based_on_grouping_by_stdev(self):
        if not self.hourly_cm_results_df:
            raise ValueError("hourly_cm_results_df not initialized!")
        recs = self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()
        rec_count = 0

        for i in range(1, len(recs)):
            if stdev([recs[i - 1], recs[i]]) != 0:
                rec_count += 1

        # result = rec_count
        result = rec_count / len(self.hourly_cm_results_df)
        print("-- Recommendations count based on grouping recommendations that are the same: ", result)
        return result

    def set_recommendation_count_based_on_grouping(self):
        if not self.hourly_cm_results_df:
            raise ValueError("hourly_cm_results_df not initialized!")
        recommendations_count = self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[0]
        for i in range(1, len(self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'])):
            if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] == \
                    self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i - 1]:
                recommendations_count += 0
            else:
                if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] in [0.5, -0.5]:
                    recommendations_count += 1
                if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] in [1, -1]:
                    recommendations_count += 1.5
                if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] in [2, -2]:
                    recommendations_count += 2

        result = recommendations_count / len(self.hourly_cm_results_df)
        print("-- Recommendations count based on grouping recommendations that are the same: ", result)

        return result

    def set_recommendation_count_simple(self):
        if not self.hourly_cm_results_df:
            raise ValueError("hourly_cm_results_df not initialized!")
        recommendations_count = 0
        for i in range(len(self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'])):
            if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] in [0.5, -0.5]:
                recommendations_count += 1
            if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] in [1, -1]:
                recommendations_count += 1.5
            if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] in [2, -2]:
                recommendations_count += 2

        result = recommendations_count / len(self.hourly_cm_results_df)
        print("-- Recommendations count considering strong recommendation influence: ", result)
        return result

    def count_recommendations(self):
        if not self.hourly_com_results_df:
            raise ValueError("hourly_com_results_df not initialized!")
        if not self.hourly_cm_results_df:
            raise ValueError("hourly_cm_results_df not initialized!")
        recommendations_count = 0
        if self.params.recommendation_strategy == 'coaching':
            for i in range(len(self.hourly_com_results_df)):
                recommendations_count += 5
        if (self.params.recommendation_strategy == 'informative' or self.params.recommendation_strategy == 'dummy') \
                and self.params.manager_stages == 'one_stage':
            for agent_type in self.agent_types_in_simulation:
                recommendations_count = len(
                    self.hourly_cm_results_df['first_' + agent_type + '_agent_recommended_actions_total'])
        if self.params.recommendation_strategy == 'informative' and self.params.manager_stages == 'reactive':
            for i in range(len(self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'])):
                if self.hourly_cm_results_df['first_ideal_agent_recommended_actions_total'].to_list()[i] in [0, 1, -1, 0.5,
                                                                                                          -0.5]:
                    recommendations_count += 1
                else:
                    recommendations_count += 2
        print("RECOMMENDATIONS COUNT:", recommendations_count)
        return recommendations_count

    def set_performances_all_simulation(self):

        if len(self.est_com_load_total.data) == 0 or len(self.est_com_prod_total.data) == 0 or len(self.sim_com_load_total.data) == 0:
            raise ValueError("Community hourly energy models not initialized!")
        if self.cm_models is None:
            raise ValueError("cm models not initialized!")

        print("- Simulation performances: ")

        for com_load in [self.est_com_load_total, self.sim_com_load_total]:
            self.performances.append(Performance(com_load.data_source_type, com_load.asignee_type, IndicatorsLP.SC, self_consumption(com_load.data, self.est_com_prod_total.data))) 
            ss = self_sufficiency(com_load.data, self.est_com_prod_total.data)
            if ss is not None:
                self.performances.append(Performance(com_load.data_source_type, com_load.asignee_type, IndicatorsLP.SS, ss)) 
            self.performances.append(Performance(com_load.data_source_type, com_load.asignee_type, IndicatorsLP.COST, cost(com_load.data, self.est_com_prod_total.data))) 
            self.performances.append(Performance(com_load.data_source_type, com_load.asignee_type, IndicatorsLP.NEEG_PER_DAY, NEEG_per_day(com_load.data, self.est_com_prod_total.data))) 
            self.performances.append(Performance(com_load.data_source_type, com_load.asignee_type, IndicatorsLP.NEEG, NEEG(com_load.data, self.est_com_prod_total.data))) 
        
        self.performances.append(Performance(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLP.NORMALISED_NEEG, NEEG(self.sim_com_load_total.data, self.est_com_prod_total.data)/ NEEG(self.est_com_load_total.data, self.est_com_prod_total.data)))

        self.performances.append(Performance(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLoad.EFFORT, effort(self.est_com_load_total.data, self.sim_com_load_total.data))) 
        self.performances.append(Performance(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLoad.RENUNCIATION, renunciation(self.est_com_load_total.data, self.sim_com_load_total.data))) 

        savings_val = (indicators.cost(self.est_com_load_total.data, self.est_com_prod_total.data) - indicators.cost(self.sim_com_load_total.data, self.est_com_prod_total.data)) / self.days_count / len(self.agents_id_list)
        self.performances.append(Performance(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLP.DAILY_SAVINGS, savings_val))
       
        if self.print_member_performances:
            print("-- Agent performances: ")

        # Calculate individual indicators (for each member)
        if self.spec.calculate_individual_indicators:
            for agent_type in self.agent_types_in_simulation:
                for agent_id in self.agents_ids_by_type[agent_type]:
                    self.performances_each_cm[agent_id] = []
                    # Get differential share values
                    est_member_load : Data  = get_member_model_based_on_features(self.cm_models[agent_id], DSType.ESTIMATED, DType.CONSUMPTION)
                    if est_member_load.data is None:
                        raise ValueError(f"no est consumption available for member {agent_id}")
                    dif_rho_list = [est_member_load.data[i] / self.est_com_load_total.data[i] if self.est_com_load_total.data[i] != 0 else 0 for i in range(len(est_member_load.data))]

                    #!!cannot differentiate during simulatio between eq share and dif share. must renounce in the future 
                    # Get equal share values
                    #eq_rho_list = [1 / len(self.agents_id_list) for i in range(len(exp_consumption))]

                    member_p_pv = []
                    for i in range(len(self.est_com_load_total.data)):
                        member_p_pv.append(self.est_com_prod_total.data[i] * dif_rho_list[i])

                    # Individual neeg with differential share
                    sim_member_load : Data = get_member_model_based_on_features(self.cm_models[agent_id], DSType.SIMULATED, DType.CONSUMPTION)
                    if sim_member_load.data is None:
                        raise ValueError(f"no sim consumption available for member {agent_id}")
                    
                    p3 = NEEG(sim_member_load.data, member_p_pv)
                    member_neeg = Performance(DSType.SIMULATED, AsignType.MEMBER,IndicatorsLP.NEEG, p3/1000)
                    self.performances_each_cm[agent_id].append(member_neeg)
                    if self.print_member_performances:
                        print(f"--- {agent_type} agent {agent_id} NEEG per day: {p3 / 1000} [kWh]")

                    # Effort with differential share
                    p4 = involvement_based_on_contribution(est_member_load.data, sim_member_load.data, self.est_com_prod_total.data, self.est_com_load_total.data, dif_rho_list)
                    member_involvement = Performance(DSType.SIMULATED, AsignType.MEMBER, IndicatorsInd.INVOLVEMENT_BASED_ON_CONTRIBUTION, p4)
                    self.performances_each_cm[agent_id].append(member_involvement)
                    if self.print_member_performances:
                        print(f"--- {agent_type} agent {agent_id} involvement dif contrib: {round(p4, 2)} [kWh]")

                    # Average expected consumption for agent_id per day
                    p6 = sum(est_member_load.data) * 24 / len(self.est_com_prod_total.data) / 1000
                    av_exp_load_member = Performance(DSType.ESTIMATED, AsignType.MEMBER, IndicatorsInd.AVERAGE_ESTIMATED_LOAD_DAY, p6)
                    self.performances_each_cm[agent_id].append(av_exp_load_member)
        
                    if est_member_load.time_frame is None:
                        raise ValueError("Cannot have empty time frame for estimated consumption model of member!")
                    p7 = avg_dissatisfaction_member(est_member_load.data, sim_member_load.data, est_member_load.time_frame, 4)
                    dis_member = Performance(DSType.SIMULATED, AsignType.MEMBER, IndicatorsInd.DISSATISFACTION_PER_CM, p7)
                    self.performances_each_cm[agent_id].append(dis_member)
                    
                    if self.params.subway_flag:
                        fav_commuting_hour = 0
                        for i in range(72):
                            if est_member_load.data[i] > 0:
                                fav_commuting_hour = est_member_load.time_frame[i]
                                break
                        if fav_commuting_hour == 0:
                            raise ValueError("Favourite commuting hour could not be identified!")
                        p8 = Performance(DSType.ESTIMATED, AsignType.MEMBER, IndicatorsInd.FAV_COMMUTING_HOUR, fav_commuting_hour.hour)
                        self.performances_each_cm[agent_id].append(p8)



        