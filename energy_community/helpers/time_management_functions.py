import datetime
from typing import Dict, List, Tuple, Union
import dateutil
import pytz
from tzlocal import get_localzone
import pandas as pd
from buildingenergy.timemg import stringdate_to_datetime, datetime_to_stringdate

def stringdate_shift_year(stringdate: str, shifted_years: int, date_format: str = '%d/%m/%Y %H:%M:%S'):
    """Use to shift dates from several years.

    :param stringdate: the stringdate to be shifted
    :type stringdate: str
    :param shifted_years: the number of year for the shift (the day of weeks should correspond to each day of month
    :type shifted_years: int
    :return: the shifted date
    :rtype: str
    """
    _datetime = stringdate_to_datetime(stringdate, date_format=date_format)
    return datetime_to_stringdate(datetime_shift_year(_datetime, shifted_years), date_format=date_format)


def datetime_shift_year(the_datetime: datetime.datetime, shifted_years):
    """Use to shift dates from several years.

    :param the_datetime: the date to be shifted
    :type the_datetime: datetime.datetime
    :param shifted_years: number of year to add (can be a negative number
    :type shifted_years: int
    :return: the new shifted date
    :rtype: datetime.datetime
    """
    return the_datetime + dateutil.relativedelta.relativedelta(years=shifted_years) # type: ignore
    # return the_datetime
    # if shifted_years < 0:
    #     the_datetime = the_datetime.replace(year=the_datetime.year + shifted_years)
    # else:
    #     the_datetime.replace(year=the_datetime.year + shifted_years)
    return the_datetime

def generate_start_and_end_dates_for_file_reading_writing(starting_date: Union[pd.DatetimeIndex,datetime.datetime],
                                                          ending_date: Union[pd.DatetimeIndex,datetime.datetime]) -> Tuple[str, str]:
    """Convert starting_date,ending_date to string formats for conventional file naming."""

    str_start_date = str(starting_date.strftime("%d_%m_%Y"))
    str_end_date = str(ending_date.strftime("%d_%m_%Y"))
    return str_start_date, str_end_date


def generate_local_datetime_range_and_indexes(starting_date: Union[pd.DatetimeIndex, pd.Timestamp, datetime.datetime], 
                                              ending_date: Union[pd.DatetimeIndex, pd.Timestamp, datetime.datetime]) \
        -> Tuple[List[datetime.datetime], Dict[int, int]]:
    """Generate the local datetime range and start and end day indexes for the timeframe between
    starting_date and ending_date

    rtype: (list, list, list)
    """
    #print(starting_date.tzinfo)
    #local_tz = get_localzone() # Get the local timezone
    #local_timezone = pytz.timezone(local_tz.__str__()) # Set the local timezone for conversion
    #local_timezone = "Europe/Paris"
    local_timezone = starting_date.tzinfo
    # Convert dates to utc format
    starting_date_utc = starting_date.astimezone(pytz.utc)  # type: ignore
    ending_date_utc = ending_date.astimezone(pytz.utc) # type: ignore

    previous_day_in_year_index = -1

    # utc_datetime is a variable to iterate through all utc dates generated
    utc_datetime = starting_date_utc  # initialize it because we use a while instruction to iterate

    start_of_day_hour_index_list = list()
    local_datetimes = list()

    hour_index = 0  # the hour index to iterate through all hours between the starting_date and ending_date

    day_counter = 0
    
    while utc_datetime < ending_date_utc:
        # UTC time is translated into local time (ie based on local timezone)
        #local_datetime = local_timezone.normalize(utc_datetime.replace(tzinfo=pytz.utc).astimezone(local_timezone)) OLD, was working with buildingenergy timemg functions
        local_datetime = utc_datetime.replace(tzinfo=pytz.utc).astimezone(local_timezone) # type: ignore

        # Get the day index in the year for the current datetime
        current_day_in_year_index = local_datetime.timetuple().tm_yday

        if len(local_datetimes) == 0 or current_day_in_year_index != previous_day_in_year_index:  # if a new day is detected...
            # ...store the index as the time index where a new day has been detected
            start_of_day_hour_index_list.append(hour_index)
            day_counter += 1
            # since we took account of the current day, we update previous_day_in_year_index
            previous_day_in_year_index = current_day_in_year_index  #

        # Store the local datetime
        local_datetimes.append(local_datetime)

        # reference UTC time is increased by 1 hour to keep the iteration going
        utc_datetime += datetime.timedelta(hours=1)

        hour_index += 1

    end_of_day_hour_index_list = []
    for i in range(1, len(start_of_day_hour_index_list)):
        end_of_day_hour_index_list.append(start_of_day_hour_index_list[i] - 1)
    end_of_day_hour_index_list.append(start_of_day_hour_index_list[-1] + 23)

    time_mapping = {start_of_day_hour_index_list[i]: end_of_day_hour_index_list[i] for i in range(day_counter) }
    return local_datetimes, time_mapping
    

