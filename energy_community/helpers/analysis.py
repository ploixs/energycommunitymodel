

import csv
from itertools import product
import os
from typing import Any, Dict, List, Tuple
from matplotlib import gridspec, colors
from matplotlib import pyplot as plt
import numpy as np
from energy_community.core.constants import ASSIGN_TYPE_MAP, DS_TYPE_MAP, MTYPE_MAP, AsignType, DSType, IndicatorsAll, IndicatorsLP, IndicatorsLoad, IndicatorsRec, MType
from energy_community.residential.results.base import ResidentialResults


def get_unique_types_from_scenario(scenario: ResidentialResults) -> List[MType]:
    
    unique_types_across_scenario = []

    for cm in scenario.manager.cm_list:
        if cm.type not in unique_types_across_scenario:
            unique_types_across_scenario.append(cm.type)
    
    return unique_types_across_scenario

def get_unique_types_across_scenarios(scenarios):
    """
    Get unique types across all scenarios.

    Args:
    scenarios (list): A list of scenarios from which to extract unique types.

    Returns:
    list: A list of unique types across all scenarios.
    """
    unique_types_across_scenarios = set()

    for scenario in scenarios:
        cm_types = get_unique_types_from_scenario(scenario)
        unique_types_across_scenarios.update(cm_types)

    return list(unique_types_across_scenarios)

def write_community_results_comparison(scenarios: List[ResidentialResults], results_folder_path: str, use_scenario_description:bool=False, default_file_name: str = ""):
    """Write community performances for each scenario in a csv file.

    Args:
        scenarios (List[ResidentialResults]): the list of scenarios to be compared.
    """
    
    indicators_rec = [IndicatorsRec.RECOMMENDATION_DEVIATION_RATE, IndicatorsRec.REC_INFLUENCE, IndicatorsRec.CONTRIBUTION_CHANGE_RATE, IndicatorsRec.AVG_CONTRIBUTIONS_PER_DAY]
    
    # Set detfault file name if not given as parameter
    if not default_file_name:
        file_name = f"analysis_{ '_'.join([str(scenario.scenario_id) for scenario in scenarios])}.csv"
    else:
        file_name = default_file_name
    
    # Extract column names except for categories
    column_names_raw_except_category = [key for key, _ in scenarios[0].analyzer.performances.items() if key[1] != AsignType.CATEGORY]
    
     # Generate column names for categories
    column_names_raw_for_categories = [(DSType.SIMULATED, AsignType.CATEGORY, indicator, cm_type) for cm_type, indicator in product(MType, indicators_rec)]
        
    # Combine both lists of column names
    column_names_raw = column_names_raw_except_category + column_names_raw_for_categories

    performances_by_scenario: Dict[ResidentialResults, List] = {}
    
    for scenario in scenarios:
        #  Extract performances for the given column names from a scenario analyzer, else complete with None
        performance_values = [scenario.analyzer.performances[name] if name in scenario.analyzer.performances else None for name in column_names_raw]
        performances_by_scenario[scenario] = performance_values
    
    # Mapping for better text in csv file
    column_names_final_part_1 = [f"{DS_TYPE_MAP[key[0]]} {ASSIGN_TYPE_MAP[key[1]]} {key[2]}" for key in column_names_raw if len(key) < 4]
    column_names_final_part_2 = [f"{DS_TYPE_MAP[key[0]]} {ASSIGN_TYPE_MAP[key[1]]} {key[2]} {MTYPE_MAP[key[3]]}" for key in column_names_raw if len(key) == 4]
    column_names_final = column_names_final_part_1 + column_names_final_part_2
    
    with open(os.path.join(results_folder_path, file_name), 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)

        # Write headers to the CSV file
        csv_writer.writerow(["ID"] + ["Scenario"] + column_names_final)

        # Write performance values to the CSV file
        if use_scenario_description:
            for scenario, performance_values in performances_by_scenario.items():
                csv_writer.writerow([scenario.scenario_id] + [scenario.com_model.sim_params.scenario_description] + performance_values) #type:ignore 
        else:
            for scenario, performance_values in performances_by_scenario.items():
                csv_writer.writerow([scenario.scenario_id] + [scenario.description] + performance_values)
            

def get_average_performance_over_categories(scenario, indicator):
    
    types_in_scenario = get_unique_types_from_scenario(scenario)
    performances = []
    for cm_type in types_in_scenario:
        performances.append(scenario.analyzer.performances[(DSType.SIMULATED, AsignType.CATEGORY, indicator, cm_type)])
    
    return sum(performances) / len(performances)


def get_pareto_front(pareto_length : int, x : List[float], y : List[float]) -> Tuple[List[float], List[float]]:
    """Gets the pareto front from a list of pareto values.

    Args:
        pareto_length (int): max desired length for the pareto front
        x (List[float]): the x coordinates of the pareto points
        y (List[float]): the y coordinates of the pareto points

    Returns:
        Tuple[List[float], List[float]]: first list represents the x coordinates of the Pareto front, second list represents the y coordinates of the front
    """
    
    pareto_points : List[Tuple[float, float]] = []
    for i in range(len(x)):
        if x[i] >= 0: # if the value is positive
            pareto_points.append((x[i], y[i])) # group the coordinates in a tuple

    dominant_values = []
    i = 0
    pareto_points = sorted(pareto_points, key=lambda tup: tup[0]) # sort the pareto points based on the x values

    while i < len(pareto_points) and len(dominant_values) < pareto_length:
        current_element = pareto_points[i] # get the current element
        dominant_values.append(current_element) # assign it to the dominant values list
        for j in range(i + 1, len(pareto_points)): 
            next_element = pareto_points[j] # get the next element
            if current_element[1] > next_element[1]: # if the current element is dominant
                i = j # repeat the iteration starting from the next element
                break

    sorted_data = sorted(dominant_values, key=lambda tup: tup[1]) # sort the dominant values based on y
    return [x[0] for x in sorted_data] , [x[1] for x in sorted_data]


def plot_scenario_radar_plot(scenarios: List[ResidentialResults], results_folder_path: str, save_figures_flag: bool = False, default_file_name : str = ""):
    """
    Function for creating a radar plot that visualizes the performance of different scenarios based on three indicators.
    
    Args:
        scenarios (List[ResidentialResults]): A list of ResidentialResults objects that contain performance data for the indicators.
        save_figures_flag (bool, optional): A flag indicating whether to save the plot as a PDF file (True) or display it on the screen (False). Defaults to False.
    """
    
    # If save_figures_flag is True, set up plot with LaTeX formatting, 'science' style, and grid lines
    if save_figures_flag:
        plt.rc('text', usetex=True) # add latex to plot if the user wants to save it
        plt.style.use(['science'])
        plt.grid(True)
    
    # Set file name if given as parameter
    if not default_file_name:        
        file_name = f"Figure_{ '_'.join([str(scenario.scenario_id) for scenario in scenarios])}_indicators_radar.pdf"
    else:
        file_name = default_file_name    
    
    # Define indicators and categories
    performance_indicators = [IndicatorsLP.SC, IndicatorsLP.SS, IndicatorsAll.NORMALISED_NEEG,
                              IndicatorsRec.RECOMMENDATION_DEVIATION_RATE, IndicatorsRec.CONTRIBUTION_CHANGE_RATE]
    performance_categories = ['SC','SS', 'NEEG', "Avg. RDR", "Avg. CCR"]
    #recommendation_categories_general = ['RDR', 'CCR']
    
    #cm_type_style_map = {MType.GOOD: "Enthusiastic", MType.BAD: "Reluctant", MType.NORMAL: "Normal", MType.IDEAL: "Ideal"}
    
    # Get unique community member types across all scenarios
    #unique_types_across_scenarios = get_unique_types_across_scenarios(scenarios)
    
    #for cm_type in unique_types_across_scenarios:
    #    for scenario in scenarios:
    #        if cm_type not in [cm.type for cm in scenario.manager.cm_list]:
    #            raise ValueError("CM types vary from one scenario to another!")
    
    # Generate recommendation categories across all unique types.            
    #recommendation_categories_across_scenarios = [f"{cm_type_style_map[cm_type]} CMs {category}" for category in recommendation_categories_general
    #                                                                                         for cm_type in unique_types_across_scenarios ]
    
    # Create figure and polar axis
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111, polar=True) # type: ignore

    # Plot each scenario's performance data
    for scenario in scenarios:
        values = []
        # Get indicator values for performance related indicators
        for indicator in performance_indicators:
            if indicator in IndicatorsRec:
                performance = get_average_performance_over_categories(scenario, indicator)
                values += [performance]
            else:
                values += [scenario.analyzer.performances[(DSType.SIMULATED, AsignType.COMMUNITY, indicator)]] 
        
        
        #for indicator in recommendation_indicators:
        #    performance = get_average_performance_over_categories(scenario, indicator)
        # Get indicator values for recommendation related indicators
        #rec_values = [scenario.analyzer.performances[(DSType.SIMULATED, AsignType.CATEGORY, indicator, cm_type)] for indicator in recommendation_indicators for cm_type in unique_types_across_scenarios]
        #values += rec_values
        #    values += [performance]
        #
        # Compute angles for each category
        #N = len(performance_categories) + len(recommendation_categories_across_scenarios)
        N = len(performance_categories) 
        angles = np.linspace(0, 2*np.pi, N, endpoint=False)

        # Plot the radar chart
        ax.plot(angles, values, 'o-', linewidth=2, label=scenario.description)
        ax.fill(angles, values, alpha=0.25)
    
    # Set plot's labels, font sizes, and grid lines
    #ax.set_thetagrids(angles * 180/np.pi, performance_categories + recommendation_categories_across_scenarios , fontsize=12) #type: ignore
    ax.set_thetagrids(angles * 180/np.pi, performance_categories , fontsize=12) #type: ignore
    
    ax.tick_params(axis='y', labelsize=14) # set the fontsize of the scaling inside the circles
    ax.grid(True)
    #plt.legend(loc='lower center', bbox_to_anchor=(0.5, -0.2), ncol=2, fontsize=12)
    #plt.legend()
    # Adding the legend
    legend = plt.legend(frameon = 1, bbox_to_anchor=(0.5, -0.1), loc='upper center')

    # Setting the opacity (alpha) of the legend background
    frame = legend.get_frame()
    frame.set_alpha(1) # values from 0 (transparent) to 1 (opaque)

    # Setting the border color
    frame.set_edgecolor('black')

    # Setting the background color
    frame.set_facecolor('white')
    
    # If save_figures_flag is True, save plot as PDF file in results_folder_path directory; otherwise, display plot on screen
    if save_figures_flag:
        f = plt.gcf()
        f.tight_layout()
        path = os.path.join(results_folder_path, file_name)
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()
        
        
def plot_indicators_comparison(simulation_scenarios : List[ResidentialResults], results_folder_path: str = "", save_figures_flag=False, use_scenario_description:bool=False, filename: str="Figure_indicators_comparison.pdf"):
    """Plot a comparison between indicators for the simulation scenarios given.

    Args:
        simulation_scenarios (List[ResidentialResults]): the simulation scenarios analysed.
        results_folder_path (str): the folder path tio which the figure will be saved
        save_figures_flag (bool, optional): set to TRUE if you want to save the figure. Defaults to False.
    """
    if save_figures_flag:
        plt.rc('text', usetex=True) # option to use latex
        plt.style.use(['science'])
        plt.grid(True)

    plt.figure(figsize=(25, 25)) # figure size adapted for all indicators
 
    plot_keys : List[str] = [] # the plot keys in the bar plot; represented by a list of short descriptions for scenarios
    
    performance_indicators = {IndicatorsAll.NORMALISED_NEEG: 'Normalised NEEG',
                  IndicatorsLP.SC: 'Self-consumption', IndicatorsLP.SS: 'Self-sufficiency',
                  IndicatorsRec.RECOMMENDATION_DEVIATION_RATE: 'Average recommendation deviation rate', 
                  IndicatorsRec.CONTRIBUTION_CHANGE_RATE: 'Average contribution change rate',
                  IndicatorsRec.AVG_CONTRIBUTIONS_PER_DAY: 'Average contributions per day'} # analysed indicators

    bar_colors = []
    performance_indicator_data = {}
    for indicator in performance_indicators:
        performance_indicator_data[indicator] = []

    original_colors = list(colors.BASE_COLORS.keys())  # type: ignore

    # Create a dictionary to map no_alert_thresholds to colors
    threshold_color_dict = {}

    for i, scenario in enumerate(simulation_scenarios):
        if use_scenario_description:
            plot_keys.append(fr"{scenario.com_model.sim_params.scenario_description} and $\tau = {scenario.manager.no_alert_threshold}$") #type: ignore          
        else:
            plot_keys.append(scenario.description)
        current_no_alert_threshold = scenario.manager.no_alert_threshold

        # Assign a color if this threshold hasn't been seen before
        if current_no_alert_threshold not in threshold_color_dict:
            # Use modulo operation to cycle through colors if there are more thresholds than colors
            threshold_color_dict[current_no_alert_threshold] = original_colors[len(threshold_color_dict) % len(original_colors)]

        bar_colors.append(threshold_color_dict[current_no_alert_threshold])

        for indicator in performance_indicators:
            if indicator in IndicatorsRec:
                performance_indicator_data[indicator].append(get_average_performance_over_categories(scenario, indicator))
            else:
                performance_indicator_data[indicator].append(scenario.analyzer.performances[(DSType.SIMULATED, AsignType.COMMUNITY, indicator)])

    i = 0
    for indicator, indicator_name in performance_indicators.items(): # for each indicator
        plt.subplot(6, 2, i + 1) # assign a space in the subplot
        plt.barh(plot_keys, performance_indicator_data[indicator], color=bar_colors)
        plt.title(indicator_name, fontsize=18) 
        plt.ylabel('Scenario', fontsize=16)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)
        i += 1
        
        ax = plt.gca()
        for bars in ax.containers:
            ax.bar_label(bars, fmt='%.2f', label_type='center', color='white')

    f = plt.gcf()
    f.tight_layout()

    if save_figures_flag:
        path = os.path.join(results_folder_path, filename)
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()
        

def plot_pareto_neeg_deviation(simulation_scenarios : List[ResidentialResults], results_folder_path: str = "", save_figures_flag=False,
                               use_scenario_description: bool = False,
                               filename: str ="Figure_indicators_pareto.pdf"):
    """Plot the pareto front between normalised neeg and normalised deviation.

    Args:
        simulation_scenarios (List[ResidentialResults]): the list of simulation scenarios analysed
        results_folder_path (str, optional): the folder in which the figures are saved. Defaults to "".
        save_figures_flag (bool, optional): set to TRUE if you want to save the figure to results_folder_path. Defaults to False.
    """
    original_colors = list(colors.BASE_COLORS.keys()) # type: ignore
    original_markers = ["o", "s", "p", "*", "X", "D", "d"]
    
    plt.figure(figsize=(10, 10))
    
    if use_scenario_description:
        legend_text = [f"{scenario.com_model.sim_params.scenario_description} {scenario.manager.no_alert_threshold}" #type: ignore
                       for scenario in simulation_scenarios ] 
    else:
        legend_text = [scenario.description for scenario in simulation_scenarios]
    unique_stages_exp_strategies = list(set([(scenario.manager.manager_stages, 
                                          scenario.manager.expectancy_development, 
                                          scenario.manager.strategy) for scenario in simulation_scenarios])) # get all unique combinations of strategies, comfort param and expectancy in the simulation scenarios
    unique_no_alert_thresholds = list(set([scenario.manager.no_alert_threshold for scenario in simulation_scenarios])) # get all unique no-alert-thresholds in the simulation scenarios
    
    threshold_colors = [original_colors[i] for i in range(len(unique_no_alert_thresholds))] # set an unique color for each no-alert threshold
    strategy_markers = [original_markers[i] for i in range(len(unique_stages_exp_strategies))] # set an unique marker for each combination

    if save_figures_flag:
        plt.rc('text', usetex=True) # add latex to plot if the user wants to save it
        plt.style.use(['science'])
        plt.grid(True)
        
    indicators : List[Dict[str,Tuple[Any, str]]] = [ {'x': (IndicatorsAll.NORMALISED_NEEG, 'Net-Energy-Exchanged-with-the-Grid'), 'y': (IndicatorsRec.RECOMMENDATION_DEVIATION_RATE, 'Recommendation Deviation Rate') },
                                                     {'x': (IndicatorsAll.NORMALISED_NEEG, 'Net-Energy-Exchanged-with-the-Grid'), 'y': (IndicatorsRec.CONTRIBUTION_CHANGE_RATE, 'Contribution Change Rate') },
                                                     {'x': (IndicatorsRec.CONTRIBUTION_CHANGE_RATE, ' Contribution Change Rate'), 'y': (IndicatorsRec.RECOMMENDATION_DEVIATION_RATE, 'Recommendation Deviation Rate') }]
    
    # We make a subplot amtrix of 2 rows and 4 columns. Two plots will be on the first row, spanning 2 columns.
    locations = [(0,0), (0,2), (1,1)] # row-column starting positions for each subplot
    plot_index = 0
    for plot_index in range(3):    
        ax = plt.subplot2grid(shape=(2,4), loc=locations[plot_index], colspan=2) 
        
        y_list : List[float] = []
        x_list : List[float] = []
        
        pareto_colors = [] # the list of pareto colors used in the plot, based on the order imposed by the simulation_scenarios
        pareto_markers = [] # the list of pareto markers used in the plot, based on the order imposed by the simulation_scenarios
        
        for scenario in simulation_scenarios:
            
            current_y_indicator = indicators[plot_index]['y'][0]
            if current_y_indicator in [IndicatorsRec.CONTRIBUTION_CHANGE_RATE, IndicatorsRec.RECOMMENDATION_DEVIATION_RATE]:
                y_perf = get_average_performance_over_categories(scenario, current_y_indicator)
                #y_perf = scenario.analyzer.performances[(DSType.SIMULATED, AsignType.CATEGORY, current_y_indicator, unique_type_across_scenarios)]
            else:
                y_perf = scenario.analyzer.performances[(DSType.SIMULATED, AsignType.COMMUNITY, current_y_indicator)]
                
            y_list.append(y_perf)
            
            current_x_indicator = indicators[plot_index]['x'][0]
            if current_x_indicator in [IndicatorsRec.CONTRIBUTION_CHANGE_RATE, IndicatorsRec.RECOMMENDATION_DEVIATION_RATE]:
                x_perf = get_average_performance_over_categories(scenario, current_x_indicator)
                #x_perf = scenario.analyzer.performances[(DSType.SIMULATED, AsignType.CATEGORY, current_x_indicator, unique_type_across_scenarios)]
            else:
                x_perf = scenario.analyzer.performances[(DSType.SIMULATED, AsignType.COMMUNITY, current_x_indicator)]
                
            x_list.append(x_perf)

        for scenario in simulation_scenarios:
            current_no_alert_threshold = scenario.manager.no_alert_threshold # get the current no alert threshold
            color_index = unique_no_alert_thresholds.index(current_no_alert_threshold) # get the index in the unique threshold list
            pareto_colors.append(threshold_colors[color_index]) # based on that index, assign the specific color for the threshold and append it to a list

            current_strategy = scenario.manager.strategy # get the current strategy used
            current_stages = scenario.manager.manager_stages
            current_expectancy = scenario.manager.expectancy_development
            marker_index = unique_stages_exp_strategies.index((current_stages, current_expectancy, current_strategy)) # get the index of that strategy in the unique strategy list
            pareto_markers.append(strategy_markers[marker_index])  # based on that index, assign the specific marker for the strategy and append it to a list

        for i, (x, y) in enumerate(zip(x_list, y_list)):
            #plt.scatter(x, y, label=legend_text[i], marker=pareto_markers[i], color=pareto_colors[i]) # generate the plot
            plt.scatter(x, y, marker=pareto_markers[i], color=pareto_colors[i]) # generate the plot

        deviation_pareto_values, neeg_pareto_values = get_pareto_front(10, x_list, y_list) # get the coordinates for the pareto front values
        plt.plot(deviation_pareto_values, neeg_pareto_values) # plot the pareto front
        plt.ylabel(indicators[plot_index]['y'][1], fontsize=14)
        plt.xlabel(indicators[plot_index]['x'][1], fontsize=14)
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)

    f = plt.gcf()
    f.legend(legend_text,loc='center left', bbox_to_anchor=(1, 0.5))
    

    if save_figures_flag:
        f.tight_layout()
        path = os.path.join(results_folder_path, filename)
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()