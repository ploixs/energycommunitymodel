from datetime import datetime
from statistics import stdev
from typing import Dict, List, Union
import numpy as np
import math as m
import pandas as pd

# Defining MAPE function
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error

from energy_community.core.constants import PossibleActions, Rec

def contribution_ratio(flex_period_start_hour, flex_period_end_hour, 
                       actions_by_time: Dict[Union[pd.Timestamp, datetime],
                                             PossibleActions]) -> float:
    
    actions = []
    for time, action in actions_by_time.items():
        if flex_period_start_hour  <= time.hour <= flex_period_end_hour:
            if action == PossibleActions.FOLLOW:
                actions.append(1)
            else:
                actions.append(0)
    
    return sum(actions)/len(actions)

def average_contributions_per_day(recommendations : List[Rec]) -> float:
    
    rec_count = 0
    
    for i in range(1, len(recommendations)):
        if stdev([recommendations[i - 1].value, recommendations[i].value]) != 0:
            rec_count += 1

    return rec_count / len(recommendations) * 24

def contribution_change_rate(recommendations : List[Rec]) -> float:
    """Computes how often the recommendatins changes.

    Args:
        recommendations (List[Rec]): the recommendations given during the flexibility period

    Returns:
        float: the change in recommendations
    """
    
    rec_count = 0
    
    for i in range(1, len(recommendations)):
        if stdev([recommendations[i - 1].value, recommendations[i].value]) != 0:
            rec_count += 1

    return rec_count / len(recommendations)

def recommendation_deviation_rate(recommendations_during_flexibility : List[Rec]) -> float:
    """Function to calculate the recommendation deviation rate.

    Args:
        recommendations_during_flexibility (List[Rec]): the recommendations given the flexibility period under evaluation.

    Returns:
        float: the recommendation deviation rate
    """
        
    # Initialize a variable to store the sum of squared ratings.
    squarred_cumulated_rating = 0
    
    # Iterate through each recommendation in the list.
    for recommendation in recommendations_during_flexibility:
        
        # If the recommendation value is 0.5 or -0.5, add 0.25 (0.5 * 0.5) to the sum of squared ratings.
        if recommendation.value in [0.5, -0.5]:
            squarred_cumulated_rating += 0.5 * 0.5
        
        # If the recommendation value is 1 or -1, add 0.5625 (0.75 * 0.75) to the sum of squared ratings.
        if recommendation.value in [1, -1]:
            squarred_cumulated_rating += 0.75 * 0.75
        
        # If the recommendation value is 2 or -2, add 1 (1 * 1) to the sum of squared ratings.
        if recommendation.value in [2, -2]:
            squarred_cumulated_rating += 1 * 1

    # Calculate the recommendation deviation rate by taking the square root of
    # the sum of squared ratings divided by the number of recommendations.
    return m.sqrt(squarred_cumulated_rating / len(recommendations_during_flexibility))

def recommendation_influence( recommendations_during_flexibility : List[Rec]) -> float:
    """Impact of recommendations, with a chosen impact for each recommendation type: normal, strong and blinking.

    Args:
        recommendations_during_flexibility (List[Rec]): a list of the recommendations given to one particular member

    Returns:
        float: normalised recommendation influence
    """

    recommendations_count = 0
    for recommendation in recommendations_during_flexibility:
        if recommendation.value in [0.5, -0.5]: # if it is GREEN or RED
            recommendations_count += 1 # the impact is quantified as 1
        if recommendation.value in [1, -1]: # if it is STRONG GREEN or STRONG RED
            recommendations_count += 1.5 # the impact is quantified as 1.5
        if recommendation.value in [2, -2]:  # if it is BLINKING GREEN or BLINKING RED
            recommendations_count += 2 # the impact is quantified as 2
            
    return recommendations_count / len(recommendations_during_flexibility) # we devide by the number of recommendations given in the flexibility period

def avg_dissatisfaction_member(sim_consumption: List, time_frame: List[Union[pd.Timestamp,datetime]], flexibility_period_lenght: int, pref_hour: int, pref_duration: int = 8):
    if flexibility_period_lenght == 0:
        raise ValueError("Flexibility period length should be different then 0!")
    dis_total = []
    commuted_flag = False
    for i in range(len(time_frame)):
        if time_frame[i].strftime("%A") not in ['Sunday', 'Saturday']: # if it's a working day
            if time_frame[i].hour == 0:
                commuted_flag = False
            if time_frame[i].hour < 13 and sim_consumption[i] > 0 :  # if the member has use the subway station in the morning
                commuted_flag = True
                #actual_commuting_times.append(time_frame[i])  # we consider that is the actual time when the member commuted
                dis_total.append(abs(pref_hour - time_frame[i].hour)/ flexibility_period_lenght) # we calculate the dissatisfaction and remember it
            if time_frame[i].hour > 13 and sim_consumption[i] > 0 :  # if the member has use the subway station in the morning
                #actual_returning_times.append(time_frame[i])  # we consider that is the actual time when the member commuted
                dis_total.append(abs(pref_hour + pref_duration - time_frame[i].hour)/ flexibility_period_lenght) # we calculate the dissatisfaction and remember it
            if time_frame[i].hour == 23 and commuted_flag == False: 
                dis_total.append(flexibility_period_lenght*2)
    if len(dis_total) > 0:
        return sum(dis_total) / len(dis_total)
    else:
        return 0
    


def self_consumption(consumption: List, production: List):
    """Compute the ratio of production that is consumed locally

    :param consumption: series of consumption values
    :type consumption: List[float]
    :param production: series of production values
    :type production: List[float]
    :return: self-consumption coefficient
    :rtype: float
    """
    locally_consumed_production = 0
    for i in range(len(consumption)):
        locally_consumed_production += min(production[i], consumption[i])
    if sum(production) == 0:
        return 0
    else:
        return locally_consumed_production / sum(production)


def self_sufficiency(consumption: List, production: List) -> Union[float,None]:
    total_consumption = 0
    total_production = 0

    for i in range(len(consumption)):
        total_consumption = total_consumption + consumption[i]
        total_production = total_production + production[i]
    value_accumulator = 0

    for i in range(len(consumption)):
        #    value_accumulator = min(production[i], consumption[i]) + value_accumulator
        if production[i] <= consumption[i]:
            value_accumulator = value_accumulator + production[i]
        else:
            value_accumulator = value_accumulator + consumption[i]

    if total_consumption == 0:
        return None
    else:
        return value_accumulator / total_consumption


def dependency(consumption: List[float], production: List[float]) -> float:
    """Compute the ratio of consumption that is coming from the grid

    :param consumption: series of consumption values
    :type consumption: List[float]
    :param production: series of production values
    :type production: List[float]
    :return: self-production coefficient
    :rtype: float
    """
    return 1 - self_sufficiency(consumption, production) # type: ignore


def autonomy(consumption: List, production: List):
    """Compute the ratio of autonomy wrt the grid

    :param consumption: series of consumption values
    :type consumption: List[float]
    :param production: series of production values
    :type production: List[float]
    :return: autonomy coefficient
    :rtype: float
    """
    total_consumption = 0
    total_production = 0

    for i in range(len(consumption)):
        total_consumption += consumption[i]
        total_production += production[i]
    if total_production == 0:
        return 1
    if total_consumption == 0:
        return None
    else:
        return total_production / total_consumption


def renunciation(consumption_before: List, consumption_after: List):
    """Quantity of renounced power

    :param consumption_before: expected consumption without incitation
    :type consumption_before: List[float]
    :param consumption_after: expected consumption without incitation
    :type consumption_after: List[float]
    :return: coefficient between 0 (no renunciation) to infinity
    :rtype: float
    """
    return max(0, sum(consumption_before) - sum(consumption_after)) / sum(consumption_before)


def effort(consumption_before: List, consumption_after: List, threshold: float = .2):
    """effort is the ratio of time where the consumption is significantly changed, accroding to the threshold

    :param consumption_before: expected consumption without incitation
    :type consumption_before: List[float]
    :param consumption_after: expected consumption without incitation
    :type consumption_after: List[float]
    :param threshold: variation in the consumption below which it's consider that change is not annoying for house inhabitants
    :type threshold: float
    :return: coefficient between 0 (no renunciation) to infinity
    :rtype: float
    """
    _effort = 0
    for i in range(len(consumption_before)):
        _effort += (abs(consumption_before[i] - consumption_after[i])) > (threshold * consumption_after[i])
    return _effort / len(consumption_before)


def involvement_based_on_contribution(exp_consumption_member, sim_consumption_member, production,
                                      exp_consumption_community, rho_list):
    improved_effort = 0
    for i in range(len(exp_consumption_community)):
        improved_effort += abs(sim_consumption_member[i] - exp_consumption_member[i] -
                               rho_list[i] * (production[i] - exp_consumption_community[i])) / 1000
    return improved_effort


def strategy_evaluation(exp_consumption, sim_consumption, req_consumption, recommendations):
    total_score = 0
    for i in range(len(exp_consumption)):
        if req_consumption[i] > 1.2 * exp_consumption[i] and recommendations[i] in [0.5, 1, 2]:
            current_hour_score = 1
        elif req_consumption[i] < 0.8 * exp_consumption[i] and  recommendations[i] in [-0.5, -1, -2]:
            current_hour_score = 1
        elif 0.8 * exp_consumption[i] < req_consumption[i] < 1.2 * exp_consumption[i] and recommendations[i] == 0:
            current_hour_score = 0
        else:
            current_hour_score = -1
        total_score += current_hour_score

    return total_score / len(exp_consumption)

def improved_binary_effort(exp_consumption, sim_consumption, req_consumption, recommendations):
    effort = 0
    for i in range(len(exp_consumption)):

        if req_consumption[i] > 1.2 * exp_consumption[i]:
            if 0.8 * req_consumption[i] < sim_consumption[i] < 1.2 * req_consumption[i] and \
                    recommendations[i] in [0.5, 1, 2]:
                current_effort = 1
            elif (exp_consumption[i] < sim_consumption[i] < 0.8 * req_consumption[i] or
                  1.2 * req_consumption[i] < sim_consumption[i]) and \
                    recommendations[i] in [0.5, 1, 2]:
                current_effort = 0
            else:
                current_effort = 0
        elif req_consumption[i] < 0.8 * exp_consumption[i]:
            if 0.8 * req_consumption[i] < sim_consumption[i] < 1.2 * req_consumption[i] and \
                    recommendations[i] in [-0.5, -1, -2]:
                current_effort = 1
            elif (0 < sim_consumption[i] < 0.8 * req_consumption[i] or
                  1.2 * req_consumption[i] < sim_consumption[i] < exp_consumption[i]) and \
                    recommendations[i] in [-0.5, -1, -2]:
                current_effort = 0
            else:
                current_effort = 0
        else:
            if 0.8 * req_consumption[i] < sim_consumption[i] < 1.2 * req_consumption[i] and \
                    recommendations[i] == 0:
                current_effort = 0
            else:
                current_effort = 0
        effort += current_effort

    return effort / len(exp_consumption)


def binary_effort(exp_consumption, sim_consumption, req_consumption, recommendations, number_of_recommendations):
    effort = 0
    for i in range(len(exp_consumption)):
        current_effort = None

        if recommendations[i] in [0.5, 1, 2]:
            if 0.8 * req_consumption[i] < sim_consumption[i] < 1.2 * req_consumption[i]:
                current_effort = 1
            elif exp_consumption[i] < sim_consumption[i] < 0.8 * req_consumption[i] or \
                    1.2 * req_consumption[i] < sim_consumption[i]:
                current_effort = 0
            else:
                current_effort = -1
        if recommendations[i] in [-0.5, -1, -2]:
            if 0.8 * req_consumption[i] < sim_consumption[i] < 1.2 * req_consumption[i]:
                current_effort = 1
            elif 0 < sim_consumption[i] < 0.8 * req_consumption[i] or \
                    1.2 * req_consumption[i] < sim_consumption[i] < exp_consumption[i]:
                current_effort = 0
            else:
                current_effort = -1
        if recommendations[i] == 0:
            if 0.8 * req_consumption[i] < sim_consumption[i] < 1.2 * req_consumption[i]:
                current_effort = 1
            else:
                current_effort = -1
        effort += current_effort # type: ignore
    return effort / number_of_recommendations


def cost(consumption: List, production: List, grid_injection_tariff_kWh: float = .1, grid_drawn_tariff_kwh: float = .2):
    """energy cost of the provided sequences of consumptions and production values.

    :param consumption: power consumption values
    :type consumption: list[float]
    :param production: power production values
    :type production: list[float]
    :param grid_injection_tariff_kWh: tariff for 1kWh of electricity sold to the grid
    :type grid_injection_tariff_kWh: float
    :param grid_drawn_tariff_kWh: tariff for 1kWh of electricity bought on the grid
    :type grid_injection_tariff_kWh: float
    :return: a cost in euros
    :rtype: float
    """
    _cost = 0
    for i in range(len(consumption)):
        if consumption[i] > production[i]:
            _cost += (consumption[i] - production[i]) / 1000 * grid_drawn_tariff_kwh
        else:
            _cost += (consumption[i] - production[i]) / 1000 * grid_injection_tariff_kWh
    return _cost

def money_for_grid_energy(consumption_community: List, com_dependancy: float, tariff : float = 0.2 ) -> float:
    return sum(consumption_community) * com_dependancy * tariff
    


def NEEG_per_day(consumption: List, production: List):
    """net energy exchanged with the grid per day in kWh

    :param consumption: power consumption values
    :type consumption: List[float]
    :param production: power production values
    :type production: List[float]
    :return: a quantity of energy per day in kWh
    :rtype: float
    """
    return sum([abs(consumption[i] - production[i]) for i in range(len(consumption))]) * 24 / len(consumption) / 1000


def average_NEEG_per_day(consumption: List, production: List, number_of_days):
    """net energy exchanged with the grid per day in kWh

    :param consumption: power consumption values
    :type consumption: List[float]
    :param production: power production values
    :type production: List[float]
    :return: a quantity of energy per day in kWh
    :rtype: float
    """
    return sum([abs(consumption[i] - production[i]) for i in range(len(consumption))]) / number_of_days / 1000


def savings_per_day(est_com_consumption: List[float], sim_com_consumption: List[float], est_com_production: List[float], days_in_sim: int, cm_count: int) -> float:
    return (cost(est_com_consumption, est_com_production) - cost(sim_com_consumption, est_com_production)) / days_in_sim / cm_count

def Normalised_NEEG(est_consumption: List[float], sim_consumption: List[float], est_production: List[float]) -> float:
    return NEEG(sim_consumption, est_production)/ NEEG(est_consumption, est_production)

def NEEG(consumption: List, production: List):
    """net energy exchanged with the grid per day in kWh

    :param consumption: power consumption values
    :type consumption: List[float]
    :param production: power production values
    :type production: List[float]
    :return: a quantity of energy per day in kWh
    :rtype: float
    """
    return sum([abs(consumption[i] - production[i]) for i in range(len(consumption))])


def MAPE(Y_actual, Y_Predicted):
    mape = np.mean(np.abs((Y_actual - Y_Predicted) / Y_actual)) * 100
    return mape


def evaluate_estimation_performances(original_consumption: List[float], estimated_consumption: List[float]):
    """A function evaluate estimation performances for a power profile.

            :param original_consumption: the real consumption data
            :param estimated_consumption: consumption estimation
            :type original_consumption: List
            :type estimated_consumption: List

            :returns: MAE, RMSE and R2 score, rounded to 2 decimals
            :rtype: float, float, float"""
    mean_absolute_err_val = round(mean_absolute_error(original_consumption, estimated_consumption), 2) # type: ignore
    # mean_absolute_per_err_val = round(mean_absolute_percentage_error(original_consumption, estimated_consumption), 2)
    mean_squared_error_val = round(mean_squared_error(original_consumption, estimated_consumption), 2) # type: ignore
    root_mean_squared_error_val = m.sqrt(mean_squared_error_val)
    # print(mean_absolute_per_err_val)
    r2score = round(r2_score(original_consumption, estimated_consumption), 2) # type: ignore
    return mean_absolute_err_val, root_mean_squared_error_val, r2score
