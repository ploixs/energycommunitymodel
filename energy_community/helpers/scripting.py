

import os
import random
from typing import Dict, List, Union
from energy_community.core.constants import MType

from energy_community.residential.core.residential_parameters import ResidentialParameters
from energy_community.residential.core.community import ResidentialCommunity
from energy_community.subway.SubwayParameters import SubwayParameters
from energy_community.subway.core import SubwayCommunity


def prepare_results_folder_mc(simulation_ids: List[int], paths: Dict[str,str]) -> Dict[int, str]:
    """Get the folder path to store results in based on the simulation_id

    Args:
        simulation_id (int): the simulation id of the scenario, according to scenarios.csv
        community_folder_name (str): the core folder where the community has been defined
        results_folder_name (str): the name of the results folder

    Returns:
        str: the path of the folder
    """
    
    folder_paths = {}
    for simulation_id in simulation_ids:
        # Define the results folder path based on the simulation_id
        results_folder_path = os.path.join(paths["root_folder_name"], paths['community_folder_name'], paths['results_folder_name'], f"Scenario_{simulation_id}")
        
        # Check if the results folder path exists; if not, create the directory
        if not os.path.exists(results_folder_path):
            os.makedirs(results_folder_path)

        folder_paths[simulation_id] = results_folder_path
        
    # Return the results folder path
    return folder_paths


def prepare_results_folder(simulation_id: int, paths: Dict[str,str]) -> str:
    """Get the folder path to store results in based on the simulation_id

    Args:
        simulation_id (int): the simulation id of the scenario, according to scenarios.csv
        community_folder_name (str): the core folder where the community has been defined
        results_folder_name (str): the name of the results folder

    Returns:
        str: the path of the folder
    """
    
    # Define the results folder path based on the simulation_id
    results_folder_path = os.path.join(paths['root_folder_name'], paths['community_folder_name'], paths['results_folder_name'], f"Scenario_{simulation_id}")
    
    # Check if the results folder path exists; if not, create the directory
    if not os.path.exists(results_folder_path):
        os.makedirs(results_folder_path)

    # Return the results folder path
    return results_folder_path


def get_simulation_parameters(simulation_id: int, paths: Dict[str, str], community_type: str, print_flag: bool) -> Union[ResidentialParameters, SubwayParameters]:
    """Get the corresponding simulation parameters for the simulation id passed as an argument.

    Args:
        simulation_id (int): the simulation id of the desired scenario
        print_flag (bool): choose to prin the parameters or not


    Returns:
        ResidentialParam: the simulation parameters
    """
    # Instantiate the apropriate param class with the appropriate scenario file, simulation_id, and print_flag
    
    if community_type == "subway":
        sim_params = SubwayParameters(paths['root_folder_name'], paths['community_folder_name'], paths['data_folder_name'], paths['scenarios_file_name'], simulation_id, print_flag)
    else:
        sim_params = ResidentialParameters(paths['root_folder_name'], paths['community_folder_name'], paths['data_folder_name'], paths['scenarios_file_name'], simulation_id, print_flag)
    
    # Check if the sim_params is set; if not, raise a ValueError
    if sim_params is None:
        raise ValueError("Sim params not set from scenario file!")

    # Return the instantiated sim_params object
    return sim_params

def run_simulation(model: Union[ResidentialCommunity, SubwayCommunity]):
    """Runs a simulation for the ResidentialCommunity model passed as a parameter.

    Args:
        model (ResidentialCommunity): the residential community model
    """
    
    for i in range(model.sim_params.simulation_steps):
        model.step() # run the simulation
        
        
def get_member_type_based_on_attentive_following_prob(probability: float) -> MType:

    if probability > 1 or probability < 0:
        raise ValueError("Not given a probability!")
    
    if probability > 0.55:
        if probability == 1:
            cm_true_type = MType.IDEAL
        else:
            cm_true_type = MType.GOOD
    elif probability < 0.45:
        cm_true_type = MType.BAD
    else:
        cm_true_type = MType.NORMAL
        
    return cm_true_type

def get_member_motivation_based_on_type(cm_true_type: MType) -> str:
    desc = ""
    if cm_true_type == MType.GOOD:
        desc = "Enthusiastic"
    elif cm_true_type == MType.NORMAL:
        desc = "Normal"
    elif cm_true_type == MType.IDEAL:
        desc = "Ideal"
    else:
        desc = "Reluctant"
    return desc