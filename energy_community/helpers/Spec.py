


from datetime import datetime, timedelta
from typing import Dict, List, Tuple, Union
import pandas as pd

from energy_community.core.constants import DSType, DType

class Spec:
    """Specification for an analysis.

    Raises:
        ValueError: if plotting intervals are not set, raise an error
        ValueError: if plotting intervals are not set, raise an error
    """
    
    month_mask = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    def __init__(self,
                 print_member_perf: bool,
                 plot_intervals: List[Tuple[str, str]],
                 plot_beh_agent_1_flag: bool = True,
                 get_individual_indicators_flag: bool = True,
                 profile_legend: Union[Dict[Tuple[DSType, DType],str], None] = None):
        """Initialize the specification.

        Args:
            print_member_perf (bool): print members performances in the terminal
            plot_intervals (List[Tuple[str, str]]): 2 time intervals for plotting the comparison (for a clear figure, should be at most 5 days per interval)
            plot_beh_agent_1_flag (bool, optional): Plot the bhaviour of the first agent. Defaults to True.
            get_individual_indicators_flag (bool, optional): Calculate individual indicators (only for residential community). Defaults to True.
        """
        self.print_member_performances = print_member_perf
        self.plot_intervals = plot_intervals
        self.plot_real_behaviour_of_first_agent_flag = plot_beh_agent_1_flag
        self.calculate_individual_indicators = get_individual_indicators_flag
        
        self.plt_dt : Dict[int, Tuple[datetime, datetime]] = {}
        self.plt_dt_str : Dict[int, Tuple[str, str]] = {}
        self.time_delta: Dict[int, timedelta] = {}
        self.axis_fs : int = 4
        self.title_fs : int = 18
        self.legend_fs : int = 12
        
        if profile_legend:
            self.profile_legend = profile_legend
        else:
            self.profile_legend = { (DSType.ESTIMATED, DType.CONSUMPTION):  '$p^{estimated}_{Load}$',
                                    (DSType.SIMULATED, DType.CONSUMPTION):  '$p^{real}_{Load}$',
                                    (DSType.ESTIMATED, DType.PRODUCTION):  '$p^{expected}_{PV}$'}
       
        
        self.set_plot_properties()
        self.set_plot_interval_datetime_limits()
        self.set_time_delta_between_plot_interval_datetime_limits()
        self.set_plot_interval_datetime_limits_as_strings_for_decorators()
    
    def set_time_delta_between_plot_interval_datetime_limits(self):
        if len(self.plt_dt) == 0:
            raise ValueError("plt_dt not declared yet!")
        for index in range(len(self.plot_intervals)):
            self.time_delta[index] = self.plt_dt[index][1] - self.plt_dt[index][0] 
    
    def set_plot_interval_datetime_limits(self): 
        for index in range(len(self.plot_intervals)):
            inf_dt = pd.to_datetime(self.plot_intervals[index][0], format="%d-%m-%Y")
            sup_dt = pd.to_datetime(self.plot_intervals[index][1], format="%d-%m-%Y")
            self.plt_dt[index] = (inf_dt, sup_dt)
    
    def set_plot_interval_datetime_limits_as_strings_for_decorators(self):
        if len(self.plt_dt) == 0:
            raise ValueError("plt_dt not declared yet!")
        for index in range(len(self.plot_intervals)):
            inf_dt = f'{self.plt_dt[index][0].day} {self.month_mask[self.plt_dt[index][0].month - 1]}'
            sup_dt = f'{self.plt_dt[index][1].day} {self.month_mask[self.plt_dt[index][1].month - 1]}'
            self.plt_dt_str[index] = (inf_dt, sup_dt)
            
    def set_plot_properties(self):
        self.axis_fs = 12
        self.title_fs = 18
        self.legend_fs = 12
        if len(self.plot_intervals) == 1:
            self.axis_fs = 6
            self.title_fs = 14
            self.legend_fs = 12
        

