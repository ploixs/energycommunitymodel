from enum import Enum

class MemberActionIntensity(Enum):
    REALISTIC = 1
    
cm_intensity_map = {MemberActionIntensity.REALISTIC: 'realistic'} # compatible with mybinder (instead od str enum)

class Willigness(Enum):
    FIXED = 1

cm_willigness_map = {Willigness.FIXED: 'fixed'}

class Availability(Enum):
    DAY = 1

cm_availability_map = {Availability.DAY: 'day'}

class Stages(Enum):
    ONE_TURN = 1
    TWO_TURNS = 2

STAGES_MAP_RESIDENTIAL = {Stages.ONE_TURN: 'one turn', Stages.TWO_TURNS: 'two turns'}

class TargetOfRec(Enum):
    EVERYONE = 1
    
target_rec_map = {TargetOfRec: 'everyone'}
    
class ExpectancyDev(Enum):
    BASIC = 1
    ADAPTIVE = 2
    
expectancy_map = {ExpectancyDev.BASIC: '', ExpectancyDev.ADAPTIVE: 'adaptive'}

class Strategy(Enum):
    INFORMATIVE = 1
    DUMMY = 2
    MULTI_OBJECTIVE = 3
    COACHING = 4
    DYNAMIC = 5
    OPTIMAL_INFORMATIVE = 6
    OPTIMAL_COACHING = 7
    
STRATEGY_MAP = {Strategy.INFORMATIVE: 'informative', Strategy.DUMMY: 'dummy', Strategy.MULTI_OBJECTIVE: 'multi-objective'}
STRATEGY_MAP_RESIDENTIAL = {Strategy.COACHING: 'coaching',
                            Strategy.DUMMY: 'dummy', 
                            Strategy.INFORMATIVE: 'informative', 
                            Strategy.DYNAMIC: 'dynamic', 
                            Strategy.OPTIMAL_COACHING: 'optimal coaching',
                            Strategy.OPTIMAL_INFORMATIVE: 'optimal informative'}
                            #Strategy.ADAPTIVE_INFORMATIVE: 'adaptive informative',
                            #Strategy.ADAPTIVE_OPTIMAL_COACHING: 'adaptive optimal coaching'}


class PrivacyLevel(Enum):
    PRIVATE = 1
    NOT_PRIVATE = 1
    
privacy_map = {PrivacyLevel.PRIVATE: 'private', PrivacyLevel.NOT_PRIVATE: 'not private'}

class Rec(Enum):
    BLINKING_GREEN = 2
    STRONG_GREEN = 1
    GREEN = 0.5
    WHITE = 0
    RED = -0.5
    STRONG_RED = -1
    BLINKING_RED = -2

class DType(Enum):
    ACTIONS = 1
    RECOMMENDATIONS = 2
    CONSUMPTION = 3
    PRODUCTION = 4
    STATE = 5

    
data_type_map = {DType.ACTIONS: 'actions', DType.RECOMMENDATIONS: 'recommendations', DType.CONSUMPTION: 'consumption', DType.PRODUCTION: 'production'}

class PType(Enum):
    COMMUTING = 1
    FLEXIBILITY = 2
    MORNING = 3
    MIDDAY1 = 4
    MIDDAY2 = 5
    EVENING = 6
    PEAKPROD = 7

class DSType(Enum):
    SIMULATED = 1
    ESTIMATED = 2
    REQUESTED = 3
    
DS_TYPE_MAP = {DSType.SIMULATED: 'simulated', DSType.ESTIMATED: 'estimated', DSType.REQUESTED: 'requested'}
    
class Interval(Enum):
    DAY = 1
    SIMULATION = 2
    
interval_map = {Interval.DAY: 'day', Interval.SIMULATION: 'simulation'}

class AsignType(Enum):
    MEMBER = 1
    COMMUNITY = 2
    CATEGORY = 3
    MANAGER = 4
    
ASSIGN_TYPE_MAP = {AsignType.MEMBER: 'member', AsignType.COMMUNITY: 'community', AsignType.CATEGORY: 'category', AsignType.MANAGER: 'manager'}
    
class MType(Enum):
    IDEAL = 1
    GOOD = 2
    NORMAL = 3
    BAD = 4
    
MTYPE_MAP = {MType.IDEAL: "ideal",
          MType.GOOD: "enthusiastic", 
          MType.NORMAL: "normal",
          MType.BAD: "reluctant"} # mapping for plotting purposes
    

class FlexibilityCapacity(Enum):
    FULLY_FLEXIBLE = 1
    SEMY_FLEXIBLE = 2
    NO_FLEXBILITY = 3
    
FLEX_MAP = {FlexibilityCapacity.FULLY_FLEXIBLE: "fully-flexible", 
            FlexibilityCapacity.SEMY_FLEXIBLE: "semi-flexible", 
            FlexibilityCapacity.NO_FLEXBILITY: "no-flexibility"} # mapping for plotting purposes
    
class IndicatorsLP(Enum):
    SC = 1
    SS = 2
    AUTONOMY = 3
    DEPENDENCY = 4
    COST = 5
    NEEG = 6
    NEEG_PER_DAY = 7
    DAILY_SAVINGS = 8
    
indicators_lp_map = {IndicatorsLP.SC: 'self consumption',
                     IndicatorsLP.SS: 'self sufficiency',
                     IndicatorsLP.AUTONOMY: 'autonomy',
                     IndicatorsLP.DEPENDENCY: 'grid dependency',
                     IndicatorsLP.COST: 'grid energy cost',
                     IndicatorsLP.NEEG: 'NEEG',
                     IndicatorsLP.NEEG_PER_DAY: 'NEEG per day',
                     IndicatorsLP.DAILY_SAVINGS: 'daily savings'}
    
class IndicatorsAll(Enum):
    NORMALISED_NEEG = 1
    AVERAGE_DISSATISFACTION = 2
    
class IndicatorsLoad(Enum):
    RENUNCIATION = 1
    EFFORT = 2
    MONEY_FOR_GRID_ENERGY = 3

class IndicatorsInd(Enum):
    DISSATISFACTION_PER_CM = 1
    INVOLVEMENT_BASED_ON_CONTRIBUTION = 2
    AVERAGE_ESTIMATED_LOAD_DAY = 3
    FAV_COMMUTING_HOUR = 4
    FAV_DURATION = 5

class IndicatorsRec(Enum):
    REC_INFLUENCE = 1
    RECOMMENDATION_DEVIATION_RATE = 2
    CONTRIBUTION_CHANGE_RATE = 3
    AVG_CONTRIBUTIONS_PER_DAY = 4
    
    
class DayPeriods(Enum):
    MORNING = 1
    AFTERNOON = 2
    
class PossibleStates(Enum):
    ATTENTIVE = 1
    INNATENTIVE = 2
    
POS_STATES_MAP = {PossibleStates.ATTENTIVE: "At", PossibleStates.INNATENTIVE: "NAt"}
    
class PossibleActions(Enum):
    FOLLOW = 1
    NOT_FOLLOW = 2
    
POS_ACTIONS_MAP = {PossibleActions.FOLLOW: "F", PossibleActions.NOT_FOLLOW: "NF"}
PLT_STATE_MAP = {str(state): i for i,state in enumerate(PossibleStates)}