
from datetime import datetime
from typing import List
from energy_community.core.constants import PType


class Period:
    """Class definition for a simulation time period """

    def __init__(self, type : PType, hour_min: int, hour_max: int):
        self.type = type
        self.hour_min = hour_min
        self.hour_max = hour_max
        self.length = 0

    def check_if_in_period(self, candidate_hour: int) -> bool:
        if self.hour_min <= candidate_hour <= self.hour_max:
            return True
        else:
            return False
    
    def get_hours(self) -> List[int]:
        hour_list = list(range(self.hour_min, self.hour_max + 1))
        return hour_list

