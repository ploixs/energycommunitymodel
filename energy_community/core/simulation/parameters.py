from datetime import datetime, timedelta
import os
from typing import Dict
import pandas as pd
from buildingenergy.timemg import stringdate_to_datetime
import pytz
from tzlocal import get_localzone
import yaml


class BaseParameters:
    """Parameter configuration of a simulation scenario"""

    def __init__(self, root_folder_name: str, 
                 community_folder_name: str, 
                 data_folder_name: str, 
                 configuration_file_name: str,
                 simulation_id: int, 
                 print_flag : bool = False):
        """Initialize function for Params

        Args:
            sim_id (int): simulation id from scenarios.csv file
            print_flag (bool, optional): option to print messages during the simulation. Defaults to False.
            cfg_file_path (str, optional): configuration file path. Defaults to "".
        """
        
        # Convert the simulation ID to a string and store it
        self.simulation_id = str(simulation_id)
        
        # Assign the name of the folder where the input data is stored 
        self.data_folder_name = data_folder_name
        
        self.root_folder_name = root_folder_name
        
        self.community_folder_name = community_folder_name
        
        # Store the print flag
        self.print_member_info = print_flag
        
        # Load the configuration file and retrieve the scenario configuration
        self.scenario_config = self.load_scenario_config(os.path.join(root_folder_name, community_folder_name, configuration_file_name), simulation_id)

        
        # Retrieve and store the scenario description and estimation system flag
        self.description = self.scenario_config["description"]
        
        # Initialize the solar parameters
        self.solar_parameters = self.scenario_config["solar"]
        self.init_solar_parameters()
        
        # Initialize the simulation parameters and stages
        simulation_parameters = self.scenario_config["simulation"]
        self.init_simulation_parameters(simulation_parameters)
        
        """
        self.simulation_id = str(sim_id)
        self.cfg_file_path = cfg_file_path
        self.simulation_steps : int = 0 # the number of simulation steps, each simulation step is one hour
        
        self.df = pd.read_csv(cfg_file_path)
        n = self.df.loc[self.df.simulation_parameter == 'pv_plant_number_of_panels'][self.simulation_id].item()
        self.pv_plant_number_of_modules = int(n) # the number of PV panels should be an integer
        
        p_m = self.df.loc[self.df.simulation_parameter == 'pv_plant_module_power'][self.simulation_id].item()
        self.pv_plant_module_power = int(p_m) # the power of a module should be an integer
        
        self.print_member_info = print_flag
        
        self.solar_location = self.df.loc[self.df.simulation_parameter == 'solar_data_location'][self.simulation_id].item()
        
        self.weather_file_path = ""
        
        start_date = str(self.df.loc[self.df.simulation_parameter == 'simulation_starting_date'][self.simulation_id].item())
        
        if self.solar_location == "Grenoble":
            local_timezone = pytz.timezone("Europe/Paris") # time zone of the data
        else:
            local_timezone = pytz.timezone("Europe/Bucharest")
            
        date_format = "%d/%m/%Y"
        start_date_obj = datetime.strptime(start_date, date_format)
       
        
        self.starting_date = pd.Timestamp(start_date_obj,tz=local_timezone) # start date should be in local timezone as the whole simualtion is in localtime

        end_date = str(self.df.loc[self.df.simulation_parameter == 'simulation_ending_date'][self.simulation_id].item())
        
        end_date_obj = datetime.strptime(end_date, date_format)
        #self.ending_date = stringdate_to_datetime(end_date, date_format='%d/%m/%Y') # doesnt work on windows
        self.ending_date = pd.Timestamp(end_date_obj,tz=local_timezone) # start date should be in local timezone as the whole simualtion is in localtime

        self.set_simulation_steps() 
        """
        
        
    def init_solar_parameters(self):
        """Initializes the solar parameters from the given dictionary.
        """

        # Set the location of the solar data
        self.solar_location = self.solar_parameters["solar_data_location"]

        # Set the local timezone based on the solar location
        self.local_timezone = self.get_timezone_based_on_solar_location()

        # Set the weather file path based on the scenario YAML configuration file
        self.weather_file_path = os.path.join(self.root_folder_name, self.community_folder_name, self.data_folder_name, self.solar_parameters["weather_file_name"])
    
    def get_timezone_based_on_solar_location(self):
        """Returns the local timezone based on the solar data location.

        Returns:
            pytz.timezone: Local timezone for the solar data location.
        """

        # Check the solar location and set the local timezone accordingly
        if self.solar_location == "Grenoble":
            local_timezone = pytz.timezone("Europe/Paris") # Time zone of the data
        else:
            local_timezone = pytz.timezone("Europe/Bucharest")

        return local_timezone
        
    def init_simulation_parameters(self, simulation_parameters: dict):
        """Initializes the simulation parameters from the given dictionary.

        Args:
            simulation_parameters (dict): Dictionary containing the simulation parameters.
        """

        # Convert the starting date string to a datetime object with timezone information
        self.starting_date = self.get_date_as_datetime_from_str(simulation_parameters["starting_date"])

        # Convert the ending date string to a datetime object with timezone information
        self.ending_date = self.get_date_as_datetime_from_str(simulation_parameters["ending_date"])

        # Calculate and set the number of simulation steps based on the starting and ending dates
        self.set_simulation_steps()
        
    def get_date_as_datetime_from_str(self, date_str: str, date_format: str = "%d/%m/%Y") -> pd.Timestamp:
        """Converts a date string to a datetime object with timezone information.

        Args:
            date_str (str): Date string to be converted.
            date_format (str, optional): Format of the date string. Defaults to "%d/%m/%Y".

        Returns:
            pd.Timestamp: Datetime object with timezone information.
        """

        # Convert the date string to a datetime object
        datetime_obj = datetime.strptime(date_str, date_format)

        # Create a pandas Timestamp with timezone information based on the local_timezone attribute
        date_with_timezone = pd.Timestamp(datetime_obj, tz=self.local_timezone) 

        return date_with_timezone
    
    
        
    def set_simulation_steps(self):
        """Calculates the number of simulation steps based on the starting and ending dates."""

        # Calculate the difference between the ending and starting dates
        diff = self.ending_date - self.starting_date

        # Convert the difference to hours and add the number of days multiplied by 24
        self.simulation_steps = diff.days * 24 + diff.seconds // 3600
        
    def load_scenario_config(self, config_file_path: str, sim_id: int) -> Dict:
        """Loads the scenario configuration from the YAML file.

        Args:
            config_file_path (str): Path to the configuration file.
            sim_id (int): Simulation scenario ID.

        Returns:
            dict: Configuration dictionary for the specified scenario.

        Raises:
            ValueError: If the specified scenario ID is not found in the configuration file.
        """
        # Open the configuration file and load its content into a dictionary
        with open(config_file_path, "r") as file:
            config_data = yaml.safe_load(file)

        # Create the scenario key based on the simulation ID
        scenario_key = f"scenario_{sim_id}"

        # Check if the scenario key is present in the configuration data
        if scenario_key not in config_data:
            raise ValueError("Sim id could not be identified in config file!")

        # Return the configuration dictionary for the specified scenario
        return config_data[scenario_key]
    

        