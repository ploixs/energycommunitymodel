

from energy_community.core.constants import AsignType, DSType, DType, Interval
from abc import ABC, abstractmethod, abstractproperty


class Data(ABC):
    """Abstract base class for data models"""
    
    def __init__(self, 
                 data_source_type: DSType, # estimated/simulated/requested
                 data_type: DType, # energy/recommendations/actions
                 asign_type: AsignType,  # member/category/community
                 length: Interval) -> None: # current day/simulation
        """
        Initializes the `Data` object with the specified data source type, data type, assignee type, and length.
        
        Args:
            data_source_type (DSType): The type of data source.
            data_type (DType): The type of data.
            asign_type (AsignType): The type of assignee.
            length (Interval): The length of time for the data.
        """
        self.data_type = data_type
        self.data_source_type = data_source_type
        self.asignee_type = asign_type
        self.interval = length
        self._data = {}

    @abstractproperty
    def data(self):
        """Abstract property representing the data stored by the `Data` object.
        
        Returns:
            Any: The stored data.
        """
        pass
   
    @abstractmethod
    def update_data_by_slicing(self):
        """Abstract method for updating the `Data` object by slicing the stored data according to a specified time interval."""
        pass
    
    @abstractmethod
    def update_data_with_other_models(self):
        """Abstract method for updating the `Data` object with data from other models."""
        
        pass