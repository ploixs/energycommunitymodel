

from datetime import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Union
from energy_community.core.data.base import Data
from energy_community.core.constants import AsignType, DSType, DType, Interval, Rec
import pandas as pd


if TYPE_CHECKING:
    from energy_community.core.entities.community import EnergyCommunityModel

class MemberDataTotal(Data):
    """Member data for the whole simulation"""
    
    def __init__(self,data_source_type: DSType, # the data source type: estimated/simulated/requested
                 data_type: DType, # energy/recommendations
                 asign_type: AsignType = AsignType.MEMBER, # member only
                 time_frame: Union[List[Union[pd.Timestamp, datetime]],None] = None,  # time frame for the corresponding data
                 member_data_total: Union[List[float], None] = None): # actual data
        """
        Initializes the `MemberDataTotal` object with the specified data source type, data type, assignee type, and data.

        Args:
            data_source_type (DSType): The type of data source.
            data_type (DType): The type of data.
            asign_type (AsignType, optional): The type of assignee. Defaults to AsignType.MEMBER.
            time_frame (Union[List[Union[datetime]], None], optional): The time frame for the data. Defaults to None.
            member_data_total (Union[List[Union[float, int]], None], optional): The actual data. Defaults to None.
        """
        super().__init__(data_source_type=data_source_type, data_type=data_type, asign_type=asign_type, length=Interval.SIMULATION)
        
        if time_frame and member_data_total: # if data is given as arguments...
            self._data = {time_frame[i]: member_data_total[i] for i in range(len(member_data_total))} # ...already assign it
        
    @property
    def data(self):
        """
        Returns the `_data` attribute of the `MemberDataTotal` object.

        Returns:
            Dict[datetime, Union[float, int]]: The stored data.
        """
        return self._data
    
    @data.setter
    def data(self, member_data: Dict[datetime, Any]):
        """
        Sets the `_data` attribute of the `MemberDataTotal` object to the specified member data.

        Args:
            member_data (Dict[datetime, Any]): The member data to set.
        """
        self._data = member_data
        if len(self._data) == 0:
            raise ValueError("Data not properly assigned!")

    def update_data_by_slicing(self):
        """
        Updates the `MemberDataTotal` object by slicing the stored data according to the current day.
        """
        pass
    
    def update_data_with_other_models(self, data_day_model: 'MemberDataDay'):
        """
        Updates the `MemberDataTotal` object with data from a `MemberDataDay` object.

        Args:
            data_day_model (MemberDataDay): The `MemberDataDay` object to update the data with.
        """
        if not data_day_model.data: # if data is not assigned before
            raise ValueError(f"Estimated member {data_day_model.data_type} model day data not set") # raise an error

        self._data.update(data_day_model.data)

    
class MemberDataDay(Data):
    """Member data for the current day"""
     
    def __init__(self,
                 data_source_type: DSType,  # the data source type: estimated/simulated/requested
                 data_type: DType,  # energy/recommendations
                 asign_type: AsignType = AsignType.MEMBER):
        super().__init__(data_source_type=data_source_type, data_type=data_type, asign_type=asign_type, length=Interval.DAY)
    
    @property
    def data(self):
        return self._data
    
    @data.setter
    def data(self, community_data: Dict[datetime, Any]):
        self._data.clear()
        self._data.update(community_data)
    
    def update_data_with_other_models(self):
        pass
   
    def update_data_by_slicing(self, community_model: 'EnergyCommunityModel', member_total_data_model: MemberDataTotal):
        if member_total_data_model.data is None: # if the model data is not set before
            raise ValueError(f"Cannot extract {member_total_data_model.data_source_type} community {member_total_data_model.data_type} for the day! Data not set!") # raise an error
        
        start_id, end_id = community_model.get_start_id_end_id_current_day() # get the start id and end_id of the current day
        start_time = community_model.tf_total[start_id] # based on the tf_total mapping, get the start and end time of current day
        end_time = community_model.tf_total[end_id]
        self._data = {key:value for (key,value) in member_total_data_model.data.items() if start_time <= key <= end_time} # filter the total data for the current day only
        
    def append_to_data(self, current_time: Union[pd.Timestamp, datetime], data_value: Union[float, Rec]):
        self.data[current_time] = data_value