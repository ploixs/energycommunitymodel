from collections import Counter
from datetime import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Union
from energy_community.core.data.base import Data
from energy_community.core.constants import AsignType, DSType, DType, Interval, Rec
from abc import ABC, abstractmethod, abstractproperty
import pandas as pd

if TYPE_CHECKING:
    from energy_community.core.entities.community import EnergyCommunityModel
    from energy_community.core.data.member_data import MemberDataDay


class CollectiveDataTotal(Data):
    """Collective data (category/community) for the whole simulation"""
    
    def __init__(self, 
                 data_source_type: DSType, # estimated/simulated/requested
                 data_type: DType, # energy/recommendations/actions
                 asign_type: AsignType, # category/community
                 time_frame: Union[List[Union[pd.Timestamp, datetime]], None] = None, # data time series
                 community_data: Union[List[float], None] = None): # actual data
        """Initializes the `CollectiveDataTotal` object with the specified data source type, data type, assignee type,
        time frame, and community data.
        
        Args:
            data_source_type (DSType): The type of data source.
            data_type (DType): The type of data.
            asign_type (AsignType): The type of assignee.
            time_frame (List[Timestamp] or None): The time series for the data. Defaults to None.
            community_data (List[float] or None): The actual data. Defaults to None.
        """
        super().__init__(data_source_type=data_source_type, data_type=data_type, asign_type=asign_type, length=Interval.SIMULATION)
        
        if time_frame and community_data: # if the data is initialized ...
            self._data = {time_frame[i]: community_data[i] for i in range(len(community_data))} # ... already assign it
    
    @property
    def data(self):
        """Returns the `_data` attribute of the `CollectiveDataTotal` object.
        
        Returns:
            Dict[datetime, Any]: The stored data.
        """
        return self._data
    
    @data.setter
    def data(self, community_data: Dict[datetime, Any]):
        """Sets the `_data` attribute of the `CollectiveDataTotal` object to the specified community data.
        
        Args:
            community_data (Dict[datetime, Any]): The community data to set.
        """
        self._data = community_data
        if len(self._data) == 0:
            raise ValueError("Data not properly assigned!")
    
    def update_data_by_slicing(self):
        """Updates the `CollectiveDataTotal` object by slicing the stored data according to a specified time interval."""
        
        pass
    
    def update_data_with_other_models(self, data_day_model: 'CollectiveDataDay'):
        """Updates the `CollectiveDataTotal` object with data from another `CollectiveDataDay` object.
        
        Args:
            data_day_model (CollectiveDataDay): The `CollectiveDataDay` object to update the data with.
        """
        if data_day_model.data is None: # if the given data model has not been assigned
            raise ValueError(f"{data_day_model.data_source_type} member {data_day_model.data_type} model day data not set") # raise an error
        
        if data_day_model.data_type != self.data_type: # if the type of data (recs/energy) differes
             raise ValueError(f"{data_day_model.data_type} for passed energy model not matching with {self.data_type}") # raise an error
        
        if data_day_model.data_source_type != self.data_source_type: # if the data source type differs
             raise ValueError(f"{data_day_model.data_source_type} for passed energy model not matching with {self.data_source_type}") # raise an error
        
        self._data.update(data_day_model.data) # type: ignore


class CollectiveDataDay(Data):
    """Collective data (category/community) for the current day"""
    
    def __init__(self, 
                 data_source_type: DSType,  # estimated/simulated/requested
                 data_type: DType, # energy/recommendations/actions
                 asign_type : AsignType): # category/community
        """
        Initializes the `CollectiveDataDay` object with the specified data source type, data type, and assignee type.
        
        Args:
            data_source_type (DSType): The type of data source.
            data_type (DType): The type of data.
            asign_type (AsignType): The type of assignee.
        """
        super().__init__(data_source_type=data_source_type, data_type=data_type, asign_type=asign_type, length=Interval.DAY)
    
    @property
    def data(self):
        """
        Returns the `_data` attribute of the `CollectiveDataDay` object.
        
        Returns:
            Dict[datetime, Union[float, int, Rec]]: The stored data.
        """
        return self._data
    
    @data.setter
    def data(self, community_data: Dict[datetime, Union[float, int, Rec]]):
        """
        Sets the `_data` attribute of the `CollectiveDataDay` object to the specified community data.
        
        Args:
            community_data (Dict[datetime, Union[float, int, Rec]]): The community data to set.
        """
        self._data = community_data
        if len(self._data) == 0:
            raise ValueError("Data not properly assigned!")
    
    def update_data_by_slicing(self, community_model: 'EnergyCommunityModel', total_data_model: CollectiveDataTotal):
        """
        Updates the `CollectiveDataDay` object by slicing the stored data according to the current day.
        
        Args:
            community_model (EnergyCommunityModel): The `EnergyCommunityModel` object to get the current day start and end IDs from.
            total_data_model (CollectiveDataTotal): The `CollectiveDataTotal` object to extract data for the current day from.
        """
        
        if total_data_model.data is None: # if data is not assigned to the model before
            raise ValueError(f"Cannot extract {total_data_model.data_source_type} community {total_data_model.data_type} for the day! Data not set!") # raise an error
        
        start_id, end_id = community_model.get_start_id_end_id_current_day() # get the start id and end_id of the current day
        start_time = community_model.tf_total[start_id] # based on the tf_total mapping, get the start and end time of current day
        end_time = community_model.tf_total[end_id]
        self._data = {key: total_data_model.data[key] for key in total_data_model.data if start_time <= key  and key <= end_time} # filter the total data for the current day only
    
    def update_data_with_other_models(self, data_day_models: List['MemberDataDay']):
        """
        Updates the `CollectiveDataDay` object with data from other `MemberDataDay` objects.
        
        Args:
            data_day_models (List[MemberDataDay]): The list of `MemberDataDay` objects to update the data with.
        """
        
        empty_data_flag = False
        for model in data_day_models:
            if model.data is None: # if data for one of the models is not assigned
                empty_data_flag = True # ...keep track of it
        if empty_data_flag:
            raise ValueError("One member energy data for the day is not set!")  # and raise an error 
        c = Counter() # we use counter so we can make the sum each time
        for model in data_day_models: # for each model...
            c.update(model.data) # update the sum
        self._data = dict(c)