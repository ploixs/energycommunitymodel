from typing import List, Union
import numpy
import pandas as pd
from energy_community.helpers.time_management_functions import generate_local_datetime_range_and_indexes, datetime_shift_year, stringdate_shift_year
from buildingenergy.timemg import datetime_to_stringdate, stringdate_to_epochtimems, epochtimems_to_datetime

class Appliance:
    """An appliance related to a house."""

    def __init__(self, database_connection, id: int, house_id: int, name: str, start_datetime, end_datetime):
        """Initialize the appliance.

        :param database_connection: sqlite3 database connection
        :type database_connection: SQLalchemy
        :param id: identifier of the appliance in a house
        :type id: int
        :param house_id: identifier of the house containing the appliance
        :type house_id: int
        :param name: name of the appliance
        :type name: str
        :param simulator: the simulator that is using the IRISE database
        :type from_datestring: scheduler.IRISEsimulator
        """
        self.name = name.split(" (")[0]
        self.name = self.name.replace(" ", "_")
        self.name = self.name.lower()
        self.values = list()
        local_datetimes, _ = generate_local_datetime_range_and_indexes(start_datetime, end_datetime)
        #local_datetimes, _ = generate_utc_datetime_range_and_indexes(start_datetime, end_datetime)

        self.datetimes = local_datetimes


        from_simulation_datestring = datetime_to_stringdate(start_datetime)
        to_simulation_datestring = datetime_to_stringdate(end_datetime)

        from_irise_datestring = stringdate_shift_year(from_simulation_datestring, -17)
        to_irise_datestring = stringdate_shift_year(to_simulation_datestring, -17)
        df = pd.read_sql_query("SELECT EpochTime, Value "
                               "FROM Consumption "
                               "WHERE HouseIDREF=%i AND ApplianceIDREF=%i AND EpochTime >= %i AND EpochTime < %i ORDER BY EpochTime ASC" %
                               (house_id, id, stringdate_to_epochtimems(from_irise_datestring) / 1000, stringdate_to_epochtimems(to_irise_datestring) / 1000),
                               database_connection)

        epochtime_values = list(zip(df['EpochTime'].to_list(), df['Value'].to_list()))
        epochtime_values_dict = dict()
        if epochtime_values:
            for i in range(0, len(epochtime_values), 6):
                epochtime_values_dict[datetime_shift_year(epochtimems_to_datetime(epochtime_values[i][0] * 1000), 17)] = sum(
                    [epochtime_values[i + j][1] for j in range(0, 6)])
            previous_naive_simulator_datetime = None
            for dt in local_datetimes:
                naive_simulator_datetime = dt.replace(tzinfo=None)
                if naive_simulator_datetime in epochtime_values_dict:
                    self.values.append(epochtime_values_dict[naive_simulator_datetime])
                else:
                    self.values.append(epochtime_values_dict[previous_naive_simulator_datetime])
                previous_naive_simulator_datetime = naive_simulator_datetime

            if self.name == 'site_consumption':
                self.values = self.filter_outliers(self.values, outlier_sensitivity=3)
        else:
            self.values = None
            print("Appliance has no consumption!")

    def filter_outliers(self, values: list, outlier_sensitivity: float = 3, min_value: Union[float, None] = None,
                        max_value: Union[float, None] = None) -> Union[List, None]:
        """Remove outliers. Outliers are defined as 2 opposite important derivatives in consecutive values (possibly with different time deltas) or as values out of a given range. Function has no output but replace each detected outlier but the average value of the left and right neighbors in the 'values' vector provided as input.
        :param variable_name: name of the variable used to display info when an outlier is detected
        :param values: list of values
        :param outlier_sensitivity: detection sensitivity to detection of 2 consecutive opposite important derivatives; it defines a threshold with outlier_sensitivity * standard deviation of delta values
        :param min_value: minimum data value for acceptable range
        :param max_value: maximum data value for acceptable range
        """
        delta_values = [values[k + 1] - values[k] for k in range(len(values) - 1)]
        mean_delta = numpy.mean(delta_values)
        sigma_delta = numpy.std(delta_values)
        for k in range(1, len(values) - 1):
            backward_diff = values[k] - values[k - 1]
            forward_diff = values[k + 1] - values[k]
            if (abs(backward_diff) > mean_delta + outlier_sensitivity * sigma_delta) and (
                    abs(forward_diff) > mean_delta + outlier_sensitivity * sigma_delta) and backward_diff * forward_diff < 0:
                values[k] = (values[k - 1] + values[k + 1]) / 2
            if (max_value is not None) and (values[k] > max_value):
                values[k] = values[k - 1]
            elif (min_value is not None) and (values[k] < min_value):
                values[k] = values[k - 1]
        return values


