import datetime
import os
from typing import TYPE_CHECKING
import pytz

import pandas as pd

from buildingenergy import timemg
from buildingenergy import solar
from buildingenergy.openweather import OpenWeatherMapJsonReader
from energy_community.core.entities.community import EnergyCommunityModel


    
class PVModel:

    def __init__(self, community_model: EnergyCommunityModel, pv_surface:float = 50):
        
        self.weather_file_path = community_model.sim_params.weather_file_path
        self.community_model = community_model
        self.solar_model = self.get_solar_model()
        
        self.pv_efficiency = 0.13
        self.pv_surface = pv_surface
        #self.pv_surface = 50
        #self.pv_surface = 100


    def get_estimated_production_for_community_in_Grenoble(self):
        """ Sets the estimated PV production for a community in Grenoble based on weather data."""
        
        # Load the weather data for Grenoble
        solar_model = self.get_solar_model()

        # Predict the solar irradiation at each time step for a PV panel with slope=30 and exposure=0
        phisun_predicted = solar_model.solar_irradiations(slope_in_deg=30, exposure_in_deg=0)['total']

        # Calculate the predicted supplied power based on the predicted solar irradiation
        predicted_supplied_powers = [P * self.pv_efficiency * self.pv_surface for P in phisun_predicted]
        predicted_supplied_powers.pop()  # Remove the last element since it corresponds to the next day

        # Assign the estimated PV production data to the manager for the entire simulation period
        data = {time: predicted_supplied_powers[i] for i, time in self.community_model.tf_total.items()}
    
        return data
        
    def get_solar_model(self):
        """
        Generates a solar model for a given weather file path.

        Returns:
        - solar.SolarModel: A solar model developed in the buildingenergy project.
        """
        
        # Convert the datetime object to a string
        
        starting_str_date = timemg.datetime_to_stringdate(self.community_model.sim_params.starting_date, date_format='%d/%m/%Y %H:%M:%S')
        ending_str_date = timemg.datetime_to_stringdate(self.community_model.sim_params.ending_date, date_format='%d/%m/%Y %H:%M:%S')

        # Read the weather data from the JSON file using OpenWeatherMapJsonReader
        site_weather_data = OpenWeatherMapJsonReader(self.weather_file_path, 
                                                     from_stringdate=starting_str_date, 
                                                     to_stringdate=ending_str_date, 
                                                     sea_level_in_meter=330, 
                                                     albedo=0.1).site_weather_data

        # Create a solar model using the site weather data
        solar_model = solar.SolarModel(site_weather_data=site_weather_data)

        # Return the solar model
        return solar_model
    