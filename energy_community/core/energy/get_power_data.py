from typing import Tuple
from datetime import datetime
import sys
from typing import Dict

from energy_community.core.energy.Appliance import Appliance
import sqlite3
import pandas as pd
from energy_community.helpers.time_management_functions import generate_start_and_end_dates_for_file_reading_writing
import os
from tzlocal import get_localzone 


def filter_weekend_days(df: pd.DataFrame) -> pd.DataFrame:
    """ Assigns 0's to the ICP values during the weeeknd since commuting passengers don't go to work during weekend."""

    for date in df.index:
        if date.strftime("%A") in ['Sunday', 'Saturday']:
            df.loc[df.index == date, 'icp morning [Wh]'] = 0
            df.loc[df.index == date, 'icp afternoon [Wh]'] = 0
    return df


def get_mean_icp_values_from_files(sim_starting_date: datetime, sim_ending_date: datetime) -> Dict[pd.Timestamp, Tuple[float,float]]:
        """Sets the mean_consumption_per_passenger_df according to data from files"""

        #folder_path = cfg.get_consumption_data_config('icp_folder_path')
        folder_path = os.path.join('energy_community','subway','data')
        #timezone = str(get_localzone())
        #timezone = 'utc'
        str_start_date, str_end_date = generate_start_and_end_dates_for_file_reading_writing(sim_starting_date,sim_ending_date)
        file_path = os.path.join(folder_path, f'icp_estimated_mean_values_for_data_between_{str_start_date}_and_{str_end_date}.csv')
        df = pd.read_csv(file_path)

        dates = [pd.to_datetime(x, format="%Y-%m-%d") for x in df['date']]
        dates = [x.tz_localize(tz="Europe/Bucharest") for x in dates]
        energy_footprints_morning = [x * 1000 for x in df['icp morning [kWh]'].to_list()]
        energy_footprints_afternoon = [x * 1000 for x in df['icp afternoon [kWh]'].to_list()]
        
        df_final_data = {dates[i] : (energy_footprints_morning[i], energy_footprints_afternoon[i] ) for i in range(len(dates))}

        return df_final_data

def write_total_house_consumption_to_csv(house_ids, start_datetime, end_datetime):
    consumption_source_folder = Configuration.get_consumption_data_config('source_folder')
    db_path = os.path.join(consumption_source_folder, 'irise.sqlite3')
    location = os.path.normpath(db_path)
    db = sqlite3.connect(location)

    for house_id in house_ids:
        df_appliances = pd.read_sql_query("SELECT ID, Name FROM Appliance WHERE HouseIDREF=%i" % house_id, db)
        appliances_dict = {}
        for index, row in df_appliances.iterrows():
            if row['Name'] == 'Site consumption ()':
                print("EXTRACTING ONLY TOTAL CONSUMPTION")
                x = Appliance(db, row['ID'], house_id, row['Name'], start_datetime, end_datetime)
                datetimes = x.datetimes
                appliances_dict[row['Name']] = x.values

        appliances_dict['Date'] = datetimes
        consumption_hourly = pd.DataFrame(appliances_dict)
        consumption_hourly = consumption_hourly[['Date', 'Site consumption ()']]
        consumption_hourly.set_index('Date', inplace=True)
        consumption_hourly.index.name = None
        consumption_hourly.columns = ['Consumption [Wh]']

        str_starting_date = start_datetime.strftime("%d_%m_%Y")
        str_ending_date = end_datetime.strftime("%d_%m_%Y")
        result_folder_path = 'input'
        file_name = f'house_{house_id}_consumption_between_{str_starting_date}_and_{str_ending_date}.csv'
        consumption_file_path = os.path.join(result_folder_path, file_name)
        consumption_hourly.to_csv(consumption_file_path)


def get_consumption_for_a_house_from_csv(house_id, start_datetime, end_datetime, models_root_path, community_type, data_folder_name) -> pd.DataFrame:
    str_start_datetime = start_datetime.strftime("%d_%m_%Y")
    str_end_datetime = end_datetime.strftime("%d_%m_%Y")
    file_name = f'house_{house_id}_consumption_between_{str_start_datetime}_and_{str_end_datetime}.csv'
    file_path = os.path.join(models_root_path,community_type,data_folder_name, file_name)
    df = pd.read_csv(file_path, index_col=0)
    #print(f"Consumption extracted successfully from file for house {house_id}!")
    return df


def get_consumption_for_a_house_from_database(house_id, start_datetime, end_datetime, only_total_consumption_ok):
    db_path = Configuration.get_consumption_data_config('source_folder')
    db_path = os.path.join(db_path, 'irise.sqlite3')
    location = os.path.normpath(db_path)
    db = sqlite3.connect(location)
    df_appliances = pd.read_sql_query("SELECT ID, Name FROM Appliance WHERE HouseIDREF=%i" % house_id, db)

    appliances_dict = {}
    if only_total_consumption_ok:
        for index, row in df_appliances.iterrows():
            if row['Name'] == 'Site consumption ()':
                print("EXTRACTING ONLY TOTAL CONSUMPTION")
                x = Appliance(db, row['ID'], house_id, row['Name'], start_datetime, end_datetime)
                datetimes = x.datetimes
                appliances_dict[row['Name']] = x.values
    else:
        for index, row in df_appliances.iterrows():
            x = Appliance(db, row['ID'], house_id, row['Name'], start_datetime, end_datetime)
            datetimes = x.datetimes
            appliances_dict[row['Name']] = x.values

    appliances_dict['Date'] = datetimes
    consumption_hourly = pd.DataFrame(appliances_dict)

    consumption_hourly = consumption_hourly[['Date', 'Site consumption ()']]
    consumption_hourly.set_index('Date', inplace=True)
    consumption_hourly.index.name = None
    consumption_hourly.columns = ['Consumption [Wh]']

    ok = True
    return consumption_hourly, ok
