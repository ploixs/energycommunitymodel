import statistics
import sys

import numpy as np
import pandas as pd
from sklearn.experimental import enable_iterative_imputer

from simulation.Configurator import *


class Estimator:
    non_observed_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    observed_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']

    def __init__(self, community_model):
        # Energy community model
        self.ec_model = community_model
        self.sim_id = Configuration.get_simulations_parameters_set()
        self.est_id = Configuration.get_estimation_parameters_set(self.sim_id)

        self.obs_days_sim_data_dict_no_decision = {'sim_time': [],
                                                   'sim_consumption_total': []}

        self.obs_days_sim_data_dict_with_decision = {'mondays_sim_time': [],
                                                     'mondays_sim_consumption_total': [],
                                                     'tuesdays_sim_consumption_total': [],
                                                     'tuesdays_sim_time': [],
                                                     'wednesdays_sim_consumption_total': [],
                                                     'wednesdays_sim_time': [],
                                                     'thursdays_sim_consumption_total': [],
                                                     'thursdays_sim_time': [],
                                                     'fridays_sim_consumption_total': [],
                                                     'fridays_sim_time': [],
                                                     'saturdays_sim_consumption_total': [],
                                                     'saturdays_sim_time': []}
        self.est_df = None

        self.exp_obs_days_list = []

        self.passed_obs_days_count = 0
        self.exp_obs_days_data_with_decision_dict = {}
        self.exp_non_obs_days_data_with_decision_dict = {}

        # Estimator parameters from the config file
        self.est_method = Configuration.get_estimation_method(self.est_id)
        self.obs_days_frequency = Configuration.get_estimation_observation_days_frequency(self.est_id)
        self.obs_days_decision = Configuration.get_estimation_observation_days_decision(self.est_id)
        self.window_type = Configuration.get_estimation_observation_window_type(self.est_id)
        self.window_length_days = Configuration.get_estimation_observation_window_length_days(self.est_id)
        self.use_forgetting_factor = Configuration.get_estimation_forgetting_factor_decision(self.est_id)

        if self.use_forgetting_factor:
            self.forgetting_factor = Configuration.get_estimation_forgetting_factor(
                Configuration.get_estimation_parameters_set(Configuration.get_simulations_parameters_set()))

        if self.obs_days_decision == 'fixed_w_b_w1':
            self.set_expected_observation_days_dict_based_on_weekday_1_week()
        elif self.obs_days_decision == 'fixed_w_b_w2':
            self.set_expected_observation_days_dict_based_on_weekday_2_week()
        self.set_expected_observation_days_list_based_on_frequency()

        # print(len(self.observing_days_list))
        if self.ec_model.sim_params.print_member_info:
            print("------ ESTIMATION SYSTEM USED --------")
            print("ESTIMATION METHOD", self.est_method)
            print("OBSERVATION WINDOW TYPE", self.window_type)
            print("OBSERVATION WINDOW LENGTH DAYS", self.window_length_days)
            print("OBSERVATION DAYS DECISION", self.obs_days_decision)
            print("OBSERVATION DAYS FREQUENCY", self.obs_days_frequency)

    def prepare_observation_data_for_estimation_generation(self):
        if self.obs_days_decision in ['fixed_w_b_w1', 'fixed_w_b_w2']:
            for day in self.non_observed_days:
                if self.ec_model.current_time in self.exp_non_obs_days_data_with_decision_dict[day + 's']:
                    if day == 'sunday':
                        current_non_observed_day = 'saturday'  # we treat a non observed sunday as a saturday
                    else:
                        current_non_observed_day = day
                    if self.ec_model.sim_params.print_member_info:
                        print("Estimating consumption for ", current_non_observed_day)

            # Create a datetime based dataframe from the previously observed days
            observation_data = {'time': self.obs_days_sim_data_dict_with_decision[current_non_observed_day + 's_sim_time'],
                                'Consumption': self.obs_days_sim_data_dict_with_decision[current_non_observed_day + 's_sim_consumption_total']}
        else:
            # Create a datetime based dataframe from the previously observed days
            observation_data = {'time': self.obs_days_sim_data_dict_no_decision['sim_time'],
                                'Consumption': self.obs_days_sim_data_dict_no_decision['sim_consumption_total']}

        df = pd.DataFrame(observation_data, columns=['time', 'Consumption'])
        df = df.set_index('time')

        return df

    @staticmethod
    def prepare_estimation_data_for_usage_by_agent(agent, consumption_dict):
        # For simplicity, convert the dict to a list
        consumption_list_final = []
        time_temp_list = agent.est_consumption_current_day_df.index
        for hour in range(24):
            consumption_list_final.append(consumption_dict[hour])

        # Generate a datetime index dataframe from the mean values
        # using a datetime index for the current day
        data = {'time': time_temp_list,
                'Consumption': consumption_list_final}

        df_final = pd.DataFrame(data)
        df_final = df_final.set_index('time')
        return df_final

    def generate_estimation(self, agent):
        df = self.prepare_observation_data_for_estimation_generation()

        consumption_dict = {key: None for key in range(24)}

        for hour in range(24):
            # For each hour, we gather consumption in a list
            consumption_at_hour_x_list = df.loc[df.index.hour == hour]['Consumption'].to_list()

            if self.est_method == 'linear_weighted_average':
                if len(consumption_at_hour_x_list) > 1:
                    weights = list(range(1, len(consumption_at_hour_x_list) + 1))
                    if self.ec_model.sim_params.print_member_info:
                        print(" ----- Linear weighs generated ")
                else:
                    weights = [1]
                    if self.ec_model.sim_params.print_member_info:
                        print(" ----- Weighs equal to 1 ")
                weighted_mean_value = sum([consumption_at_hour_x_list[i] * weights[i] for i in range(len(consumption_at_hour_x_list))]) / sum(weights)
                consumption_dict[hour] = weighted_mean_value
                if self.ec_model.sim_params.print_member_info:
                    print(" -- Generated estimation with linear weighted average method.")

            if self.est_method == 'exponential_weighted_average':
                if len(consumption_at_hour_x_list) > 1:
                    weights = [1 for i in range(len(consumption_at_hour_x_list))]
                    for i in range(1, len(weights)):
                        weights[i] = (1 - self.forgetting_factor) ** i
                    weights.reverse()
                    if self.ec_model.sim_params.print_member_info:
                        print(" ----- Exponential weighs generated ")
                else:
                    weights = [1]
                    if self.ec_model.sim_params.print_member_info:
                        print(" ----- Weighs equal to 1 ")
                print("weights", weights)
                weighted_mean_value = sum([consumption_at_hour_x_list[i] * weights[i] for i in range(len(consumption_at_hour_x_list))]) / sum(weights)
                consumption_dict[hour] = weighted_mean_value
                if self.ec_model.sim_params.print_member_info:
                    print(" -- Generated estimation with exponential weighted average method.")

            if self.est_method == 'average':
                if len(consumption_at_hour_x_list) > 1:
                    consumption_dict[hour] = statistics.mean(consumption_at_hour_x_list)
                if self.ec_model.sim_params.print_member_info:
                    print(" -- Generated estimation with simple average method.")

            if self.est_method == 'regression':
                df['time'] = pd.to_datetime(df['time'])
                df = df.set_index('time')
                df['day'] = df.index.day
                df['hour'] = df.index.hour
                df['month'] = df.index.month
                df['day_of_week'] = df.index.dayofweek
                # print(previous_data_df.columns)
                features_names_list = ['Consumption', 'day', 'hour', 'month', 'day_of_week']

                # Generate the estimation using the multivariate imputer
                imp = IterativeImputer(max_iter=10, random_state=0, imputation_order='roman', skip_complete=True)
                # imp = imp.fit(previous_data_df)
                data = imp.fit_transform(df)
                # print(data)
                if np.shape(data)[1] == len(features_names_list):
                    # Convert the estimation to a dataframe with datetime index
                    df = pd.DataFrame(data, columns=features_names_list)
                    df['time'] = df.index
                    df = df.set_index('time')
                    # print(df)
                    # Keep only current day
                    df = df.loc[self.ec_model.current_time:self.ec_model.current_time + pd.DateOffset(hours=23)]

                    # Keep only the consumption
                    df = df[['Consumption']].copy()
                    if self.ec_model.sim_params.print_member_info:
                        print(" -- Generated estimation with regression method.")

        self.est_df = self.prepare_estimation_data_for_usage_by_agent(agent, consumption_dict)
        if self.est_method == 'regression':
            self.est_df = df.copy()

    def set_expected_observation_days_dict_based_on_weekday_1_week(self):
        self.exp_obs_days_data_with_decision_dict = {
            'mondays': [pd.to_datetime(x) for x in pd.date_range('2015-01-01', '2016-01-02', freq='2W-MON').strftime('%m/%d/%Y').tolist()],
            'tuesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-01', '2016-01-02', freq='2W-TUE').strftime('%m/%d/%Y').tolist()],
            'thursdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-08', '2016-01-02', freq='2W-THU').strftime('%m/%d/%Y').tolist()],
            'wednesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-01', '2016-01-02', freq='2W-WED').strftime('%m/%d/%Y').tolist()],
            'fridays': [pd.to_datetime(x) for x in pd.date_range('2015-01-09', '2016-01-02', freq='2W-FRI').strftime('%m/%d/%Y').tolist()],
            'saturdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-10', '2016-01-02', freq='2W-SAT').strftime('%m/%d/%Y').tolist()]
        }
        # VERIFICATION
        self.exp_non_obs_days_data_with_decision_dict = {
            'mondays': [pd.to_datetime(x) for x in pd.date_range('2015-01-12', '2016-01-02', freq='2W-MON').strftime('%m/%d/%Y').tolist()],
            'tuesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-13', '2016-01-02', freq='2W-TUE').strftime('%m/%d/%Y').tolist()],
            'wednesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-14', '2016-01-02', freq='2W-WED').strftime('%m/%d/%Y').tolist()],
            'thursdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-01', '2016-01-02', freq='2W-THU').strftime('%m/%d/%Y').tolist()],
            'fridays': [pd.to_datetime(x) for x in pd.date_range('2015-01-02', '2016-01-02', freq='2W-FRI').strftime('%m/%d/%Y').tolist()],
            'saturdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-03', '2016-01-02', freq='2W-SAT').strftime('%m/%d/%Y').tolist()],
            'sundays': [pd.to_datetime(x) for x in pd.date_range('2015-01-04', '2016-01-02', freq='W-SUN').strftime('%m/%d/%Y').tolist()]
        }

    def set_expected_observation_days_dict_based_on_weekday_2_week(self):
        self.exp_obs_days_data_with_decision_dict = {
            'mondays': [pd.to_datetime(x) for x in pd.date_range('2015-01-01', '2016-01-02', freq='3W-MON').strftime('%m/%d/%Y').tolist()],
            'tuesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-01', '2016-01-02', freq='3W-TUE').strftime('%m/%d/%Y').tolist()],
            'thursdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-08', '2016-01-02', freq='3W-THU').strftime('%m/%d/%Y').tolist()],
            'wednesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-01', '2016-01-02', freq='3W-WED').strftime('%m/%d/%Y').tolist()],
            'fridays': [pd.to_datetime(x) for x in pd.date_range('2015-01-09', '2016-01-02', freq='3W-FRI').strftime('%m/%d/%Y').tolist()],
            'saturdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-10', '2016-01-02', freq='3W-SAT').strftime('%m/%d/%Y').tolist()]
        }
        print(self.exp_obs_days_data_with_decision_dict)
        # VERIFICATION
        self.exp_non_obs_days_data_with_decision_dict = {
            'mondays': [pd.to_datetime(x) for x in pd.date_range('2015-01-12', '2016-01-02', freq='3W-MON').strftime('%m/%d/%Y').tolist()] +
                       [pd.to_datetime(x) for x in pd.date_range('2015-01-19', '2016-01-02', freq='3W-MON').strftime('%m/%d/%Y').tolist()],
            'tuesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-13', '2016-01-02', freq='3W-TUE').strftime('%m/%d/%Y').tolist()] +
                        [pd.to_datetime(x) for x in pd.date_range('2015-01-20', '2016-01-02', freq='3W-TUE').strftime('%m/%d/%Y').tolist()],
            'wednesdays': [pd.to_datetime(x) for x in pd.date_range('2015-01-14', '2016-01-02', freq='3W-WED').strftime('%m/%d/%Y').tolist()] +
                          [pd.to_datetime(x) for x in pd.date_range('2015-01-21', '2016-01-02', freq='3W-WED').strftime('%m/%d/%Y').tolist()],
            'thursdays': [pd.to_datetime('2015-01-01')] +
                         [pd.to_datetime(x) for x in pd.date_range('2015-01-15', '2016-01-02', freq='3W-THU').strftime('%m/%d/%Y').tolist()] +
                         [pd.to_datetime(x) for x in pd.date_range('2015-01-22', '2016-01-02', freq='3W-THU').strftime('%m/%d/%Y').tolist()],
            'fridays': [pd.to_datetime('2015-01-02')] +
                       [pd.to_datetime(x) for x in pd.date_range('2015-01-16', '2016-01-02', freq='3W-FRI').strftime('%m/%d/%Y').tolist()] +
                       [pd.to_datetime(x) for x in pd.date_range('2015-01-23', '2016-01-02', freq='3W-FRI').strftime('%m/%d/%Y').tolist()],
            'saturdays': [pd.to_datetime('2015-01-03')] +
                         [pd.to_datetime(x) for x in pd.date_range('2015-01-17', '2016-01-02', freq='3W-SAT').strftime('%m/%d/%Y').tolist()] +
                         [pd.to_datetime(x) for x in pd.date_range('2015-01-24', '2016-01-02', freq='3W-SAT').strftime('%m/%d/%Y').tolist()],
            'sundays': [pd.to_datetime(x) for x in pd.date_range('2015-01-04', '2016-01-02', freq='W-SUN').strftime('%m/%d/%Y').tolist()]
        }
        print(self.exp_non_obs_days_data_with_decision_dict)

    def set_expected_observation_days_list_based_on_frequency(self):
        if self.obs_days_frequency > 0 and self.obs_days_decision == 'no_decision':
            self.exp_obs_days_list = [pd.to_datetime(x) for x in pd.date_range(
                start=str(2015), end=str(2016), freq=str(self.obs_days_frequency) + 'D').strftime('%m/%d/%Y').tolist()]

        if self.obs_days_frequency == 0 and self.obs_days_decision == 'random':
            min_date = pd.to_datetime('2015-01-01')
            max_date = pd.to_datetime('2015-12-31')
            day_count = (max_date - min_date).days + 1
            random_day = min_date + pd.to_timedelta(pd.np.random.randint(day_count, size=122), unit='d')
            self.exp_obs_days_list = [pd.to_datetime(x) for x in random_day]

        if self.obs_days_decision in ['fixed_w_b_w1', 'fixed_w_b_w2']:
            if self.exp_obs_days_data_with_decision_dict:
                all_observed_days = self.exp_obs_days_data_with_decision_dict['mondays'] + self.exp_obs_days_data_with_decision_dict['tuesdays'] + \
                                    self.exp_obs_days_data_with_decision_dict['wednesdays'] + self.exp_obs_days_data_with_decision_dict['thursdays'] + \
                                    self.exp_obs_days_data_with_decision_dict['fridays'] + self.exp_obs_days_data_with_decision_dict['saturdays']
                all_observed_days.sort()

                self.exp_obs_days_list = all_observed_days

                all_non_observed_days = self.exp_non_obs_days_data_with_decision_dict['mondays'] + \
                                        self.exp_non_obs_days_data_with_decision_dict['tuesdays'] + \
                                        self.exp_non_obs_days_data_with_decision_dict['wednesdays'] + \
                                        self.exp_non_obs_days_data_with_decision_dict['thursdays'] + \
                                        self.exp_non_obs_days_data_with_decision_dict['fridays'] + \
                                        self.exp_non_obs_days_data_with_decision_dict['saturdays'] + \
                                        self.exp_non_obs_days_data_with_decision_dict['sundays']
                all_non_observed_days.sort()
                # print(len(self.observing_days_list) )
            else:
                sys.exit("Observing days dict not initialized")

    def update_data_buffers_with_new_data(self, agent):
        if self.obs_days_decision in ['fixed_w_b_w1', 'fixed_w_b_w2']:
            current_day = None
            for day in self.observed_days:
                if self.ec_model.current_time in self.exp_obs_days_data_with_decision_dict[day + 's']:
                    current_day = day

            self.obs_days_sim_data_dict_with_decision[current_day + 's_sim_time'].extend(agent.est_consumption_current_day_df.index)
            self.obs_days_sim_data_dict_with_decision[current_day + 's_sim_consumption_total'].extend(
                agent.est_consumption_current_day_df['Consumption'].to_list())

            if self.ec_model.sim_params.print_member_info:
                print(" - Updated data buffer for observation days with sliding window and fixed days week by week 1 for day: ", day)
        else:
            # Update data buffers with data from the newly observed days
            self.obs_days_sim_data_dict_no_decision['sim_time'].extend(agent.est_consumption_current_day_df.index)
            self.obs_days_sim_data_dict_no_decision['sim_consumption_total'].extend(agent.est_consumption_current_day_df['Consumption'].to_list())

            if self.ec_model.sim_params.print_member_info:
                print(" - Updated data buffer for observation days with sliding window and no decision.")

    def update_data_buffers_remove_old_days(self):

        if self.window_type == 'sliding_window' and self.obs_days_decision in ['fixed_w_b_w1', 'fixed_w_b_w2']:
            current_day = None
            for day in self.observed_days:
                if self.ec_model.current_time in self.exp_obs_days_data_with_decision_dict[day + 's']:
                    current_day = day
            if len(self.obs_days_sim_data_dict_with_decision[current_day + 's_sim_consumption_total']) > self.window_length_days * 24:
                self.obs_days_sim_data_dict_with_decision[current_day + 's_sim_time'] = \
                    self.obs_days_sim_data_dict_with_decision[current_day + 's_sim_time'][24:]
                self.obs_days_sim_data_dict_with_decision[current_day + 's_sim_consumption_total'] = \
                    self.obs_days_sim_data_dict_with_decision[current_day + 's_sim_consumption_total'][24:]
                if self.ec_model.sim_params.print_member_info:
                    print(" - Removed oldest day from the data buffer - weekday case")

        elif self.window_type == 'sliding_window' and self.obs_days_decision not in ['fixed_w_b_w1', 'fixed_w_b_w2']:
            if len(self.obs_days_sim_data_dict_no_decision['sim_consumption_total']) > self.window_length_days * 24:
                self.obs_days_sim_data_dict_no_decision['sim_time'] = self.obs_days_sim_data_dict_no_decision['sim_time'][24:]
                self.obs_days_sim_data_dict_no_decision['sim_consumption_total'] = self.obs_days_sim_data_dict_no_decision['sim_consumption_total'][24:]

            if self.ec_model.sim_params.print_member_info:
                print(" - Removed oldest day from the data buffer - normal scenario")

    '''
    def generate_estimation_based_on_weekdays_and_sliding_window_exponential_weighted_average(self, agent):
        df = self.prepare_observation_data_for_estimation_generation()

        consumption_dict = {key: None for key in range(24)}
        for hour in range(24):
            # For each hour, we gather consumption in a list
            consumption_at_hour_x_list = df.loc[df.index.hour == hour]['Consumption'].to_list()

            if len(consumption_at_hour_x_list) > 1:
                weights = [1 for i in range(len(consumption_at_hour_x_list))]
                for i in range(1, len(weights)):
                    weights[i] = (1 - self.forgetting_factor) ** i
                weights.reverse()
            else:
                weights = [1]

            weighted_mean_value = sum([consumption_at_hour_x_list[i] * weights[i] for i in range(len(consumption_at_hour_x_list))]) / sum(weights)

            # Then store the mean value of the list in a dictionary
            consumption_dict[hour] = weighted_mean_value

        # For simplicity, convert the dict to a list
        consumption_list_final = []
        time_temp_list = agent.expected_consumption_current_day.index
        for hour in range(24):
            consumption_list_final.append(consumption_dict[hour])

        # Generate a datetime index dataframe from the mean values
        # using a datetime index for the current day
        data = {'time': time_temp_list,
                'Consumption': consumption_list_final}

        df_final = pd.DataFrame(data)
        df_final = df_final.set_index('time')
        self.estimation_df = df_final

    def generate_estimation_based_on_sliding_window_exponential_weighted_average(self, agent):
        df = self.prepare_observation_data_for_estimation_generation()

        # Compute the mean values by hour for the consumption from previously observed days
        # These mean values are stored in a dictionary
        consumption_dict = {key: None for key in range(24)}
        for hour in range(24):
            # For each hour, we gather consumption in a list
            consumption_at_hour_x_list = df.loc[df.index.hour == hour]['Consumption'].to_list()
            # print(df)
            # print(consumption_at_hour_x_list)
            # print("consumption",consumption_at_hour_x_list)
            if len(consumption_at_hour_x_list) > 1:
                weights = [1 for i in range(len(consumption_at_hour_x_list))]
                for i in range(1, len(weights)):
                    weights[i] = (1 - self.forgetting_factor) ** i
                weights.reverse()
            else:
                weights = [1]
            print("weights", weights)
            weighted_mean_value = sum([consumption_at_hour_x_list[i] * weights[i] for i in range(len(consumption_at_hour_x_list))]) / sum(weights)

            # Then store the mean value of the list in a dictionary
            consumption_dict[hour] = weighted_mean_value

        # For simplicity, convert the dict to a list
        consumption_list_final = []
        time_temp_list = agent.expected_consumption_current_day.index
        for hour in range(24):
            consumption_list_final.append(consumption_dict[hour])

        # Generate a datetime index dataframe from the mean values
        # using a datetime index for the current day
        data = {'time': time_temp_list,
                'Consumption': consumption_list_final}

        df_final = pd.DataFrame(data)
        df_final = df_final.set_index('time')
        self.estimation_df = df_final

    def generate_estimation_based_on_weekdays_and_sliding_window_linear_weighted_average(self, agent):
        df = self.prepare_observation_data_for_estimation_generation()

        # Compute the mean values by hour for the consumption from previously observed days
        # These mean values are stored in a dictionary
        consumption_dict = {key: None for key in range(24)}
        for hour in range(24):
            # For each hour, we gather consumption in a list
            consumption_at_hour_x_list = df.loc[df.index.hour == hour]['Consumption'].to_list()
            # print("consumption",consumption_at_hour_x_list)
            if len(consumption_at_hour_x_list) > 1:
                # weights = list(np.linspace(0.1, 1, num=len(consumption_at_hour_x_list)))
                weights = list(range(1, len(consumption_at_hour_x_list) + 1))
            else:
                weights = [1]
            # print("weights",weights)
            weighted_mean_value = sum([consumption_at_hour_x_list[i] * weights[i] for i in range(len(consumption_at_hour_x_list))]) / sum(weights)

            # Then store the mean value of the list in a dictionary
            consumption_dict[hour] = weighted_mean_value

        # For simplicity, convert the dict to a list
        consumption_list_final = []
        time_temp_list = agent.expected_consumption_current_day.index
        for hour in range(24):
            consumption_list_final.append(consumption_dict[hour])

        # Generate a datetime index dataframe from the mean values
        # using a datetime index for the current day
        data = {'time': time_temp_list,
                'Consumption': consumption_list_final}

        df_final = pd.DataFrame(data)
        df_final = df_final.set_index('time')
        print('AAA', df_final)
        self.estimation_df = df_final.copy()

    def generate_estimation_based_on_sliding_window_linear_weighted_average(self, agent):
        df = self.prepare_observation_data_for_estimation_generation()
        # These mean values are stored in a dictionary
        consumption_dict = {key: None for key in range(24)}
        for hour in range(24):
            # For each hour, we gather consumption in a list
            consumption_at_hour_x_list = df.loc[df.index.hour == hour]['Consumption'].to_list()
            # print("consumption",consumption_at_hour_x_list)
            if len(consumption_at_hour_x_list) > 1:
                # weights = list(np.linspace(1, self.model.estimation_parameters.observation_window_length_days, num=len(consumption_at_hour_x_list)))
                weights = list(range(1, len(consumption_at_hour_x_list) + 1))
            else:
                weights = [1]
            # print("weights", weights)
            weighted_mean_value = sum([consumption_at_hour_x_list[i] * weights[i] for i in range(len(consumption_at_hour_x_list))]) / sum(weights)

            # Then store the mean value of the list in a dictionary
            consumption_dict[hour] = weighted_mean_value

        # For simplicity, convert the dict to a list
        consumption_list_final = []
        time_temp_list = agent.expected_consumption_current_day.index
        for hour in range(24):
            consumption_list_final.append(consumption_dict[hour])

        # Generate a datetime index dataframe from the mean values
        # using a datetime index for the current day
        data = {'time': time_temp_list,
                'Consumption': consumption_list_final}

        df_final = pd.DataFrame(data)
        df_final = df_final.set_index('time')
        self.estimation_df = df_final

    def generate_estimation_based_on_sliding_window_average(self, agent):
        """Generate an estimation for the consumption of the current day based on mean hourly values of previously observed days.

           Data from previously observed days is computed from a sliding window of 3 days.

            :returns: Dataframe with consumption estimation for current day and datetime index
            :rtype: Pd.DataFrame"""
        # Create a datetime based dataframe from the previously observed days
        df = self.prepare_observation_data_for_estimation_generation()
        # Compute the mean values by hour for the consumption from previously observed days
        # These mean values are stored in a dictionary
        consumption_dict = {key: None for key in range(24)}
        for hour in range(24):
            # For each hour, we gather consumption in a list
            consumption_at_hour_x_list = df.loc[df.index.hour == hour]['Consumption'].to_list()

            # Or like this, but it takes too much time
            # for index, row in df.iterrows():
            #    if index.hour == hour:
            #        consumption_at_hour_x_list.append(row['Consumption'])

            # Then store the mean value of the list in a dictionary
            consumption_dict[hour] = statistics.mean(consumption_at_hour_x_list)

        # For simplicity, convert the dict to a list
        consumption_list_final = []
        time_temp_list = agent.expected_consumption_current_day.index
        for hour in range(24):
            consumption_list_final.append(consumption_dict[hour])

        # Generate a datetime index dataframe from the mean values
        # using a datetime index for the current day
        data = {'time': time_temp_list,
                'Consumption': consumption_list_final}

        df_final = pd.DataFrame(data)
        df_final = df_final.set_index('time')
        self.estimation_df = df_final
    

    def generate_estimation_based_on_simple_regressor(self, agent):
        """Function to estimate consumption for current day based on previously observed days.

             :returns: Dataframe with consumption estimation for current day and datetime index
             :rtype: Pd.DataFrame"""

        # Make copies of consumption and time from observed days;
        # the estimation is built on respective copies because consumption data from observed days cannot be modified (it is used iteratively)
        time_temp_list = self.observed_days_recorded_time[:]
        consumption_temp_list = self.observed_days_recorded_consumption_total[:]
        # community_consumption_temp_list = self.observed_days_community_consumption_total[:]

        # Extend the lists so it is enough space for the new estimation to be developed
        time_temp_list.extend(agent.expected_consumption_current_day.index)
        consumption_temp_list.extend([np.nan for i in range(24)])
        # community_consumption_temp_list.extend([0 for i in range(len(consumption_temp_list) -
        #                                                              len(community_consumption_temp_list))])
        # print(len(consumption_temp_list))
        # print(len(community_consumption_temp_list))
        # Combine the data into a dataframe
        previous_data = {'time': time_temp_list,
                         'Consumption': consumption_temp_list}
        # 'community_consumption': community_consumption_temp_list}
        previous_data_df = pd.DataFrame(previous_data)

        # Extract features as columns
        previous_data_df['time'] = pd.to_datetime(previous_data_df['time'])
        previous_data_df = previous_data_df.set_index('time')
        previous_data_df['day'] = previous_data_df.index.day
        previous_data_df['hour'] = previous_data_df.index.hour
        previous_data_df['month'] = previous_data_df.index.month
        previous_data_df['day_of_week'] = previous_data_df.index.dayofweek
        # print(previous_data_df.columns)
        features_names_list = ['Consumption', 'day', 'hour', 'month', 'day_of_week']
        # Print estimation data to csv for debugging
        # previous_data_df.to_csv("Output_Data/TEMP/Agent_" + str(self.unique_id) + ".csv")

        # Generate the estimation using the simple imputer
        # imp = SimpleImputer(missing_values=np.nan, strategy='mean')
        # imp = imp.fit(previous_data_df)
        # data = imp.transform(previous_data_df)

        # Generate the estimation using the multivariate imputer
        imp = IterativeImputer(max_iter=10, random_state=0, imputation_order='roman', skip_complete=True)
        # imp = imp.fit(previous_data_df)
        data = imp.fit_transform(previous_data_df)
        # print(data)
        if np.shape(data)[1] == len(features_names_list):
            # Convert the estimation to a dataframe with datetime index
            df = pd.DataFrame(data, columns=features_names_list)
            df['time'] = previous_data_df.index
            df = df.set_index('time')
            # print(df)
            # Keep only current day
            df = df.loc[self.community_model.current_time:self.community_model.current_time + pd.DateOffset(hours=23)]

            # Keep only the consumption
            df = df[['Consumption']].copy()
            # df_final = df.copy()

            # Print estimation data to csv for debugging
            # df.to_csv("Output_Data/TEMP/Agent_" + str(self.unique_id) + ".csv")
            self.estimation_df = df
    '''
