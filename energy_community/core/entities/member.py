from abc import ABC, abstractmethod, abstractproperty
from datetime import datetime
from multiprocessing.sharedctypes import Value
from typing import Dict, Tuple, Union
from mesa import Model, Agent
import numpy as np
from energy_community.core.data.member_data import MemberDataDay, MemberDataTotal
from energy_community.core.constants import AsignType, Availability, DSType, DType, ExpectancyDev, MType, MemberActionIntensity, Stages, PrivacyLevel, Rec, Strategy, TargetOfRec, Willigness
#from simulation.params import Params
from energy_community.helpers.time_management_functions import generate_local_datetime_range_and_indexes
#from energycommunity.helpers.Locator import Locator
import pandas as pd
from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from energy_community.core.entities.community import EnergyCommunityModel

class CommunityMember(Agent, ABC):
    """Community member abstract class.

    Args:
        Agent (_type_): Inherits the MESA agent model.
        ABC (_type_): abstract class.
    """

    def __init__(self, unique_id: int, model: 'EnergyCommunityModel', agent_type: MType, member_stages: Stages, member_action_intensity: MemberActionIntensity, willigness_type: Willigness, availability: Availability): 
        """Abstract constructor for Community Member.

        Args:
            unique_id (int): the agent id
            model (EnergyCommunityModel): the community model
            agent_type (MType): member type: ideal/enthusiastic/reluctant
            member_stages (Stages): one turn/two turns
            member_action_intensity (MemberActionIntensity): ealistic/fixed
            willigness_type (Willigness): fixed
            availability (Availability): day/always
            est_consumption_total (pd.DataFrame | None): estimated consumption
        """
        super().__init__(unique_id, model)
        
        self.type = agent_type
        self.member_stages = member_stages
        self.member_action_intensity = member_action_intensity
        self.willigness_type = willigness_type
        self.availability = availability
        
        self.est_member_load_day = MemberDataDay(DSType.ESTIMATED, DType.CONSUMPTION) # estimated member consumption for current day
        self.req_member_load_day_eq_share = MemberDataDay(DSType.REQUESTED, DType.CONSUMPTION) # the requested member consumption current day based on an equal share
        self.req_member_load_day_dif_share = MemberDataDay(DSType.REQUESTED, DType.CONSUMPTION) # the requested member consumption current day based on a differential share
        self.req_member_load_total_eq_share = MemberDataTotal(DSType.REQUESTED, DType.CONSUMPTION) # the requested member consumption for the simulation based on an equal share
        self.req_member_load_total_dif_share = MemberDataTotal(DSType.REQUESTED, DType.CONSUMPTION) # the requested member consumption for the simulation  based on a differential share
        self.sim_member_load_day =  MemberDataDay(DSType.SIMULATED, DType.CONSUMPTION) # simulated member consumption for the current day
        self.sim_member_load_total = MemberDataTotal(DSType.SIMULATED, DType.CONSUMPTION) # simulated member consumption for simulation

        self.sim_member_actions_day = MemberDataDay(DSType.SIMULATED, DType.ACTIONS) # simulated member actions current day
        self.sim_member_actions_total = MemberDataTotal(DSType.SIMULATED, DType.ACTIONS) # simulated member action for the whole simulation
        self.sim_member_rec_day = MemberDataDay(DSType.SIMULATED, DType.RECOMMENDATIONS) # recommendations current day
        self.sim_member_rec_total = MemberDataTotal(DSType.SIMULATED, DType.RECOMMENDATIONS) # recommendations whole simulation
        self.recommendation_current_hour : Union[Rec, None] = None
        
        self.cm_type_decision_map = { MType.NORMAL: self.random.randint(0, 1),
                                     MType.GOOD: np.random.choice(np.arange(0, 2), p=[0.7, 0.3]),
                                     MType.BAD: np.random.choice(np.arange(0, 2), p=[0.3, 0.7]),
                                     MType.IDEAL: 0} # decision map for cm according to his type
        
    @abstractmethod
    def init_cm_day(self):
        pass
    
    
    def update_cm(self, data_models: Tuple[MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal]):
        self.est_member_load_total : MemberDataTotal = data_models[0]
        self.sim_member_load_total : MemberDataTotal = data_models[1]
        self.sim_member_actions_total : MemberDataTotal = data_models[2]
        self.sim_member_rec_total : MemberDataTotal = data_models[3]