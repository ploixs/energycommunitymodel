

from abc import ABC, abstractmethod, abstractproperty
from datetime import datetime
from multiprocessing.sharedctypes import Value
from typing import Dict, Tuple, Union
from mesa import Model, Agent
import numpy as np
from energy_community.core.constants import AsignType, Availability, DSType, DType, ExpectancyDev, MType, MemberActionIntensity, Stages, PrivacyLevel, Rec, Strategy, TargetOfRec, Willigness
from energy_community.helpers.time_management_functions import generate_local_datetime_range_and_indexes
import pandas as pd
from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from energy_community.core.entities.community import EnergyCommunityModel

class CommunityManager(Agent, ABC):
    """Community manager abstract class.

    Args:
        Agent (_type_): inherits the MESA agent model.
        ABC (_type_): abstract class.
    """

    def __init__(self, unique_id: int, model: 'EnergyCommunityModel', manager_stages: Stages, target_of_recommendations: TargetOfRec, expectancy_development: ExpectancyDev, strategy: Strategy, privacy_level: PrivacyLevel, no_alert_threshold: float): 
        """Abstract constructor for the community manager

        Args:
            unique_id (int): community member id
            model (EnergyCommunityModel): the community model
            manager_stages (Stages): one turn / two turns
            target_of_recommendations (TargetOfRec): everyone/groups
            expectancy_development (ExpectancyDev): basic/adaptive
            strategy (Strategy):informative, etc
            privacy_level (PrivacyLevel): private/not private
            no_alert_threshold (float): a comfort threshold for members
        """
        
        super().__init__(unique_id, model)
        
        self.manager_stages = manager_stages
        self.target_of_recommendations = target_of_recommendations
        self.expectancy_development = expectancy_development
        self.strategy = strategy
        self.privacy_level = privacy_level
        self.no_alert_threshold = no_alert_threshold
        #self.locator : Locator = Locator(self.model)
        
    @abstractproperty
    def cm_list(self):
        pass
    
    @abstractproperty
    def com_model(self):
        pass
    
    @abstractmethod
    def update_manager(self):
        pass
    
    @abstractmethod
    def init_manager(self):
        pass
    
    @abstractmethod
    def set_member_requested_consumption_levels(self):
        pass
    
    @abstractmethod
    def communicate_actions_to_cms(self):
        pass
    
    @abstractmethod
    def record_data_members_day(self):
        pass
    
    @abstractmethod
    def record_data_community_day(self):
        pass
    
    @abstractmethod
    def write_cm_hourly_results_simulation_to_csv(self):
        pass
    
    @abstractmethod
    def write_com_hourly_results_simulation_to_csv(self):
        pass