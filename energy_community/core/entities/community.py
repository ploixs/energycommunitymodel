

from abc import ABC, abstractmethod, abstractproperty
from datetime import datetime
from multiprocessing.sharedctypes import Value
from typing import Dict, Tuple, Union
from mesa import Model, Agent
import numpy as np
from energy_community.core.constants import AsignType, Availability, DSType, DType, ExpectancyDev, MType, MemberActionIntensity, Stages, PrivacyLevel, Rec, Strategy, TargetOfRec, Willigness
#from simulation.params import Params
from energy_community.helpers.time_management_functions import generate_local_datetime_range_and_indexes
#from energycommunity.helpers.Locator import Locator
import pandas as pd
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from energy_community.core.simulation.parameters import BaseParameters
    from energy_community.residential.core.residential_parameters import ResidentialParameters
    from energy_community.subway.SubwayParameters import SubwayParameters

class EnergyCommunityModel(Model, ABC):
    """
    The EnergyCommunityModel is a MESA-based model that simulates an energy community model 
    and stores its results in a specified folder path. It is an abstract class that contains 
    common properties and methods for energy community models.

    Args:
        Model (_type_): the MESA-based model class
        ABC (_type_): abstract class

    Attributes:
        sim_params (BaseParameters): the simulation parameters of the scenario
        start_of_day_end_of_day_index_map (Dict[int, int]): a map of start and end indices of a day
        tf_total (Dict[int, Union[datetime, pd.Timestamp]]): a dictionary representing the simulation time frame
        tf_day (Dict[int, Union[datetime, pd.Timestamp]]): a dictionary representing the current day time frame
        current_hour_index (int): the current hour index
        current_hour_index_day_relative (int): the current hour index relative to the current day
        time_and_day_names_map (Dict[datetime, str]): a map of time values and days
        results_folder_path (str): the folder path where the results will be stored
        current_time (datetime): the current time of the simulation
    """
    def __init__(self, sim_params: Union['BaseParameters','ResidentialParameters','SubwayParameters'], results_folder_path: str):
        """
        Abstract constructor for the EnergyCommunityModel.

        Args:
            sim_params (BaseParameters): the simulation parameters of the scenario
            results_folder_path (str): the folder path where the results will be stored

        Raises:
            ValueError: if the time frame cannot be set according to the sim_params, then raise an error.
        """
        super().__init__()

        self.sim_params = sim_params # simulation parameters
        self.start_of_day_end_of_day_index_map: Dict[int, int] = {} # map of start and end indices of a day
        self.tf_total: Dict[int, Union[datetime, pd.Timestamp]] = {} # simulation time frame
        self.tf_day: Dict[int, Union[datetime, pd.Timestamp]] = {} # current day time frame
        self.current_hour_index: int = 0 # current hour index
        self.current_hour_index_day_relative: int = 0 # current hour index relative to the current day
        self.time_and_day_names_map: Dict[datetime, str] = {} # map of time values and days
        self.results_folder_path: str = results_folder_path # folder path to store the results
        
        # set the simulation time frame and create the map between time and days
        self.set_time_range_and_new_detected_days_index()
        
        if len(self.tf_total) == 0: # raise an error if the simulation time frame is not set
            raise ValueError("self.tf_total not set!")
        
        # create a map between time and days where each time value is linked to the corresponding day
        self.time_and_day_names_map = {time_value: time_value.strftime("%A") for time_value in list(self.tf_total.values())}
        
        # set the current time as the first time sample in the simulation time frame
        self.current_time: datetime = self.tf_total[0]
    
    @abstractproperty
    def community_manager(self):
        pass
    
    @abstractmethod
    def add_new_community_members(self):
        pass

    def get_start_id_end_id_current_day(self) -> Tuple[int, int]:
        """Generate the start id and end id for the current day, based on self.start_of_day_end_of_day_index_map

        Returns:
            Tuple[int, int]: start_id and end_id for the current day
        """

        end_id = self.start_of_day_end_of_day_index_map[self.current_hour_index]
        return self.current_hour_index, end_id
    
    def set_timeframe_current_day(self):
        """ Sets the time range for the current day"""

        start_id, end_id = self.get_start_id_end_id_current_day() # get the start id and end if for the current day
        self.tf_day = {key:value for (key,value) in self.tf_total.items() if start_id <= key <= end_id} # filter the simulation time frame according to these days

    def set_time_range_and_new_detected_days_index(self):
        """ Sets the time range for the whole simulation, based on local date times. Also set the starting indexes and ending indexes for days."""
        
        local_datetimes, day_start_end_index_map = generate_local_datetime_range_and_indexes( self.sim_params.starting_date, self.sim_params.ending_date)
        self.tf_total = { time_index : local_datetimes[time_index] for time_index in range(len(local_datetimes)) } # the total time frame: for each index we have a corresponding time
        self.start_of_day_end_of_day_index_map = day_start_end_index_map