



from collections import Counter
from typing import Union
from energy_community.core.constants import AsignType, DSType, IndicatorsAll, IndicatorsInd, IndicatorsLP, IndicatorsLoad, IndicatorsRec, Rec

from energy_community.helpers.indicators import NEEG, NEEG_per_day, Normalised_NEEG, average_contributions_per_day, contribution_change_rate, cost, effort, money_for_grid_energy, recommendation_deviation_rate, recommendation_influence, renunciation, savings_per_day, self_consumption, self_sufficiency


class PerformanceAnalyser():
    
    def __init__(self, community, manager):
        
        
        if manager.verify_data:
            self.manager = manager
        
        self.performances  = {} # performances at community level
        self.performances_each_cm = {} # performances at cm level
        
        self.days_in_simulation = self.get_day_count_in_simulation(community)
        

        self.com_indicators_load_prod = {IndicatorsLP.SC: self_consumption, 
                                         IndicatorsLP.SS: self_sufficiency,
                                         IndicatorsLP.COST: cost,
                                         IndicatorsLP.NEEG_PER_DAY: NEEG_per_day,
                                         IndicatorsLP.NEEG: NEEG} # a mapping dict between indicators and corresponding functions from indicators.py
        
        self.COM_INDICATORS_LOAD = {IndicatorsLoad.EFFORT: effort,  
                                    IndicatorsLoad.RENUNCIATION: renunciation} # a mapping dict between load-specific indicators and corresponding functions from indicators.py
        
    def get_day_count_in_simulation(self, community_model):
        
        days_in_sim = len(community_model.start_of_day_end_of_day_index_map) # get the number of days
        
        return days_in_sim
    
    
    def calculate_community_performances_based_on_load_and_prod(self):
        
        for com_load in [self.manager.est_com_load_total, self.manager.sim_com_load_total]: # for each case: estimated and simulated consumption
            for indicator in self.com_indicators_load_prod:
                # calculate the indicator based on the mapping self.com_indicators_load_prod
                value = round(self.com_indicators_load_prod[indicator](list(com_load.data.values()), list(self.manager.est_com_prod_total.data.values())),2) 
                self.performances[(com_load.data_source_type, com_load.asignee_type, indicator)] = value
                print(f"-- {com_load.data_source_type} {com_load.asignee_type} {indicator} : {self.performances[(com_load.data_source_type, com_load.asignee_type, indicator)]} ")

    
    def calculate_specific_community_energy_indicators(self):
        for indicator in self.COM_INDICATORS_LOAD: # for each indicator based specifically on load: effort, renounciation
            self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, indicator)] = round(self.COM_INDICATORS_LOAD[indicator](list(self.manager.est_com_load_total.data.values()), list(self.manager.sim_com_load_total.data.values())),2)
        
        self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsAll.NORMALISED_NEEG)] = round(Normalised_NEEG(list(self.manager.est_com_load_total.data.values()), list(self.manager.sim_com_load_total.data.values()), list(self.manager.est_com_prod_total.data.values())),2)
        self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLP.DAILY_SAVINGS)] = round(savings_per_day(list(self.manager.est_com_load_total.data.values()), list(self.manager.sim_com_load_total.data.values()), list(self.manager.est_com_prod_total.data.values()), self.days_in_simulation, len(self.manager.cm_list)),2)
        self.performances[(DSType.ESTIMATED, AsignType.COMMUNITY, IndicatorsLoad.MONEY_FOR_GRID_ENERGY)] = round(money_for_grid_energy(list(self.manager.est_com_load_total.data.values()), 1-self.performances[(DSType.ESTIMATED, AsignType.COMMUNITY, IndicatorsLP.SS)]),2)
        self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLoad.MONEY_FOR_GRID_ENERGY)] = round(money_for_grid_energy(list(self.manager.sim_com_load_total.data.values()), 1-self.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLP.SS)]),2)
        
    def calculate_specific_community_recommendation_indicators(self):
        
        rec_map = { str(rec): rec for rec in Rec  }
        
        unique_cms = {}
        for cm in self.manager.cm_list:
                # Add the first encountered community member of each type to the dictionary
            if cm.type not in unique_cms:
                unique_cms[cm.type] = cm
        
        # Iterate through the unique community members
        for _, cm in unique_cms.items():
            # Create a dictionary of recommendations given during the flexibility period
            recs_during_flexibility : Dict[pd.Timestamp,Rec] = {time : rec_map[rec] for time, rec in cm.sim_member_rec_total.data.items() if self.manager.flexibility_period.hour_min <= time.hour <= self.manager.flexibility_period.hour_max } # type: ignore 

            # Calculate and store performances for the community member
            self.performances[(DSType.SIMULATED, AsignType.CATEGORY, IndicatorsRec.RECOMMENDATION_DEVIATION_RATE, cm.type)] = round(recommendation_deviation_rate(list(recs_during_flexibility.values())), 2)
            self.performances[(DSType.SIMULATED, AsignType.CATEGORY, IndicatorsRec.CONTRIBUTION_CHANGE_RATE, cm.type)] = round(contribution_change_rate(list(recs_during_flexibility.values())), 2)
            self.performances[(DSType.SIMULATED, AsignType.CATEGORY, IndicatorsRec.AVG_CONTRIBUTIONS_PER_DAY, cm.type)] = round(average_contributions_per_day(list(recs_during_flexibility.values())), 2)
            self.performances[(DSType.SIMULATED, AsignType.CATEGORY, IndicatorsRec.REC_INFLUENCE, cm.type)] = round(recommendation_influence(list(recs_during_flexibility.values())), 2)

        
    def calculate_individual_indicators(self):
        
        for cm in list(self.manager.cm_list): # for each  member
                self.performances_each_cm[cm.unique_id] = {}
                
                dif_rho_list = [(cm.est_member_load_total.data[time] / est_com_load) if est_com_load != 0 else 0 for time,est_com_load in self.manager.est_com_load_total.data.items() ] # Get differential share values
                member_p_pv = [prod_val * dif_rho_list[i] for i, prod_val in enumerate(list(self.manager.est_com_load_total.data.values()))] # calculate the corresponding share of production for each member
                
                self.performances_each_cm[cm.unique_id][(DSType.SIMULATED, AsignType.MEMBER, IndicatorsLP.NEEG)] = NEEG(list(cm.sim_member_load_total.data.values()), member_p_pv)
                self.performances_each_cm[cm.unique_id][(DSType.ESTIMATED, AsignType.MEMBER, IndicatorsInd.AVERAGE_ESTIMATED_LOAD_DAY)] = sum(list(cm.est_member_load_total.data.values())) * 24 / len(list(self.manager.est_com_prod_total.data.values())) / 1000
                
    def print_collective_indicators(self):
        
        for key, value in self.performances.items(): # print community performances
            print(f"-- {[perf for perf in key]} : {value}")
    
    def print_individual_indicators(self):
        
        print("-- Community Member Performances: ") 
        for cm_id in self.performances_each_cm: 
                    for key, value in self.performances.items(): # print member performances
                        print(f"-- CM {cm_id} : {[perf for perf in key]} : {value}")

