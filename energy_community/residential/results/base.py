


import os
import numpy as np
import pytz
from typing import Dict, List, Tuple, Union
import pandas as pd
import matplotlib.dates as mdates
from matplotlib import gridspec, pyplot as plt
import matplotlib.lines as mlines
from matplotlib import markers
from energy_community.core.data.collective_data import CollectiveDataTotal
from energy_community.core.data.member_data import MemberDataTotal
from energy_community.core.constants import MTYPE_MAP, PLT_STATE_MAP, POS_ACTIONS_MAP, POS_STATES_MAP, STAGES_MAP_RESIDENTIAL, STRATEGY_MAP_RESIDENTIAL, AsignType, DSType, DType, ExpectancyDev, Interval, MType, PType, PossibleActions, PossibleStates, Rec, Stages, Strategy, expectancy_map
from energy_community.residential.core.community import ResidentialCommunity
from energy_community.residential.core.member import ResidentialCommunityMember
from energy_community.residential.core.profiles import OneTurnResidentialManager, TwoTurnsResidentialManager, OneTurnResidentialMarkovCommunityMember, TwoTurnsResidentialMarkovCommunityMember
from energy_community.helpers.Spec import Spec
from energy_community.residential.results.analyser import PerformanceAnalyser
from energy_community.residential.results.reader import ResultsReader


class ResidentialResults():
    """Results analysis for a residential community"""
    
    def __init__(self, scenario_id: int, folder_id: int, analysis_spec: Spec, paths: Dict[str,str], scenarios_config_file_name: str):
        """Initialization function for the subway community results.

        Args:
            id (int): subway community scenario ID
            analysis_spec (Spec): plotting and writing specification
            scenario_file_name (str): the name of the scenario file
        """
        self.spec : Spec = analysis_spec
        self.scenario_id = scenario_id
        self.local_tz = 'Europe/Paris'
        
        
        self.cm_file_writing_data = {} # cm data to be written in files
        self.cm_file_writing_column_names = [] # cm data columns
        self.file_writing_data = [] # community level data to be written in files
        self.file_writing_column_names : List[str] = [] # community level data column names
        self.description = "" # scenario short description for plotting purposes
        
        self.reader = ResultsReader(self.scenario_id, folder_id, paths, scenarios_config_file_name)
                            
        self.MEMBER_MAP = {(str(Stages.ONE_TURN)) : OneTurnResidentialMarkovCommunityMember,
                           (str(Stages.TWO_TURNS)) : TwoTurnsResidentialMarkovCommunityMember} # a mapping between member profile and corresponding class init
        
        self.params = self.reader.get_simulation_paramters()
        
        self.com_model = ResidentialCommunity(self.params, self.reader.folder_path, self.params.stages) # STEP 1: generate the community model 
        self.set_community_manager_based_on_profile() #  STEP 2: assign the manager
        com_energy_models = self.get_com_energy_models() # STEP 3: get community energy models
        if self.manager.strategy == Strategy.COACHING:
            self.manager.sim_periods_total = self.get_daily_periods_from_csv()
        members = self.get_community_members_based_on_profiles() # STEP 4: init the members
        self.manager.update_manager(com_energy_models, members) # STEP 5: update the manger with the models and the members
        cm_data_models = self.get_cm_data_models() # STEP 6: update only the representative members
        for member in self.manager.cm_list: # ... for all members, assign the simulated data
            member.update_cm(cm_data_models[member.unique_id]) # type: ignore
            member.sim_member_states_total = cm_data_models[member.unique_id][-1] # get also the states
            member.set_df_for_plotting()
        
        self.manager.set_df_for_plotting()  # STEP 8: set the manager dataframe for plotting purposes
        
        self.description = fr"{STAGES_MAP_RESIDENTIAL[self.manager.manager_stages]} {expectancy_map[self.manager.expectancy_development]} {STRATEGY_MAP_RESIDENTIAL[self.manager.strategy]} ($\tau$ = {self.manager.no_alert_threshold})"
        
        self.short_description = fr"{STAGES_MAP_RESIDENTIAL[self.manager.manager_stages]} {STRATEGY_MAP_RESIDENTIAL[self.manager.strategy]}"
        
        self.analyzer = PerformanceAnalyser(self.com_model, self.manager)
        
        self.analyzer.calculate_community_performances_based_on_load_and_prod()
        self.analyzer.calculate_specific_community_energy_indicators()
        self.analyzer.calculate_specific_community_recommendation_indicators()
        
        if self.spec.calculate_individual_indicators:  # Calculate individual indicators (for each member)
            self.analyzer.calculate_individual_indicators()
        
        print("- Simulation performances: ")
        self.analyzer.print_collective_indicators()
        
        if self.spec.print_member_performances:
            self.analyzer.print_individual_indicators()
        
        #self.set_performances_all_simulation() # STEP 9: calculte performances
        self.set_results_file_name() # STEP 10: set the file name for writing data
        #self.set_file_writing_data_and_column_names()  # STEP 11: Set the data to be written in files
           
        
    def set_community_manager_based_on_profile(self):
        """Asign the community manager based on the profile read from the csv results"""
        
        MAP_STRATEGY = {str(strategy): strategy for strategy in Strategy}
        MAP_EXP_DEV = {str(exp): exp for exp in ExpectancyDev}
        
        if self.reader.manager_profile_path == "":
            raise ValueError("Manager profile path not set!")
        
        df = pd.read_csv(self.reader.manager_profile_path) # extract the profile from the csv results file
        
        if df['stages'].item() == str(Stages.ONE_TURN):
            self.manager = OneTurnResidentialManager(df['pv_surface'],self.com_model, df['target_of_recommendations'].item(),  MAP_EXP_DEV[df['expectancy_development'].item()], MAP_STRATEGY[df['strategy'].item()], df['privacy_level'].item(), df['no_alert_threshold'].item())
        else: # otherwise, we assign the weights to 0
            self.manager = TwoTurnsResidentialManager(df['pv_surface'],self.com_model, df['target_of_recommendations'].item(),  MAP_EXP_DEV[df['expectancy_development'].item()],MAP_STRATEGY[df['strategy'].item()], df['privacy_level'].item(), df['no_alert_threshold'].item())
        
        self.com_model.community_manager = self.manager
        
    
    def get_com_energy_models(self) -> Tuple[CollectiveDataTotal,CollectiveDataTotal, CollectiveDataTotal]:
        """Extracts the community profiles (simulated consumption, estimated consumption and estiamted production) from files.

        Returns:
            Tuple[CollectiveDataTotal,CollectiveDataTotal, CollectiveDataTotal]: simulated consumption, estimated consumption, estimated production
        """
        
        df = pd.read_csv(self.reader.hourly_com_results_full_path)
        eu_tz = pytz.timezone(self.local_tz)
        self.timeframe_simulation = [pd.to_datetime(x).astimezone(eu_tz) for x in df['sim_time'].to_list()] # extract the simulation hourly time
        est_com_load_total_model = CollectiveDataTotal(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.COMMUNITY) # estimated consumption empty model
        sim_com_load_total_model = CollectiveDataTotal(DSType.SIMULATED, DType.CONSUMPTION, AsignType.COMMUNITY) # simulated consumption empty model
        est_com_prod_total_model = CollectiveDataTotal(DSType.ESTIMATED, DType.PRODUCTION, AsignType.COMMUNITY) # estimated production empty model

        est_com_load_total_model.data = {self.timeframe_simulation[i] : value for i,value in enumerate(df[f'{DSType.ESTIMATED}_{DType.CONSUMPTION}_{AsignType.COMMUNITY}_{Interval.SIMULATION}'].to_list()) }
        sim_com_load_total_model.data = {self.timeframe_simulation[i] : value for i,value in enumerate(df[f'{DSType.SIMULATED}_{DType.CONSUMPTION}_{AsignType.COMMUNITY}_{Interval.SIMULATION}'].to_list()) }
        est_com_prod_total_model.data = {self.timeframe_simulation[i] : value for i,value in enumerate(df[f'{DSType.ESTIMATED}_{DType.PRODUCTION}_{AsignType.COMMUNITY}_{Interval.SIMULATION}'].to_list()) }
        return sim_com_load_total_model, est_com_load_total_model, est_com_prod_total_model
    
    def get_daily_periods_from_csv(self) -> Dict[Union[PType, pd.Timestamp], List[Tuple[int,int]]]:
        """Read the periods from each day from the simulation results.

        Raises:
            ValueError: If the file path is not set, raise an error.

        Returns:
            Dict[Union[PType, pd.Timestamp], List[Tuple[int,int]]]: the periods during each day as a tuple of hour_min and hour_max
        """
        daily_data = {}
        eu_tz = pytz.timezone(self.local_tz)
        if self.reader.daily_periods_path == "":
            raise ValueError("Daily periods path not set!")
        
        df = pd.read_csv(self.reader.daily_periods_path)
        daily_data['sim_day'] = [pd.to_datetime(time).astimezone(eu_tz)  for time in df['sim_day']]
        for period_name in self.manager.period_names_list:
            daily_data[period_name] = []
        for _, row in df.iterrows(): # for each day
            for period_name in self.manager.period_names_list:
                daily_data[period_name].append(eval(row[str(period_name)])) # convert each tuple string to tuple
        
        return daily_data
         
        
    def get_community_members_based_on_profiles(self) -> List[ResidentialCommunityMember]:
        """Initiate and assign community members based on the profiles read from the csv results

        Raises:
            ValueError: If the manager profiles is not set, raise an error

        Returns:
            List[ResidentialCommunityMember]: a list of the community members
        """
        
        members = []
        if self.reader.cm_profiles_path == "":
            raise ValueError("Member profile path not set!")
        
        CM_TYPE_STRING_MAP = {str(cm_type): cm_type for cm_type in MType}
        CM_STATE_ACTION = {f'{POS_STATES_MAP[state]}, {POS_ACTIONS_MAP[action]}': None for state in PossibleStates for action in PossibleActions }

        df = pd.read_csv(self.reader.cm_profiles_path)
        for _, row in df.iterrows(): # for each profile
            emissions = [row[state_action] for state_action in CM_STATE_ACTION]
            emit_explit = [[emissions[0], emissions[1]], [emissions[2], emissions[3]]]
            #emit_explit = {(state,action): None for state in PossibleStates for action in PossibleActions}
            #for i,k in enumerate(emit_explit):
            #    emit_explit[k] = emissions[i]    
            new_cm = self.MEMBER_MAP[(row['stages'])](row['unique_id'], row['description'], CM_TYPE_STRING_MAP[row['type']], self.com_model, None,  explicit_emissions=emit_explit) # use self.member_map to initiate empty members
            members.append(new_cm) 
        return members
    
    
    def get_cm_data_models(self) -> Dict[int, Tuple[MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal]]:
        """Return the data models for each cm

        Raises:
            ValueError: if the manager cm_list is empty, raise an error
            ValueError: if no models are extracted, raise an error

        Returns:
            Dict[int, Tuple[MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal]]: a dictionary with cm id as key and a tuple as value, with the structure: (estimated cm load, simulated cm load, simulated cm actions, simulated cm recommendations)
        """
        
        if len(self.manager.cm_list) == 0:
            raise ValueError("No member id list available, cannot read data from file!")
        cm_data_models : Dict[int, Tuple[MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal, MemberDataTotal]] = {}
        
        df = pd.read_csv(self.reader.hourly_cm_results_full_path) # read the results for all members
        eu_tz = pytz.timezone(self.local_tz)
        time_frame = [pd.to_datetime(x).astimezone(eu_tz) for x in df[f'sim_time'].to_list()] # convert the time to the local time zone
        
        for cm in self.manager.cm_list: # for each presentative member - flex capacity and preferred hour
            cm_data_models[cm.unique_id] = (MemberDataTotal(DSType.ESTIMATED, DType.CONSUMPTION),  
                                            MemberDataTotal(DSType.SIMULATED, DType.CONSUMPTION), 
                                            MemberDataTotal(DSType.SIMULATED, DType.ACTIONS),
                                            MemberDataTotal(DSType.SIMULATED, DType.RECOMMENDATIONS),
                                            MemberDataTotal(DSType.SIMULATED, DType.STATE)) # create empty data models
            for data_model in cm_data_models[cm.unique_id]: # update the models according to the df
                data_model.data = { time_frame[i] : value for i,value in enumerate(df[f'{cm.unique_id}_{cm.type}_{data_model.data_source_type}_{data_model.data_type}_{data_model.asignee_type}_{data_model.interval}'].to_list())} # get the model parameters from each column for the respective member and connect it with the corresponding time key
        if len(cm_data_models) == 0:
            raise ValueError("Data models could not be set!")
        return cm_data_models
    
    def set_results_file_name(self):
        """Asign the path ccording to the most relevant scenario parameters"""
        
        path = f'Scenario_{self.scenario_id}_PV_Manager_{self.manager.manager_stages}_{self.manager.target_of_recommendations}_{self.manager.expectancy_development}_{self.manager.strategy}_{self.manager.privacy_level}_{self.manager.no_alert_threshold}' # include all relevant parameters
        self.results_plot_file_name = path
        
    def set_file_writing_data_and_column_names(self):
        """Get the data to be written in files in a suitable format (along with the desired column names)"""
        
        self.file_writing_data.append(self.results_plot_file_name) # first colum is the scenario description
        self.file_writing_column_names.append('Scenario')
        self.file_writing_data.append(len(self.manager.cm_list)) # second column is the total member count
        self.file_writing_column_names.append('Member count')

        for key, value in self.analyzer.performances.items(): # for each community performances
            if len(key) > 3:
                self.file_writing_data.append(value) # get the value
                self.file_writing_column_names.append(f'{key[0]} {key[1]} {key[2]} {key[3]}') # ... the colum  name is composed of: DSType, AsignType, Indicator name and CM Type
            else:
                self.file_writing_data.append(value) # get the value
                self.file_writing_column_names.append(f'{key[0]} {key[1]} {key[2]}') # ... the colum  name is composed of: DSType, AsignType and Indicator name
        
        for cm in self.manager.cm_list: # for each member
            self.cm_file_writing_data[cm.unique_id] = [] # we empty the buffer for the performances
    
        if self.spec.calculate_individual_indicators:
            self.cm_file_writing_column_names += [f'cm neeg',f'average exp load per day']
            
            for cm in self.manager.cm_list: # for each representative member
                for _, performance in self.analyzer.performances_each_cm[cm.unique_id].items(): # for each performance of each representative member
                    self.cm_file_writing_data[cm.unique_id].append(performance) # add the performance to the cm coresponding buffer
         
                            
    def plot_hourly_comparison_with_states(self, save_figures_flag: bool = True, use_tex_flag: bool = False):
        
        plt.rcParams['timezone']='UTC' # the time axis must be in UTC; plotting_df time frame should be in UTC, too
        if save_figures_flag:
            if use_tex_flag:
                plt.rc('text', usetex=True) # use Latex if we have figures
                plt.style.use(['science']) # type: ignore
            plt.grid(True)

        plt.figure(figsize=(30, 20))

        grid_row_length = 3 # the total number of spaces in the grid - we account for the power profile comparison, actions comparison and plotting intervals
        gs = gridspec.GridSpec(grid_row_length, 1) # create the grid with one column and grid_row_length rows

        rep_cm =  self.manager.cm_list[0] # for each unique cm_type
        
        ax1 = plt.subplot(gs[0, 0]) # the first subplot is the power profile comparison
        self.plot_power_profile_comparison_general(plt, index=0) 
        self.set_plot_properties_power_profile_comparison_general(plt, index=0)

        plt.subplot(gs[1, 0], sharex=ax1) # share the axes between the first subplot and this one
        self.plot_actions_comparison_for_cm_type(plt, 0, rep_cm ) # plot the comparison between actions and recommendations for representative members
        self.set_plot_properties_actions_comparison(plt, 0, rep_cm.type)

        plt.subplot(gs[2, 0], sharex=ax1) # share the axes between the first subplot and this one
        self.plot_rep_cm_states(rep_cm,0)
        self.set_plot_properties_state_evolution(plt)

        f = plt.gcf()
        plt.subplots_adjust(wspace=0.1, hspace=0.6)
        f.set_size_inches(20, 10) 
        f.tight_layout()
        print(f"Plotted monthly comparison - Scenario {self.results_plot_file_name}")
 
        if save_figures_flag:
            path = os.path.join(self.reader.folder_path, f"Figure_Scenario_{self.scenario_id}_analysis_with_states.pdf") 
            f.savefig(path, bbox_inches='tight')
        else:
            plt.show()
    
    def plot_rep_cm_states(self, rep_cm, index: int):
        """Plot the state evolution for the representative community member.

        Args:
            rep_cm (ResidentialCommunityMember): the preresentative community member.
            index (int): interval index
        """
        # Select a subset of the 'plotting_df' dataframe between the specified start and end dates from 'plt_dt[index]' and select the 'STATE' column of the 'SIMULATED' data type
        df = rep_cm.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]][f'{DSType.SIMULATED}_{DType.STATE}']

        # Map the values in the 'STATE' column of the selected data to the corresponding values in 'PLT_STATE_MAP'
        df = df.map(PLT_STATE_MAP)

        # Plot the resulting dataframe, where the x-axis corresponds to the index of the dataframe, and the y-axis corresponds to the values in the 'STATE' column, after being mapped to 'PLT_STATE_MAP' values
        plt.plot(df)

    def set_plot_properties_state_evolution(self, plt):
        """add plot properties to the state evolution plot.

        Args:
            plt (_type_): matplotlib object.
        """
        # Turn on the grid for the plot
        plt.grid(True)
        
        # Set the major locator for the x-axis to show ticks every 4 hours
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=4))
        
        # Set the major formatter for the x-axis to show time in the format of '%H:00'
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:00'))

        # Set the x-axis label to 'Time' with the specified font size
        plt.xlabel('Time', fontsize=self.spec.axis_fs)
        
        # Set the plot title to 'State transitions of the representative member' with the specified font size
        plt.title(f"State transitions of the representative member", fontsize=self.spec.title_fs)
        
        # Set the font size and rotation for the x-axis tick labels
        plt.xticks(fontsize=self.spec.axis_fs, rotation=90)
        
        # Set the font size and labels for the y-axis tick labels
        plt.yticks(list(PLT_STATE_MAP.values()), ['Attentive','Inattentive'], fontsize=self.spec.axis_fs)
        # Note that 'PLT_STATE_MAP.values()' returns a list of the values of the dictionary 'PLT_STATE_MAP'
        # which are the categories of the data that will be plotted on the y-axis. 
        # The corresponding labels are specified as ['At', 'NAt'], which will be shown on the y-axis.
        
    
                            
    def plot_hourly_evolution_comparison_specific_months_and_days(self, plot_vertical_axes: bool = True, save_figures_flag: bool = True, use_tex_flag: bool = False):
        """Plot hourly comparison for the plotting intervals set in the specification

        Args:
            vertical_axes_flex_periods (bool, optional): Plot vertical axes for flexibility periods. Defaults to True.
            save_figures_flag (bool, optional): Save figures in the results folder. Defaults to True.
            use_tex_flag (bool, optional): Use LaTeX in plots. Defaults to False.
        """
        
        plt.rcParams['timezone']='UTC' # the time axis must be in UTC; plotting_df time frame should be in UTC, too
        if save_figures_flag:
            if use_tex_flag:
                plt.rc('text', usetex=True) # use Latex if we have figures
                plt.style.use(['science']) # type: ignore
            plt.grid(True)

        plt.figure(figsize=(30, 20))
            
        cm_types_in_sim = list(set([cm.type for cm in self.manager.cm_list])) # get the cm types in the scenario

        if len(cm_types_in_sim) > 1: # if we have more then one cm type
            specific_plots_grid_spaces = len(cm_types_in_sim) # we have a subplot space for each cm type
        else:
            specific_plots_grid_spaces = 2 # one space for the plot, one space for the state evolution
        
        grid_row_length = (1 + specific_plots_grid_spaces) * len(self.spec.plot_intervals) # the total number of spaces in the grid - we account for the power profile comparison, actions comparison and plotting intervals
        gs = gridspec.GridSpec(grid_row_length, 1) # create the grid with one column and grid_row_length rows
        grid_index, interval_index = 0, 0 # grid index is the place in the grid, interval index is the current plotting interval

        rep_cms = [] # get the representative members to analyse
        for cm_type in list(set([cm.type for cm in self.manager.cm_list])): # for each unique cm_type
            for cm in self.manager.cm_list: 
                if cm.type == cm_type:
                    rep_cms.append(cm) # get the first member with that type, then break the second loop
                    break
        
        while interval_index < len(self.spec.plot_intervals):
            ax1 = plt.subplot(gs[grid_index, 0]) # the first subplot is the power profile comparison
            self.plot_power_profile_comparison_general(plt, interval_index) 
            if plot_vertical_axes and self.manager.strategy == Strategy.COACHING:
                self.plot_period_vertical_lines_day_periods(plt, interval_index)
            self.set_plot_properties_power_profile_comparison_general(plt, interval_index)
            grid_index += 1 # get to the next grid position
            category_index = 0 # ... but is the same flexibilty capacity

            if len(cm_types_in_sim) > 1: # if we have more then one cm type
                while category_index < len(cm_types_in_sim):
                    cm_type = cm_types_in_sim[category_index] # get the cm type based on the current cat index
                    plt.subplot(gs[grid_index, 0], sharex=ax1) # share the axes between the first subplot and this one
                    
                    self.plot_actions_comparison_for_cm_type(plt, interval_index, rep_cms[category_index] ) # plot the comparison between actions and recommendations for representative members
                    self.set_plot_properties_actions_comparison(plt, interval_index, rep_cms[category_index].type)
                    grid_index += 1
                    category_index += 1
            else:
                plt.subplot(gs[grid_index, 0], sharex=ax1) # share the axes between the first subplot and this one
                self.plot_actions_comparison_for_cm_type(plt, interval_index, rep_cms[0] ) # plot the comparison between actions and recommendations for representative members
                self.set_plot_properties_actions_comparison(plt, interval_index, cm_types_in_sim[0])
                grid_index += 1
                
                plt.subplot(gs[grid_index, 0], sharex=ax1) # share the axes between the first subplot and this one
                self.plot_rep_cm_states(rep_cms[0], interval_index)
                self.set_plot_properties_state_evolution(plt)
                grid_index += 1

            interval_index += 1 # get to the next plotting interval

        f = plt.gcf()
        if len(cm_types_in_sim) == 1: # if we have only one flex capacity
            plt.subplots_adjust(wspace=0.1, hspace=0.6)
            f.set_size_inches(10, 5) if len(self.spec.plot_intervals) == 1 else f # set the figure size accoring to the number of plotting intervals
            f.set_size_inches(20, 15) if len(self.spec.plot_intervals) == 2 else f
            f.set_size_inches(20, 15) if len(self.spec.plot_intervals) == 3 else f
            f.tight_layout()
            
        if len(cm_types_in_sim) == 2:
            plt.subplots_adjust(wspace=0.1, hspace=1)
            f.set_size_inches(20, 25)
        if len(cm_types_in_sim) == 3:
            f.set_size_inches(20, 30)
            plt.subplots_adjust(wspace=0.1, hspace=1)

        print(f"Plotted monthly comparison - Scenario {self.results_plot_file_name}")

        if save_figures_flag:
            path = os.path.join(self.reader.folder_path, f"Figure_Scenario_{self.scenario_id}_analysis.pdf") 
            f.savefig(path, bbox_inches='tight')
        else:
            plt.show()
            
    def plot_power_profile_comparison_general(self, plt, index: int):
        """Plot the community power profiles"""
        
        if self.manager.plotting_df is None:
            raise ValueError("Plotting df not set yet!")
        plt.plot(self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]][f'{DSType.ESTIMATED}_{DType.CONSUMPTION}'] / 1000, color='blue', linestyle='dashed', label=self.spec.profile_legend[(DSType.ESTIMATED, DType.CONSUMPTION)]) # the estimated community consumption profile
        plt.plot(self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]][f'{DSType.SIMULATED}_{DType.CONSUMPTION}'] / 1000, color='blue', label=self.spec.profile_legend[(DSType.SIMULATED, DType.CONSUMPTION)]) # the simulated community consumption profile
        plt.plot(self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]][f'{DSType.ESTIMATED}_{DType.PRODUCTION}'] / 1000, color='red', linestyle='dashed', label=self.spec.profile_legend[(DSType.ESTIMATED, DType.PRODUCTION)]) # the estimated community PV profile
    
    def plot_period_vertical_lines_day_periods(self, plt, index: int):
        """Plot vertical lines for each day period hour start/end"""
        
        if self.manager.plotting_df is None:
            raise ValueError("Plotting df not set yet!")
        
        df_temp = self.manager.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]] # get the plotting df for given interval index
        period_times = []
        day_initial_times = [] # the first times of each day in the plotting interval: i.e. 12.12.2022 00:00:00
        for date in df_temp.index: # for each date 
            if date.hour == 0: # type: ignore
                day_initial_times.append(date)
                
        for i, time in enumerate(self.manager.sim_periods_total['sim_day']): # for each day in the simulation
            if time.date() in [time.date() for time in day_initial_times]: # if this day is in the plotting interval
                hour_min = self.manager.sim_periods_total[PType.PEAKPROD][i][0] # get the first element of the tuple i.e. hour_min
                hour_max = self.manager.sim_periods_total[PType.PEAKPROD][i][1] # get the second element of the tuple i.e. hour_max
                local_time_min = time.replace(hour=hour_min)
                utc_time_min = pd.Timestamp(local_time_min).tz_localize(None) # convert to utc time because the plot is in utc time
                local_time_max = time.replace(hour=hour_max)
                utc_time_max = pd.Timestamp(local_time_max).tz_localize(None) # convert to utc time because the plot is in utc time
                period_times.append(utc_time_min) # append to a list
                period_times.append(utc_time_max)

        period_times = pd.DatetimeIndex(period_times) # convert the list to a datetime index
        
        plt.vlines(period_times, ymin=0, ymax=plt.gca().get_ylim()[1], linestyles='dashed', colors='orange', label='Peak prod. \n period ')  # plot a vertical line at each flex time
  
    def set_plot_properties_power_profile_comparison_general(self, plt, index):
        """Assign plot properties to power profile comparison subplot"""
        
        plt.title(fr"Community power profile comparison between {self.spec.plt_dt_str[index][0]} and {self.spec.plt_dt_str[index][1]} ", fontsize=self.spec.title_fs)
        plt.ylabel('Power [kW]', fontsize=self.spec.axis_fs) # the axis are shared with the action/recommendation subplot
        plt.xlabel('Time', fontsize=self.spec.axis_fs) 
        plt.gca().xaxis_date(tz='UTC')
        plt.xticks(rotation=90)
        plt.legend(framealpha=0.8, fontsize=self.spec.legend_fs, loc='center left', bbox_to_anchor=(1, 0.5)) # plot the legend at the right, outside the plot
    
    
    def plot_actions_comparison_for_cm_type(self, plt, index: int, rep_cm: ResidentialCommunityMember):
        """Subplot for action/recommendations comparison for a representative cm"""
        
        # Define dictionaries to map the recommendation and action values to corresponding magnitudes for plotting
        rec_magnitudes = {np.nan: np.nan, str(Rec.BLINKING_RED): 2, str(Rec.STRONG_RED): 4, str(Rec.RED): 6, str(Rec.WHITE):  8, str(Rec.GREEN): 10, str(Rec.STRONG_GREEN): 12, str(Rec.BLINKING_GREEN): 14}
        act_magnitudes = {np.nan: np.nan, str(Rec.BLINKING_RED): 1, str(Rec.STRONG_RED): 3, str(Rec.RED): 5, str(Rec.WHITE):  7, str(Rec.GREEN): 9, str(Rec.STRONG_GREEN): 11, str(Rec.BLINKING_GREEN): 13}
        
        # Get a copy of the plotting dataframe for the specified plotting interval
        df_temp = rep_cm.plotting_df[self.spec.plt_dt[index][0]:self.spec.plt_dt[index][1]].copy()
        
        # Set the values of the 'RECOMMENDATIONS' and 'ACTIONS' columns to np.nan for hours before the flexibility period
        flexibility_hour_min = rep_cm.com_model.community_manager.flexibility_period.hour_min
        mask = (df_temp.index.hour < flexibility_hour_min) # type: ignore
        df_temp.loc[mask, f'{DSType.SIMULATED}_{DType.RECOMMENDATIONS}'] = np.nan
        df_temp.loc[mask, f'{DSType.SIMULATED}_{DType.ACTIONS}'] = np.nan
        
        # Map the recommendation values to corresponding magnitudes for plotting
        rec_magnitudes_for_plot = [rec_magnitudes[x] for x in df_temp[f'{DSType.SIMULATED}_{DType.RECOMMENDATIONS}']]
        
        # Create a boolean array indicating which recommendation magnitudes correspond to a low limit value (i.e., a GREEN recommendation)
        rec_low_limits = [True if x in [2, 4, 6, 8, 10, 12, 14] else False for x in rec_magnitudes_for_plot]
        
        # Create arrays for the x and y values of the plot
        x_values = list(df_temp.index)
        y_values = [0] * len(df_temp.index)
        
        # Plot arrows for the recommendations, using the magnitudes for the y-error values and the boolean array to set the low limit values
        plt.errorbar(x_values, y_values, yerr=rec_magnitudes_for_plot, uplims=False, lolims=rec_low_limits, label=f'Recommendation', color='blue')
        
        # If the flag for plotting the real behaviour of the first agent is set to True
        if self.spec.plot_real_behaviour_of_first_agent_flag: 
            
            # Map the action values to corresponding magnitudes for plotting
            act_magnitudes_for_plot = [act_magnitudes[x] for x in df_temp[f'{DSType.SIMULATED}_{DType.ACTIONS}']]
            
            # Create a boolean array indicating which action magnitudes correspond to a low limit value (i.e., a GREEN action)
            act_low_limits = [True if x in [1,3,5,7,9,11,13] else False for x in act_magnitudes_for_plot]
            
            # Plot arrows for the actions, using the magnitudes for the y-error values and the boolean array to set the low limit values
            label_text = f'CM has followed rec' if self.spec.plot_real_behaviour_of_first_agent_flag else None # set the label for the actions plot if the flag is True
            plt.errorbar(x_values, y_values, yerr=act_magnitudes_for_plot, uplims=False, lolims=act_low_limits, label=label_text, color='green')
            
        # Set the plot properties (grid, x/y-axis labels and titles, tick labels, etc.)
        self.set_plot_properties_state_evolution(plt)
        
        # Set the legend properties based on whether the flag for plotting the real behaviour of the first agent is set to True
        legend_labels = [f'Recommendation'] if not self.spec.plot_real_behaviour_of_first_agent_flag else [f'Recommendation', f'CM has followed rec']
        plt.legend(loc='upper left', fontsize=self.spec.axis_fs, labels=legend_labels)
    
          
    def set_plot_properties_actions_comparison(self, plt, index : int, cm_type: MType):
        """Assign plot properties to actions/recommendations subplot"""
        
        delta = self.spec.plt_dt[index][1] - self.spec.plt_dt[index][0] # get the difference in time between the plotting interval bounds
        plt.grid(True)

        if delta.days > 2 and delta.days < 10: # if we have a small number of days
            plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=4)) # we modify the time axis to show ticks every 4 hours
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:00'))
        else:
            plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=1)) # we modify the time axis to show ticks every 1 hour
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%H'))
        plt.xlabel('Time', fontsize=self.spec.axis_fs)

        cm_types_in_scenario = set([cm.type for cm in self.manager.cm_list])
        if len(cm_types_in_scenario) == 1:
            if self.manager.com_model.sim_params.stages == Stages.TWO_TURNS:  # type: ignore
                plt.title(f"Latest recommendations given to the representative member", fontsize=self.spec.title_fs) # we assign the title with the given specification format
            else:
                plt.title(f"Recommendations given to the representative member", fontsize=self.spec.title_fs) # we assign the title with the given specification format
        else:
            if self.manager.com_model.sim_params.stages == Stages.TWO_TURNS:  # type: ignore
                plt.title(f"Latest recommendations given to the representative {MTYPE_MAP[cm_type]} member", fontsize=self.spec.title_fs) # we assign the title with the given specification format
            else:
                plt.title(f"Recommendations given to the representative {MTYPE_MAP[cm_type]} member", fontsize=self.spec.title_fs) # we assign the title with the given specification format
        
        plt.yticks([2, 4, 6, 8, 10 , 12, 14]) # for each preferred hour group, we assign a tick number
        plt.ylim(0, 16) # the limits are asssigned to include the arrows
        plt.gca().set_yticklabels(['Blinking RED', 'Strong RED', 'RED','WHITE', 'GREEN', 'Strong GREEN','Blinking GREEN']) # we always know we have these commuting groups

        plt.xticks(fontsize=self.spec.axis_fs)
        plt.yticks(fontsize=self.spec.axis_fs)
        plt.xticks(rotation=90)
        
        legend_elements = [mlines.Line2D([], [], color='blue', marker=markers.CARETUP, linestyle='None', markersize=7, label='Recommendation'), # type: ignore # we create a custom symbol for the recommendation
                           mlines.Line2D([], [], color='green', marker=markers.CARETUP, linestyle='None', markersize=7, label='Rep. member real action')] # type: ignore # ... and for the action
        plt.legend(handles=legend_elements, framealpha=0.8, fontsize=self.spec.legend_fs, loc='center left', bbox_to_anchor=(1, 0.5))
        