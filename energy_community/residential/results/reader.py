

import os
from typing import Dict

from energy_community.residential.core.residential_parameters import ResidentialParameters


class ResultsReader():
    
    def __init__(self, scenario_id, folder_id: int, paths: Dict[str, str], scenario_config_file_name):
        
        self.paths = paths
        self.scenario_id = scenario_id
        self.folder_id = folder_id
        self.scenario_config_file_name = scenario_config_file_name
        
        self.folder_path = self.get_results_folder_path()
        self.init_file_paths()
        
    
    def get_results_folder_path(self):
        folder_path = os.path.join(self.paths["root_folder_name"], self.paths["community_folder_name"], self.paths["results_folder_name"], f"Scenario_{self.folder_id}")
        
        return folder_path
    
    def init_file_paths(self):
        
        self.hourly_com_results_full_path = os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_com_hourly.csv")
        self.agent_results_full_path = os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_agents.csv")
        self.hourly_cm_results_full_path = os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_cm_hourly.csv")
        self.cm_profiles_path =  os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_cm_profiles.csv")
        self.manager_profile_path =  os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_manager_profile.csv")
        self.daily_periods_path = os.path.join(self.folder_path, f"Scenario_{self.scenario_id}_com_periods_daily.csv")
        
    def get_simulation_paramters(self):
        
        params = ResidentialParameters(self.paths["root_folder_name"], self.paths["community_folder_name"], self.paths["data_folder_name"], self.scenario_config_file_name, self.scenario_id, False) # define parameters
        if params.stages is None:
            raise ValueError("Stages not set from scenario file!")
        
        return params