from typing import List, Union
from mesa.time import StagedActivation
from energy_community.core.entities.community import EnergyCommunityModel
from energy_community.core.constants import Rec, Stages
import plotly.graph_objs as go
from plotly.subplots import make_subplots
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from energy_community.residential.core.manager import ResidentialCommunityManager
    from energy_community.residential.core.member import ResidentialCommunityMember

class ResidentialCommunity(EnergyCommunityModel):
    """
    A class representing a residential community.

    Parameters:
        sim_params (SimulationParameters): The simulation parameters.
        results_folder_path (str): The path to the folder where simulation results will be saved.
        community_type (Stages): The type of the community (two stages or three stages).

    Attributes:
        schedule (StagedActivation): The schedule for activating the stages of the community.
        _community_manager (Union['ResidentialCommunityManager', None]): The manager of the residential community.
        days_passed (int): The number of days passed since the start of the simulation. 
    """

    def __init__(self, sim_params, results_folder_path: str, community_type: Stages):
        """
        Initializes a ResidentialCommunity object with the given simulation parameters, results folder path and community type.

        Parameters:
            sim_params (SimulationParameters): The simulation parameters.
            results_folder_path (str): The path to the folder where simulation results will be saved.
            community_type (Stages): The type of the community (two stages or three stages).
        """
        super().__init__(sim_params, results_folder_path)

        # Define the schedule for activating the stages based on the community type
        if community_type == Stages.TWO_TURNS:
            self.schedule = StagedActivation(self, ['first_stage', 'second_stage', 'third_stage'])
        else:
            self.schedule = StagedActivation(self, ['first_stage', 'second_stage'])

        # Initialize the community manager to None
        self._community_manager: Union['ResidentialCommunityManager', None] = None

        # Set the number of days passed to -1 to indicate that the simulation hasn't started yet
        self.days_passed = -1

    @property
    def community_manager(self) -> 'ResidentialCommunityManager':
        """
        Gets the community manager.

        Returns:
            ResidentialCommunityManager: The community manager.

        Raises:
            ValueError: If the community manager has not been set, raises an error.
        """
        if self._community_manager is None:
            raise ValueError("Community manager not set!")
        return self._community_manager

    @community_manager.setter
    def community_manager(self, community_manager: 'ResidentialCommunityManager'):
        """
        Sets the community manager according to the specification in the scenario_description file.

        Parameters:
            community_manager (ResidentialCommunityManager): The community manager to be assigned.

        Raises:
            ValueError: If the community manager is not set, raises an error.
        """
        # Assign the community manager to the object
        self._community_manager = community_manager

        # Check if the community manager is None, and raise an error if it is
        if self._community_manager is None:
            raise ValueError("Community manager not set!")

        # Add the community manager to the schedule
        self.schedule.add(self._community_manager)

    def add_new_community_members(self, new_community_members: List['ResidentialCommunityMember']): # type: ignore
        """
        Adds new community members to the model.

        Parameters:
            new_community_members (List['ResidentialCommunityMember']): The list of residential community members to be added.

        Raises:
            ValueError: If the community manager is not assigned yet, raises an error.
        """
        # Check if the community manager is assigned, and raise an error if it is not
        if self.community_manager is None:
            raise ValueError("Community manager not assigned yet!")

        # Add each community member to the schedule and to the community manager's member list
        for cm in new_community_members:
            self.schedule.add(cm)
            self.community_manager.cm_list.append(cm)

            # Print the member's info if the 'print_member_info' parameter is True
            if self.sim_params.print_member_info:
                print(f"Agent {cm.unique_id} added to the community as a {cm.type} agent")

    def step(self):
        """
        Advances the model by one step.
        """
        # Update the current time based on the total time frame and the current hour index
        self.current_time = self.tf_total[self.current_hour_index]

        # Check if it's the beginning of the day, and update the day-related hour index and the number of days passed
        if self.current_hour_index in self.start_of_day_end_of_day_index_map:
            self.current_hour_index_day_relative = 0
            self.days_passed += 1
            self.set_timeframe_current_day() # set the time frame current day

        # Print the current simulation time if the 'print_member_info' parameter is True
        if self.sim_params.print_member_info:
            print(f"Sim dt: {self.tf_total[self.current_hour_index]}, sim h index: {self.current_hour_index}, sim h day-relative index: {self.current_hour_index_day_relative}")

        # Step the schedule
        self.schedule.step()

        # Update the current hour index and the day-related hour index
        self.current_hour_index += 1
        self.current_hour_index_day_relative += 1


    def show_results_plot_in_browser(self):
        """
        After a simulation, plot a comparison between the results in the memory.

        Raises:
            ValueError: If the manager is not set, raises an error.
        """
        # Check if the community manager is set, and raise an error if it is not
        if self.community_manager is None:
            raise ValueError("Manager not set!")

        # Create a subplot with shared x-axis for the production, simulated consumption and estimated consumption plots
        fig = make_subplots(rows=2, cols=1, shared_xaxes=True)

        # Add the estimated PV production plot to the first row of the subplot
        fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=list(self.community_manager.est_com_prod_total.data.values()), name='estimated PV production', marker_color='red'), row=1, col=1)

        # Add the simulated consumption plot to the first row of the subplot
        fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=list(self.community_manager.sim_com_load_total.data.values()), name='real community consumption', marker_color='green'), row=1, col=1)

        # Add the estimated consumption plot to the first row of the subplot
        fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=list(self.community_manager.est_com_load_total.data.values()), name='estimated community consumption', marker_color='blue'), row=1, col=1)

        # Define a map between recommendation codes and the assigned colors
        colors = {Rec.BLINKING_RED: 'magenta', Rec.STRONG_RED: 'darkred', Rec.RED: 'red', Rec.WHITE: 'white', Rec.GREEN: 'green', Rec.STRONG_GREEN: 'darkgreen', Rec.BLINKING_GREEN: 'darkcyan'}

        # Define a map between the colors and the corresponding legend texts
        legend_map = {'red': 'rec. to decrease consumption', 'white': 'no rec.', 'green': 'rec. to increase consumption', 'darkgreen': 'rec. to strongly increase', 'darkred': 'rec. to strongly decrease', 'darkcyan': 'two turn rec. to increase', 'magenta': 'two turn rec. to decrease'}

        # Plot the recommendations for each color code in the second row of the subplot
        for color_code, plot_color in colors.items():
            fig.add_trace(go.Scatter(x=list(self.tf_total.values()), y=[color_code.value if c == color_code else None for i,c in self.community_manager.req_manager_rec_total.data.items()], mode='markers', name=f'{legend_map[plot_color]}', marker_color=plot_color, marker_size=20, showlegend=True), row=2, col=1)

        # Set the plot properties
        fig.update_layout(title=f"Residential community", yaxis=dict(title='Power [W]'), yaxis2=dict(type='category', title='Recommendation type'))

        # Show the plot in a new browser window
        fig.show()