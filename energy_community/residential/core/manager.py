from datetime import datetime
import os
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Union

import numpy as np
from energy_community.core.data.collective_data import CollectiveDataDay, CollectiveDataTotal
from energy_community.core.data.member_data import MemberDataDay, MemberDataTotal
from energy_community.core.energy.PV_model import PVModel
from energy_community.core.entities.manager import CommunityManager
from energy_community.core.simulation.Period import Period
from energy_community.core.constants import POS_ACTIONS_MAP, POS_STATES_MAP, AsignType, DSType, DType, ExpectancyDev, MType, PType, PossibleActions, PossibleStates, Rec, Strategy
import pandas as pd
from energy_community.helpers.indicators import contribution_ratio

from energy_community.residential.core.classification_system import ClassificationSystem
from energy_community.residential.core.recommendation_system import RecommendationSystem

if TYPE_CHECKING:
    from energy_community.residential.core.community import ResidentialCommunity
    from energy_community.residential.core.member import ResidentialCommunityMember
    
class ResidentialCommunityManager(CommunityManager):
    """Class for managing a residential community.

    Args:
        CommunityManager (type): Inherits the Community Manager abstract class.
    """
    
    def __init__(self, pv_surface, model, manager_stages, target_of_recommendations, expectancy_development, strategy, privacy_level, no_alert_threshold, rec_ratios: Tuple[float,float,float,float,float], is_known_community:bool, bw_model_tol: float, bw_conv_tol: float):
        """
        Initializes a ResidentialCommunityManager object.

        Args:
            pv_surface (float): the surface of the PV plant
            model (ResidentialCommunity): the residential community model to which the manager is assigned
            manager_stages (List[Stages]): the manager's stages of activation
            target_of_recommendations (List[AsignType]): the types of members targeted by the manager's recommendations
            expectancy_development (Dict[Rec, List[float]]): expected development of recommendations
            strategy (Union[str, None]): the manager's strategy, if any
            privacy_level (Union[str, None]): the manager's privacy level, if any
            no_alert_threshold (float): the manager's no alert threshold
            rec_ratios (Tuple[float, float, float, float, float]): ratios for the manager's recommendations
            is_known_community (bool): if the managere knows the profile of each community member
            bw_model_tol (float): baum welch model tolerance; used for classifying members
            bw_conv_tol (float): baum welch convergence tolerance; used for baum welch algorithm convergence 

        Attributes:
            _com_model (ResidentialCommunity): the residential community model to which the manager is assigned
            _cm_list (List[ResidentialCommunityMember]): the list of community members
            sim_com_load_day (CollectiveDataDay): simulated community consumption profiles for the current day
            sim_com_load_total (CollectiveDataTotal): simulated community consumption profiles for the entire simulation
            sim_com_prod_day (CollectiveDataDay): simulated community production profiles for the current day
            sim_com_prod_total (CollectiveDataTotal): simulated community production profiles for the entire simulation
            est_com_load_day (CollectiveDataDay): estimated community consumption profiles for the current day
            est_com_load_total (CollectiveDataTotal): estimated community consumption profiles for the entire simulation
            est_com_prod_day (CollectiveDataDay): estimated community production profiles for the current day
            est_com_prod_total (CollectiveDataTotal): estimated community production profiles for the entire simulation
            req_manager_rec_day (MemberDataDay): recommendations assigned to the member for the current day
            req_manager_rec_total (MemberDataTotal): recommendations assigned to the member for the entire simulation
            flexibility_period (Period): the period of flexibility for the community
            periods_current_day (Dict[PType, Period]): the periods of the current day
            period_names_list (List[PType]): the names of the periods in the order they appear
            sim_periods_total (Dict[PType, List[Dict]]): the total number of periods for each type
            optimal_recs_by_period (Dict[str, List[Rec]]): the optimal recommendations for each period
            delta_power_by_recommendation (Dict[float, int]): the change in power for each recommendation
            rec_ratios (Tuple[float, float, float, float, float]): the ratios for the manager's recommendations
            possible_recs_dynamic (List[Rec]): the possible recommendations for dynamic strategies
            know_community (bool): whether or not the manager knows the community
        """
        super().__init__(99999, model, manager_stages, target_of_recommendations, expectancy_development, strategy, privacy_level, no_alert_threshold)
        
        # Assign the residential community model and initialize the list of community members
        self._com_model : ResidentialCommunity = model  # type: ignore
        self._cm_list : List[ResidentialCommunityMember] = [] # the list of community members
        
        # Initialize the data profiles for consumption and production
        self.sim_com_load_day, self.sim_com_load_total = CollectiveDataDay(DSType.SIMULATED, DType.CONSUMPTION, AsignType.COMMUNITY), CollectiveDataTotal(DSType.SIMULATED, DType.CONSUMPTION, AsignType.COMMUNITY) # simulated community consumption profiles
        self.sim_com_prod_day, self.sim_com_prod_total = CollectiveDataDay(DSType.SIMULATED, DType.PRODUCTION, AsignType.COMMUNITY), CollectiveDataTotal(DSType.SIMULATED, DType.PRODUCTION, AsignType.COMMUNITY) # simulated community production profiles - needed for dynamic strategy
        self.est_com_load_day, self.est_com_load_total = CollectiveDataDay(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.COMMUNITY), CollectiveDataTotal(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.COMMUNITY) # estimated community consumption profiles
        self.est_com_prod_day, self.est_com_prod_total = CollectiveDataDay(DSType.ESTIMATED, DType.PRODUCTION, AsignType.COMMUNITY), CollectiveDataTotal(DSType.ESTIMATED, DType.PRODUCTION, AsignType.COMMUNITY) # estimated community production profiles

        # Initialize the recommendation profiles
        self.req_manager_rec_day, self.req_manager_rec_total = MemberDataDay(DSType.REQUESTED, DType.RECOMMENDATIONS, AsignType.MANAGER), MemberDataTotal(DSType.REQUESTED,DType.RECOMMENDATIONS, AsignType.MANAGER) # recommendations assigned to the member for the current day
        
        # Initialize PV model
        self.pv_model = PVModel(self.com_model, pv_surface)
        
        # Get the esitmated production for the simulation period
        self.est_com_prod_total.data = self.pv_model.get_estimated_production_for_community_in_Grenoble()
        
        # Initialize the recommendation system
        self.recommendation_system: RecommendationSystem = RecommendationSystem(self, self.com_model)
        
        # Initialize the classification system
        self.classification_system = ClassificationSystem(self, bw_model_tol, bw_conv_tol)
        
        # Define the flexibility period
        self.flexibility_period = Period(PType.FLEXIBILITY, 7, 23)
        
        # Initialize the periods
        self.periods_current_day : Dict[PType, Period] = {}
        self.period_names_list = [PType.MORNING, PType.MIDDAY1, PType.MIDDAY2, PType.EVENING, PType.PEAKPROD] # the order matters: peak production is last to be an inclusive interval
        self.sim_periods_total = {}
        for period_name in self.period_names_list:
            self.sim_periods_total[period_name] = []
        self.optimal_recs_by_period = {}
        self.optimal_recs_by_time_by_cm = {}
        
        
        # Define the change in power for each recommendation
        self.delta_power_by_recommendation = {-1: -600, -0.5: -300, 0: 0, 0.5: 300, 1: 600} # change in power by recommendation
        
        # Initialize the ratios and possible recommendations for dynamic strategies
        self.rec_ratios = rec_ratios
        
        # Initialize the knowledge of the community
        self.know_community = is_known_community
        
        # Time used in splitting observation sequences between training and testing
        self.testing_data_start_time = None

    
    @property
    def cm_list(self) -> List['ResidentialCommunityMember']:
        """Returns the community member list.

        Returns:
            _type_: a list of community members assigned to the model.
        """
        return self._cm_list
    
    @cm_list.setter
    def cm_list(self, community_member_list: List['ResidentialCommunityMember']):
        """Sets the cm list of the model with the given argument.

        Args:
            community_member_list (List[&#39;ResidentialCommunityMember&#39;]): A list of residential community members.
        """
        self._cm_list = community_member_list
        
    @property
    def com_model(self) -> 'ResidentialCommunity':
        """Returns the residential community model.

        Returns:
            ResidentialCommunity: the residential community model to which the manager is assigned.
        """
        return self._com_model
    
    @com_model.setter
    def com_model(self, residential_community_model: 'ResidentialCommunity'):
        """Sets the community model of the manager.

        Args:
            residential_community_model (ResidentialCommunity): A residential community model.
        """
        self._com_model = residential_community_model
        
    def update_manager(self, energy_models: Tuple[CollectiveDataTotal, CollectiveDataTotal, CollectiveDataTotal], members: List['ResidentialCommunityMember']):
        """Updates the manager energy models.

        Args:
            energy_models (tuple[CollectiveDataTotal, CollectiveDataTotal, CollectiveDataTotal]): a tuple with the following configuration: (simulated consumption, estimated consumption, estimated production)
            members (list[&#39;ResidentialCommunityMember&#39;]): a list of residential community members
        """
        self.sim_com_load_total = energy_models[0]
        self.est_com_load_total = energy_models[1]
        self.est_com_prod_total = energy_models[2]
        self.com_model.add_new_community_members(members)
        
    def set_member_requested_consumption_levels(self):
        pass
    
    def init_manager(self):
        """Initialize the manager at the beginning of each day"""
        
        self.est_com_prod_day.data = {val : self.est_com_prod_total.data[val] for _,val in self.com_model.tf_day.items() } # sets the production for the current day
        self.sim_com_prod_day.data = self.est_com_prod_day.data
        member_est_load_day_models = [member.est_member_load_day for member in self.cm_list] # get estimated consumption for each member
        self.est_com_load_day.update_data_with_other_models(member_est_load_day_models) # update the community estimated consumption
        
        if self.strategy in [Strategy.COACHING, Strategy.OPTIMAL_COACHING]:
            production = list(self.est_com_prod_day.data.values())

            # Adapt the midday period to take into account only the PV production period
            non_zero_production_indexes_list = [index for index in range(len(production)) if production[index] > 0] # type: ignore

            peak_production_indexes_list = self.get_peak_production_period()
            # If the peak period is represented by only one sample, we ignore it
            if len(peak_production_indexes_list) > 0:
                self.peak_production_current_day = True
                if self.com_model.sim_params.print_member_info:
                    print("Peak production period:", peak_production_indexes_list)
            else:
                peak_production_indexes_list = []
                self.peak_production_current_day = False
                if self.com_model.sim_params.print_member_info:
                    print("No peak production period")

            self.set_periods_for_current_day(non_zero_production_indexes_list, peak_production_indexes_list)

            # Print some data
            if self.peak_production_current_day and self.com_model.sim_params.print_member_info:
                print(f"AEM Ready (current day production data from {non_zero_production_indexes_list[0]} to {non_zero_production_indexes_list[-1]})")

            if self.com_model.sim_params.print_member_info:
                print("------------------------------------")
                print("AEM HAS SET THE PERIODS OF THE DAY:")
                for name in self.period_names_list:
                    print(self.period_names_list)
                    print(f'{self.periods_current_day[name].type} : h_min = {self.periods_current_day[name].hour_min} h_max = {self.periods_current_day[name].hour_max}')
                print("------------------------------------")
    
    def get_peak_production_period(self) -> List[int]:
        """
        Returns the starting time and ending time for the peak production period of the current day.
        The method calculates the peak production of the current day and checks if the production from the previous and next
        sample is at least 80% of the peak value. If it is, the method includes that sample in the peak period.

        :return: A list of the index values for the start and end of the peak production period in the current day's PV production
            DataFrame.
        :rtype: list[int]
        """
        peak_production_indexes_list = []
        peak_production_int_index : int = 0
        # Get production peak in the day
        production = list(self.est_com_prod_day.data.values())
        peak_production_value = max(list(self.est_com_prod_day.data.values())) # type: ignore
        if peak_production_value:
            # Get index value of production peak
            for i in range(len(production)):
                if production[i] == peak_production_value:
                    peak_production_int_index = i

            # Check if production from previous sample is bigger then 80% of peak value
            if production[peak_production_int_index - 1] > 0.8 * peak_production_value:
                peak_production_indexes_list.append(peak_production_int_index - 1)

            peak_production_indexes_list.append(peak_production_int_index)
            # Check if production from next sample is bigger then 80% of peak value
            if production[peak_production_int_index + 1] > 0.8 * peak_production_value:
                peak_production_indexes_list.append(peak_production_int_index + 1)

        return peak_production_indexes_list
    
    
    def set_periods_for_current_day(self, non_zero_production_indexes_list: List[int], peak_production_indexes_list: List[int]):
        """Sets the periods for the current day based on the non-zero production indexes and peak production indexes.

        Arguments:
        - non_zero_production_indexes_list (list): A list of index values for the non-zero production periods in the current day's PV production DataFrame.
        - peak_production_indexes_list (list): A list of index values for the start and end of the peak production period in the current day's PV production DataFrame.

        Returns: None
        """

        # Set the MORNING period from the start of the day until the start of the first non-zero production period
        self.periods_current_day[PType.MORNING] = Period(PType.MORNING, 7, non_zero_production_indexes_list[0])

        # Set the EVENING period from the end of the last non-zero production period until the end of the day
        self.periods_current_day[PType.EVENING] = Period(PType.EVENING, non_zero_production_indexes_list[-1], 23)

        # Set the minimum and maximum hours of the flexibility period
        self.flexibility_period.hour_min = 7
        self.flexibility_period.hour_max = 23

        # If the start of the first non-zero production period is before the flexibility period's minimum hour,
        # set the MIDDAY1 period from the start of the day until the start of the peak production period
        if non_zero_production_indexes_list[0] < self.flexibility_period.hour_min: 
            #print("AA")
            self.periods_current_day[PType.MIDDAY1] = Period(PType.MIDDAY1, 7, peak_production_indexes_list[0])
        # Otherwise, set the MIDDAY1 period from the start of the first non-zero production period until the start of the
        # peak production period
        else:
            self.periods_current_day[PType.MIDDAY1] = Period(PType.MIDDAY1, non_zero_production_indexes_list[0], peak_production_indexes_list[0])

        # Set the PEAKPROD period from the start of the peak production period until the end of the peak production period
        self.periods_current_day[PType.PEAKPROD] = Period(PType.PEAKPROD, peak_production_indexes_list[0], peak_production_indexes_list[-1])

        # Set the MIDDAY2 period from the end of the peak production period until the end of the last non-zero production period
        self.periods_current_day[PType.MIDDAY2] = Period(PType.MIDDAY2, peak_production_indexes_list[-1], non_zero_production_indexes_list[-1])
        
                
        #for period, val in self.periods_current_day.items():
        #    print(f"{period} {val.hour_min} {val.hour_max}") 

    
    def get_color_index_delta_powers(self) -> Union[Dict[int, List[float]],None]:
        """Computes recommendations for the one turn dynamic approach with basic expectancy"""

        # Get required data - consumption and production until now
        consumption_until_now : List[float] = [load for time, load in self.sim_com_load_total.data.items() if self.flexibility_period.hour_min <= time.hour <= self.flexibility_period.hour_max ]
        production_until_now : List[float] = [prod for time, prod in self.sim_com_prod_total.data.items() if self.flexibility_period.hour_min <= time.hour <= self.flexibility_period.hour_max ]

        # Set delta powers from the simulation until now
        delta_powers = [production_until_now[i] - consumption_until_now[i] for i in range(len(consumption_until_now))]
        delta_powers.extend([-d for d in delta_powers[::-1]]) # extend the list with the all members multiplied by (-1)
        delta_powers.sort() # sort in ascending order; note that recommendations should go from red to green

        if self.com_model.days_passed > 7: # several days are needed so delta powers increases in size

            # Based on ratios, determine the number of delt powers per recommendation until now
            # This is to cluster the delta powers between recommendations, mataining a certain ratio
            number_of_delta_powers_per_rec = [int(color_ratio * len(delta_powers)) for color_ratio in self.rec_ratios]
            number_of_delta_powers_per_rec[int((2 * len(self.recommendation_system.possible_recs_dynamic) - 1) / 2)] += len(delta_powers) - sum(number_of_delta_powers_per_rec)

            # Define a dictionary with indexes starting from strong decrease in consumption (-1) and going to strong increase (+1)
            rec_index_deltas_powers : Dict[int, List[float]] = {i: [] for i in range(len(self.recommendation_system.possible_recs_dynamic))}
            color_index = 0
            rec_index_deltas_powers[0] = list()
            for i in range(len(delta_powers)):
                # Assign delta powers according to the recommendation and previous ratios
                if len(rec_index_deltas_powers[color_index]) < number_of_delta_powers_per_rec[color_index]:
                    rec_index_deltas_powers[color_index].append(delta_powers[i])
                else:
                    color_index += 1
                    rec_index_deltas_powers[color_index].append(delta_powers[i])

            return rec_index_deltas_powers
        else:
            return None
   

    def communicate_actions_to_cms(self) -> None:
        """Communicate recommended hours to commute to each community member.

        Raises:
            ValueError: if the list of community members is empty.
        """
        
        # Dictionary that maps category types to recommendation types
        # The keys are in the form 'cat_{type}_{strength}' and the values are Rec constants
       
        OPTIMAL_RECOMMENDATION_MAP = {}
        for type in MType:
            OPTIMAL_RECOMMENDATION_MAP[type] = {f'cat_{type}_strong_dec': Rec.STRONG_RED, 
                                                f'cat_{type}_strong_inc': Rec.STRONG_GREEN, 
                                                f'cat_{type}_mod_dec': Rec.RED, 
                                                f'cat_{type}_mod_inc': Rec.GREEN, 
                                                f'cat_{type}_same': Rec.WHITE}
            
        CM_OPTIMAL_RECOMMENDATION_MAP = {}
        for cm in self.cm_list:
            CM_OPTIMAL_RECOMMENDATION_MAP[cm.unique_id] = {f'cm_{cm.unique_id}_{Rec.STRONG_RED}': Rec.STRONG_RED, 
                                            f'cm_{cm.unique_id}_{Rec.STRONG_GREEN}': Rec.STRONG_GREEN, 
                                            f'cm_{cm.unique_id}_{Rec.RED}': Rec.RED, 
                                            f'cm_{cm.unique_id}_{Rec.GREEN}': Rec.GREEN, 
                                            f'cm_{cm.unique_id}_{Rec.WHITE}': Rec.WHITE}
        
        # Check if there are any community members to communicate with
        if len(self.cm_list) == 0:
            raise ValueError("No community members to commuicate with!")
        
        #if self.strategy in [ Strategy.ADAPTIVE_OPTIMAL_COACHING, Strategy.OPTIMAL_COACHING] and self.know_community:
        if self.strategy  == Strategy.OPTIMAL_COACHING and self.know_community:
            
            if not self.optimal_recs_by_period:
                for member in self.cm_list:
                    member.sim_member_rec_day.data = self.req_manager_rec_day.data
            else:
            
                # Iterate through each period of the current day
                for period_name, period in self.periods_current_day.items():
                    
                    # Initialize the final recommendation for the period to None
                    rec_for_period_final = None 
                    filtered_period_recomendation = None
                    
                    # Iterate through each community member
                    for cm in self.cm_list:
                        
                        # Get the optimal recommendation dictionary for each community member during the period
                        rec_for_period_raw = self.optimal_recs_by_period[period_name][cm.unique_id] 
                        
                        #if self.strategy == Strategy.OPTIMAL_COACHING:
                            # Filter the recommendation dictionary to only include recommendations for the community member's category type
                        #    filtered_period_recomendation = {key: OPTIMAL_RECOMMENDATION_MAP[cm.type][key] for key,var in rec_for_period_raw.items() if var.varValue == 1 and str(cm.type) in key} # get specifically the optimal recommendation
                        #else:
                        filtered_period_recomendation = {key: CM_OPTIMAL_RECOMMENDATION_MAP[cm.unique_id][key] for key,var in rec_for_period_raw.items() if var.varValue == 1 and str(cm.unique_id) in key} # get specifically the optimal recommendation
                        
                        if filtered_period_recomendation:
                            # Get the first recommendation from the filtered recommendation dictionary
                            rec_for_period_final = list(filtered_period_recomendation.values())[0]
                        else:
                            rec_for_period_final = Rec.WHITE

                        # Iterate through the day
                        for time, _ in cm.sim_member_rec_day.data.items():
                            
                            # Check if the time falls within the current period
                            if period.hour_min <= time.hour <= period.hour_max:
                                
                                # Assign the period recommendation to the community member's simulated recommendation data
                                cm.sim_member_rec_day.data[time] = rec_for_period_final 
            
        
        # If the strategy is not optimal coaching or the community members are unknown                    
        elif self.expectancy_development == ExpectancyDev.ADAPTIVE and self.strategy == Strategy.OPTIMAL_INFORMATIVE and self.optimal_recs_by_time_by_cm:
            # Iterate through each community member
            for cm in self.cm_list:
                # Iterate through the day
                for time, _ in cm.sim_member_rec_day.data.items():
                    if self.flexibility_period.hour_min <= time.hour <= self.flexibility_period.hour_max:
                        cm.sim_member_rec_day.data[time] = self.optimal_recs_by_time_by_cm[time][cm.unique_id]
                    else:
                        cm.sim_member_rec_day.data[time] = Rec.WHITE
        
        else:
            
            # Set the simulated recommendation data of each community member to the request manager's recommendation data
            for member in self.cm_list:
                member.sim_member_rec_day.data = self.req_manager_rec_day.data
            
    def record_data_members_day(self):
        """Update member total data models, according to day data models."""
        
        for member in self.cm_list:
            member.sim_member_load_total.update_data_with_other_models(member.sim_member_load_day) 
            member.sim_member_actions_total.update_data_with_other_models(member.sim_member_actions_day)
            member.est_member_actions_total.update_data_with_other_models(member.est_member_actions_day)
            member.sim_member_rec_total.update_data_with_other_models(member.sim_member_rec_day)
            member.est_member_load_total.update_data_with_other_models(member.est_member_load_day) 
            member.sim_member_states_total.update_data_with_other_models(member.sim_member_states_day)
            
            member.sim_member_followed_total.update_data_with_other_models(member.sim_member_followed_day)
        
    def record_data_community_day(self):
        """Update community total data models, according to day data models."""
        
        for period_name, period in self.periods_current_day.items():
            self.sim_periods_total[period_name].append(period) # update the total period buffer for each period during the current day
            
        self.req_manager_rec_total.update_data_with_other_models(self.req_manager_rec_day) # update with recommendations for the current day
        self.sim_com_load_day.update_data_with_other_models([member.sim_member_load_day for member in self.cm_list]) # get the simulated consumption for the day
        self.sim_com_load_total.update_data_with_other_models(self.sim_com_load_day) # update the total simulated consumption
        self.est_com_load_day.update_data_with_other_models([member.est_member_load_day for member in self.cm_list]) # get the estimated consumption for the day
        self.est_com_load_total.update_data_with_other_models(self.est_com_load_day) # update the total estimated consumption
        self.sim_com_prod_total.update_data_with_other_models(self.sim_com_prod_day) # update the simulated production with day production
        
    def write_cm_hourly_results_simulation_to_csv(self):
        """Write cm data models to csv"""
        
        hourly_data = {}    
        hourly_data['sim_time'] = list(self.com_model.tf_total.values()) # get the time frame
        for member in self.cm_list:
            data_models  = [member.est_member_load_total, member.sim_member_load_total, member.sim_member_rec_total, member.sim_member_actions_total, member.sim_member_states_total] # get the data models
            for model in data_models:
                hourly_data[f'{member.unique_id}_{member.type}_{model.data_source_type}_{model.data_type}_{model.asignee_type}_{model.interval}'] = list(model.data.values()) # assign the data models to a representative key
                
        df = pd.DataFrame(hourly_data) 
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_cm_hourly.csv"))
    
    def write_com_daily_periods_to_csv(self):
        daily_data = {}
        daily_data['sim_day'] =[time for _,time in self.com_model.tf_total.items() if time.hour == 0]
        for period_name in self.sim_periods_total:
            daily_data[period_name] = [(period.hour_min, period.hour_max) for period in self.sim_periods_total[period_name]]
        df = pd.DataFrame(daily_data)
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_com_periods_daily.csv"))
              
    def write_com_hourly_results_simulation_to_csv(self):  
        """Write community data models to csv"""
         
        hourly_data = {}     
        hourly_data['sim_time'] = list(self.com_model.tf_total.values()) # get the time frame
        energy_models  = [self.est_com_load_total, self.sim_com_load_total, self.est_com_prod_total] # get the data models
        for model in energy_models:
            hourly_data[f'{model.data_source_type}_{model.data_type}_{model.asignee_type}_{model.interval}'] = list(model.data.values()) # assign the data models to a representative key
        
        df = pd.DataFrame(hourly_data)
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_com_hourly.csv"))
        
    def write_cm_profiles_to_csv(self):
        """Get a description of each member profile in the community and write them to csv for analysis."""
        
        temp_dict = {}
        temp_dict['unique_id'] = [member.unique_id for member in self.cm_list]
        temp_dict['description'] = [member.description for member in self.cm_list]
        temp_dict['type'] = [member.type for member in self.cm_list]
        temp_dict['stages'] = [member.member_stages for member in self.cm_list]
        temp_dict['action_intensity'] = [member.member_action_intensity for member in self.cm_list]
        temp_dict['willigness_type'] = [member.willigness_type for member in self.cm_list]
        temp_dict['availability'] = [member.availability for member in self.cm_list]

        for (state, action) in [(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW),
                                (PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW),
                                (PossibleStates.INNATENTIVE, PossibleActions.FOLLOW),
                                (PossibleStates.INNATENTIVE, PossibleActions.NOT_FOLLOW)]:       
                temp_dict[f'{POS_STATES_MAP[state]}, {POS_ACTIONS_MAP[action]}'] = []
        
        for cm in self.cm_list:
            for (state, action) in cm.true_emit_expl:
                temp_dict[f'{POS_STATES_MAP[state]}, {POS_ACTIONS_MAP[action]}'] += [cm.true_emit_expl[(state, action)]]
                    
        df = pd.DataFrame(temp_dict)
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_cm_profiles.csv"))
        
    def write_manager_profile_to_csv(self):
        """Get a description of the manager profile in the community and write it to csv for analysis."""
         
        temp_dict = {}
        temp_dict['pv_surface'] = self.pv_model.pv_surface
        temp_dict['stages'] = self.manager_stages
        temp_dict['target_of_recommendations'] = self.target_of_recommendations
        temp_dict['expectancy_development'] = self.expectancy_development
        temp_dict['strategy'] = self.strategy
        temp_dict['privacy_level'] = self.privacy_level
        temp_dict['no_alert_threshold'] = self.no_alert_threshold
        df = pd.DataFrame(temp_dict, index=[0])
        df.to_csv(os.path.join(self.com_model.results_folder_path,f"Scenario_{self.com_model.sim_params.simulation_id}_manager_profile.csv"))
        
    def set_df_for_plotting(self):
        """Sets a dataframe for plotting in the analysis.

        Raises:
            ValueError: if the estimated consumption of the community is not set, raise an error.
            ValueError: if the simulated consumption of the community is not set, raise an error.
            ValueError: if the estimated production of the community is not set, raise an error.
        """
        if len(self.est_com_load_total.data) == 0:
            raise ValueError("Est community load data not set!")
        if len(self.sim_com_load_total.data) == 0:
            raise ValueError("Sim community load data not set!")
        if len(self.est_com_prod_total.data) == 0:
            raise ValueError("Est com prod data not set!")
        time_frame = [pd.Timestamp(time).tz_localize(None) for time in list(self.com_model.tf_total.values())]

        df = pd.DataFrame({f'{DSType.ESTIMATED}_{DType.CONSUMPTION}': list(self.est_com_load_total.data.values()), f'{DSType.ESTIMATED}_{DType.PRODUCTION}': list(self.est_com_prod_total.data.values()), f'{DSType.SIMULATED}_{DType.CONSUMPTION}': list(self.sim_com_load_total.data.values())}, index=time_frame) 
        self.plotting_df = df
        
    def verify_data(self):
        
        if len(self.est_com_load_total.data) == 0 or len(self.est_com_prod_total.data) == 0 or len(self.sim_com_load_total.data) == 0:
            raise ValueError("Community hourly energy models not initialized!")
        if len(self.cm_list) == 0:
            raise ValueError("CMs not initialized!")
        
        return True
    
    
    def get_trans_emis_matrix_by_cm_by_time(self) -> Tuple[Dict[int, Dict[datetime, Any]], Dict[int, Any]]:
    
        trans_matrix_by_cm_by_time : Dict[int, Dict[datetime, Any]] = {}
        emis_matrix_by_cm : Dict[int, Any] = {}
        
        for cm in self.cm_list:
            trans_matrix_by_cm_by_time[cm.unique_id] = {}
            emis_matrix_by_cm[cm.unique_id] = cm.true_emission_matrix
            for _, time in self.com_model.tf_day.items():
                trans_matrix_by_cm_by_time[cm.unique_id][time] = cm.transition_matrix_by_time[time] #type:ignore
                
        return trans_matrix_by_cm_by_time, emis_matrix_by_cm
    
    def indetify_cms_based_on_contributions_ratio(self):
        
        for cm in self.cm_list:
            cm.identified_type = MType.NORMAL
            #contributions = [1 if x == PossibleActions.FOLLOW else 0 for x in list(cm.sim_member_followed_total.data.values())]
            contrib_ratio = contribution_ratio(self.flexibility_period.hour_min,
                                                     self.flexibility_period.hour_max,
                                                     cm.sim_member_followed_total.data) # type: ignore
            cm.measured_contribution_ratio = contrib_ratio
            if contrib_ratio < 0.4:
                cm.identified_type = MType.GOOD
            if contrib_ratio > 0.6:
                if contrib_ratio == 1:
                    cm.identified_type = MType.IDEAL
                else:
                    cm.identified_type = MType.BAD
