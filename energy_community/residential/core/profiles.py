

import random
from typing import Dict, List, Tuple, Union
import numpy as np
import pandas as pd
from energy_community.core.data.collective_data import CollectiveDataDay


from energy_community.core.constants import AsignType, Availability, DSType, DType, ExpectancyDev, MType, MemberActionIntensity, PossibleActions, PossibleStates, PrivacyLevel, Rec, Stages, Strategy, TargetOfRec, Willigness
from energy_community.residential.core.manager import ResidentialCommunityManager
from energy_community.residential.core.member import ResidentialCommunityMember
from energy_community.residential.core.community import ResidentialCommunity
from scipy.stats import truncnorm # type: ignore



class OneTurnResidentialManager(ResidentialCommunityManager):

    def __init__(self, pv_surface, model: ResidentialCommunity, target_of_recommendations: TargetOfRec, expectancy_development: ExpectancyDev, strategy: Strategy, privacy_level: PrivacyLevel, no_alert_threshold: float, rec_ratios=(.05, .02, .5, .2, .05), is_known_community:bool=True,  bw_model_tol: float = 0.07, bw_conv_tol: float = 0.05):
        super().__init__(pv_surface, model, Stages.ONE_TURN, target_of_recommendations, expectancy_development, strategy, privacy_level, no_alert_threshold, rec_ratios, is_known_community, bw_model_tol, bw_conv_tol)

    def __str__(self):
        if self.cm_list:
            return f"One turn manager with {self.strategy} strategy with: {[cm.type for cm in self.cm_list]} "
        else:
            return f"One turn manager with {self.strategy} strategy."
            
    def first_stage(self):

        if self.com_model.current_hour_index == self.com_model.community_manager.flexibility_period.hour_min + 24 \
            and self.strategy == Strategy.OPTIMAL_INFORMATIVE :
            for cm in self.cm_list:
                if not isinstance(cm, OneTurnResidentialMarkovCommunityMember) :
                    raise ValueError("Not a Markov member!")
                cm.set_transition_matrix_by_time()

        if self.com_model.current_hour_index in self.com_model.start_of_day_end_of_day_index_map and self.cm_list:
            for agent in self.cm_list:
                agent.init_cm_day() # we initialize est_member_load_day for each member
            self.init_manager() # calculate est_com_load_day for the recommendations

            if self.expectancy_development == ExpectancyDev.BASIC:
                if self.strategy == Strategy.DUMMY:
                    self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_dummy_approach()
                if self.strategy == Strategy.INFORMATIVE:
                    self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_one_turn_informative_approach()
                if self.strategy == Strategy.COACHING:
                    self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_coaching_approach()
                if self.strategy == Strategy.DYNAMIC:
                    color_index_delta_powers = self.get_color_index_delta_powers()
                    self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_dynamic_approach(color_index_delta_powers) # type: ignore
            
            #if self.expectancy_development == ExpectancyDev.BASIC and \
            #   self.strategy == Strategy.OPTIMAL_COACHING or self.strategy == Strategy.INFORMATIVE:
                    
            #    if self.know_community and self.strategy == Strategy.OPTIMAL_COACHING:
            #        self.optimal_recs_by_period = self.recommendation_system.get_recommendations_current_day_optimal_coaching()
            #        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_dummy_approach()
            #    elif self.know_community and self.strategy == Strategy.INFORMATIVE:
            #        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_one_turn_informative_approach()
            #    else:
            #        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_one_turn_informative_approach()
            if self.expectancy_development == ExpectancyDev.ADAPTIVE:
                if self.strategy == Strategy.INFORMATIVE:
                    if self.com_model.days_passed > 20:
                        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_adaptive_informative_approach()
                    else:
                        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_one_turn_informative_approach()
                        
                if  self.strategy == Strategy.OPTIMAL_INFORMATIVE:
                    if self.com_model.days_passed > 20:
                        self.optimal_recs_by_time_by_cm = self.recommendation_system.get_recommendations_current_day_adaptive_optimal_informative()
                    else:
                        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_one_turn_informative_approach()
                        
                if self.strategy == Strategy.OPTIMAL_COACHING:
                    if self.com_model.days_passed > 20:
                        self.optimal_recs_by_period = self.recommendation_system.get_recommendations_current_day_adaptive_optimal_coaching()
                        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_dummy_approach()
                    else:
                        self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_coaching_approach()

            self.communicate_actions_to_cms()
     
    def second_stage(self):
        
        # At the end of the day
        if self.com_model.current_hour_index in list(self.com_model.start_of_day_end_of_day_index_map.values()) and self.cm_list:
            self.record_data_members_day()
            self.record_data_community_day()
        
        # At the end of the simulation
        if self.com_model.current_hour_index == list(self.com_model.start_of_day_end_of_day_index_map.values())[-1]:  # in the last day of the simulation, write data to files
            self.write_cm_hourly_results_simulation_to_csv()
            self.write_com_hourly_results_simulation_to_csv()
            if self.strategy == Strategy.COACHING:
                self.write_com_daily_periods_to_csv()
            self.write_cm_profiles_to_csv()
            self.write_manager_profile_to_csv()
            
            if self.know_community == False:
                # Assign the representative member
                self.classification_system.unknown_cm = self.cm_list[0]
                self.classification_system.rep_cms = self.cm_list[1:]
                
                # Train and validate a HMM model to estiamte the probabilities with the baum welch algorithm
                self.classification_system.train_and_test_model_new()

            
            
class TwoTurnsResidentialManager(ResidentialCommunityManager):

    def __init__(self, pv_surface, model: ResidentialCommunity, target_of_recommendations: TargetOfRec, expectancy_development: ExpectancyDev, strategy: Strategy, privacy_level: PrivacyLevel, no_alert_threshold: float, is_known_community:bool=True):
        super().__init__(pv_surface, model, Stages.TWO_TURNS, target_of_recommendations, expectancy_development, strategy, privacy_level, no_alert_threshold, (0,0,0,0,0), is_known_community, 0.0, 0.0)

    def __str__(self):
        if self.cm_list:
            return f"Two turns manager with {self.strategy} strategy with: {[cm.type for cm in self.cm_list]} "
        else:
            return f"Two turns manager with {self.strategy} strategy."
    
    def first_stage(self):
        
        if self.com_model.current_hour_index in self.com_model.start_of_day_end_of_day_index_map and self.cm_list:
            for agent in self.cm_list:
                agent.init_cm_day() # we initialize est_member_load_day for each member
            self.init_manager() # calculate est_com_load_day for the recommendations
        
            if self.strategy == Strategy.INFORMATIVE:
                self.req_manager_rec_day.data = self.recommendation_system.get_recommendations_current_day_basic_two_turns_informative_approach()
            self.communicate_actions_to_cms()
            
    def second_stage(self):
        time = self.com_model.current_time
        
        if self.cm_list and self.req_manager_rec_day.data[time] != Rec.WHITE:
            sim_com_load = CollectiveDataDay(DSType.SIMULATED, DType.CONSUMPTION, AsignType.COMMUNITY)  # simulated community consumption profiles
            sim_com_load.update_data_with_other_models([cm.sim_member_load_day for cm in self.cm_list])
            
            if self.req_manager_rec_day.data[time] == Rec.STRONG_RED and self.est_com_prod_day.data[time] + self.no_alert_threshold < sim_com_load.data[time]: # type: ignore
                self.req_manager_rec_day.data[time] = Rec.BLINKING_RED
            elif self.req_manager_rec_day.data[time] == Rec.STRONG_GREEN and  sim_com_load.data[time] + self.no_alert_threshold <  self.est_com_prod_day.data[time]: # type: ignore
                self.req_manager_rec_day.data[time] = Rec.BLINKING_GREEN
            self.communicate_actions_to_cms()
     

    def third_stage(self):

        if self.com_model.current_hour_index in list(self.com_model.start_of_day_end_of_day_index_map.values()) and self.cm_list:
            self.record_data_members_day()
            self.record_data_community_day()
        if self.com_model.current_hour_index == list(self.com_model.start_of_day_end_of_day_index_map.values())[-1]:  # in the last day of the simulation, write data to files
            self.write_cm_hourly_results_simulation_to_csv()
            self.write_com_hourly_results_simulation_to_csv()
            if self.strategy == Strategy.COACHING:
                self.write_com_daily_periods_to_csv()
            self.write_cm_profiles_to_csv()
            self.write_manager_profile_to_csv()
            

class OneTurnResidentialMarkovCommunityMember(ResidentialCommunityMember):
    """one stage, realistic intensity, no model for willingness (ideal agent), always available"""

    def __init__(self, unique_id: int, 
                 cm_description: str, 
                 cm_type: MType,
                 model, 
                 consumption_df: Union[pd.DataFrame, None], 
                 explicit_emissions :  Union[List[List[float]], None] = None, 
                 explicit_transmission :  Union[List[List[float]], None] = None ):
        super().__init__(unique_id,cm_description, model,  cm_type, Stages.ONE_TURN, MemberActionIntensity.REALISTIC, Willigness.FIXED, Availability.DAY, consumption_df, 
                         model_type="Markov", explicit_emissions=explicit_emissions, explicit_transmission=explicit_transmission)

    def first_stage(self):
        
        decision = None
        if self.com_model.community_manager.flexibility_period.hour_min <= self.com_model.current_time.hour <= self.com_model.community_manager.flexibility_period.hour_max:
            
            # Get the previous state index, based on the previous state from the last hour
            previous_state_index = self.previous_state.value - 1
            
            # Get the current state index based on the transition matrix applied to the previous state
            
            current_state_index = np.random.choice([0, 1], 1, p=self.true_transition_matrix[previous_state_index])[0]
            
            # Update current state based on current state index
            self.current_state = PossibleStates(current_state_index + 1)
            
            #state_choice_list = [self.current_state.value] + [state.value for state in PossibleStates if state.value != self.current_state.value ] # possible choices, but the current state is positioned first
            # Record current state
            self.sim_member_states_day.data[self.com_model.current_time] = self.current_state # update the data buffer
            
            
            decision  = True
            if self.consumption_df is None:
                raise ValueError("self.consumption_df not set!")
            
            # Make a decision based on current state and emission probabilities associated to current state
            decision = np.random.choice([True, False], 1, p=self.true_emission_matrix[current_state_index])[0]

            if decision: # Agent follows the recommendation
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 3)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 1.5)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.WHITE:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 1.2)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.5, 1.2)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0, 1.2)
                self.sim_member_actions_day.data[self.com_model.current_time] = self.sim_member_rec_day.data[self.com_model.current_time] # the member follows the recommendation
            
                self.sim_member_followed_day.data[self.com_model.current_time] = PossibleActions.FOLLOW
            else: # Agent doesn't follow the recommendation
                self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] 
                self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE # white means here there is no recommendation given
                
                self.sim_member_followed_day.data[self.com_model.current_time] = PossibleActions.NOT_FOLLOW
                
        else: # outside the flexibility window
            self.sim_member_states_day.data[self.com_model.current_time] = None
            self.sim_member_followed_day.data[self.com_model.current_time] = None
            self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time]
            self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE
        
        
        #print("-----")
        #print("Time: ", self.com_model.current_time)
        #print("Previous member state: ", self.previous_state)
        #print("CURRENT MEMBER STATE: ", self.current_state)
        #print("Current member decision: ", decision)
        #print("Current member action:", self.sim_member_followed_day.data[self.com_model.current_time] )
        #print("-----")

    def second_stage(self):
        
        if self.com_model.community_manager.flexibility_period.hour_min <= self.com_model.current_time.hour <= self.com_model.community_manager.flexibility_period.hour_max:
            self.previous_state = self.current_state # update the current state
            
    def set_transition_matrix_by_time(self):
        
        initial_transition = list(self.sim_member_states_total.data.values())[self.com_model.community_manager.flexibility_period.hour_min]
        init_prob = None
        if initial_transition == PossibleStates.ATTENTIVE:
            init_prob = np.array([1,0])
        else:
            init_prob = np.array([0,1])
        
        self.transition_matrix_by_time = {time: None for _,time in self.com_model.tf_total.items()}
        for i, time in self.com_model.tf_total.items():
            if self.com_model.community_manager.flexibility_period.hour_min <= time.hour <= self.com_model.community_manager.flexibility_period.hour_max:

                t = np.array(self.true_transition_matrix)
                current_transition_matrix = np.dot(init_prob, np.linalg.matrix_power(t, i))
                self.transition_matrix_by_time[time] = current_transition_matrix

            
            
            
class OneTurnResidentialSimpleCommunityMember(ResidentialCommunityMember):
    """one stage, realistic intensity, no model for willingness (ideal agent), always available"""

    def __init__(self, unique_id: int, cm_description: str, cm_type: MType, model, consumption_df: Union[pd.DataFrame, None]):
        super().__init__(unique_id, cm_description, model, cm_type, Stages.ONE_TURN, MemberActionIntensity.REALISTIC, Willigness.FIXED, Availability.DAY, consumption_df, 
                         model_type="Simple", explicit_emissions=None, explicit_transmission=None)

    def first_stage(self):
        
        decision = None
        if self.com_model.community_manager.flexibility_period.hour_min <= self.com_model.current_time.hour <= self.com_model.community_manager.flexibility_period.hour_max:
            
            decision  = True
            if self.consumption_df is None:
                raise ValueError("self.consumption_df not set!")
            
            # Make a decision based on current state and emission probabilities associated to current state
            if self.type == MType.IDEAL:
                decision = True
            elif self.type == MType.GOOD:
                decision = np.random.choice([True, False], 1, p=[0.7, 0.3])[0]
            elif self.type == MType.BAD:
                decision = np.random.choice([True, False], 1, p=[0.3, 0.7])[0]
            elif self.type == MType.NORMAL:
                decision = np.random.choice([True, False], 1, p=[0.5, 0.5])[0]

            if decision: # Agent follows the recommendation
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 3)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 1.5)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.WHITE:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 1.2)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.5, 1.2)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0, 1.2)
                self.sim_member_actions_day.data[self.com_model.current_time] = self.sim_member_rec_day.data[self.com_model.current_time] # the member follows the recommendation
            
                self.sim_member_followed_day.data[self.com_model.current_time] = PossibleActions.FOLLOW
            else: # Agent doesn't follow the recommendation
                self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] 
                self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE # white means here there is no recommendation given
                
                self.sim_member_followed_day.data[self.com_model.current_time] = PossibleActions.NOT_FOLLOW
                
        else: # outside the flexibility window
            self.sim_member_states_day.data[self.com_model.current_time] = None
            self.sim_member_followed_day.data[self.com_model.current_time] = None
            self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time]
            self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE
        

    def second_stage(self):
        pass
    
    
class OneTurnResidentialPresenceCommunityMember(ResidentialCommunityMember):
    """one stage, realistic intensity, no model for willingness (ideal agent), always available"""

    def __init__(self, unique_id: int, cm_description: str, cm_type: MType, model, consumption_df: Union[pd.DataFrame, None] ):
        super().__init__(unique_id, cm_description, model, cm_type, Stages.ONE_TURN, MemberActionIntensity.REALISTIC, Willigness.FIXED, Availability.DAY, consumption_df, 
                         model_type="Presence", explicit_emissions=None, explicit_transmission=None)

    def presence_model(self):
        if self.com_model.current_time.hour < 8 or self.com_model.current_time.hour > 22:
            return False
        if self.com_model.current_time.weekday() <= 5:
            if 8 <= self.com_model.current_time.hour <= 18:
                return random.uniform(0,1) < .35
            else:
                return random.uniform(0,1) < .95
        else:
            if 8 <= self.com_model.current_time.hour <= 18:
                return random.uniform(0,1) < .70
            else:
                return random.uniform(0,1) < .80

    
    def first_stage(self):
        
        if self.com_model.community_manager.flexibility_period.hour_min <= self.com_model.current_time.hour <= self.com_model.community_manager.flexibility_period.hour_max:
            
            if self.consumption_df is None:
                raise ValueError("self.consumption_df not set!")

            is_present = self.presence_model()
            
            if is_present: # Agent is present
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 3)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 1.5)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.WHITE:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 1.2)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.5, 1.2)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0, 1.2)
                self.sim_member_actions_day.data[self.com_model.current_time] = self.sim_member_rec_day.data[self.com_model.current_time] # the member follows the recommendation
            
                self.sim_member_followed_day.data[self.com_model.current_time] = PossibleActions.FOLLOW
            else: # Agent is not present
                self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] 
                self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE # white means here there is no recommendation given
                
                self.sim_member_followed_day.data[self.com_model.current_time] = PossibleActions.NOT_FOLLOW
                
        else: # outside the flexibility window
            self.sim_member_states_day.data[self.com_model.current_time] = None
            self.sim_member_followed_day.data[self.com_model.current_time] = None
            self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time]
            self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE
        

    def second_stage(self):
        pass
   
    
class TwoTurnsResidentialMarkovCommunityMember(ResidentialCommunityMember):
    """reactive, realistic intensity, no model for willingness, always available"""

    def __init__(self, unique_id: int, cm_description: str, cm_type: MType, model, consumption_df: Union[pd.DataFrame, None], 
                 explicit_emissions : Union[List[List[float]], None] = None,
                 explicit_transmission : Union[List[List[float]], None] = None):
        super().__init__(unique_id, cm_description, model, cm_type, Stages.TWO_TURNS, MemberActionIntensity.REALISTIC, Willigness.FIXED, Availability.DAY, consumption_df, 
                         model_type="Markov", explicit_emissions=explicit_emissions, explicit_transmission=explicit_transmission)

    def first_stage(self):
        
        # Get the previous state index, based on the previous state from the last hour
        previous_state_index = self.previous_state.value - 1
        
        # Get the current state index based on the transition matrix applied to the previous state
        
        current_state_index = np.random.choice(len(PossibleStates), 1, p=self.true_transition_matrix[previous_state_index])[0]
        
        # Update current state based on current state index
        self.current_state = PossibleStates(current_state_index + 1)
        
        #state_choice_list = [self.current_state.value] + [state.value for state in PossibleStates if state.value != self.current_state.value ] # possible choices, but the current state is positioned first
        # Record current state
        self.sim_member_states_day.data[self.com_model.current_time] = self.current_state # update the data buffer
        
        if self.com_model.community_manager.flexibility_period.hour_min <= self.com_model.current_time.hour <= self.com_model.community_manager.flexibility_period.hour_max:
            decision: bool = True
            if self.consumption_df is None:
                raise ValueError("self.consumption_df not set!")
            #decision = self.cm_type_decision_map[self.type] # make decision according to cm profile
            #decision = self.DECISION_MAP[(self.type, self.new_state)]() # make a decision accoring to the new state of the cm
            
             # Make a decision based on current state and emission probabilities associated to current state
            decision = np.random.choice([True, False], 1, p=self.true_emission_matrix[current_state_index])[0]
        
        
            if decision: # Agent follows the recommendation
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 3)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.WHITE:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0.8, 1.2)
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.STRONG_RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] * random.uniform(0, 1.2)
                self.sim_member_actions_day.data[self.com_model.current_time] = self.sim_member_rec_day.data[self.com_model.current_time] # the member follows the recommendation
            else: # Agent doesn't follow the recommendation
                self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] 
                self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE # white means here there is no recommendation given
            
        else: # outside the flexibility window
            self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time]
            self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE


    def second_stage(self):
        if self.com_model.community_manager.flexibility_period.hour_min <= self.com_model.current_time.hour <= self.com_model.community_manager.flexibility_period.hour_max:
            decision : bool = True
            if self.consumption_df is None:
                raise ValueError("self.consumption_df not set!")
            
            #decision = self.cm_type_decision_map[self.type] # make decision according to cm profile
            #decision = self.DECISION_MAP[(self.type, self.current_state)]()  # make a decision accoring to the new state of the cm
            
            current_state_index =  self.current_state.value - 1
            # Make a decision based on current state and emission probabilities associated to current state
            decision = np.random.choice([True, False], 1, p=self.true_emission_matrix[current_state_index])[0]
            
            if decision: # Agent follows the recommendation
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.BLINKING_GREEN:
                    self.sim_member_load_day.data[self.com_model.current_time] = truncnorm.rvs(1, 2) * 1.5 * self.sim_member_load_day.data[self.com_model.current_time] # type: ignore
                if self.sim_member_rec_day.data[self.com_model.current_time] == Rec.BLINKING_RED:
                    self.sim_member_load_day.data[self.com_model.current_time] = abs(random.normalvariate(0, .5)) * self.sim_member_load_day.data[self.com_model.current_time] 
                self.sim_member_actions_day.data[self.com_model.current_time] = self.sim_member_rec_day.data[self.com_model.current_time] # the member follows the recommendation
            else: # Agent doesn't follow the recommendation
                self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time] 
                self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE # white means here there is no recommendation given
            
        else: # outside the flexibility window
            self.sim_member_load_day.data[self.com_model.current_time] = self.est_member_load_day.data[self.com_model.current_time]
            self.sim_member_actions_day.data[self.com_model.current_time] = Rec.WHITE
    
    def third_stage(self):
        self.previous_state = self.current_state # update the current state