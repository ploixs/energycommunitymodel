


import random
from typing import TYPE_CHECKING, List, Union
from hmmlearn import hmm
import pandas as pd
from sklearn.metrics import classification_report
from energy_community.core.constants import MType, PossibleActions
from typing import Dict, List, Tuple
from sklearn.metrics import f1_score
import numpy as np
from numpy.linalg import matrix_power

from energy_community.core.constants import PossibleActions, PossibleStates

if TYPE_CHECKING:
    from energy_community.residential.core.community import ResidentialCommunity
    from energy_community.residential.core.manager import ResidentialCommunityManager
    from energy_community.residential.core.member import ResidentialCommunityMember

from typing import Dict, Tuple, List, Any

def split_list(input_list, chunk_size):
    # Using list comprehension to split the list
    return [input_list[i:i + chunk_size] for i in range(0, len(input_list), chunk_size)]


def viterbi(prior: Dict[Any, float], 
            trans: Dict[Tuple[Any, Any], float], 
            emit: Dict[Tuple[Any, Any], float], 
            obs: List[Any]) -> List[Any]:
    """
    Viterbi algorithm for most likely sequence computation.

    Args:
        prior (Dict[Any, float]): Prior probabilities for each state.
        trans (Dict[Tuple[Any, Any], float]): Transition probabilities between states.
        emit (Dict[Tuple[Any, Any], float]): Emission probabilities for each state-action pair.
        obs (List[Any]): List of observed actions.

    Returns:
        List[Any]: The most likely sequence of states given the observed actions.
    """
    import math

    V = [{}]   # Initialize a list for storing the max probability for each state at each time step
    path = {}  # Initialize a dictionary for storing the most likely paths for each state

    # Initialize base cases (t == 0)
    for state in prior:
        # Calculate the max probability for each state at time step 0
        V[0][state] = math.log(prior[state]) + math.log(emit[(state, obs[0])])
        # The most likely path to each state at time step 0 is the state itself
        path[state] = [state]

    # Run Viterbi for t > 0
    for t in range(1, len(obs)):
        V.append({})
        newpath = {}

        for state_to in prior:
            max_prob = float('-inf')
            max_state = None
            for state_from in prior:
                prob = V[t-1][state_from] + math.log(trans.get((state_from, state_to), float('-inf'))) + \
                       math.log(emit.get((state_to, obs[t]), float('-inf')))
                if prob > max_prob:
                    max_prob, max_state = prob, state_from

            V[t][state_to] = max_prob  # Store the max probability for state_to at time step t
            newpath[state_to] = path[max_state] + [state_to]  # Update the most likely path for state_to

        # Don't need to remember the old paths
        path = newpath

    # Get the state with max probability at the final time step
    max_prob = max(V[-1].values())
    max_state = [state for state, prob in V[-1].items() if prob == max_prob][0]

    # Return the most likely path leading to the state with max probability at the final time step
    return path[max_state]

    
def legacy_cluster_cm(best_f1_score, best_model):
    cm_identified_type = MType.NORMAL
    # Type clustering:
    if best_f1_score < 0.55:
        cm_identified_type = MType.NORMAL
    else:
        if best_model.emissionprob_[0][0] > best_model.emissionprob_[0][1]:
            cm_identified_type = MType.GOOD
        elif best_model.emissionprob_[0][0] <= best_model.emissionprob_[0][1]:
            cm_identified_type= MType.BAD
    return cm_identified_type

def cluster_cm( est_percentage_recs_followed: float):
    
    cm_identified_type = MType.NORMAL
    if  est_percentage_recs_followed > 0.6:
        cm_identified_type = MType.GOOD
    elif est_percentage_recs_followed < 0.4:
        cm_identified_type = MType.BAD
        
    return cm_identified_type

def improved_cluster_cm( log_prob: float, known_log_probs_by_description: Dict[MType, float]):
    
    min_dist = 99999
    cm_identified_description = MType.NORMAL
    
    for cm_description, known_log_prob in known_log_probs_by_description.items():
        if  abs(log_prob - known_log_prob) < min_dist:
            min_dist = abs(log_prob - known_log_prob)
            cm_identified_description = cm_description

    return cm_identified_description



class ClassificationSystem:
    
    def __init__(self, community_manager: 'ResidentialCommunityManager', model_tol: float, bw_tol: float):
        
        self.manager = community_manager
        self.rep_cms : Union[List['ResidentialCommunityMember'], None] = None
        self.unknown_cm : Union['ResidentialCommunityMember', None] = None
        self.init_model_tol = model_tol
        self.bw_tolerance = bw_tol
        
    
    def get_threshold_time_validation_data(self):
        
        if len(self.manager.sim_com_load_total.data) == 0:
            raise ValueError("No data to split!")
        
        data_length = len(self.manager.sim_com_load_total.data)
        
        threshold = round(data_length / 2)
        #threshold = round(data_length / 5)
        
        threshold_time = self.manager.com_model.tf_total[threshold]
        
        return threshold_time
    
 
    def get_initial_probabilities_by_time(self, threshold_time):
        
        if not self.rep_cms:
            raise ValueError("Representative cm not set!")
        
        state = self.unknown_cm.sim_member_states_total.data[threshold_time]
        
        if state == PossibleStates.ATTENTIVE:
            prior_probabilities = [1, 0]
        else:
            prior_probabilities = [0, 1]

        return prior_probabilities
    
    
    #def train_and_test_model_new(self,  n_init=30, bw_iter=20000, bw_tol = 0.01, verbose=True):
    def train_and_test_model_new(self,  n_init=100, bw_iter=20000, bw_tol = 0.01, verbose=True):
        
        initial_state = self.unknown_cm.sim_member_states_total.data[self.manager.com_model.tf_total[7]]
        
        # The initial probabilities are always the same, they are extracted from the real data
        startprob = self.get_initial_probabilities_by_time(self.manager.com_model.tf_total[7])
        startprob = np.array(startprob)
        
        threshold_time = pd.Timestamp(year=2015, month=12, day=1, hour=0, tz="Europe/Paris")
        #threshold_time = pd.Timestamp(year=2015, month=11, day=17, hour=0, tz="Europe/Paris")
        
        # The initial probability at the beginning of the testing period
        startprob_test = self.get_initial_probabilities_by_time(threshold_time)
        startprob_test = np.array(startprob_test)
        
        # Get observations from training data
        obs_seq_by_time = { time: action for time,action in self.unknown_cm.sim_member_followed_total.data.items() if time < threshold_time and action}
        obs_seq_train = [0 if val == PossibleActions.FOLLOW else 1 for time, val in obs_seq_by_time.items()]
        training_obs_seq_final = np.array([obs_seq_train]).T
        
        # Get testing observations
        testing_obs_seq_by_time = { time: action for time,action in self.unknown_cm.sim_member_followed_total.data.items() if time >= threshold_time and action}
        testing_obs_list = [0 if val == PossibleActions.FOLLOW else 1 for time, val in testing_obs_seq_by_time.items()]
        testing_obs_seq_final = np.array([testing_obs_list]).T
        
        followed_recs_perc_real = 1 - sum(testing_obs_list)/len(testing_obs_list)
             
        # Generate models for the other members
        generative_models: Dict[str, hmm.CategoricalHMM] = {}
        
        for cm in self.rep_cms:
            generative_models[cm.description] = hmm.CategoricalHMM(n_components=2)
            generative_models[cm.description].startprob_ = startprob_test
            generative_models[cm.description].transmat_ = np.array(cm.true_transition_matrix)
            generative_models[cm.description].emissionprob_ = (cm.true_emission_matrix)
            
                   
        best_logprob = -99999
        best_overall_model = None

        for i in range(n_init):

            model = hmm.CategoricalHMM(n_components=2, random_state=i)
            
            # Train the model
            model.fit(training_obs_seq_final)
    
            logprob_test = model.score(testing_obs_seq_final)
            
            if verbose:
                print(f"iteration {i}:  {logprob_test}, converged: {model.monitor_.converged} after {model.monitor_.iter}")
                
            if logprob_test > best_logprob:     
                
                print("Best model updated!")
                best_overall_model = model
                best_logprob = logprob_test
                
        
        obs, sts = best_overall_model.sample(30000)
        result = 1 - sum(obs)/len(obs)
        followed_recs_perc_est = result[0]
        
        known_log_probs_by_type = {}
        for cm in self.rep_cms:
            known_log_probs_by_type[cm.description] = generative_models[cm.description].score(testing_obs_seq_final)
        
        cm_identified_description = improved_cluster_cm(best_logprob, known_log_probs_by_type)

        # Group performances in a dict for analysis
        self.best_performances = {
            "Scenario": self.manager.com_model.sim_params.simulation_id,
            "Model": self.unknown_cm.model_type,
            "CM true description": self.unknown_cm.description,
            "CM identified description": cm_identified_description
        }
        
        if self.unknown_cm.model_type == "Markov":
            self.best_performances["True trans"] = np.round(self.unknown_cm.true_transition_matrix, 2)
            self.best_performances["True emiss"] = np.round(self.unknown_cm.true_emission_matrix, 2)
        else:
            self.best_performances["True trans"] = None
            self.best_performances["True emiss"] = None
            
        self.best_performances["Est. trans"] = np.round(best_overall_model.transmat_, 2).tolist()
        self.best_performances["Est. emiss"] = np.round(best_overall_model.emissionprob_,2).tolist()

        self.best_performances["Log prob test"] = round(best_logprob, 2)

        for cm in self.rep_cms:
            score = generative_models[cm.description].score(testing_obs_seq_final)
            self.best_performances[f"Optimal log prob test {cm.description}"] = round(score, 2)
        
        
        self.best_performances["Followed recs percentage real"] = round(followed_recs_perc_real,2)
        self.best_performances["Followed recs percentage est"] = round(followed_recs_perc_est,2)
        
        for cm in self.rep_cms:
            gen_obs, sts = generative_models[cm.description].sample(30000)
            result = 1 - sum(gen_obs)/len(gen_obs)
            self.best_performances[f"Followed recs percentage {cm.description}"] = round(result[0], 2)
        
        print("Performances of the best model:")
        for key, val in self.best_performances.items():
            print(key, val)
            
        print("Training and validation done!")


        
            

        
        
"""
Note: old version, really slow!


def forward(prior: Dict[PossibleStates, float], 
            trans: Dict[Tuple[PossibleStates, PossibleStates], float], 
            emit: Dict[Tuple[PossibleStates, PossibleActions], float], 
            obs: List[PossibleActions]) -> List[Dict[PossibleStates, float]]:
    '''
    Implementation of the Forward algorithm related to Hidden Markov Processes.

    Args:
        prior (Dict[Any, float]): Prior probabilities for each state.
        trans (Dict[Tuple[Any, Any], float]): Transition probabilities between states.
        emit (Dict[Tuple[Any, Any], float]): Emission probabilities for each state-action pair.
        obs (List[Any]): List of observed actions.

    Returns:
        List[Any]: the forward probabilities alpha vector.
    '''
        
    # Initialize the forward probabilities alpha
    alpha = [{state : 0.0 for state in PossibleStates } for _ in range(len(obs))]
    
    # Compute the initial forward probabilities using the prior and the first observation
    for state in PossibleStates: # for each index of states 
        #for action in PossibleActions:
        alpha[0][state] = prior[state] * emit[state,obs[0]]
        
    # Iterate over the rest of the observations
    for t in range(1, len(obs)):
        # Compute the forward probabilities at time t using the transition and emission probabilities
        # and the forward probabilities at time t-1

        #for j in range(len(prior)):
        for state_to in PossibleStates:
            prob = 0
            for state_from in PossibleStates:
                prob += alpha[t-1][state_from] * trans[(state_from, state_to)]       
            alpha[t][state_to] = prob * emit[(state_to, obs[t])]
    
    
    for t in range(len(obs)):
        normalization_factor = sum(alpha[t][state] for state in PossibleStates) 
        for state in PossibleStates:
            alpha[t][state] /= normalization_factor 
    
    return alpha






def backward(trans: Dict[Tuple[PossibleStates, PossibleStates], float], 
             emit: Dict[Tuple[PossibleStates, PossibleActions], float], 
             obs: List[PossibleActions]) -> List[Dict[PossibleStates, float]]:
    '''
    Implementation of the Backward algorithm related to Hidden Markov Processes.

    Args:
        trans (Dict[Tuple[Any, Any], float]): Transition probabilities between states.
        emit (Dict[Tuple[Any, Any], float]): Emission probabilities for each state-action pair.
        obs (List[Any]): List of observed actions.

    Returns:
        List[Any]: the backward probabilities beta vector.
    '''
    
    # Initialize the backward probabilities beta
    beta = [{state : 0.0 for state in PossibleStates } for _ in range(len(obs))]
    # The probability of seeing the rest of the observations given that we are in any state is 1
    for state in PossibleStates: # for each index of states 
        beta[-1][state] = 1
    # Iterate backwards over the observations
    for t in range(len(obs)-2, -1, -1):
        # Compute the backward probabilities at time t using the transition and emission probabilities
        # and the backward probabilities at time t+1
        for state_from in PossibleStates:
            prob = 0
            for state_to in PossibleStates:
                prob += trans[(state_from, state_to)] * emit[(state_to, obs[t+1])] * beta[t+1][state_to]
            beta[t][state_from] = prob
            
    for t in range(len(obs)):
        normalization_factor = sum(beta[t][state] for state in PossibleStates) 
        for state in PossibleStates:
            beta[t][state] /= normalization_factor
    return beta

def identify_cm_type(real_prior: Dict[PossibleStates, float], 
                     real_trans: Dict[Tuple[PossibleStates, PossibleStates], float], 
                     real_emit: Dict[Tuple[PossibleStates, PossibleActions], float], 
                     obs: List[PossibleActions], 
                     max_iter: int = 1000, 
                     bw_tol: float = 0.0001, 
                     model_tol: float = 0.05):
    '''Identify the cm type described by real_prior, real_trans and real_emit using the Baum Welch algorithm.

    Args:
        real_prior (Dict[PossibleStates, float]): the real 
        real_trans (Dict[Tuple[PossibleStates, PossibleStates], float]): _description_
        real_emit (Dict[Tuple[PossibleStates, PossibleActions], float]): _description_
        obs (List[PossibleActions]): _description_
        max_iter (int, optional): _description_. Defaults to 100.
        bw_tol (float, optional): _description_. Defaults to 0.01.
        model_tol (float, optional): _description_. Defaults to 0.01.
    '''
    
    #def identify_cm_type(real_prior, real_trans, real_emit, obs, max_iter=50, bw_tol=0.05, model_tol = 0.07):
    possible_trans : Dict[Tuple[PossibleStates, PossibleStates], float] = {(PossibleStates.ATTENTIVE, PossibleStates.ATTENTIVE): 0.5, (PossibleStates.ATTENTIVE, PossibleStates.INNATENTIVE): 0.5,
                (PossibleStates.INNATENTIVE, PossibleStates.ATTENTIVE): 0.5, (PossibleStates.INNATENTIVE, PossibleStates.INNATENTIVE): 0.5} # possible transition matrix
        
    emit_pos_good = {(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW): 0.7, (PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW): 0.3,
                    (PossibleStates.INNATENTIVE, PossibleActions.FOLLOW): 0.5, (PossibleStates.INNATENTIVE, PossibleActions.NOT_FOLLOW): 0.5} # possible emission matrix for good agents
    emit_pos_normal = {(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW): 0.5, (PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW): 0.5,
                    (PossibleStates.INNATENTIVE, PossibleActions.FOLLOW): 0.5, (PossibleStates.INNATENTIVE, PossibleActions.NOT_FOLLOW): 0.5} # possible emission matrix normal agents
    emit_pos_bad = {(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW): 0.3, (PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW): 0.7,
                    (PossibleStates.INNATENTIVE, PossibleActions.FOLLOW): 0.5, (PossibleStates.INNATENTIVE, PossibleActions.NOT_FOLLOW): 0.5} # possible emission matrix bad agents
    possible_emits : Dict[MType, Dict[Tuple[PossibleStates, PossibleActions], float]] = {MType.GOOD: emit_pos_good, MType.NORMAL: emit_pos_normal, MType.BAD: emit_pos_bad}

    result_cm_type = None 

    possible_vals = [0.3, 0.4, 0.5, 0.6, 0.7]
    #possible_vals = [0.001, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    
    #print("REAL TRANS:")
    #print(real_trans)
    
    #print("REAL EMIT:")
    #print(real_emit)
    
    #seeds = [(a,b,c) for a in possible_vals for b in possible_vals for c in possible_vals] # all combinations of a,b,c
    
    #seeds = [(0.4, 0.6, 0.3), (0.5, 0.5, 0.3)]
    
    seeds = [(0.4, 0.3), (0.5, 0.3)]
    
    emit_total = {}
    trans_total = {}
    succesfull_seeds = []
    for seed in seeds:
        # Initialization
        print("SEEEED: ", seed)
        
        prior = {PossibleStates.ATTENTIVE: 0.9999, PossibleStates.INNATENTIVE: 0.0001} # initial prior
        initial_prior = prior.copy()
        
        trans  = {(PossibleStates.ATTENTIVE, PossibleStates.ATTENTIVE): seed[0], 
                  (PossibleStates.ATTENTIVE, PossibleStates.INNATENTIVE): 1 - seed[0],
                (PossibleStates.INNATENTIVE, PossibleStates.ATTENTIVE): 1 - seed[0], 
                (PossibleStates.INNATENTIVE, PossibleStates.INNATENTIVE): seed[0]}  # initial trans
        initial_trans = trans.copy()
        
        emit  = {(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW): seed[1], 
                 (PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW): 1 - seed[1],
                (PossibleStates.INNATENTIVE, PossibleActions.FOLLOW): 0.5, 
                (PossibleStates.INNATENTIVE, PossibleActions.NOT_FOLLOW): 0.5} # initial emit
        initial_emit = emit.copy()
        
        log_likelihood = 0 # Initialize the log likelihood
        iter_count = 0
        # Baul welch algorithm

        for _ in range(max_iter):
            iter_count += 1

            # Initialize the expected counts
            prior_counts = {state : 0.0 for state in PossibleStates }
            trans_counts = {(state_from, state_to) : 0.0 for state_from in PossibleStates for state_to in PossibleStates }
            emit_counts = {(state, action) : 0.0 for state in PossibleStates for action in PossibleActions}

            # Initialize the forward and backward probabilities
            #alpha = forward(prior, trans, emit, obs)  
            alpha = log_forward(prior, trans, emit, obs)    
            #alpha = new_improved_forward(prior, trans, emit, obs)
            #beta = backward(trans, emit, obs)
            beta = log_backward(trans, emit, obs)
            # Compute the expected counts
            for t in range(len(obs)):
                for state_from in PossibleStates:
                    for state_to in PossibleStates:
                        trans_counts[(state_from, state_to)] += alpha[t][state_from] * trans[(state_from, state_to)] * emit[(state_to, obs[t])] * beta[t][state_to] 
                        if t==0:
                            prior_counts[state_from] += alpha[t][state_from] * beta[t][state_from]
            for t in range(len(obs)):
                for current_state in PossibleStates:
                    emit_counts[(current_state, obs[t])] += alpha[t][current_state] * beta[t][current_state] # expected number of times when in current_state and observing obs[t]
                    denom = sum(alpha[t][state] * beta[t][state] for state in PossibleStates) # at T, compute the denom - the probability to have the observation sequence, given the initial params
            for state_from in PossibleStates:
                for state_to in PossibleStates:
                    trans_counts[(state_from, state_to)] /= denom # type: ignore # expected number of transitions from state i to state j, given the observed sequence and initial params
            for state in PossibleStates:
                prior_counts[state] /= denom # type: ignore
                for action in PossibleActions:
                    emit_counts[(state, action)] /= denom # type: ignore

            # Update the transition, prior, and emission probabilities
            for state_from in PossibleStates:
                for state_to in PossibleStates:
                    trans[(state_from, state_to)] = trans_counts[(state_from, state_to)] / sum(trans_counts[(state_from, s)] for s in PossibleStates) 

            trans_total[(seed,iter_count)] = trans
            
            total_count = sum(prior_counts.values())
            for state in PossibleStates:
                #prior[state] = prior_counts[state] / len(obs)
                prior[state] = prior_counts[state] / total_count
                for action in PossibleActions:
                    emit[(state, action)] = emit_counts[(state, action)] / sum(emit_counts[(state, a)] for a in PossibleActions)
            
            emit_total[(seed,iter_count)] = emit
            

            
            # Compute the log likelihood
            new_log_likelihood = 0
            for t in range(len(obs)):
                new_log_likelihood += math.log(sum(alpha[t][state] * beta[t][state] for state in PossibleStates))

            #print(new_log_likelihood)
            # Check for baul welch convergence with log; with this, we avoid computational errors generated by very small alphas and betas
            if abs(new_log_likelihood - log_likelihood) < bw_tol: # Model has converged
                break # log likelihood should increase for model to converge; 
            
            log_likelihood = new_log_likelihood
 
        
        # Call the Baum-Welch algorithm
        prior,trans, emit = log_baum_welch(obs, prior, trans, emit, max_iter, bw_tol)

        
        # check for model convergence based on the difference between the new model parameters and the real model parameters
        prior_converged = all(abs(prior[state] - initial_prior[state]) < model_tol for state in PossibleStates)
        trans_converged = all(abs(trans[(state_from, state_to)] - initial_trans[(state_from, state_to)]) < model_tol for state_from in PossibleStates for state_to in PossibleStates)
        emit_converged = all(abs(emit[(state, action)] - initial_emit[(state, action)]) < model_tol for state in PossibleStates for action in PossibleActions)
        succesfull_seeds += [seed]
        if prior_converged and trans_converged and emit_converged:
            
            ok_trans = False
            ok_emit = False
            for type, possible_emit in possible_emits.items():
                if all(abs(emit[(state, action)] - possible_emit[(state, action)]) < model_tol for state in PossibleStates for action in PossibleActions):
                    result_cm_type = type
                    ok_emit = True
                    break
            if all(abs(trans[(state_from, state_to)] - possible_trans[(state_from, state_to)]) < model_tol for state_from in PossibleStates for state_to in PossibleStates):
                ok_trans = True
            if ok_emit and ok_trans:
                print(f"Community members is {result_cm_type}, with a known hidden model.")
                print(trans)
                print(emit)
            break
        
    return result_cm_type, emit, trans, succesfull_seeds, emit_total, trans_total  
    
    
    def train_and_test_model(self,  n_init=300, bw_iter=1000, bw_tol = 0.0001,random_state=None, verbose=True):
        '''
        Use the Baum Welch algorithm to identify if a member is Enthusiastic/Normal/Reluctant
        '''

        if not self.rep_cm:
            raise ValueError("Representative cm not set!")

        best_logprob_test = -99999
        best_accuracy = 0
        best_model = None
        min_diff = 9999
        
        rng = check_random_state(random_state)
        
        #possible_vals = [0.1,  0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
        possible_vals = np.linspace(0.1, 0.9,50)
        
        seeds = [(a,b) for a in possible_vals for b in possible_vals]

        rng = np.random.default_rng()
        record_used_params = []
        best_f1_score = 0
        
        for i in range(n_init):
        #for seed in seeds:
            initial_state = self.rep_cm.sim_member_states_total.data[self.manager.com_model.tf_total[0]]
            
            # Initialize the model with random start probabilities, transition probabilities, and emission probabilities
            startprob = self.get_initial_probabilities_by_time(self.manager.com_model.tf_total[0])
            startprob = np.array(startprob)
            
            
            a = rng.random()  # generate random a in [0, 1)
            b = rng.random()  # generate random b in [0, 1)

            while (a, b) in record_used_params:  # if the pair (a, b) was previously used, generate new ones
                a = rng.random()
                b = rng.random()

            record_used_params.append((a, b))

            # Construct the transition and emission matrices
            #transmat = np.array([[seed[0], 1-seed[0]], [1-seed[0], seed[0]]])
            #emissionprob = np.array([[seed[1], 1-seed[1]], [0.5, 0.5]])

                    
            transmat = np.array([[a, 1 - a],
                                    [1 - a, a]])
            
            emissionprob = np.array([[b, 1 - b],
                                        [0.5, 0.5]])
            
            #transmat = np.array([[0.6, 0.4 ],[0.4, 0.6]])
            #emissionprob = np.array([[0.7, 0.3],[0.5, 0.5]])
            
            # Get time to split between training and testing data
            threshold_time = self.get_threshold_time_validation_data()
            
            # Get observations from training data
            #obs_seq_by_time = self.get_observations_training_data()
            obs_seq_by_time = { time: action for time,action in self.rep_cm.sim_member_followed_total.data.items() if time < threshold_time}
            obs_seq = [1 if val == PossibleActions.FOLLOW else 0 for time, val in obs_seq_by_time.items()]
            training_obs_seq_final = np.array([obs_seq]).T

            # Train the model
            model = hmm.CategoricalHMM(n_components=2, n_iter=bw_iter, tol=bw_tol)
            #model.startprob_ = startprob
            #model.transmat_ = transmat
            #model.emissionprob_ = emissionprob
            
            #a,b = model.sample(100)
            #c,d = model.sample(300)
            #model.fit(a)
            #pred_val = model.predict(c)

            model.fit(training_obs_seq_final)

            # Get testing observations
            #testing_obs_by_time = self.get_observations_testing_data()
            testing_obs_seq_by_time = { time: action for time,action in self.rep_cm.sim_member_followed_total.data.items() if time >= threshold_time}
            testing_obs_list = [1 if val == PossibleActions.FOLLOW else 0 for time, val in testing_obs_seq_by_time.items()]
            testing_obs_seq_final = np.array([testing_obs_list]).T

            # predict states with viterbi algorithm
            predicted_final_states = model.predict(testing_obs_seq_final)

            # get true final states
            final_states_by_time = {time: state for time, state in self.rep_cm.sim_member_states_total.data.items() if time in testing_obs_seq_by_time }
            final_states = [1 if x == PossibleStates.ATTENTIVE else 0 for x in list(final_states_by_time.values())]

            # Compute accuracy, precision, recall and f1-score
            correct_predictions = sum(predicted == true for predicted, true in zip(predicted_final_states, final_states)) # type: ignore
            accuracy = correct_predictions / len(final_states)

            # Compute log probability of the observed sequence for training and testing data
            logprob_train = model.score(training_obs_seq_final)
            logprob_test = model.score(testing_obs_seq_final)

                
            f1 = f1_score(final_states, predicted_final_states, average=None)
            avg_f1 = (f1[0] + f1[1])/2
            #report = classification_report(final_states, predicted_final_states, output_dict=True)
            #f1_state0 = report['0']['f1-score'] # type: ignore
            #f1_state1 = report['1']['f1-score']  # type: ignore    
            
            # Verbose option to print progress
            if verbose:
                print(f"iteration {i}: accuracy {accuracy}, log prob train: {logprob_train}, log prob test: {logprob_test}, converged: {model.monitor_.converged}")

            #if f1 > best_f1_score:
            if f1[0] > 0.5 and f1[1] > 0.5 and avg_f1 > best_f1_score:
                best_f1_score = avg_f1
                best_logprob_test = logprob_test
                best_accuracy = accuracy
                best_model = model

        predicted_final_states = best_model.predict(testing_obs_seq_final)
        
        print("-----")
        print("Initial state", initial_state)
        print("Best log prob", best_logprob_test)
        print("Best accuracy", best_accuracy)
        print("Best f1 score", best_f1_score)
        print("Best model performance:")
        print(classification_report(final_states, predicted_final_states)) # type: ignore
        #print("Best seed",best_seed)
        print("Start probabilities:", best_model.startprob_) # type: ignore
        print("Transition probabilities:\n", best_model.transmat_) # type: ignore
        print("Emission probabilities:\n", best_model.emissionprob_) # type: ignore
    
"""