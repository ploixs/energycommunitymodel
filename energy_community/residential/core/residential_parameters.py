
from datetime import datetime
import os
from typing import Dict
import pandas as pd

import pytz
import yaml
from energy_community.core.constants import Stages
from energy_community.core.simulation.parameters import BaseParameters


class ResidentialParameters(BaseParameters):
    """
    A class representing the parameters of a simulation scenario related to a residential energy community.

    Attributes:
        simulation_id (str): ID of the simulation scenario.
        description (str): Description of the simulation scenario.
        estimation_system_flag (bool): Flag indicating whether the estimation system is active.
        pv_plant_number_of_panels (int): Number of panels in the PV plant.
        pv_plant_module_power (int): Power of the modules in the PV plant.
        solar_location (str): Location of the solar data.
        local_timezone (pytz.timezone): Local timezone based on the solar location.
        print_member_info (bool): Flag indicating whether to print messages during the simulation.
        starting_date (pd.Timestamp): Starting date of the simulation.
        ending_date (pd.Timestamp): Ending date of the simulation.
        simulation_steps (int): Number of simulation steps.
        stages (Stages): Simulation stages enum.

    Methods:
        __init__: Initializes the ResidentialParameters object.
        load_scenario_config: Loads the scenario configuration from a YAML file.
        get_date_as_datetime_from_str: Converts a string to a datetime object.
        get_timezone_based_on_solar_location: Returns the local timezone based on the solar data location.
        set_simulation_steps: Calculates the number of simulation steps based on the starting and ending dates.
        init_solar_parameters: Initializes the solar parameters from a dictionary.
        init_simulation_parameters: Initializes the simulation parameters from a dictionary.
        init_stages: Initializes the simulation stages based on a string.
    """

    def __init__(self, root_folder_name: str, community_folder_name: str, data_folder_name: str, configuration_file_name: str, simulation_id: int, print_flag : bool = False):
        """
        Initializes the ResidentialParameters object.

        Args:
            ommunity_folder_name (str)L Name of the community folder
            configuration_file_name (str): Name of the configuration file
            simulation_id (int): ID of the simulation scenario.
            print_flag (bool, optional): Flag indicating whether to print messages during the simulation. Defaults to False.
        
        Raises:
            ValueError: if the stages are not set, raise an error

        """
        super().__init__(root_folder_name, community_folder_name, data_folder_name, configuration_file_name, simulation_id, print_flag)

        
        self.estimation_system_flag = self.scenario_config["estimation_system"]
        self.scenario_description = self.scenario_config["description"]

  
        stages = self.scenario_config["stages"]
        self.init_stages(stages)

        # Check if the stages attribute of sim_params is set; if not, raise a ValueError
        if self.stages is None:
            raise ValueError("Stages not set from scenario file!")
        
    def get_comfort_parameter(self):
        
        return self.scenario_config["comfort_parameter"]

    
    def init_stages(self, stages: str):
        """Initializes the simulation stages based on the given string.

        Args:
            stages (str): String describing the simulation stages.

        Raises:
            ValueError: If the specified string is not valid.
        """

        # Set the simulation stages based on the given string
        if stages == "one turn":
            self.stages = Stages.ONE_TURN
        elif stages == "two turns":
            self.stages = Stages.TWO_TURNS
        else:
            raise ValueError("Invalid stages value")

    
