

from datetime import datetime
import random
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Union
import numpy as np
import itertools
import pandas as pd
from energy_community.core.constants import MType, PType, Rec
from energy_community.residential.core.community import ResidentialCommunity
import pulp
from pulp import PULP_CBC_CMD

if TYPE_CHECKING:
    from energy_community.residential.core.manager import ResidentialCommunityManager


def calculate_expected_load_for_action(rec: Rec, cm, 
                                       trans_matrix_by_cm_by_time, emis_matrix_by_cm,
                                       average_delta_power_val_by_rec, time) -> float:
    # Use action_mapping to get the corresponding recommendation for this action
    expected_load_adjustment = 0.0
    # Calculate the expected load adjustment based on the probabilities of the HMM states and emissions
    for s in [0, 1]:  # assuming 0 is for attentive state, and 1 for inattentive state
        for e in [0, 1]:  # assuming 0 is for following recommendation, and 1 for not following
            # Calculate the probability to be in state s and have emission e
            prob_se = trans_matrix_by_cm_by_time[cm.unique_id][time][s] * emis_matrix_by_cm[cm.unique_id][s][e]
            if e == 0:  # if following recommendation
                expected_load_adjustment += prob_se * (cm.est_member_load_day.data[time] + average_delta_power_val_by_rec[rec])
            else:  # if not following recommendation
                expected_load_adjustment += prob_se * cm.est_member_load_day.data[time]


    return expected_load_adjustment


def most_frequent_element(lst):
    """Returns the most frequently occurring element in a list.
    
    Args:
        lst (list): A list of elements
        
    Returns:
        The most frequently occurring element in the list
    """
    # Initialize variables to store the most frequent element and its count
    most_frequent_element = None
    highest_count = 0
    
    # Loop through each element in the list
    for element in lst:
        # Count the number of times the current element appears in the list
        current_count = lst.count(element)
        
        # If the current element appears more frequently than the previous most frequent element
        if current_count > highest_count:
            # Update the most frequent element and its count
            most_frequent_element = element
            highest_count = current_count
    
    if not most_frequent_element:
        raise ValueError("Most frequent element could not be found!")
    
    # Return the most frequent element
    return most_frequent_element

def optimisation_problem_general(p_load_dict: Dict[str, List[float]], 
                                 p_pv : List[float], manager: 'ResidentialCommunityManager', 
                                 exp_intensity_change_by_cm_and_rec: Dict[int, Dict[Rec, float]], 
                                 exp_probability_to_follow_by_type: Dict[MType, float], 
                                 exp_probability_to_consume_same_by_type: Dict[MType, float], print_info: bool = False) -> Dict[str, Dict[str, pulp.LpVariable]]:
    """
    Solves an optimization problem for a community of agents.

    Args:
        p_load_dict (dict): A dictionary of load profiles for each agent type
        p_pv (list): A list of PV generation profiles for each time step, for the whole period
        manager (ResidentialCommunityManager):The community manager
        exp_intensity_change_by_cm_and_rec (Dict[int, Dict[Rec, float]]): the expected intensity change when requesting contribution, by community member and then by recommendation
        exp_probability_to_follow_by_type (Dict[MType, float]): expected probability to follow recommendation by community member type
        exp_probability_to_consume_same_by_type (Dict[int, Dict[Rec, float]]):  expected probability to consume same by community member type
        print_info (bool, optional): Whether to print information about the optimization process. Defaults to False.

    Returns:
        act_all_agents_dict (Dict[str, Dict[str, pulp.LpVariable]]): A dictionary of solutions for the decision variables for each agent; the decision variables are for the whole period
    """
    
    # Extract agent types and group agents by type
    cm_types = list(set([cm.type for cm in manager.cm_list]))
    cms_by_type = {type: [cm for cm in manager.cm_list if type == cm.type] for type in cm_types }

    # Initialize PuLP model and decision variable dictionaries
    model = pulp.LpProblem("Problem_Optimal_Decision", pulp.LpMinimize)
    time_step_index = []
    action_index = []
    for i in range(len(cm_types)):
        action_index.extend([f'cat_{cm_types[i]}_same', f'cat_{cm_types[i]}_mod_inc', f'cat_{cm_types[i]}_mod_dec',
                             f'cat_{cm_types[i]}_strong_inc', f'cat_{cm_types[i]}_strong_dec'])

    # We generate the error index of length of the optimisation horizon
    for i in range(len(p_load_dict[str(cms_by_type[cm_types[0]][0].unique_id)])):
        time_step_index.append(str(i))

    error_dictionary = pulp.LpVariable.dicts("e", (i for i in time_step_index), cat='Continuous')
    action_dictionary = pulp.LpVariable.dicts("a", (i for i in action_index), cat='Binary')
    act_all_agents_dict = {}

    # Create a dictionary of decision variables for each agent
    for agent in manager.cm_list:
        act_all_agents_dict[agent.unique_id] = action_dictionary

    # Add the objective function to the model
    model += pulp.lpSum([error_dictionary[i] for i in time_step_index])

    # Add the constraints to the mode
    for hour in range(len(p_load_dict[str(cms_by_type[cm_types[0]][0].unique_id)])):
        total_load = 0
        for agent in manager.cm_list:
            # Calculate the total load for each agent at each time step
            # At each time step, the load for each agent is adjusted with the probability to follow recommendation 
            # and a random value to account for the intensity (which is dependent on the recommendation)
            total_load += (act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_same'] * ( exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.WHITE] * p_load_dict[str(agent.unique_id)][hour]) + 
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_inc'] * (exp_probability_to_follow_by_type[agent.type] *  exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.GREEN] * p_load_dict[str(agent.unique_id)][hour] + exp_probability_to_consume_same_by_type[agent.type] * exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.WHITE] * p_load_dict[str(agent.unique_id)][hour]) +
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_dec'] * (exp_probability_to_follow_by_type[agent.type] *  exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.RED] * p_load_dict[str(agent.unique_id)][hour] + exp_probability_to_consume_same_by_type[agent.type] * exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.WHITE] * p_load_dict[str(agent.unique_id)][hour]) +
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_inc'] * (exp_probability_to_follow_by_type[agent.type] * exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.STRONG_GREEN] * p_load_dict[str(agent.unique_id)][hour] + exp_probability_to_consume_same_by_type[agent.type] * exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.WHITE] * p_load_dict[str(agent.unique_id)][hour]) +
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_dec'] * (exp_probability_to_follow_by_type[agent.type] *  exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.STRONG_RED] * p_load_dict[str(agent.unique_id)][hour] + exp_probability_to_consume_same_by_type[agent.type] * exp_intensity_change_by_cm_and_rec[agent.unique_id][Rec.WHITE] * p_load_dict[str(agent.unique_id)][hour]))
            # Add a constraint to ensure that each agent takes exactly one action at each time step
            model += (act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_same'] + act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_inc'] +
                      act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_dec'] + act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_inc'] +
                      act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_dec']) == 1
  
        # Constraint to model the absolute value of the neeg, in a linear form
        model += (error_dictionary[str(hour)] + (total_load - p_pv[hour])) >= 0
        model += (error_dictionary[str(hour)] - (total_load - p_pv[hour])) >= 0

    # Solve the optimization problem
    model.solve(PULP_CBC_CMD(msg=0)) # type: ignore
    
    # Print information about the optimization process if requested
    if print_info:
        # Define the necessary variables and data structures
        hours = len(p_load_dict[str(manager.cm_list[0].unique_id)])
        total_loads = []
        print("------------------------------------")
        # Iterate over each hour
        for hour in range(hours):
            # Sum the loads of all agents at that hour
            t_load = sum(p_load_dict[str(cm.unique_id)][hour] for cm in manager.cm_list)
            total_loads.append(t_load)

            # Print the total load for that hour
            print(f"Hour {hour}: Total load = {t_load}; Total PV= {p_pv[hour]}")
        print("------------------------------------")
        print(f'--- OPTIMISATION - CASE with {len(cm_types)} agent types -----')
        for agent_type in cm_types:
            print(f"{agent_type} agents actions:")
            print(f"Action 0 (same cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_same'].varValue}")
            print(f"Action 1 (+ moderate cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_mod_inc'].varValue}")
            print(f"Action 2 (- moderate cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_mod_dec'].varValue}")
            print(f"Action 3 (+ strong cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_strong_inc'].varValue}")
            print(f"Action 4 (- strong cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_strong_dec'].varValue}")
        print("------------------------------------")
    return act_all_agents_dict


def optimisation_problem_with_adaptive_and_hmm(period_time_frame,
                                 p_load_by_cm: Dict[int, List[float]], 
                                 p_pv_by_period : List[float], manager: 'ResidentialCommunityManager', 
                                 average_delta_power_val_by_rec: Dict[Rec, float], 
                                 print_info: bool = False) -> Dict[str, Dict[str, pulp.LpVariable]]:
    """
    Solves an optimization problem for a community of agents.

    Returns:
        act_all_agents_dict (Dict[str, Dict[str, pulp.LpVariable]]): A dictionary of solutions for the decision variables for each agent; the decision variables are for the whole period
    """
    for cm in manager.cm_list:
        if not cm.measured_contribution_ratio: 
            raise ValueError("Measured contribution ratio not set!")
    

    # Initialize PuLP model and decision variable dictionaries
    model = pulp.LpProblem("Problem_Optimal_Decision", pulp.LpMinimize)
    time_step_index = []
    action_index = []
    for cm in manager.cm_list:
        for rec in average_delta_power_val_by_rec:
            action_index.append(f'cm_{cm.unique_id}_{rec}')

    # We generate the error index of length of the optimisation horizon
    for hour_index in range(len(p_load_by_cm[manager.cm_list[0].unique_id])):
        time_step_index.append(str(hour_index))

    error_dictionary = pulp.LpVariable.dicts("e", (i for i in time_step_index), cat='Continuous')
    act_all_agents_dict = {}
    
    # Create a dictionary of decision variables for each agent
    for cm in manager.cm_list:
        #act_all_agents_dict[cm.unique_id] = action_dictionary
        act_all_agents_dict[cm.unique_id] = pulp.LpVariable.dicts("a", (i for i in action_index if str(cm.unique_id) in i), cat='Binary')
        
    # Add the objective function to the model
    model += pulp.lpSum([error_dictionary[i] for i in time_step_index])

    # Add the constraints to the mode
   #for hour in range(len(p_load_by_cm[manager.cm_list[0].unique_id])):
    for hour_index,(_,time) in enumerate(period_time_frame.items()):
        total_load = 0
        for cm in manager.cm_list:
            # Calculate the total load for each agent at each time step
            # At each time step, the load for each agent is adjusted with the probability to follow recommendation 
            for recommendation in average_delta_power_val_by_rec:
                # Calculate the expected load adjustment based on member motivation and expected power impact
                exp_adjustment_when_following = cm.measured_contribution_ratio * (p_load_by_cm[cm.unique_id][hour_index] + average_delta_power_val_by_rec[recommendation]) # type: ignore
                exp_adjustment_when_not_following = (1- cm.measured_contribution_ratio) * (p_load_by_cm[cm.unique_id][hour_index]) # type: ignore
                total_load +=  act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_{recommendation}'] * (exp_adjustment_when_following + exp_adjustment_when_not_following)
                               
            # Add a constraint to ensure that each agent takes exactly one action at each time step
            model += (act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_{Rec.WHITE}'] + act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_{Rec.GREEN}'] +
                      act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_{Rec.RED}']) == 1
  
        # Constraint to model the absolute value of the neeg, in a linear form
        model += (error_dictionary[str(hour_index)] + (total_load - p_pv_by_period[hour_index])) >= 0
        model += (error_dictionary[str(hour_index)] - (total_load - p_pv_by_period[hour_index])) >= 0

    # Solve the optimization problem
    model.solve(PULP_CBC_CMD(msg=0)) # type: ignore
    
    # Print information about the optimization process if requested
    if print_info:
        # Define the necessary variables and data structures
        #hours = len(p_load_by_cm[str(manager.cm_list[0].unique_id)])
        total_loads = []
        print("------------------------------------")
        # Iterate over each hour
        for hour_index in range(len(list(period_time_frame.values()))):
            # Sum the loads of all agents at that hour
            t_load = sum(p_load_by_cm[cm.unique_id][hour_index] for cm in manager.cm_list)
            total_loads.append(t_load)

            # Print the total load for that hour
            print(f"Hour {hour_index}: Total load = {t_load}; Total PV= {p_pv_by_period[hour_index]}")
        print("------------------------------------")
        print(f'--- OPTIMISATION - CASE agent types -----')
        #for agent_type in cm_types:
        for cm in manager.cm_list:
            print(f"{cm.unique_id} agents actions:")
            for recommendation in average_delta_power_val_by_rec:
                print(f"Action {recommendation}: {act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_{recommendation}'].varValue}")

        print("------------------------------------")
    return act_all_agents_dict
    
class RecommendationSystem:
    """A recommendation system for a residential community.

    Attributes:
        community_model (ResidentialCommunity): The residential community model.
        manager (ResidentialCommunityManager): The residential community manager.
        possible_recs_dynamic (List[Rec]): The list of possible dynamic recommendations.
        flex_hour_min (int): The minimum flexibility hour for the current day.
        flex_hour_max (int): The maximum flexibility hour for the current day.
        time_frame_current_day (Dict[str, datetime]): The time frame for the current day.
        flexibility_time_frame (Dict[str, datetime]): The time frame for the flexibility period.
        no_alert_threshold (float): The threshold for no alert recommendation.
        production (List[float]): The estimated community production data for the current day.
        consumption (List[float]): The estimated community consumption data for the current day.
    """
    
    def __init__(self, community_manager: 'ResidentialCommunityManager', community_model: ResidentialCommunity):
        """Initializes a recommendation system for a residential community.

        Args:
            community_manager (ResidentialCommunityManager): The residential community manager.
            community_model (ResidentialCommunity): The residential community model.
        """
        
        self.community_model = community_model
        self.manager = community_manager
        
        # Define the possible dynamic recommendations in reverse order
        self.possible_recs_dynamic = [rec for rec in Rec if rec not in [Rec.BLINKING_GREEN, Rec.BLINKING_RED]]
        self.possible_recs_dynamic.reverse() # the order matters: red must come before green
        
    def set_parameters_current_day(self):
        """Sets the parameters for the current day."""
        
        # Set the flexibility period hours
        self.flex_hour_min = self.manager.flexibility_period.hour_min
        self.flex_hour_max = self.manager.flexibility_period.hour_max
        
        # Set the time frame for the current day
        self.time_frame_current_day = self.community_model.tf_day
        
        # Set the time frame for the flexibility period
        self.flexibility_time_frame = {k: time for k, time in self.time_frame_current_day.items() if self.flex_hour_min <= time.hour <= self.flex_hour_max }
        
        # Set the comfort threshold
        self.no_alert_threshold = self.manager.no_alert_threshold
        
        # Set the estimated community production and consumption data for the current day
        self.production : Dict[datetime, float] = self.manager.est_com_prod_day.data # type:ignore
        self.consumption : Dict[datetime, float] = self.manager.est_com_load_day.data # type:ignore
        
    def recommendation_stratistics(self) -> Tuple[Dict[Rec, float], Dict[Rec, float]]:
        
        delta_powers_by_rec: Dict[Rec, List] = {}
        
        for time, recommendation in self.manager.req_manager_rec_total.data.items():
            if recommendation not in delta_powers_by_rec:
                delta_powers_by_rec[recommendation] = [] #type: ignore
            else:
                delta_powers_by_rec[recommendation].append(self.manager.sim_com_load_total.data[time] - self.manager.est_com_load_total.data[time]) #type: ignore
        
        average_delta_power_val_by_rec: dict[Rec, float] = {}
        delta_power_ratio_by_rec: dict[Rec, float] = {}
        
        for recommendation, delta_powers in delta_powers_by_rec.items():
            if len(delta_powers) == 0:
                average_delta_power_val_by_rec[recommendation] = None #type: ignore
                delta_power_ratio_by_rec[recommendation] = 0.0
            else:
                average_delta_power_val_by_rec[recommendation] = sum(delta_powers) / len(delta_powers)
                delta_power_ratio_by_rec[recommendation] = len(delta_powers) / len(list(self.manager.req_manager_rec_total.data.values()))
        
        return delta_power_ratio_by_rec, average_delta_power_val_by_rec

        
    def get_recommendations_current_day_dummy_approach(self):
        """Gets the recommendations for the current day for the dummy approach"""
        
        self.set_parameters_current_day()
        
        recommendations_by_hour = {time : Rec.WHITE for _, time in self.time_frame_current_day.items()}
        return recommendations_by_hour
   
    def get_recommendations_current_day_basic_one_turn_informative_approach(self, mode: str = "soft") -> Dict[datetime, Rec]:
        """Gets basic informative recommendations for the current day using a one-turn approach.

        Returns:
            Dict[datetime, Rec]: A dictionary with recommendations by hour for the current day.
        """

        if self.community_model.sim_params.print_member_info:
            print("SETTING RECOMMENDATIONS ONE TURN INFORMATIVE BASIC APPROACH")
        
        self.set_parameters_current_day()

        # Initialize the recommendations by hour to WHITE for all hours
        recommendations_by_hour = {time : Rec.WHITE for _, time in self.time_frame_current_day.items()}
        
        # Set the recommendations for the flexibility period based on production and consumption data
        if mode == "soft":
            for _, time in self.flexibility_time_frame.items():
                if self.production[time] > self.consumption[time] + self.no_alert_threshold: # type: ignore
                    recommendations_by_hour[time] = Rec.GREEN
                elif self.consumption[time] > self.production[time] + self.no_alert_threshold:  # type: ignore
                    recommendations_by_hour[time] = Rec.RED
        else:
            for _, time in self.flexibility_time_frame.items():
                if self.production[time] > self.consumption[time] + self.no_alert_threshold: # type: ignore
                    recommendations_by_hour[time] = Rec.STRONG_GREEN
                elif self.consumption[time] > self.production[time] + self.no_alert_threshold:  # type: ignore
                    recommendations_by_hour[time] = Rec.STRONG_RED
                
        return recommendations_by_hour
    
    
    def get_recommendations_current_day_adaptive_informative_approach(self, mode: str = "simple") -> Dict[datetime, Rec]:
        """Gets adaptive informative recommendations for the current day using a one-turn approach.

        Returns:
            Dict[datetime, Rec]: A dictionary with recommendations by hour for the current day.
        """

        if self.community_model.sim_params.print_member_info:
            print("SETTING RECOMMENDATIONS ONE TURN INFORMATIVE BASIC APPROACH")
        
        self.set_parameters_current_day()
        _, average_delta_power_val_by_rec = self.recommendation_stratistics()

        # Initialize the recommendations by hour to WHITE for all hours
        recommendations_by_hour = {time : Rec.WHITE for _, time in self.time_frame_current_day.items()}
        
        if mode == "simple":
            # Set the recommendations for the flexibility period based on production and consumption data
            for _, time in self.flexibility_time_frame.items():
                rec_match = None
                for recommendation, avg_val in average_delta_power_val_by_rec.items():
                    match = abs(self.consumption[time] - self.production[time] + avg_val) # type:ignore
                    if rec_match is None or match < rec_match[1]:
                            rec_match = (recommendation, match)
                if match > self.no_alert_threshold: # type:ignore
                    recommendations_by_hour[time] = rec_match[0] # type:ignore
                
        return recommendations_by_hour
    
    def get_recommendations_current_day_basic_two_turns_informative_approach(self) -> Dict[datetime, Rec]:
        """Gets basic informative recommendations for the current day using a two-turn approach.

        Returns:
            Dict[datetime, Rec]: A dictionary with recommendations by hour for the current day.
        """
        
        if self.community_model.sim_params.print_member_info:
            print("SETTING RECOMMENDATIONS TWO TURNS INFORMATIVE BASIC APPROACH")

        self.set_parameters_current_day()
        
        # Initialize the recommendations by hour to WHITE for all hours
        recommendations_by_hour = {time : Rec.WHITE for _, time in self.time_frame_current_day.items()}
        
        # Set the recommendations for the flexibility period based on production and consumption data
        for _, time in self.flexibility_time_frame.items():
            if self.production[time] > self.consumption[time] + self.no_alert_threshold: # type: ignore
                recommendations_by_hour[time] = Rec.STRONG_GREEN
            elif self.consumption[time] > self.production[time] + self.no_alert_threshold:  # type: ignore
                recommendations_by_hour[time] = Rec.STRONG_RED
                
        return recommendations_by_hour
                
                
    def get_recommendations_current_day_basic_coaching_approach(self, mode: str = "easy") -> Dict[datetime, Rec]:
        """Gets basic coaching recommendations for the current day using a one-turn approach.

        Returns:
            Dict[datetime, Rec]: A dictionary with recommendations by hour for the current day.
        """
        if self.community_model.sim_params.print_member_info:
                print("SETTING RECOMMENDATIONS PRIVATE COACHING APPROACH NO ADAPTIVE EXPECTANCY")
                
        self.set_parameters_current_day()
        
        # Initialize the recommendations by hour to WHITE for all hours
        recommendations_by_hour = {time : Rec.WHITE for _, time in self.time_frame_current_day.items()}        
    
        for period in self.manager.period_names_list:
            # Get the consumption and the production
            p_com_load_period = {time : load for time, load in self.consumption.items() if self.manager.periods_current_day[period].hour_min <= time.hour <= self.manager.periods_current_day[period].hour_max} # get the consumption for the period
            p_com_prod_period = {time : prod for time, prod in self.production.items() if self.manager.periods_current_day[period].hour_min <= time.hour <= self.manager.periods_current_day[period].hour_max} # get the production for the period
            
            # Create a list of all recommendations during the period
            recs_for_period = [] 
            for time,_ in p_com_load_period.items():
                if mode == "easy":
                    if p_com_load_period[time] > p_com_prod_period[time] + self.no_alert_threshold: # type: ignore
                        recs_for_period.append(Rec.RED)
                    elif p_com_prod_period[time] > p_com_load_period[time] + self.no_alert_threshold: # type: ignore
                        recs_for_period.append(Rec.GREEN)
                    else:
                        recs_for_period.append(Rec.WHITE)
                else:
                    if p_com_load_period[time] > p_com_prod_period[time] + self.no_alert_threshold: # type: ignore
                        recs_for_period.append(Rec.STRONG_RED)
                    elif p_com_prod_period[time] > p_com_load_period[time] + self.no_alert_threshold: # type: ignore
                        recs_for_period.append(Rec.STRONG_GREEN)
                    else:
                        recs_for_period.append(Rec.WHITE)

            # Select the most frequent recomendation for the period
            if len(recs_for_period) == 0:
                most_frequent_rec = Rec.WHITE
            else:
                most_frequent_rec = most_frequent_element(recs_for_period) 
            
            # Assign the most frequent recommendation to all the interval
            for time in p_com_load_period:
                recommendations_by_hour[time] = most_frequent_rec 

        return recommendations_by_hour
    
    
    def get_recommendations_current_day_dynamic_approach(self, color_index_deltas_powers: Dict[int, List[float]]) -> Dict[datetime, Rec]:
        """Gets basic dynamic recommendations for the current day using a one-turn approach.

        Returns:
            Dict[datetime, Rec]: A dictionary with recommendations by hour for the current day.
        """
        
        if self.community_model.sim_params.print_member_info:
                print("SETTING RECOMMENDATIONS PRIVATE DYNAMIC APPROACH NO ADAPTIVE EXPECTANCY")
        
        self.set_parameters_current_day()
        
        # Initialize the recommendations for the day to white (no recommendation)
        recommendations_by_hour = {time: Rec.WHITE for _, time in self.time_frame_current_day.items()}

        # Set recommendations
        for _, time in self.flexibility_time_frame.items():
            if self.community_model.days_passed > 7:
                # Compute the difference between the estimated load and estimated production at the current time
                delta_power = self.production[time] - self.consumption[time] #type: ignore

                # If the difference is greater than the no alert threshold, determine the recommended color
                if abs(delta_power) > self.no_alert_threshold:
                    returned_color = Rec.WHITE
                    # Determine the recommended color based on the delta power value
                    if len(color_index_deltas_powers[0]) > 0 and delta_power < color_index_deltas_powers[0][-1]:
                        returned_color = self.possible_recs_dynamic[0]
                    elif len(color_index_deltas_powers[len(self.possible_recs_dynamic) - 1]) > 0 and delta_power > color_index_deltas_powers[len(self.possible_recs_dynamic) - 1][0]:
                        returned_color = self.possible_recs_dynamic[-1]
                    else:
                        for i in range(1, len(self.possible_recs_dynamic) - 1, 1):
                            if len(color_index_deltas_powers[i]) > 0 and color_index_deltas_powers[i][0] <= delta_power <= color_index_deltas_powers[i][-1]:
                                returned_color = self.possible_recs_dynamic[i]

                    # Set the recommendation to the returned color if it is not None
                    if returned_color is not None:
                        recommendation = returned_color
                    else:
                        recommendation = Rec.WHITE
                else:
                    recommendation = Rec.WHITE
            else:
                recommendation = Rec.WHITE
            
            # Set the recommendation for the current time
            recommendations_by_hour[time] = recommendation
            
        return recommendations_by_hour
    
    def get_recommendations_current_day_optimal_coaching(self) -> Dict[PType, Dict[str, Dict[str, pulp.LpVariable]]] :
        """Gets optimal recommendation considering known member profiles in the community.

        Returns:
            Dict[PType, Dict[str, Dict[str, pulp.LpVariable]]]: A dictionary containing optimal recommendations by period and member type
        """
        
        if self.community_model.sim_params.print_member_info:
            print("SETTING RECOMMENDATIONS ONE TURN OPTIMAL COACHING APPROACH")
            
        self.set_parameters_current_day()
        
        # Generate the expected intensity change by recommendation
        
        exp_intensity_change_by_rec = {Rec.WHITE: 1, Rec.RED: 0.5,  Rec.GREEN: 1.5, Rec.STRONG_RED: 0.2, Rec.STRONG_GREEN: 1.8}
        
        exp_intensity_change_by_cm_and_rec = {cm.unique_id: {rec: exp_intensity_change_by_rec[rec] for rec in exp_intensity_change_by_rec} for cm in self.manager.cm_list}
        
        # Generate the expected probability to follow recommendations by cluster member type
        exp_probability_to_follow_by_type = {MType.GOOD: 0.7, MType.NORMAL: 0.5, MType.BAD: 0.3}  
        
        # Generate the expected probability to consume the same amount of electricity by cluster member type
        exp_probability_to_consume_same_by_type = {MType.GOOD: 0.3, MType.NORMAL: 0.5, MType.BAD: 0.7} 
        
        # Create an empty dictionary to store recommendations by period and member type
        recommendations_by_period = {period: {} for period in self.manager.period_names_list}

        # Create dictionaries to store consumption and production by period and cluster member
        consumption_by_period_and_cm : Dict[PType, Dict[str, List[float]]] = { period: {} for period in self.manager.period_names_list}
        production_by_period : Dict[PType, List[Any]] = {period: [] for period in self.manager.period_names_list}

        # Populate consumption and production dictionaries
        for period_name in self.manager.period_names_list:
            consumption_by_period_and_cm[period_name] = {str(cm.unique_id) : [load for time,load in cm.est_member_load_day.data.items() if self.manager.periods_current_day[period_name].hour_min <= time.hour <= self.manager.periods_current_day[period_name].hour_max] for cm in self.manager.cm_list  }
            production_by_period[period_name] = [prod for time,prod in self.production.items() if self.manager.periods_current_day[period_name].hour_min <= time.hour <= self.manager.periods_current_day[period_name].hour_max ]
        
        # Generate optimal recommendations for each period
        for period_name in self.manager.period_names_list:
            recommendations_by_period[period_name] = optimisation_problem_general(consumption_by_period_and_cm[period_name], production_by_period[period_name], self.manager, exp_intensity_change_by_cm_and_rec, 
                                                                                  exp_probability_to_follow_by_type, exp_probability_to_consume_same_by_type,self.community_model.sim_params.print_member_info)
        
        return recommendations_by_period
    
    
    def get_recommendations_current_day_adaptive_optimal_coaching(self)  :
        """Gets optimal recommendation considering known member profiles in the community """
        
        if self.community_model.sim_params.print_member_info:
            print("SETTING RECOMMENDATIONS ONE TURN OPTIMAL COACHING APPROACH")
            
        self.set_parameters_current_day()
        
        _, average_delta_power_val_by_rec = self.recommendation_stratistics()
        
        #trans_matrix_by_cm_by_time, emis_matrix_by_cm = self.manager.get_trans_emis_matrix_by_cm_by_time()
        
        # Create an empty dictionary to store recommendations by period and member type
        recommendations_by_period = {period: {} for period in self.manager.period_names_list}

        # Create dictionaries to store consumption and production by period and cluster member
        consumption_by_period_and_cm : Dict[PType, Dict[int, List[float]]] = { period: {} for period in self.manager.period_names_list}
        production_by_period : Dict[PType, List[Any]] = {period: [] for period in self.manager.period_names_list}

        # Populate consumption and production dictionaries
        for period_name in self.manager.period_names_list:
            consumption_by_period_and_cm[period_name] = {cm.unique_id : [load for time,load in cm.est_member_load_day.data.items() if self.manager.periods_current_day[period_name].hour_min <= time.hour <= self.manager.periods_current_day[period_name].hour_max] for cm in self.manager.cm_list  }
            production_by_period[period_name] = [prod for time,prod in self.production.items() if self.manager.periods_current_day[period_name].hour_min <= time.hour <= self.manager.periods_current_day[period_name].hour_max ]
        
        # Identify cms
        self.manager.indetify_cms_based_on_contributions_ratio()
        
        # Generate optimal recommendations for each period
        for period_name in self.manager.period_names_list:
            period_time_frame = {i: time for i, time in self.community_model.tf_day.items() 
                                 if self.manager.periods_current_day[period_name].hour_min <= time.hour <= self.manager.periods_current_day[period_name].hour_max}
            recommendations_by_period[period_name] = optimisation_problem_with_adaptive_and_hmm(period_time_frame,
                                                                                                consumption_by_period_and_cm[period_name], 
                                                                                                production_by_period[period_name], 
                                                                                                self.manager, 
                                                                                                average_delta_power_val_by_rec, 
                                                                                                self.community_model.sim_params.print_member_info)
        
        return recommendations_by_period
    
    
    
    def get_recommendations_current_day_adaptive_optimal_informative(self)  :
        """Gets optimal recommendation considering known member profiles in the community """
        
        self.set_parameters_current_day()
        
        trans_matrix_by_cm_by_time, emis_matrix_by_cm = self.manager.get_trans_emis_matrix_by_cm_by_time()
        
        _, average_delta_power_val_by_rec = self.recommendation_stratistics()
        
        # Define possible actions for each agent
        possible_actions = [Rec.WHITE, Rec.GREEN, Rec.RED]

        best_actions_by_time_by_cm = {}
        
        # For each time step
        for i, (_, time) in enumerate(self.community_model.tf_day.items()):
            best_actions_by_time_by_cm[time] = {}
            
            if self.manager.flexibility_period.hour_min <= time.hour <= self.manager.flexibility_period.hour_max:

                min_error = float('inf')  # Initialize minimum error as infinity
                best_actions = None  # Initialize best action combination
                best_total_load = float('inf')

                # Generate all possible action combinations
                all_action_combinations = itertools.product(possible_actions, repeat=len(self.manager.cm_list))
                
                # Iterate over all possible action combinations
                for action_combination in all_action_combinations:
                    total_load = 0  # Initialize total load
                    
                    # Iterate over all community members and their corresponding action
                    for cm, action in zip(self.manager.cm_list, action_combination):
                        
                        expected_load_adjustment = calculate_expected_load_for_action(action, cm, 
                                                                                    trans_matrix_by_cm_by_time, emis_matrix_by_cm,
                                                                                    average_delta_power_val_by_rec, time)
                        total_load += expected_load_adjustment
                        
                    # Calculate error
                    error = abs(total_load - self.production[time])

                    # Update minimum error and best action combination if necessary
                    if error < min_error:
                        best_total_load = total_load
                        min_error = error
                        best_actions = action_combination

                best_actions_dict: Dict[int, Rec] = {}
                if min_error > self.no_alert_threshold: # type:ignore
                    
                    for cm, action in zip(self.manager.cm_list, best_actions): #type: ignore
                        best_actions_dict[cm.unique_id] = action
                else:
                    for cm in self.manager.cm_list: 
                        best_actions_dict[cm.unique_id] = Rec.WHITE

                best_actions_by_time_by_cm[time] = best_actions_dict
                
                if self.community_model.sim_params.print_member_info:
                    print(f"Time {time}:")
                    print(f"Production : {self.production[time]}")
                    print(f"Consumption: {self.consumption[time]}")
                    print(f"Best total load : {best_total_load}")
                    print(f"Min error : {min_error}")
                    for cm in self.manager.cm_list:
                        print(f"CM {cm.unique_id}: initial load: {cm.est_member_load_day.data[time]} rec action: {best_actions_dict[cm.unique_id]} ")
                    print("----------")
                    
        return best_actions_by_time_by_cm
    
    
    
    
    '''
            if average_delta_power_val_by_rec[Rec.GREEN] >  0.0:
                total_load += act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_mod_inc'] * (trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][0] *  (average_delta_power_val_by_rec[Rec.GREEN] + p_load_by_cm[cm.unique_id][i]) +  # attentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][1] *  (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i]) + # attentive and not following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][0] * (average_delta_power_val_by_rec[Rec.GREEN] + p_load_by_cm[cm.unique_id][i]) + # inattentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][1] * (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i]))  # inattentive and not following
            if average_delta_power_val_by_rec[Rec.RED] <  0.0:
                total_load +=  act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_mod_dec'] * (trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][0] *  (average_delta_power_val_by_rec[Rec.RED] + p_load_by_cm[cm.unique_id][i]) +  # attentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][1] *  (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i]) + # attentive and not following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][0] * (average_delta_power_val_by_rec[Rec.RED] + p_load_by_cm[cm.unique_id][i]) + # inattentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][1] * (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i]))  # inattentive and not following
            if average_delta_power_val_by_rec[Rec.STRONG_GREEN] >  0.0:        
                total_load += act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_strong_inc'] * (trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][0] *  (average_delta_power_val_by_rec[Rec.STRONG_GREEN] + p_load_by_cm[cm.unique_id][i]) +  # attentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][1] *  (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i]) + # attentive and not following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][0] * (average_delta_power_val_by_rec[Rec.STRONG_GREEN] + p_load_by_cm[cm.unique_id][i]) + # inattentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][1] * (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i])) # inattentive and not following
            if average_delta_power_val_by_rec[Rec.STRONG_RED] <  0.0:       
                total_load += act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_strong_dec'] * (trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][0] *  (average_delta_power_val_by_rec[Rec.STRONG_RED] + p_load_by_cm[cm.unique_id][i]) +  # attentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][0] * emis_matrix_by_cm[cm.unique_id][0][1] *  (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i]) + # attentive and not following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][0] * (average_delta_power_val_by_rec[Rec.STRONG_RED] + p_load_by_cm[cm.unique_id][i]) + # inattentive and following
                                                                                        trans_matrix_by_cm_by_time[cm.unique_id][time][1] * emis_matrix_by_cm[cm.unique_id][1][1] * (average_delta_power_val_by_rec[Rec.WHITE] + p_load_by_cm[cm.unique_id][i])) # inattentive and not following
            '''
            
            
            
"""
        # Initialize PuLP model and decision variable dictionaries
        model = pulp.LpProblem("Problem_Optimal_Decision", pulp.LpMinimize)
        time_step_index = []
        action_index = []
        for cm in self.manager.cm_list:
            action_index.extend([f'cm_{cm.unique_id}_same', f'cm_{cm.unique_id}_mod_inc', f'cm_{cm.unique_id}_mod_dec', 
                                f'cm_{cm.unique_id}_strong_inc', f'cm_{cm.unique_id}_strong_dec'])

        # We generate the error index of length of the optimisation horizon
        #for i in range(len(self.consumption)):
        #    time_step_index.append(str(i))
        time_step_index = [0]

        error_dictionary = pulp.LpVariable.dicts("e", (i for i in time_step_index), cat='Continuous')
        act_all_agents_dict = {}

        # Create a dictionary of decision variables for each agent
        for cm in self.manager.cm_list:
            act_all_agents_dict[cm.unique_id] = pulp.LpVariable.dicts("a", (i for i in action_index if str(cm.unique_id) in i), cat='Binary')
            
        # Add the objective function to the model
        model += pulp.lpSum([error_dictionary[i] for i in time_step_index])

        # Add the constraints to the mode
        #for hour in range(len(p_load_by_cm[manager.cm_list[0].unique_id])):
        for i,(_,time) in enumerate(self.community_model.tf_day.items()):
            if self.manager.flexibility_period.hour_min <= time.hour <= self.manager.flexibility_period.hour_max:
                total_load = 0
                for cm in self.manager.cm_list:
                    # Calculate the total load for each agent at each time step
                    # At each time step, the load for each agent is adjusted with the probability to follow recommendation 
                    total_load += act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_same'] * cm.est_member_load_day.data[time]

                    # Let's calculate the expected_load_adjustment for each action separately
                    for action, rec in zip(["mod_inc", "mod_dec"], [Rec.GREEN, Rec.RED]):
                        expected_load_adjustment = 0.0
                        # Calculate the expected load adjustment based on the probabilities of the HMM states and emissions
                        for s in [0, 1]:  # assuming 0 is for attentive state, and 1 for inattentive state
                            for e in [0, 1]:  # assuming 0 is for following recommendation, and 1 for not following
                                prob_se = trans_matrix_by_cm_by_time[cm.unique_id][time][s] * emis_matrix_by_cm[cm.unique_id][s][e]
                                if e == 0:  # if following recommendation
                                    if ((rec in [Rec.GREEN, Rec.STRONG_GREEN] and average_delta_power_val_by_rec[rec] > 0.0) or 
                                        (rec in [Rec.RED, Rec.STRONG_RED] and average_delta_power_val_by_rec[rec] < 0.0)):
                                        expected_load_adjustment += prob_se * (average_delta_power_val_by_rec[rec] + cm.est_member_load_day.data[time])
                                else:  # if not following recommendation
                                    expected_load_adjustment += prob_se * cm.est_member_load_day.data[time]
                        total_load += act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_{action}'] * expected_load_adjustment
                                    
                    # Add a constraint to ensure that each agent takes exactly one action at each time step
                    model += (act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_same'] + act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_mod_inc'] +
                            act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_mod_dec'] + act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_strong_dec']
                            + act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_strong_inc']) == 1
        
                # Constraint to model the absolute value of the neeg, in a linear form
                model += (error_dictionary[0] + (total_load - self.production[time])) >= 0
                model += (error_dictionary[0] - (total_load - self.production[time])) >= 0

                # Solve the optimization problem
                model.solve(PULP_CBC_CMD(msg=0)) # type: ignore
            
                # Print information about the optimization process if requested
                if self.community_model.sim_params.print_member_info:
                    # Define the necessary variables and data structures
                    #hours = len(p_load_by_cm[str(manager.cm_list[0].unique_id)])
                    
                    print(f"PV production at time {time}: {self.production[time]}")
  
                    for cm in self.manager.cm_list:
                        print("------------------------------------")
                        print(f"{cm.unique_id} agents actions:")
                        print(f"CM load at time {time}: {cm.est_member_load_day.data[time]}")
                        print(f"CM trans: {trans_matrix_by_cm_by_time[cm.unique_id][time]}")
                        print(f"CM est: {emis_matrix_by_cm[cm.unique_id]}")
                        print(f"Action 0 (same cons): {act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_same'].varValue}")
                        print(f"Action 1 (+ moderate cons): {act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_mod_inc'].varValue}")
                        print(f"Action 2 (- moderate cons): {act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_mod_dec'].varValue}")
                        print(f"Action 3 (+ strong cons): {act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_strong_inc'].varValue}")
                        print(f"Action 4 (- strong cons): {act_all_agents_dict[cm.unique_id][f'cm_{cm.unique_id}_strong_dec'].varValue}")
                    print("------------------------------------")
                    
                # Get the chosen actions
                for cm_id, action_dict in act_all_agents_dict.items():
                    for action, var in action_dict.items():
                        if var.varValue == 1.0:
                            # Remove the prefix from the action
                            action_without_prefix = action.replace(f'cm_{cm_id}_', '')
                            # Convert the action string to a Rec enum
                            recs_by_cm_by_time[cm_id][time] = action_to_rec[action_without_prefix]
                            break  # Break after finding the chosen action for this cm
                        """