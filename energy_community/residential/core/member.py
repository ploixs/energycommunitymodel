from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Union
import numpy as np
from energy_community.core.data.member_data import MemberDataDay, MemberDataTotal
from energy_community.core.entities.member import CommunityMember
from energy_community.core.constants import AsignType, Availability, DSType, DType, MType, MemberActionIntensity, PossibleActions, PossibleStates, Rec, Stages, Willigness
import pandas as pd


if TYPE_CHECKING:
    from energy_community.residential.core.community import ResidentialCommunity
    from energy_community.residential.core.manager import ResidentialCommunityManager

class ResidentialCommunityMember(CommunityMember):
    """
    A class representing a residential community member.

    Attributes:
        unique_id (int): The unique ID of the community member.
        model (ResidentialCommunity): An object representing the community of residential households being modeled.
        agent_type (MType): The type of the community member.
        member_stages (Stages): The stages of the community member.
        member_action_intensity (MemberActionIntensity): The action intensity of the community member.
        willigness_type (Willigness): The willingness of the community member to adopt energy-saving actions.
        availability (Availability): The availability of the community member to participate in energy-saving actions.
        consumption_df (Union[pd.DataFrame, None]): A pandas DataFrame containing the consumption data for the community member, or None if there is no consumption data.
        explicit_emissions (Dict): A dictionary containing explicit emission values, if specified.
    """

    def __init__(self, unique_id, 
                 cm_description: str,  
                 model: 'ResidentialCommunity', 
                 agent_type: MType, 
                 member_stages: Stages, 
                 member_action_intensity: MemberActionIntensity, 
                 willigness_type: Willigness, 
                 availability: Availability, 
                 consumption_df: Union[pd.DataFrame, None], 
                 model_type: str,
                 explicit_emissions:  Union[List[List[float]], None],
                 explicit_transmission:  Union[List[List[float]], None]):
        """
        Initializes a new instance of the ResidentialCommunityMember class.

        Parameters:
            unique_id (int): The unique ID of the community member.
            model (ResidentialCommunity): An object representing the community of residential households being modeled.
            agent_type (MType): The type of the community member.
            member_stages (Stages): The simulation stage of the community member.
            member_action_intensity (MemberActionIntensity): The action intensity of the community member.
            willigness_type (Willigness): The willingness of the community member to adopt energy-saving actions.
            availability (Availability): The availability of the community member to participate in energy-saving actions.
            consumption_df (Union[pd.DataFrame, None]): A pandas DataFrame containing the consumption data for the community member, or None if there is no consumption data.
            explicit_emissions (Dict): A dictionary containing explicit emission values, if specified.
        """
        super().__init__(unique_id, model, agent_type, member_stages, member_action_intensity, willigness_type, availability)
        
        if not cm_description:
            raise ValueError("Description not set!")
        
        self.description = cm_description
        self.model_type = model_type
        self.com_model : 'ResidentialCommunity' = model
        self.consumption_df = consumption_df
        self.previous_state = PossibleStates.ATTENTIVE
        self.current_state = None # new state that is chosen every hour
        self.sim_member_states_day = MemberDataDay(DSType.SIMULATED, DType.STATE, AsignType.MEMBER) # a data bufer to track the states during a day
        self.sim_member_states_total = MemberDataTotal(DSType.SIMULATED, DType.STATE, AsignType.MEMBER, None) # track states over the simulation
        self.identified_type : Union[MType, None] = None
        self.measured_contribution_ratio : Union[float, None] = None
        
        self.true_transition_matrix: List[List[float]] = [[]]
        self.true_trans_expl = {(PossibleStates.ATTENTIVE, PossibleStates.ATTENTIVE): None,
                                    (PossibleStates.ATTENTIVE, PossibleStates.INNATENTIVE): None,
                                    (PossibleStates.INNATENTIVE, PossibleStates.ATTENTIVE): None,
                                    (PossibleStates.INNATENTIVE, PossibleStates.INNATENTIVE): None} 
        self.true_emission_matrix: List[List[float]] = [[]]
        self.true_emit_expl = {}
        self.true_emit_expl[(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW)] = None
        self.true_emit_expl[(PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW)] =  None
        self.true_emit_expl[(PossibleStates.INNATENTIVE, PossibleActions.FOLLOW)] =  None
        self.true_emit_expl[(PossibleStates.INNATENTIVE, PossibleActions.NOT_FOLLOW)] = None
        
        # Emission matrix standard mapping for each type
        self.TYPE_EMISSION_MTX_MAP = {MType.GOOD: [[0.7, 0.3],[0.5,0.5]],
                                    MType.NORMAL: [[0.5, 0.5],[0.5,0.5]],
                                    MType.BAD: [[0.3, 0.7],[0.5,0.5]],
                                    MType.IDEAL: [[1, 0],[0.5,0.5]]}
        
        # If transmission matrix is given as parameter at member initialization
        if self.model_type == "Markov":
            if explicit_transmission:
                # Assign it
                self.true_transition_matrix = explicit_transmission
                # For writing to files, define explicit forms for emission and transmission matrix
            
            else:
                if self.type == MType.IDEAL:
                    self.true_transition_matrix = [[1, 0], [1, 0]]
                else:
                    self.true_transition_matrix = [[0.5, 0.5], [0.5, 0.5]]
                # For writing to files, define explicit forms for emission and transmission matrix
            self.true_trans_expl = {(PossibleStates.ATTENTIVE, PossibleStates.ATTENTIVE): self.true_transition_matrix[0][0],
                                    (PossibleStates.ATTENTIVE, PossibleStates.INNATENTIVE): self.true_transition_matrix[0][1],
                                    (PossibleStates.INNATENTIVE, PossibleStates.ATTENTIVE): self.true_transition_matrix[1][0],
                                    (PossibleStates.INNATENTIVE, PossibleStates.INNATENTIVE): self.true_transition_matrix[1][1]} 
                
            
            # Ifemission matrix is given as parameter at member initialization
            if explicit_emissions:
                self.true_emission_matrix = explicit_emissions
            else:
                self.true_emission_matrix = self.TYPE_EMISSION_MTX_MAP[self.type]
            self.true_emit_expl = {}
            self.true_emit_expl[(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW)] = self.true_emission_matrix[0][0]
            self.true_emit_expl[(PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW)] = self.true_emission_matrix[0][1]
            self.true_emit_expl[(PossibleStates.INNATENTIVE, PossibleActions.FOLLOW)] = self.true_emission_matrix[1][1]
            self.true_emit_expl[(PossibleStates.INNATENTIVE, PossibleActions.NOT_FOLLOW)] = self.true_emission_matrix[1][1]
        
        if self.model_type == "Markov" and not self.true_emission_matrix:
            raise ValueError("Emission matrix not set for member following a HMM!")
        if self.model_type == "Markov" and not self.true_transition_matrix:
            raise ValueError("Transmission matrix not set for member following a HMM!")
        

        if isinstance(self.consumption_df, pd.DataFrame):
            # Create a MemberDataTotal object for estimated member load data, if a consumption DataFrame is provided.
            self.est_member_load_total = MemberDataTotal(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.MEMBER, list(self.com_model.tf_total.values()), consumption_df['Consumption [Wh]'].to_list()) # type: ignore
        else:
            # Create a MemberDataTotal object for estimated member load data, if no consumption DataFrame is provided.
            self.est_member_load_total = MemberDataTotal(DSType.ESTIMATED, DType.CONSUMPTION, AsignType.MEMBER, list(self.com_model.tf_total.values()))
        
        # Create MemberDataDay and MemberDataTotal objects for estimated member action data.
        self.est_member_actions_day, self.est_member_actions_total  = MemberDataDay(DSType.ESTIMATED, DType.ACTIONS), MemberDataTotal(DSType.ESTIMATED, DType.ACTIONS)

        self.sim_member_followed_day, self.sim_member_followed_total  = MemberDataDay(DSType.SIMULATED, DType.ACTIONS), MemberDataTotal(DSType.SIMULATED, DType.ACTIONS)

        
    
    def init_cm_day(self):
        """This function is executed at the start of each day"""

        self.sim_member_load_day.data = {time_value : 0 for _, time_value in self.com_model.tf_day.items()}
        self.sim_member_actions_day.data = {time_value : Rec.WHITE for _, time_value in self.com_model.tf_day.items()}
        self.sim_member_rec_day.data = {time_value :Rec.WHITE for _, time_value in self.com_model.tf_day.items()}
        self.sim_member_states_day.data = {time_value : PossibleStates.ATTENTIVE for _, time_value in self.com_model.tf_day.items()}
        
        self.sim_member_followed_day.data = {time_value : PossibleActions.FOLLOW for _, time_value in self.com_model.tf_day.items()}
        self.set_est_member_consumption_current_day() # based on the hours to commute and the mean est icp in morning/afternoon
        
        
    def set_est_member_consumption_current_day(self):
        """Sets the est_consumption_current_day_list according to self.consumption_df

        Raises:
            ValueError: if there are no estimated hours to commute for the current day, raise an error
            ValueError: if there are no ICP values for the current day, raise an error
        """
        
        if self.consumption_df is None:
            raise ValueError("self.consumption_df not set!")

        temp_model = {}
        for time in list(self.com_model.tf_day.values()):
            temp_model[time] = self.est_member_load_total.data[time]

        temp_actions_model = {time : Rec.WHITE for i,time in self.com_model.tf_day.items()}

        self.est_member_load_day.data = temp_model  # type: ignore
        self.est_member_actions_day.data = temp_actions_model # type: ignore
        
        
    def set_df_for_plotting(self):
        """
        Set the `plotting_df` attribute of the ResidentialCommunity object by creating a pandas DataFrame
        containing estimated and simulated member load, action, recommendation, and state data.

        Raises:
            ValueError: If the estimated or simulated member load data is not set.
        """
        
        # Check that the estimated and simulated member load data is set.
        if len(self.est_member_load_total.data) == 0:
            raise ValueError("Est member load data not set!")
        if len(self.sim_member_load_total.data) == 0:
            raise ValueError("Sim member load data not set!") 
        
        # Create a list of timestamps corresponding to the simulation time frame.
        time_frame = [pd.Timestamp(time).tz_localize(None) for time in list(self.com_model.tf_total.values())] # the simulation time frame
        
        # Convert the timestamps to Python datetime objects.
        new_tf = [x.to_pydatetime() for x in time_frame] # convert it to timestamp
        
        # Create a pandas DataFrame containing estimated and simulated member load, action, recommendation, and state data.
        df = pd.DataFrame({f'{DSType.ESTIMATED}_{DType.CONSUMPTION}': list(self.est_member_load_total.data.values()), 
                           f'{DSType.SIMULATED}_{DType.CONSUMPTION}': list(self.sim_member_load_total.data.values()),
                            f'{DSType.SIMULATED}_{DType.ACTIONS}': list(self.sim_member_actions_total.data.values()), 
                            f'{DSType.SIMULATED}_{DType.RECOMMENDATIONS}': list(self.sim_member_rec_total.data.values()),
                            f'{DSType.SIMULATED}_{DType.STATE}': list(self.sim_member_states_total.data.values())},
                        index = new_tf)

        # Set the `plotting_df` attribute of the ResidentialCommunity object to the created DataFrame.
        self.plotting_df = df