

from collections import defaultdict
import math
from typing import Dict, List, Tuple

from energy_community.core.constants import PossibleActions, PossibleStates

# Note: old version, quite slow

def logsumexp(values: List[float]) -> float:
    max_value = max(values)
    return max_value + math.log(sum(math.exp(value - max_value) for value in values))


def log_forward(log_prior: Dict[PossibleStates, float], 
                log_trans: Dict[Tuple[PossibleStates, PossibleStates], float], 
                log_emit: Dict[Tuple[PossibleStates, PossibleActions], float], 
                obs: List[PossibleActions]) -> List[Dict[PossibleStates, float]]:
  
    # Initialize the log forward probabilities alpha
    log_alpha = [{state: float('-inf') for state in PossibleStates} for _ in range(len(obs))]
    
    # Compute the initial log forward probabilities using the log prior and the first observation
    for state in PossibleStates:
        log_alpha[0][state] = log_prior[state] + log_emit[state, obs[0]]
        
    # Iterate over the rest of the observations
    for t in range(1, len(obs)):
        for state_to in PossibleStates:
            prob = []
            for state_from in PossibleStates:
                prob.append(log_alpha[t-1][state_from] + log_trans[state_from, state_to])
            log_alpha[t][state_to] = logsumexp(prob) + log_emit[state_to, obs[t]]

    return log_alpha


def log_backward(
                log_trans: Dict[Tuple[PossibleStates, PossibleStates], float], 
                log_emit: Dict[Tuple[PossibleStates, PossibleActions], float], 
                obs: List[PossibleActions]) -> List[Dict[PossibleStates, float]]:

    # Initialize the backward probabilities beta
    log_beta = [{state: float('-inf') for state in PossibleStates} for _ in range(len(obs))]

    # Set the backward probability for the final state to 1 (0 in log space)
    for state in PossibleStates:
        log_beta[-1][state] = 0.0

    # Iterate over the observations in reverse
    for t in range(len(obs)-2, -1, -1):
        for state_from in PossibleStates:
            prob = []
            for state_to in PossibleStates:
                prob.append(log_trans[state_from, state_to] + log_emit[state_to, obs[t+1]] + log_beta[t+1][state_to])
            log_beta[t][state_from] = logsumexp(prob)

    return log_beta



def log_baum_welch(obs, init_prior, init_trans, init_emit, max_iter=100, tol=1e-3):
    prior = {state: math.log(p) for state, p in init_prior.items()}
    trans = {(s_from, s_to): math.log(p) for (s_from, s_to), p in init_trans.items()}
    emit = {(state, action): math.log(p) for (state, action), p in init_emit.items()}
    
    old_log_likelihood = None

    for n in range(max_iter):
        log_alpha = log_forward(prior, trans, emit, obs)
        log_beta = log_backward(trans, emit, obs)
        
        xi = defaultdict(float)
        gamma = defaultdict(float)

        # E step
        for t in range(len(obs) - 1):
            for s_from in PossibleStates:
                for s_to in PossibleStates:
                    xi[(t, s_from, s_to)] = log_alpha[t][s_from] + trans[(s_from, s_to)] + emit[(s_to, obs[t+1])] + log_beta[t+1][s_to]
                    
            norm = logsumexp([xi[(t, s_from, s_to)] for s_from in PossibleStates for s_to in PossibleStates])
            for s_from in PossibleStates:
                for s_to in PossibleStates:
                    xi[(t, s_from, s_to)] -= norm

        for t in range(len(obs)):
            for s in PossibleStates:
                gamma[(t, s)] = log_alpha[t][s] + log_beta[t][s]
                
            norm = logsumexp([gamma[(t, s)] for s in PossibleStates])
            for s in PossibleStates:
                gamma[(t, s)] -= norm
        
        # M step
        for s in PossibleStates:
            prior[s] = gamma[(0, s)]
            
        for s_from in PossibleStates:
            for s_to in PossibleStates:
                trans[(s_from, s_to)] = logsumexp([xi[(t, s_from, s_to)] for t in range(len(obs) - 1)]) \
                - logsumexp([gamma[(t, s_from)] for t in range(len(obs) - 1)])

        for s in PossibleStates:
            for o in PossibleActions:
                mask = [1 if obs[t] == o else 0 for t in range(len(obs))]
                emit[(s, o)] = logsumexp([gamma[(t, s)] for t, m in zip(range(len(obs)), mask) if m]) \
                - logsumexp([gamma[(t, s)] for t in range(len(obs))])
        
        log_likelihood = logsumexp([log_alpha[-1][s] for s in PossibleStates])  # Compute log likelihood for convergence check
        print(f"Iteration {n}:",log_likelihood)
        # Check for convergence using the computed log_likelihood.
        if old_log_likelihood is not None and abs(log_likelihood - old_log_likelihood) < tol:
            break

        old_log_likelihood = log_likelihood  # Update old log likelihood


    # Convert back to normal probabilities
    prior = {state: math.exp(p) for state, p in prior.items()}
    trans = {(s_from, s_to): math.exp(p) for (s_from, s_to), p in trans.items()}
    emit = {(state, action): math.exp(p) for (state, action), p in emit.items()}
    
    return prior, trans, emit
