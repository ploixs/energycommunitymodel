import os
import random
from typing import TYPE_CHECKING, List

import numpy as np
from energy_community.core.constants import ExpectancyDev, MType, PossibleActions, PossibleStates, PrivacyLevel, Stages, Strategy, TargetOfRec
from energy_community.core.energy.get_power_data import get_consumption_for_a_house_from_csv
from energy_community.helpers.scripting import get_member_motivation_based_on_type, get_member_type_based_on_attentive_following_prob, get_simulation_parameters, prepare_results_folder, run_simulation
from energy_community.residential.core.residential_parameters import ResidentialParameters
from energy_community.residential.core.community import ResidentialCommunity
from energy_community.residential.core.profiles import OneTurnResidentialMarkovCommunityMember, OneTurnResidentialManager, OneTurnResidentialPresenceCommunityMember, OneTurnResidentialSimpleCommunityMember, TwoTurnsResidentialMarkovCommunityMember, TwoTurnsResidentialManager
from configuration import Config
import csv

if TYPE_CHECKING:
    from energy_community.residential.core.community import ResidentialCommunity

def main(script_config):
    
    # Set the attributes of the Config object based on the configuration data for the residential script
    
    # Get the list of simulation scenarios to run from the configuration object
    simulation_scenarios_to_run = range(script_config["simulation_scenarios_to_run"]['start'],
                                        script_config["simulation_scenarios_to_run"]['end'])

    # Get the plot_in_browser flag from the configuration object
    plot_in_browser = script_config["plot_in_browser"]

    # Get the print_flag from the configuration object
    print_flag = script_config["print_flag"]

    # Get the list of house IDs from the configuration object
    HOUSE_IDS = script_config["house_ids"]
    
    # Record models
    models : List["ResidentialCommunity"] = []

    # Iterate through each simulation scenario
    for simulation_id in simulation_scenarios_to_run:
        
        # Print a message in the console (because there are many simulation scenarios)
        print(f"Running simulation {simulation_id}...")

        # Prepare the results folder for the current simulation
        results_folder_path = prepare_results_folder(simulation_id, script_config["paths"])

        # Get the simulation parameters for the current simulation
        sim_params = get_simulation_parameters(simulation_id, script_config["paths"], community_type="residential", print_flag=print_flag)

        if type(sim_params) != ResidentialParameters:
            raise ValueError("Parameters not set appropriate")
        
        # Instantiate the ResidentialCommunity model with the simulation parameters and stages
        model = ResidentialCommunity(sim_params, results_folder_path, sim_params.stages)

        # Get the manager for the current simulation
        manager = get_manager(simulation_id, model, sim_params, script_config["pv_surface"])

        # If a manager is defined, assign it to the community
        if manager:
            model.community_manager = manager

        # Create the community members for the current simulation
        members = create_community_members(simulation_id, HOUSE_IDS, sim_params, model)

        # Add the community members to the community
        model.add_new_community_members(members)

        # Run the simulation on the community model
        run_simulation(model)

        # If plot_in_browser is True, plot the results in the browser
        if plot_in_browser:
            model.show_results_plot_in_browser()


                     
def create_community_members(simulation_id: int, house_ids: list[int], sim_params: ResidentialParameters, model: ResidentialCommunity) -> List:
    """
    Creates a list of residential community members based on the given input parameters.

    Parameters:
        house_ids (List[int]): A list of integers representing the IDs of the houses whose consumption data will be retrieved.
        sim_params (ResidentialParam): An object containing simulation parameters such as the starting and ending dates of the simulation and the simulation stage.
        model (ResidentialCommunity): An object representing the community of residential households being modeled.

    Returns:
        List: A list of ResidentialCommunityMember objects.

    """
    # Initialize an empty list to store the community members
    members = []
    thresholds_scenario_by_match = {1: {(0,MType.IDEAL): [1, 1], (1,MType.IDEAL): [1, 1], (2,MType.IDEAL): [1, 1], (3,MType.IDEAL): [1, 1], (4,MType.IDEAL): [1, 1]},
                                    2: {(0,MType.NORMAL): [0.45, 0.55], (1,MType.NORMAL): [0.45, 0.55], (2,MType.NORMAL): [0.45, 0.55], (3,MType.NORMAL): [0.45, 0.55], (4,MType.NORMAL): [0.45, 0.55]},
                                    3: {(0,MType.GOOD): [0.6, 1], (1, MType.GOOD): [0.6, 1],(2, MType.GOOD): [0.6, 1], (2,MType.GOOD): [0.6, 1],(3,MType.GOOD): [0.6, 1], (4,MType.GOOD): [0.6, 1]},
                                    4: {(0,MType.BAD): [0, 0.4], (1, MType.BAD): [0, 0.4],(2,MType.BAD): [0, 0.4], (3,MType.BAD): [0, 0.4],(4,MType.BAD): [0, 0.4]},
                                    6: {(0,MType.IDEAL): [1, 1], (1,MType.IDEAL): [1, 1], (2,MType.IDEAL): [1, 1], (3,MType.IDEAL): [1, 1], (4,MType.IDEAL): [1, 1]},
                                    7: {(0,MType.NORMAL): [0.45, 0.55], (1,MType.NORMAL): [0.45, 0.55], (2,MType.NORMAL): [0.45, 0.55], (3,MType.NORMAL): [0.45, 0.55], (4,MType.NORMAL): [0.45, 0.55]},
                                    8: {(0,MType.GOOD): [0.6, 1], (1, MType.GOOD): [0.6, 1],(2, MType.GOOD): [0.6, 1], (2,MType.GOOD): [0.6, 1],(3,MType.GOOD): [0.6, 1], (4,MType.GOOD): [0.6, 1]},
                                    9: {(0,MType.BAD): [0, 0.4], (1, MType.BAD): [0, 0.4],(2,MType.BAD): [0, 0.4], (3,MType.BAD): [0, 0.4],(4,MType.BAD): [0, 0.4]}}
                                    
    if simulation_id in thresholds_scenario_by_match:
        for match, thresholds in thresholds_scenario_by_match[simulation_id].items():
        #for house_id in house_ids:
            # Retrieve the consumption data for the current house
            df = get_consumption_for_a_house_from_csv(house_id=house_ids[match[0]], 
                                                      start_datetime=sim_params.starting_date, 
                                                      end_datetime=sim_params.ending_date, 
                                                      models_root_path=sim_params.root_folder_name, 
                                                      community_type=sim_params.community_folder_name, 
                                                      data_folder_name=sim_params.data_folder_name)
            
            x = random.uniform(thresholds[0], thresholds[1])
            # Get member true type based on this probability
            cm_true_type = get_member_type_based_on_attentive_following_prob(x)
            cm_desc_motivation = get_member_motivation_based_on_type(cm_true_type)
            
            exp_transmission_temp = [[0.5, 0.5],[0.5, 0.5]]
            if cm_desc_motivation == "Enthusiastic":
                exp_transmission_temp = [[0.8, 0.2],[0.8, 0.2]]
            if cm_desc_motivation == "Reluctant":
                exp_transmission_temp = [[0.2, 0.8],[0.2, 0.8]]
            if cm_desc_motivation == "Ideal":
                exp_transmission_temp = [[1, 0],[0.5, 0.5]]
            
            
            # Set emission matrix based on probability 
            temp_emis = [[x, 1 - x], [0.5,  0.5]]
            
            new_cm = OneTurnResidentialMarkovCommunityMember(unique_id=house_ids[match[0]] % 1000, 
                                                            cm_description="Balanced " + cm_desc_motivation, 
                                                            cm_type=cm_true_type, 
                                                            model=model, 
                                                            consumption_df=df, 
                                                            explicit_emissions=temp_emis, 
                                                            explicit_transmission=exp_transmission_temp) 
            # Append the new community member to the list of members
            members.append(new_cm)  
    else:
        for house_id in house_ids:
            # Retrieve the consumption data for the current house
            df = get_consumption_for_a_house_from_csv(house_id=house_id, 
                                                      start_datetime=sim_params.starting_date, 
                                                      end_datetime=sim_params.ending_date, 
                                                      models_root_path=sim_params.root_folder_name, 
                                                      community_type=sim_params.community_folder_name, 
                                                      data_folder_name=sim_params.data_folder_name)

            x = random.random()
            # Get member true type based on this probability
            cm_true_type = get_member_type_based_on_attentive_following_prob(x)
            cm_desc_motivation = get_member_motivation_based_on_type(cm_true_type)
            
            # Set emission matrix based on probability 
            temp_emis = [[x, 1 - x], [0.5,  0.5]]
            exp_transmission_temp = [[0.5, 0.5],[0.5, 0.5]]
            if cm_desc_motivation == "Enthusiastic":
                exp_transmission_temp = [[0.8, 0.2],[0.8, 0.2]]
            if cm_desc_motivation == "Reluctant":
                exp_transmission_temp = [[0.2, 0.8],[0.2, 0.8]]
            
            
            new_cm = OneTurnResidentialMarkovCommunityMember(unique_id=house_id % 1000, 
                                                            cm_description="Balanced " + cm_desc_motivation, 
                                                            cm_type=cm_true_type, 
                                                            model=model, 
                                                            consumption_df=df, 
                                                            explicit_emissions=temp_emis, 
                                                            explicit_transmission=exp_transmission_temp) 
            # Append the new community member to the list of members
            members.append(new_cm)  

    
    # Return the list of community members
    return members


def get_manager(simulation_id: int, model: ResidentialCommunity, sim_params: ResidentialParameters, pv_surface: float):
    
    # Initialize manager variable to None
    manager = None

    # Based on the simulation_id, create an instance of the appropriate manager class with the required parameters.
    # For each simulation_id, different parameters are passed to either the OneTurnResidentialManager or TwoTurnsResidentialManager classes.

    # The following dictionary maps a configuration for a manager instance to each simulation_id (1 to 15) with their respective parameters.

    if simulation_id <=5:
        manager = OneTurnResidentialManager(pv_surface=pv_surface, 
                                            model=model, 
                                            target_of_recommendations=TargetOfRec.EVERYONE, 
                                            expectancy_development=ExpectancyDev.BASIC, 
                                            strategy=Strategy.COACHING, 
                                            privacy_level=PrivacyLevel.PRIVATE, 
                                            no_alert_threshold=sim_params.get_comfort_parameter(), 
                                            is_known_community=True)
    else:
        manager = OneTurnResidentialManager(pv_surface=pv_surface, 
                                            model=model, 
                                            target_of_recommendations=TargetOfRec.EVERYONE, 
                                            expectancy_development=ExpectancyDev.ADAPTIVE, 
                                            strategy=Strategy.OPTIMAL_COACHING, 
                                            privacy_level=PrivacyLevel.PRIVATE, 
                                            no_alert_threshold=sim_params.get_comfort_parameter(), 
                                            is_known_community=True)


    if not manager:
        raise ValueError("Manager not set!")
    
    return manager

 
if __name__ == "__main__":
    
    config = Config()
    residential_config = config.get_script_config("residential_realistic_adaptive_coaching")
    main(residential_config)
        