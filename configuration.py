import yaml

class Config:
    def __init__(self, config_file="config.yaml"):
        # Open the configuration file and read its contents
        with open(config_file) as file:
            self.config_data = yaml.safe_load(file)

        
    def get_script_config(self, configuration_name: str):
        """Get the script configuration, given a configuration_name.

        Args:
            configuration_name (str): the required configuration name for the script.

        Raises:
            ValueError: If no configuration found, raise an error.

        Returns:
            _type_: a configuration for a script
        """
        
        # Retrieve the configuration data for the given configuration name
        script_config = self.config_data.get(configuration_name)
        
        # If no configuration data is found for the residential script, raise an error
        if not script_config:
            raise ValueError(f"No configuration found for residential.py")
        
        return script_config
    
