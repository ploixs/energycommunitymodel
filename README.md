# RENO - a multi-agent simulation tool for a **r**enewable **en**ergy c**o**mmunity

Author: Mircea Stefan Simoiu _mircea_stefan.simoiu@upb.ro_

Department of Automatic Control and Industrial Informatics, Faculty of Automatic Control and Computers, University Politehnica of Bucharest, Bucharest, Romania

Univ. Grenoble Alpes, CNRS, Grenoble INP*, G-SCOP, 38000 Grenoble, France

GitLab public repository: https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/energycommunitymodel

## Description 

The project implements a multi-agent simulation tool for simulating energy communities. 

The aim of the project is to show the collective effect of an intelligent recommendation system (a community "manager" service) which provides recommendations to community members regarding their daily energy consumption activities. The general architecture of such a system is presented below:

![Community architecture](Figure_community_architecture.png)

It is assumed that each energy community has a shared PV plant that produces renewable energy. This energy is shared among members. 

The community is modeled as a multi-agent system, meaning that each community member is modeled as an individual, autonomous agent. The manager is modeled as an agent too, implementing various recommendation strategies.


## Project Structure

- `energy_community/core` - the core for the multi-agent simulation framework; it contains generic definitions for a community member, a community manager and an energy community model. The folder also contains models for the data structures that are further used in simulations at individual and collective level (energy, recommendations data, etc.)
- `energy_community/helpers` - a set of modules with functions for scripting, result analysis, and simulation-related mechanisms.
- `energy_community/subway` - implementation of the simulation model for a community of commuting passengers living near a subway station
    - `energy_community/subway/results` - default results folder
    - `energy_community/subway/data` - input data folder
    - `energy_community/subway/scenarios_strategy_comparison.yaml` - default scenario configuration file
- `energy_community/residential` - implementation of the simulation model for a simple residential community, under the influence of various recommendation strategies while implementing different community member profiles
    - `energy_community/residential/input_data` - input weather and consumption data
    - `energy_community/residential/core` - core model definitions
    - `energy_community/residential/results` - structures and functions for results analysis

## Getting Started

To set up the project locally, follow these steps:

1. Clone the repository
2. Install the required packages by running `pip install -r requirements.txt`

## Configuration

- The file `config.yaml` represents the general configuration file for all the case studies available
- For each case study, there is a particular configuration file (example: `scenarios_monte_carlo.yaml`) which provides specific parameter definitions for the respective case study.

## Available DEMOs

Several Jupyter Notebooks are available as demo case studies, which describe how to configure, simulate, and analyze an energy community. Each demo file name starts with 'tutorial_'.

For example, to run a demo for the community near the subway station:

**Step 1**: Use _mybinder.org_ by clicking on [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fploixs%2Fenergycommunitymodel.git/HEAD) . Alternatively, if _mybinder_ is unavailable, clone the project on your computer and install the requirements.

**Step 2**: Open the Jupyter Notebook [tutorial_subway_community.ipynb](tutorial_subway_community.ipynb). In this notebook, a sample example with 20 fully flexible community members is implemented.

**Step 3**: Run the cells. The results will be generated [here](energy_community/subway/results/). The analysis of on the results can be investigated at the end of each notebook.

## Scripts and case studies

Several case studies are formulated in scripts (file name starting with 'residential_'/'subway_'). For each case study, results can be analysed using the corresponding analysis script ('starting with 'analysis_')

A particular case study is dedicated to investigating a community placed near a subway station composed of members that influence the energy consumption of the station while commuting to work. The case study investigates the collective effect of a recommendation strategies that provides the best commuting times such as the system performances are maximised and community member dissatisfaction is take into account. The method is fully described in the paper:  *Managing human involvement in an energy community: application to a subway station* (https://doi.org/10.1016/j.scs.2023.104597).

To run the simulations in the paper, follow these steps:

- download the zip project on personal computer (result files will use several hundreds MBs)
- install the packages from requirements.txt (pip install -r requirements.txt)
- run the following scripts, considering simulation scenarios from 9 to 15 from the configuration file `scenarios_strategy_comparison.yaml`:  
 - [subway_strategy_comparison.py](subway_strategy_comparison.py) to run the simulations and generate simulated data
 - [analysis_subway_peak_shifting.py](analysis_subway_peak_shifting.py) for analyzing the results using various indicators and plotting individual scenario analyses.





