import random
from typing import List
from energy_community.core.constants import ExpectancyDev, PrivacyLevel, Strategy, TargetOfRec
from energy_community.core.energy.get_power_data import get_mean_icp_values_from_files
from energy_community.helpers.scripting import get_simulation_parameters, prepare_results_folder, run_simulation
from energy_community.subway.SubwayParameters import SubwayParameters
from energy_community.subway.core import SubwayCommunity
from energy_community.subway.profiles import OneTurnFullFlexibilitySubwayMember, OneTurnNoFlexibilitySubwayMember, OneTurnSemiFlexibleSubwayMember, OneTurnSubwayManager
from configuration import Config


def main(script_config):
    
    # Set the attributes of the Config object based on the configuration data for the residential script
    
    # Get the list of simulation scenarios to run from the configuration object
    simulation_scenarios_to_run = range(script_config["simulation_scenarios_to_run"]['start'],script_config["simulation_scenarios_to_run"]['end'])

    # Get the plot_in_browser flag from the configuration object
    plot_in_browser = script_config["plot_in_browser"]

    # Get the print_flag from the configuration object
    print_flag = script_config["print_flag"]

    # Iterate through each simulation scenario
    for simulation_id in simulation_scenarios_to_run:
        
        # Print a message in the console (because there are many simulation scenarios)
        print(f"Running simulation {simulation_id}...")

        # Prepare the results folder for the current simulation
        results_folder_path = prepare_results_folder(simulation_id, script_config["paths"])

        # Get the simulation parameters for the current simulation
        sim_params = get_simulation_parameters(simulation_id, script_config["paths"], community_type="subway", print_flag=print_flag)
        
        if type(sim_params) != SubwayParameters:
            raise ValueError("Parameters not set appropriate")
        
        est_mean_icp_df = get_mean_icp_values_from_files(sim_params.starting_date, sim_params.ending_date) # get consumption data
        
        # Instantiate the SubwayCommunity model with the simulation parameters
        model = SubwayCommunity(sim_params, results_folder_path)
        
        # Get the manager for the current simulation
        manager = get_manager(simulation_id, model)
        
        # If a manager is defined, assign it to the community
        if manager:
            model.community_manager = manager
        
        # Create the community members for the current simulation
        members = create_community_members(simulation_id, model, est_mean_icp_df)

        # Add the community members to the community
        model.add_new_community_members(members)
        
         # Run the simulation on the community model
        run_simulation(model)

        # If plot_in_browser is True, plot the results in the browser
        if plot_in_browser:
            model.plot_results_in_browser()
        
        
def get_pref_hr_list(pref_hrs : List[int], distribution: List[float], cm_count: int) -> List[int]:
    """A function to propose a list of preferred hours to commute for all the members, according to the given distribution.

    Args:
        pref_hrs (List[int]): possible preferred hours to commute
        distribution (List[float]): the distribution between preferred hours
        cm_count (int): total number of community members

    Returns:
        List[int]: a list of preferred hours to commute, with the same size as the cm_count
    """
    pref_hours_dist = {pref_hour : int(distribution[i] * cm_count)  for i,pref_hour in enumerate(pref_hrs)}
    temp =  list([pref_hour] * times for pref_hour, times in pref_hours_dist.items())
    pref_hr_list = [item for sublist in temp for item in sublist]   
    return pref_hr_list

def create_community_members(simulation_id: int, model: SubwayCommunity, est_mean_icp_df):
    members = []
    if simulation_id in [1,2,3]:
        cm_count = 20
        pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
        for i in range(cm_count):
            new_cm = OneTurnFullFlexibilitySubwayMember(i,model, est_mean_icp_df, pref_hr_list[i] )
            members.append(new_cm)
    
    if simulation_id in [4,5,6]:
        cm_count = 20
        pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
        for i in range(cm_count):
            new_cm = OneTurnSemiFlexibleSubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
            members.append(new_cm)
    
    if simulation_id in [7]:
        cm_count = 20
        pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
        for i in range(cm_count):
            new_cm = OneTurnNoFlexibilitySubwayMember(i,model, est_mean_icp_df,pref_hr_list[i])
            members.append(new_cm)
            
    if simulation_id in [9,13]:
        cm_count = 4000
        pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
        for i in range(cm_count):
            new_cm = OneTurnFullFlexibilitySubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
            members.append(new_cm)
            
    if simulation_id in [10,14]:
        cm_count = 4000
        pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
        for i in range(cm_count):
            new_cm = OneTurnSemiFlexibleSubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
            members.append(new_cm)
            
    if simulation_id == 11:
        cm_count = 4000
        pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
        for i in range(cm_count):
            new_cm = OneTurnNoFlexibilitySubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
            members.append(new_cm)
            
    if simulation_id in [12,15]:
        cm_count = 4000
        pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count) # set pref hours for all members
        random.shuffle(pref_hr_list) # shuffle prefered hours between members
        cm_count_flexible = 0.2 * cm_count # 20% are fully flexible members
        cm_flexible_ids = list(range(int(cm_count_flexible)))
        cm_count_semi_flexible = 0.6 * cm_count # 60% are semi-fully flexible members
        cm_semi_flexible_ids = list(range(cm_flexible_ids[-1] + 1,cm_flexible_ids[-1] + int(cm_count_semi_flexible) + 1))
        cm_count_no_flexibility = 0.2 * cm_count # 20% have no flexibility
        cm_no_flexibility_ids = list(range(cm_semi_flexible_ids[-1] + 1,cm_semi_flexible_ids[-1] + int(cm_count_no_flexibility) + 1))
        
        for i in range(int(cm_count_flexible)):
            new_cm = OneTurnFullFlexibilitySubwayMember(cm_flexible_ids[i] ,model, est_mean_icp_df, pref_hr_list[i])
            members.append(new_cm)
        
        for i in range(int(cm_count_semi_flexible)):
            new_cm = OneTurnSemiFlexibleSubwayMember(cm_semi_flexible_ids[i],model, est_mean_icp_df, pref_hr_list[i + int(cm_count_flexible)])
            members.append(new_cm)
            
        for i in range(int(cm_count_no_flexibility)):
            new_cm = OneTurnNoFlexibilitySubwayMember(cm_no_flexibility_ids[i],model, est_mean_icp_df, pref_hr_list[i + int(cm_count_flexible + cm_count_semi_flexible)])
            members.append(new_cm)
    
    return members

def get_manager(simulation_id: int, model: SubwayCommunity):
    
    # Initialize manager variable to None
    manager = None

    # Based on the simulation_id, create an instance of the appropriate manager class with the required parameters.
    # For each simulation_id, different parameters are passed to either the OneTurnResidentialManager or TwoTurnsResidentialManager classes.

    if simulation_id in [1, 4, 7]:
        manager = OneTurnSubwayManager(999, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.1, weight_dis=0.9)
    if simulation_id in [2, 5, 8]:
        manager = OneTurnSubwayManager(999, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.5, weight_dis=0.5)
    if simulation_id in [3, 6]:
        manager = OneTurnSubwayManager(999, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.9, weight_dis=0.1)
    if simulation_id in [9,10,11]:
        manager = OneTurnSubwayManager(10000, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=1, weight_dis=0)
    if simulation_id == 12:
        manager = OneTurnSubwayManager(10000, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.5, weight_dis=0.5)
    if simulation_id in [13,14,15]:
        manager = OneTurnSubwayManager(10000, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.INFORMATIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0, weight_dis=0)

    if manager is None:
            raise ValueError("Manager not set!")
        
    return manager        
        
        
        
if __name__ == "__main__":
    
    config = Config()
    residential_config = config.get_script_config("subway")
    main(residential_config)