import logging
import os
import sys
import matplotlib.pyplot as plt
from typing import List
from tzlocal import get_localzone 
from matplotlib.ticker import FormatStrFormatter

from energy_community.helpers.Spec import Spec
import pandas as pd
from energy_community.core.constants import FLEX_MAP, STRATEGY_MAP, AsignType, DSType, IndicatorsAll, IndicatorsLP, Strategy
from energy_community.subway.SubwayResults import SubwayResults

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("energy_community/subway/results/debug_analysis.log", mode='w'),
        logging.StreamHandler(sys.stdout),
    ]
)

plt.rcParams["timezone"] =  str(get_localzone())

def write_member_results_each_scenario(scenarios: List[SubwayResults]):
    """Write community member individual performances to a csv for comparison between scenarios.

    Args:
        scenarios (List[SubwayResults]): the list of scenarios to be compared.
    """
    for scenario in scenarios:
        members = {}
        file_path = os.path.join(scenario.results_folder_path , f'Scenario_{scenario.scenario_id}_cm_analysis.csv')
        for flex_type in scenario.manager.cm_by_flex_and_pref_hr: # for each flexibility capacity
            for cm in list(scenario.manager.cm_by_flex_and_pref_hr[flex_type].values()): # for each representative community member
                members[f'{cm.unique_id} {cm.flexibility_capacity}'] = scenario.cm_file_writing_data[cm.unique_id]
        df = pd.DataFrame(members, index=scenario.cm_file_writing_column_names)
        df.to_csv(file_path)


def write_community_results_comparison(scenarios: List[SubwayResults], results_folder_path: str):
    """Write community performances for each scenario in a csv file.

    Args:
        scenarios (List[SubwayResults]): the list of scenarios to be compared.
    """
    data_all_scenarios_list = []
    column_names = None
    column_names_ok = False
    for scenario in scenarios:
        data_all_scenarios_list.append(scenario.file_writing_data)
        if not column_names_ok:
            column_names = scenario.file_writing_column_names
            column_names_ok = True

    df = pd.DataFrame(data_all_scenarios_list, columns=column_names)

    output_path = os.path.join(results_folder_path, 'analysis.csv')
    df.to_csv(output_path)
    
def plot_pareto_neeg_dissatisfaction(scenarios : List[SubwayResults], results_folder_path : str, save_figures_flag: bool, use_tex_flag: bool):
    if save_figures_flag:
        if use_tex_flag:
            plt.rc('text', usetex=True)
            plt.style.use(['science'])
        plt.grid(True)

    plt.figure(figsize=(15, 7))
    
    text_legend = []
    for scenario in scenarios:
        if scenario.manager.strategy == Strategy.MULTI_OBJECTIVE:
            text_legend.append(f" Scenario with {STRATEGY_MAP[scenario.manager.strategy]} strategy, {','.join(list(set([ FLEX_MAP[cm.flexibility_capacity] for cm in scenario.manager.cm_list])))} members, NEEG weight: {scenario.manager.weight_neeg}, DIS weight: {scenario.manager.weight_dis}")
        else:
            text_legend.append(f" Scenario with {STRATEGY_MAP[scenario.manager.strategy]} strategy, {','.join(list(set([FLEX_MAP[cm.flexibility_capacity] for cm in scenario.manager.cm_list])))} members")

    pareto_markers = ["o", "s", "p", "*", "X", "D", "d","h", "H"]
    neeg = [scenario.performances[(DSType.SIMULATED, AsignType.COMMUNITY, IndicatorsLP.NEEG)] /1000000 for scenario in scenarios]
    dis =  [scenario.performances[(DSType.ESTIMATED, AsignType.COMMUNITY, IndicatorsAll.AVERAGE_DISSATISFACTION)] for scenario in scenarios] # i should refer to the estimated because it is calculated for the representative members and it s the same

    plt.subplot(1, 2, 1)
    for i, (neeg_val, dis_val) in enumerate(zip(neeg, dis)):
        plt.scatter(neeg_val, dis_val, label=text_legend[i], marker=pareto_markers[i]) # type: ignore

    plt.xlabel('NEEG [MWh]', fontsize=14)
    plt.ylabel('Average dissatisfaction', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=14)
    plt.gca().xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    plt.gca().yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    plt.gca().legend(loc='upper center', bbox_to_anchor=(0.5, -0.1))

    f = plt.gcf()
    f.tight_layout()

    if save_figures_flag:
        path = os.path.join(results_folder_path, "Figure_pareto_neeg_and_dissatisfaction.pdf")
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()

if __name__ == '__main__':
    # If plots are to be generated with LaTeX, point the environment path to the bin folder (Mac OS) 
    os.environ['PATH'] = os.environ['PATH'] + ':/Users/mirceastefansimoiu/bin'  # For Mac os, add Latex to plots
    use_tex = True # comment if you don't want to use LaTeX for plots
    
    # For profilling, uncomment the following lines
    # yappi -f pstat  -o profiling_analysis.out analysis_subway_peak_shifting.py
    # snakeviz profiling_analysis.out
    
    #scenarios_to_plot = [9,10,11,12,13,14,15] # paper scenarios
    #scenarios_to_plot = [3] # DEMO SCENARIO
    #scenarios_to_plot = [15]
    scenarios_to_plot = [2,3,4]
    
    plot_hourly_comp_flag = True
    plot_pareto = True
    #results_folder_path = "/Users/mirceastefansimoiu/Library/CloudStorage/OneDrive-UniversitateaPolitehnicaBucuresti/_Cercetare/_Data_and_results/energy_community_input_data/results/subway_station"
    results_folder_path = os.path.join("energy_community","subway","results") # default results folder - must be the folder where simulation results have been stored
    

    analysis_spec = Spec(print_member_perf=True, plot_intervals=[('06-02-2017', '11-02-2017'), ('19-06-2017', '24-06-2017')], 
                         plot_beh_agent_1_flag=True, get_individual_indicators_flag=True) # define the specification

    simulation_scenarios = []
    for simulation_id in scenarios_to_plot:
        
        scenario = SubwayResults(simulation_id, analysis_spec, results_folder_path) # calculate performances for each scenario
        simulation_scenarios.append(scenario)

    write_community_results_comparison(simulation_scenarios, results_folder_path) 
    
    write_member_results_each_scenario(simulation_scenarios)
    
    if plot_pareto:
        plot_pareto_neeg_dissatisfaction(simulation_scenarios, results_folder_path, True, use_tex) # plot pareto comparison

    if plot_hourly_comp_flag:
        for scenario in simulation_scenarios:
            scenario.plot_hourly_evolution_comparison_specific_months_and_days_subway_community(use_tex_flag = use_tex)