import os


#from simulation.Configurator import Configuration
from os import path
import pandas as pd
#from simulation.params import SubwayComParams

'''
class Locator:

    def __init__(self, model):
        self.results_folder_root_path = ""
        self.agents_results_path = ""
        self.com_hourly_results_path = ""
        self.cm_hourly_results_path = ""
        self.cm_profile_path = ""
        self.manager_profile_path = ""
        self.model = model
        self.set_results_folder_path()
        self.set_com_hourly_results_path()
        self.set_agents_results_path()
        self.set_cms_hourly_results_path()
        self.set_cm_profile_path()
        self.set_manager_profile_path()
        
    def set_manager_profile_path(self):
        self.manager_profile_path = os.path.join(self.results_folder_root_path,f"Scenario_{self.model.sim_params.simulation_id}_manager_profile.csv")

    def set_cm_profile_path(self):
        self.cm_profile_path = os.path.join(self.results_folder_root_path,f"Scenario_{self.model.sim_params.simulation_id}_cm_profiles.csv")

    def set_agents_results_path(self):
        self.agents_results_path = os.path.join(self.results_folder_root_path,f"Scenario_{self.model.sim_params.simulation_id}_agents.csv")

    def set_com_hourly_results_path(self):
        self.com_hourly_results_path = os.path.join(self.results_folder_root_path,f"Scenario_{self.model.sim_params.simulation_id}_com_hourly.pickle")

    def set_cms_hourly_results_path(self):
        self.cm_hourly_results_path = os.path.join(self.results_folder_root_path,f"Scenario_{self.model.sim_params.simulation_id}_cm_hourly.pickle")

    @staticmethod
    def update_folder_path(current_folder_path, parameter):
        updated_folder_path = os.path.join(current_folder_path, parameter)
        if not path.exists(updated_folder_path):
            os.mkdir(updated_folder_path)
        return updated_folder_path

    def write_info_file(self):
        #results_dir_path = Configuration.get_results_config('results_folder')
        #if not path.exists(results_dir_path):
        #    os.mkdir(results_dir_path)
        if self.results_folder_root_path == "":
            raise ValueError("Results folder path not set!")
        
        info_file_path = os.path.join(self.results_folder_root_path, f'Scenario_{self.model.sim_params.simulation_id}_info.csv')
        params = ['PV plant number of modules','PV plant module power', 'Estimation System','Solar location','Starting date','Ending date']
        values = [self.model.sim_params.pv_plant_number_of_modules, self.model.sim_params.pv_plant_module_power, 
                  self.model.sim_params.estimation_system, self.model.sim_params.solar_location, 
                  self.model.sim_params.starting_date, self.model.sim_params.ending_date]
        if isinstance(self.model.sim_params, SubwayComParams):
            params.extend(['Flexibility period 1 starting hour', 'Flexibility period 1 ending hour', 'Flexibility period 2 starting hour', 'Flexibility period 2 ending hour'])
            values.extend([self.model.sim_params.flexibility_period_1_start, self.model.sim_params.flexibility_period_1_end, 
                           self.model.sim_params.flexibility_period_2_start, self.model.sim_params.flexibility_period_2_end])
        df = pd.DataFrame({'Parameters':params, 'Values': values})
        df.to_csv(info_file_path)
        
    def set_results_folder_path(self):
        #results_dir_path = Configuration.get_results_config('results_folder')
        folder_path = os.path.join("energycommunity","subway","results",f'Simulation_{self.model.sim_params.simulation_id}')
        if not path.exists(folder_path):
            os.mkdir(folder_path)
        self.results_folder_root_path = folder_path
 
    def set_results_folder_path(self):
        """Generates a string filepath for the results folder, considering the simulation parameters.

             :returns: a filepath for the results folder
             :rtype: str"""

        simulation_id = self.model.sim_params.simulation_id
        estimation_system_flag = self.model.sim_params.estimation_system
        if estimation_system_flag:
            estimation_id = Configuration.get_estimation_parameters_set(simulation_id)

        results_dir_path = Configuration.get_results_config('results_folder')
        if self.model.sim_params.solar_location == 'Grenoble':
            results_dir_path = os.path.join(results_dir_path, 'strategy_comparison')
        if not path.exists(results_dir_path):
            os.mkdir(results_dir_path)

        if self.model.sim_params.subway_station_manager:
            results_dir_path = self.update_folder_path(results_dir_path, "ss")

        results_dir_path = self.update_folder_path(results_dir_path, "pv_" + str(round(self.model.sim_params.pv_plant_number_of_modules)) +
                                                   "_" + str(round(self.model.sim_params.pv_plant_module_power)))

        results_dir_path = self.update_folder_path(results_dir_path, str(round(self.model.sim_params.no_alert_threshold)))

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.manager_stages)

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.manager_target_of_recommendations)

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.expectancy_development_approach)

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.recommendation_strategy)

        if self.model.sim_params.recommendation_strategy == 'coaching':
            results_dir_path = self.update_folder_path(results_dir_path, 'r_mod_' + str(self.model.sim_params.rec_mod_change) +
                                                       '_r_str_' + str(self.model.sim_params.rec_strong_change))

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.privacy_level)

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.member_stages)

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.member_action_intensity)

        if self.model.sim_params.member_action_intensity == 'fixed':
            results_dir_path = self.update_folder_path(results_dir_path, 'b_mod_' + str(self.model.sim_params.beh_mod_change) +
                                                       '_b_str_' + str(self.model.sim_params.beh_strong_change))

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.member_willingness)

        results_dir_path = self.update_folder_path(results_dir_path, self.model.sim_params.member_availability)

        results_dir_path = self.update_folder_path(results_dir_path, ",".join(self.model.sim_params.willingness_types_in_community))

        temp_path = 'cfg_'
        for category in self.model.sim_params.default_categories:
            temp_path += str(self.model.sim_params.agents_lim[category]) + '_'
        temp_path = temp_path[:-1]


        results_dir_path = self.update_folder_path(results_dir_path, temp_path)

        results_dir_path = self.update_folder_path(results_dir_path, "no_estimation")

        if estimation_system_flag:
            results_dir_path = os.path.join(results_dir_path, Configuration.get_estimation_results_folder())
            if not path.exists(results_dir_path):
                os.mkdir(results_dir_path)

            results_dir_path = os.path.join(results_dir_path,
                                            Configuration.get_estimation_observation_days_decision(estimation_id))
            if not path.exists(results_dir_path):
                os.mkdir(results_dir_path)

            results_dir_path = os.path.join(results_dir_path,
                                            Configuration.get_estimation_observation_window_type(estimation_id))
            if not path.exists(results_dir_path):
                os.mkdir(results_dir_path)

            results_dir_path = os.path.join(results_dir_path,
                                            str(Configuration.get_estimation_observation_window_length_days(estimation_id)) + "_day_length")
            if not path.exists(results_dir_path):
                os.mkdir(results_dir_path)

            results_dir_path = os.path.join(results_dir_path,
                                            str(Configuration.get_estimation_observation_days_frequency(estimation_id)) + "_day_interval")
            if not path.exists(results_dir_path):
                os.mkdir(results_dir_path)

            results_dir_path = os.path.join(results_dir_path, Configuration.get_estimation_method(estimation_id))
            if not path.exists(results_dir_path):
                os.mkdir(results_dir_path)

        self.results_folder_root_path = results_dir_path
    '''