import random
from typing import List, Union
from energy_community.core.constants import ExpectancyDev, MType, PrivacyLevel, Stages, Strategy, TargetOfRec
from energy_community.helpers.scripting import get_simulation_parameters, prepare_results_folder_mc, run_simulation
from energy_community.core.energy.get_power_data import get_consumption_for_a_house_from_csv
from energy_community.residential.core.community import ResidentialCommunity
from energy_community.residential.core.residential_parameters import ResidentialParameters
from energy_community.residential.core.profiles import OneTurnResidentialMarkovCommunityMember, OneTurnResidentialManager, TwoTurnsResidentialMarkovCommunityMember, TwoTurnsResidentialManager
from configuration import Config



def main(script_config):
    
    # Set the attributes of the Config object based on the configuration data for the residential script
    
    # Get the folder ids
    # The simulation scenarios are the same (since it is a stochastic process)
    # However, we need folders to keep all the results
    folder_ids_scenarios_to_run = range(script_config["simulation_scenarios_to_run"]["start"],script_config["simulation_scenarios_to_run"]["end"])

    # Get the plot_in_browser flag from the configuration object
    plot_in_browser = script_config["plot_in_browser"]

    # Get the print_flag from the configuration object
    print_flag = script_config["print_flag"]

    # Get the list of house IDs from the configuration object
    HOUSE_IDS = script_config["house_ids"]
    
    # Prepare the results folder for the current simulation
    results_folder_paths = prepare_results_folder_mc(list(folder_ids_scenarios_to_run), script_config["paths"])

    # Iterate through each folder id
    for folder_id in folder_ids_scenarios_to_run:
        
        # Print a message in the console (because there are many simulation scenarios)
        print(f"Running simulation {folder_id}...")

        # We het the simualtion id, consdering the segmentation of folder ids
        sim_id = get_simulation_id_from_folder_id(folder_id)
        
        # Get the simulation parameters for the current simulation
        sim_params = get_simulation_parameters(sim_id, script_config["paths"],"residential", print_flag)

        # Instantiate the ResidentialCommunity model with the simulation parameters and stages
        model = ResidentialCommunity(sim_params, results_folder_paths[folder_id], sim_params.stages) # type: ignore
        
         # Get the manager for the current simulation
        manager = get_manager(folder_id, model)
        
        # If a manager is defined, assign it to the community
        if manager:
            model.community_manager = manager
            
        # Create the community members for the current simulation
        members = create_community_members(HOUSE_IDS, sim_params, model) # type: ignore

        # Add the community members to the community
        model.add_new_community_members(members) #type: ignore
        
        # Given that there are a lot of simulations, we print info about the manager
        print(manager)
        
        # Run the simulation on the community model
        run_simulation(model)

        # If plot_in_browser is True, plot the results in the browser
        if plot_in_browser:
            model.show_results_plot_in_browser()

def get_simulation_id_from_folder_id(folder_id: int) -> int:
    """Set the simulation id based on the folder id.

    Args:
        folder_id (int): The folder if where the simulation results are stored for a scenario.

    Raises:
        ValueError: if the simulation id cannot be set, raise an error.

    Returns:
        int: the simulation id
    """
    simulation_id = 0
    
    # Check if simulation_id is between 1 and 100 (inclusive) and refer to the simulation for one turn manager with informative strategy
    if 1 <= folder_id <= 100:
        simulation_id = 1
    
    # Check if simulation_id is between 101 and 200 (inclusive) and refer to the simulation for two turns manager with informative strategy
    if 101 <= folder_id <= 200:
        simulation_id = 2
    
    # Check if simulation_id is between 201 and 300 (inclusive) and refer to the simulation for one turn with coaching strategy
    if 201 <= folder_id <= 300:
        simulation_id = 3
    
    if simulation_id == 0:
        raise ValueError("Simulation id not set!")
    
    return simulation_id
    
        
def get_manager(simulation_id: int, model: ResidentialCommunity) -> Union[OneTurnResidentialManager, TwoTurnsResidentialManager]:
    """
    Get the appropriate manager object based on the simulation_id and the model.

    Parameters:
        simulation_id (int): The ID of the simulation scenario to be run.
        model (ResidentialCommunity): An object representing the community of residential households being modeled.

    Returns:
        Union[OneTurnResidentialManager, TwoTurnsResidentialManager]: An instance of either OneTurnResidentialManager or TwoTurnsResidentialManager based on the simulation_id.
    """

    # Initialize manager variable to None
    manager = None

    # Based on the simulation_id, create an instance of the appropriate manager class with the required parameters.
    # For each simulation_id, different parameters are passed to either the OneTurnResidentialManager or TwoTurnsResidentialManager classes.

    # Check if simulation_id is between 1 and 100 (inclusive) and create a OneTurnResidentialManager with INFORMATIVE strategy
    if 1 <= simulation_id <= 100:
        manager = OneTurnResidentialManager(100, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.INFORMATIVE, PrivacyLevel.PRIVATE, 0)

        # Check if simulation_id is between 201 and 300 (inclusive) and create a TwoTurnsResidentialManager with INFORMATIVE strategy
    if 101 <= simulation_id <= 200:
        manager = TwoTurnsResidentialManager(100, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.INFORMATIVE, PrivacyLevel.PRIVATE, 0)
    
    # Check if simulation_id is between 101 and 200 (inclusive) and create a OneTurnResidentialManager with COACHING strategy
    if 201 <= simulation_id <= 300:
        manager = OneTurnResidentialManager(100, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.COACHING, PrivacyLevel.PRIVATE, 0)

    if not manager:
        raise ValueError("Manager not set!")
    
    # Return the manager object
    return manager


def create_community_members(house_ids: list[int], sim_params: ResidentialParameters, model: ResidentialCommunity) -> List[Union[OneTurnResidentialMarkovCommunityMember, TwoTurnsResidentialMarkovCommunityMember]]:
    """
    Creates a list of residential community members based on the given input parameters.

    Parameters:
        house_ids (list[int]): A list of integers representing the IDs of the houses whose consumption data will be retrieved.
        sim_params (ResidentialParam): An object containing simulation parameters such as the starting and ending dates of the simulation and the simulation stage.
        model (ResidentialCommunity): An object representing the community of residential households being modeled.

    Returns:
        List[Union[OneTurnResidentialCommunityMember, TwoTurnsResidentialCommunityMember]]: A list of ResidentialCommunityMember objects.

    Raises:
        ValueError: If no members are added to the list.
    """
    # Initialize an empty list to store the community members
    members = []

    # Iterate through the house_ids list
    for house_id in house_ids:
        # Retrieve consumption data for the current house_id
        df = get_consumption_for_a_house_from_csv(house_id, sim_params.starting_date, sim_params.ending_date, sim_params.root_folder_name, sim_params.community_folder_name, sim_params.data_folder_name)

        # Determine the type of community member to create based on the simulation stage
        if sim_params.stages == Stages.TWO_TURNS:
            # Create a TwoTurnsResidentialCommunityMember with a randomly chosen member type
            new_cm = TwoTurnsResidentialMarkovCommunityMember(house_id % 1000, random.choice([MType.NORMAL, MType.GOOD, MType.BAD]), model, df)
        else:
            # Create a OneTurnResidentialCommunityMember with a randomly chosen member type
            new_cm = OneTurnResidentialMarkovCommunityMember(house_id % 1000, random.choice([MType.NORMAL, MType.GOOD, MType.BAD]), model, df)

        # Append the newly created community member to the members list
        members.append(new_cm)

    # Check if any members were added to the list and raise a ValueError if not
    if not members:
        raise ValueError("Members not added!")

    return members


if __name__ == "__main__":
    
    config = Config()
    residential_config = config.get_script_config("residential_monte_carlo")
    main(residential_config)

   

        
        