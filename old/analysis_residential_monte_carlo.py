import csv
import os
from typing import Any, Dict, List, Optional, Tuple, Union


import numpy as np
from analysis_residential_extreme_comfort_param import get_pareto_front
from energy_community.helpers.Spec import Spec
from energy_community.core.constants import MTYPE_MAP, STAGES_MAP_RESIDENTIAL, STRATEGY_MAP_RESIDENTIAL, AsignType, DSType, IndicatorsAll, IndicatorsLP, IndicatorsLoad, IndicatorsRec, MType, Stages, Strategy
import matplotlib.pyplot as plt


from energy_community.residential.results.base import ResidentialResults
from residential_realistic_monte_carlo import get_simulation_id_from_folder_id
from configuration import Config


def main(script_config):
    # Set up LaTeX for plotting (optional, comment out if not using LaTeX)
    os.environ['PATH'] = os.environ['PATH'] + ':/Users/mirceastefansimoiu/bin'  # For Mac os, add Latex to plots

    # Set the range of scenarios to run based on the configuration
    folder_ids_scenarios_to_run = range(script_config["simulation_scenarios_to_run"]["start"],script_config["simulation_scenarios_to_run"]["end"])

    # Set a flag to control whether to plot Pareto front or not
    plot_pareto = False 
    
    # Define the results folder path
    results_folder_path = os.path.join(script_config["paths"]["root_folder_name"],
                                       script_config["paths"]["community_folder_name"],
                                       "output_monte_carlo_100") # default results folder - must be the folder where simulation results have been stored
    
    # Define the analysis specification
    analysis_spec = Spec(print_member_perf=False, plot_intervals=[('06-03-2015', '11-03-2015'), ('19-06-2015', '24-06-2015')], 
                         plot_beh_agent_1_flag=True, get_individual_indicators_flag=False) # define the specification

    # Create a list to store the simulation scenarios
    simulation_scenarios = []
    
    # Iterate over the folder_ids_scenarios_to_run and calculate performances for each scenario
    for folder_id in folder_ids_scenarios_to_run:
        # Get the simulation ID based on the folder ID
        sim_id = get_simulation_id_from_folder_id(folder_id)
        
        print(f"Reading scenario {sim_id} from folder id: {folder_id}")
        
        # Calculate performances for each scenario
        scenario = ResidentialResults(sim_id, folder_id, analysis_spec, script_config["paths"], "scenarios_monte_carlo.yaml") 
        simulation_scenarios.append(scenario)
    
    # Create a Monte Carlo analysis object with the simulation scenarios
    mc_analysis = MonteCarloAnalysis(simulation_scenarios, results_folder_path)
    
    # Write performances to a CSV file
    mc_analysis.write_indicator_data_to_csv()
    
    # Generate and save a radar plot for the scenarios
    mc_analysis.plot_scenario_radar_plot(save_figures_flag=True)
    
    # Plot the indicators for the Monte Carlo analysis
    mc_analysis.plot_indicators()
    
    # If the flag is set, plot the Pareto front
    if plot_pareto:
        mc_analysis.plot_pareto()

class MonteCarloAnalysis:
    
    def __init__(self, simulation_scenarios: List[ResidentialResults], results_folder_path: str, save_figures_flag = True):
        """ Initialize the MonteCarloAnalysis class with the given simulation scenarios, results folder path, and save_figures_flag.

        Args:
            simulation_scenarios (List[ResidentialResults]):the simulation scenarios to be analysed.
            results_folder_path (str): folder path to store results into
            save_figures_flag (bool, optional): A flag indicating whether to save the plot as a PDF file (True) or display it on the screen (False). Defaults to True.
        """
        
        # Define the main indicators to be analyzed in plots; will be exported to csv after
        self.main_performance_indicators = {IndicatorsLP.SC: 'SC', IndicatorsLP.SS: 'SS', IndicatorsAll.NORMALISED_NEEG: 'NEEG',}
        self.main_recommendation_indicators = {IndicatorsRec.RECOMMENDATION_DEVIATION_RATE: 'RDR', IndicatorsRec.CONTRIBUTION_CHANGE_RATE: 'CCR'}
        
        # Define all the indicators to be analyzed
        self.all_indicators = {IndicatorsLP.SC: 'SC', IndicatorsLP.SS: 'SS',
                               IndicatorsLP.NEEG_PER_DAY: 'NEEG per day', IndicatorsAll.NORMALISED_NEEG: 'NEEG',
                                IndicatorsLoad.EFFORT: 'Effort', IndicatorsLoad.RENUNCIATION: 'Renunciation',
                                 IndicatorsLoad.MONEY_FOR_GRID_ENERGY: 'Money for grid energy'} # analysed indicators
        
        self.recommendation_related_indicators = {IndicatorsRec.REC_INFLUENCE: 'Recommendation influence', IndicatorsRec.RECOMMENDATION_DEVIATION_RATE: 'Normalised deviation from white',
                                                  IndicatorsRec.CONTRIBUTION_CHANGE_RATE: 'Change from white'}
        
        # Store the simulation scenarios, save_figures_flag, and the results folder path
        self.scenarios = simulation_scenarios
        self.save_figures_flag = save_figures_flag
        self.results_folder_path = results_folder_path
        
        # Get the unique cm types across all scenarios
        self.unique_types_across_scenarios = self.get_unique_cm_types_across_scenarios()
        
        # Initialize a dictionary to store the Monte Carlo performances
        self.monte_carlo_performances : Dict[ Union[Tuple[Stages, Strategy, Union[IndicatorsAll, IndicatorsLP, IndicatorsLoad, IndicatorsRec]], Tuple[Stages, Strategy, IndicatorsRec, MType]], float] = {}
        
        # Get all unique strategies in the simulation scenarios
        self.unique_stages_strategies = list(set([(scenario.manager.manager_stages, scenario.manager.strategy) for scenario in self.scenarios])) # get all unique strategies in the simulation scenarios
        
        # Initialize a dictionary to store unique descriptions of stage-strategy pairs
        self.unique_descriptions = self._initialize_unique_descriptions()
        
        # Fill the unique descriptions dictionary with stage-strategy pairs and their descriptions
        self.set_mc_performances()

    def _initialize_unique_descriptions(self):
        """
        Initialize the unique descriptions dictionary with stage-strategy pairs and their descriptions.
        
        :return: A dictionary containing unique stage-strategy pairs and their descriptions.
        """
        unique_descriptions = {}
        for unique_stage_strategy in self.unique_stages_strategies:
            unique_descriptions[unique_stage_strategy] = fr"{STAGES_MAP_RESIDENTIAL[unique_stage_strategy[0]]} {STRATEGY_MAP_RESIDENTIAL[unique_stage_strategy[1]]}"
        return unique_descriptions
    
    def get_unique_cm_types_across_scenarios(self) -> List[MType]:
        """Get all unique community member types across all scenarios

        Returns:
            List[MType]: A list of community member types.
        """
        
        # Initialize the list
        unique_types_across_scenarios = []
        
        for scenario in self.scenarios:
            for cm in scenario.manager.cm_list:
                # If the type has not been stored yed
                if cm.type not in unique_types_across_scenarios:
                    # Store the type
                    unique_types_across_scenarios.append(cm.type)
                    
        return unique_types_across_scenarios

    
    def set_mc_performances(self):
        """Sets the mean value of all_indicators for all unique scenario types."""
        
        # Initialize a nested dictionary to store intermediate values
        performance_sum = {}
        performance_count = {}
        for unique_stage_strategy in self.unique_stages_strategies:
            performance_sum[unique_stage_strategy] = {}
            performance_count[unique_stage_strategy] = {}
            # Indicators related to systme performances
            for indicator in self.all_indicators:
                performance_sum[unique_stage_strategy][indicator] = 0
                performance_count[unique_stage_strategy][indicator] = 0
            # Indicators related to recommendations, considering community member types
            for indicator in self.recommendation_related_indicators:
                for type in self.unique_types_across_scenarios:
                    performance_sum[unique_stage_strategy][(indicator,type)] = 0
                    performance_count[unique_stage_strategy][(indicator, type)] = 0

        # Iterate over scenarios and accumulate performance values
        for scenario in self.scenarios:
            stage_strategy = (scenario.manager.manager_stages, scenario.manager.strategy)
            for indicator in self.all_indicators:
                value = scenario.analyzer.performances[(DSType.SIMULATED, AsignType.COMMUNITY, indicator)]
                performance_sum[stage_strategy][indicator] += value
                performance_count[stage_strategy][indicator] += 1
            for indicator in self.recommendation_related_indicators:
                # We refer only to the types in the respective scenario
                for type in list(set([cm.type for cm in scenario.manager.cm_list])):
                    value = scenario.analyzer.performances[(DSType.SIMULATED, AsignType.CATEGORY, indicator, type)]
                    performance_sum[stage_strategy][(indicator, type)] += value
                    performance_count[stage_strategy][(indicator, type)] += 1

        # Calculate the mean performance values for each unique stage-strategy pair and indicator
        for unique_stage_strategy in self.unique_stages_strategies:
            for indicator in self.all_indicators:
                mean_value = performance_sum[unique_stage_strategy][indicator] / performance_count[unique_stage_strategy][indicator]
                self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator)] = float(mean_value)
            for indicator in self.recommendation_related_indicators:
                # All unique member types accross scenarios
                for type in self.unique_types_across_scenarios:
                    mean_value = performance_sum[unique_stage_strategy][(indicator,type)] / performance_count[unique_stage_strategy][(indicator, type)]
                    self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator, type)] = float(mean_value)

    
    def plot_indicators(self):
        """Plot a comparison of the mean values for several indicators after the Monte Carlo Simulation."""
        
        if self.save_figures_flag:
            plt.rc('text', usetex=True) # option to use latex
            plt.style.use(['science'])
            plt.grid(True)

        plt.figure(figsize=(25, 20)) # figure size adapted for all indicators
 
        performance_indicators_data = {}
        for indicator in self.main_performance_indicators:
            performance_indicators_data[indicator] = []
            
        recommendation_indicators_data = {}
        for indicator in self.main_recommendation_indicators:
            for cm_type in self.unique_types_across_scenarios:
                recommendation_indicators_data[(indicator, cm_type)] = []
        
        for indicator in self.main_performance_indicators:
            for unique_stage_strategy in self.unique_stages_strategies:
                performance_indicators_data[indicator].append(self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator)] ) # get the mean value for that indicator
        
        for indicator in self.main_recommendation_indicators:
            for cm_type in self.unique_types_across_scenarios:
                for unique_stage_strategy in self.unique_stages_strategies:
                    recommendation_indicators_data[(indicator, cm_type)].append(self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator, cm_type)])

        i = 0
        for indicator, indicator_name in self.main_performance_indicators.items(): # for each indicator
            plt.subplot(5, 2, i + 1) # assign a space in the subplot
            plt.barh(list(self.unique_descriptions.values()), performance_indicators_data[indicator], color='blue')
            plt.title(indicator_name, fontsize=18) 
            plt.ylabel('Scenario', fontsize=16)
            plt.xticks(fontsize=16)
            plt.yticks(fontsize=16)
            i += 1
        
        for indicator, indicator_name in self.main_recommendation_indicators.items(): # for each indicator
            for cm_type in self.unique_types_across_scenarios:
                plt.subplot(5, 2, i + 1) # assign a space in the subplot
                plt.barh(list(self.unique_descriptions.values()), recommendation_indicators_data[(indicator, cm_type)], color='blue')
                plt.title(f"{indicator_name} - {MTYPE_MAP[cm_type]} members " , fontsize=18) 
                plt.ylabel('Scenario', fontsize=16)
                plt.xticks(fontsize=16)
                plt.yticks(fontsize=16)
                i += 1
            
        ax = plt.gca()
        for bars in ax.containers:
            ax.bar_label(bars, fmt='%.2f', label_type='center', fontsize=15, color='white')
        
        f = plt.gcf()
        f.tight_layout()

        if self.save_figures_flag:
            path = os.path.join(self.results_folder_path, "Figure_mc_indicators_comparison.pdf")
            f.savefig(path, bbox_inches='tight')
        else:
            plt.show()
            
    def plot_pareto(self):
        
        if len(self.unique_types_across_scenarios) > 1:
            raise ValueError("Cannot plot pareto with multiple cm types!")
        
        plt.figure(figsize=(10, 10))
        
        cm_type = self.unique_types_across_scenarios[0]
        
        if self.save_figures_flag:
            plt.rc('text', usetex=True) # add latex to plot if the user wants to save it
            plt.style.use(['science'])
            plt.grid(True)
            
        indicators : List[Dict[str,Tuple[Any, str]]] = [ {'x': (IndicatorsAll.NORMALISED_NEEG, 'Normalised NEEG'), 'y': (IndicatorsRec.RECOMMENDATION_DEVIATION_RATE, 'Deviation from WHITE') },
                                                     {'x': (IndicatorsAll.NORMALISED_NEEG, 'Normalised NEEG'), 'y': (IndicatorsRec.CONTRIBUTION_CHANGE_RATE, 'Change frequency') },
                                                     {'x': (IndicatorsRec.CONTRIBUTION_CHANGE_RATE, 'Change frequency'), 'y': (IndicatorsRec.RECOMMENDATION_DEVIATION_RATE, 'Deviation from WHITE') }]
        
        # We make a subplot amtrix of 2 rows and 4 columns. Two plots will be on the first row, spanning 2 columns.
        locations = [(0,0), (0,2), (1,1)] # row-column starting positions for each subplot
        plot_index = 0
        for plot_index in range(3):    
            ax = plt.subplot2grid(shape=(2,4), loc=locations[plot_index], colspan=2) 
            
            y_list : List[float] = []
            x_list : List[float] = []
            
            for unique_stage_strategy in self.unique_stages_strategies:
                y_list.append(self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicators[plot_index]['y'][0], cm_type)])
                x_list.append(self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicators[plot_index]['x'][0])])

            for i, (x, y) in enumerate(zip(x_list, y_list)):
                plt.scatter(x, y) # generate the plot
                
            deviation_pareto_values, neeg_pareto_values = get_pareto_front(10, x_list, y_list) # get the coordinates for the pareto front values
            plt.plot(deviation_pareto_values, neeg_pareto_values) # plot the pareto front
            plt.ylabel(indicators[plot_index]['y'][1], fontsize=14)
            plt.xlabel(indicators[plot_index]['x'][1], fontsize=14)
            plt.xticks(fontsize=14)
            plt.yticks(fontsize=14) 

        f = plt.gcf()
        f.legend(self.unique_descriptions.values(),loc='center left', bbox_to_anchor=(1, 0.5))
        f.tight_layout()

        if self.save_figures_flag:
            path = os.path.join(self.results_folder_path, "Figure_mc_indicators_pareto.pdf")
            f.savefig(path, bbox_inches='tight')
        else:
            plt.show()
            
    def plot_scenario_radar_plot(self, save_figures_flag: bool = False):
        """
        Function for creating a radar plot that visualizes the performance of different scenarios based on three indicators.
        
        Args:
            scenarios (List[ResidentialResults]): A list of ResidentialResults objects that contain performance data for the indicators.
            save_figures_flag (bool, optional): A flag indicating whether to save the plot as a PDF file (True) or display it on the screen (False). Defaults to False.
        """
        
        # If save_figures_flag is True, set up plot with LaTeX formatting, 'science' style, and grid lines
        if save_figures_flag:
            plt.rc('text', usetex=True) # add latex to plot if the user wants to save it
            plt.style.use(['science'])
            plt.grid(True)
        
        # Define indicators and categories
        indicators = list(self.main_performance_indicators.keys())
        categories = list(self.main_performance_indicators.values())
        
        cm_type_map = { MType.IDEAL: "Ideal", MType.GOOD: "Enthusiastic", MType.NORMAL: "Normal", MType.BAD:"Reluctant"} 
            
        
        for key, indicator in self.main_recommendation_indicators.items():
            for cm_type in self.unique_types_across_scenarios:
                categories += [f"{cm_type_map[cm_type]} {indicator}"]
        
        # Create figure and polar axis
        fig = plt.figure(figsize=(5, 5))
        ax = fig.add_subplot(111, polar=True) # type: ignore
        
        for unique_stage_strategy in self.unique_stages_strategies:

            values = [self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator)] for indicator in indicators]
            
            values += [self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator, cm_type)] for indicator in self.main_recommendation_indicators for cm_type in self.unique_types_across_scenarios]

            # Compute angles for each category
            N = len(categories) 
            angles = np.linspace(0, 2*np.pi, N, endpoint=False)
           
            # Plot the radar chart
            ax.plot(angles, values, 'o-', linewidth=2, label= self.unique_descriptions[unique_stage_strategy])
            ax.fill(angles, values, alpha=0.25)
        
        # Set plot's labels, font sizes, and grid lines
        ax.set_thetagrids(angles * 180/np.pi, categories, fontsize=12) #type: ignore
        ax.tick_params(axis='y', labelsize=14) # set the fontsize of the scaling inside the circles
        ax.grid(True)
        plt.legend(loc='lower center', bbox_to_anchor=(0.5, -0.2), ncol=2, fontsize=12)
        
        # If save_figures_flag is True, save plot as PDF file in results_folder_path directory; otherwise, display plot on screen
        if save_figures_flag:
            f = plt.gcf()
            f.tight_layout()
            path = os.path.join(self.results_folder_path, "Figure_mc_indicators_radar.pdf")
            f.savefig(path, bbox_inches='tight')
        else:
            plt.show()
            
    def write_indicator_data_to_csv(self):
        
        indicator_data = {indicator : { unique_stage_strategy : None} for indicator in self.main_performance_indicators for unique_stage_strategy in self.unique_stages_strategies}
        rec_indicator_data = {(indicator,type) : { unique_stage_strategy : None} for indicator in self.main_recommendation_indicators for unique_stage_strategy in self.unique_stages_strategies for type in self.unique_types_across_scenarios}
        
        for indicator in self.main_performance_indicators:
            for unique_stage_strategy in self.unique_stages_strategies:
                indicator_data[indicator][unique_stage_strategy] = self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator)] # type: ignore
                
        for indicator in self.main_recommendation_indicators:
            for unique_stage_strategy in self.unique_stages_strategies:
                for type in self.unique_types_across_scenarios:
                    rec_indicator_data[(indicator, type)][unique_stage_strategy] = self.monte_carlo_performances[(unique_stage_strategy[0], unique_stage_strategy[1], indicator, type)] # type: ignore
                
        filename = os.path.join(self.results_folder_path, "_Table_indicators_mc.csv")
        # Open a file to write the data to
        with open(filename, mode='w', newline='') as csv_file:
            # Create a CSV writer object
            writer = csv.writer(csv_file)
            
            column_names = ['Unique Stage Strategy'] + list(self.main_performance_indicators.keys())
            for cm_type in self.unique_types_across_scenarios:
                for indicator in self.main_recommendation_indicators:
                    column_names += [f"{cm_type} {indicator}"]
            
            writer.writerow(column_names)
            
            # Write the data rows
            for unique_stage_strategy in self.unique_stages_strategies:
                row_data = [self.unique_descriptions[unique_stage_strategy]] + [indicator_data[indicator][unique_stage_strategy] for indicator in self.main_performance_indicators]
                for type in self.unique_types_across_scenarios:
                    for indicator in self.main_recommendation_indicators:
                        row_data += [rec_indicator_data[(indicator, type)][unique_stage_strategy]] 
                writer.writerow(row_data)


    
if __name__ == "__main__":
    
    config = Config()
    residential_config = config.get_script_config("residential_monte_carlo")
    main(residential_config)