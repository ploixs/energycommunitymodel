from buildingenergy.timemg import stringdate_to_datetime
from numpy.distutils.fcompiler import str2bool
from energy_community.helpers.constants import MType

from simulation.Configurator import *
import pandas as pd


    
    
#class SubwayComParams(Parms):

 #  def __init__(self, sim_id, print_flag=False, est_sys_flag=False):
 #       super().__init__(sim_id, print_flag, est_sys_flag)
        
  #      start_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_1_start'][self.simulation_id].item())
   #     self.flexibility_period_1_start = stringdate_to_datetime(start_date, date_format='%H:%M')
    #    end_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_1_end'][self.simulation_id].item())
   #     self.flexibility_period_1_end = stringdate_to_datetime(end_date, date_format='%H:%M')

 #       start_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_2_start'][self.simulation_id].item())
 #       self.flexibility_period_2_start = stringdate_to_datetime(start_date, date_format='%H:%M')
 #       end_date = str(self.df.loc[self.df.simulation_parameter == 'flexibility_period_2_end'][self.simulation_id].item())
 #       self.flexibility_period_2_end = stringdate_to_datetime(end_date, date_format='%H:%M')

 #       logger.info('----------Subway community specific parameters---------------')
 #       logger.info(f'- Flexibility period 1 between: {self.flexibility_period_1_start.hour} and {self.flexibility_period_1_end.hour}')
 #       logger.info(f'- Flexibility period 2 between: {self.flexibility_period_2_start.hour} and {self.flexibility_period_2_end.hour}')



        # If TRUE, then only 5 houses will be used for the simulation in order to compare different recommendation
        # strategies
        #ac = df.loc[df.simulation_parameter == 'approach_comparison'][str(self.simulation_id)].item()
        #self.approach_comparison_ok = str2bool(str(ac))

        # Set TRUE if we want to develop the consumption estimation according to an algorithm
        #estimation_system = str(df.loc[df.simulation_parameter == 'estimation_system'][str(self.simulation_id)].item())
        #self.estimation_system = str2bool(estimation_system)





#subway_flag = df.loc[df.simulation_parameter == 'subway_station_flag'][self.simulation_id].item()
        #self.subway_flag = str2bool(str(subway_flag))  # Set if the manager is dedicated to a community near the subway station
        # Set how many times the manager intervenes during the day
        # one stage - the manager gives the recommendation only at the beginning of the day
        # two stage - the manager intervenes also every hour
        #self.manager_stages = df.loc[df.simulation_parameter == 'manager_stages'][self.simulation_id].item()
        # Set the target of recommendations: everyone has same recommendation or different recommendations
        # are given based on clusters
        #self.manager_target_of_recommendations = df.loc[df.simulation_parameter == 'target_of_recommendations'][self.simulation_id].item()
        # Set how the expectancy is developed: basic, adaptive, fixed
        #self.expectancy_development_approach = df.loc[df.simulation_parameter == 'expectancy_development_approach'][self.simulation_id].item()
        # Set how the recommendation is given: informative (every hour), coaching (according to periods), dummy
        #self.recommendation_strategy = df.loc[df.simulation_parameter == 'recommendation_strategy'][self.simulation_id].item()
        # If a fixed expectancy development strategy is set, then set the values for the change in consumption
        #rec_mod_ch = df.loc[df.simulation_parameter == 'rec_moderate_change_in_consumption'][self.simulation_id].item()
        #self.rec_mod_change = float(rec_mod_ch)
        #rec_str_ch = df.loc[df.simulation_parameter == 'rec_strong_change_in_consumption'][self.simulation_id].item()
        #self.rec_strong_change = float(rec_str_ch)
        # Set a threshold in case we don't want to disturb the members all the time
        #no_alert = df.loc[df.simulation_parameter == 'no_alert_threshold'][self.simulation_id].item()
        #self.no_alert_threshold = float(no_alert)
        # Set the privacy level: private or not private. Privacy influences how to expectancy is developed.
        # If we adopt a private approach, we must only look at community-level data
        # If we adopt a not so private approach, we are allowed to look at member level data
        #self.privacy_level = df.loc[df.simulation_parameter == 'privacy_level'][self.simulation_id].item()

        # SET THE MEMBER MODEL
        # Set how many times the member acts every day
        # one stage: the member reacts only one to the recommendation received
        # two stage: the member reacts twice, considering he receives two recommendations
        #self.member_stages = df.loc[df.simulation_parameter == 'member_stages'][self.simulation_id].item()
        # Set the intensity of each member action
        # fixed: members provide a fixed change in consumption
        # realistic: members provide a change in consumption according to a stochastic model
        #intensity = df.loc[df.simulation_parameter == 'member_action_intensity'][self.simulation_id].item()
        #self.member_action_intensity = intensity
        # If the members provide a fixed change in consumption, then there are two possibilities:
        #beh_mod_ch = df.loc[df.simulation_parameter == 'beh_moderate_change_in_consumption'][self.simulation_id].item()
        #self.beh_mod_change = float(beh_mod_ch)
        #beh_str_ch = df.loc[df.simulation_parameter == 'beh_strong_change_in_consumption'][self.simulation_id].item()
        #self.beh_strong_change = float(beh_str_ch)
        # Set the member willingness to react to recommendation:
        # fixed: between the following values: ideal, good, normal, bad
        #self.member_willingness = df.loc[df.simulation_parameter == 'willingness'][self.simulation_id].item()
        # Set the member availability
        # day: means that members are fully available during the day
        #self.member_availability = df.loc[df.simulation_parameter == 'availability'][self.simulation_id].item()
        # Set the possible member willingness profiles that may exist in a community
        # self.willingness_types_in_community = parameters.iloc[17][str(self.simulation_id)]
        #willingness_types = df.loc[df.simulation_parameter == 'willingness_types_in_community'][self.simulation_id].item()
        #self.willingness_types_in_community = self.convert_string_to_list(str(willingness_types), ",")
        # Set the number of members in each category of willigness
        #for category in self.default_categories:
        #    ag_count = df.loc[df.simulation_parameter == category + '_agents_count'][self.simulation_id].item()
        #    self.agents_lim[category] = int(ag_count)
        # OTHER PARAMETERS
        # Set if we want to print additional member info at each step
        #print_member_info = df.loc[df.simulation_parameter == 'print_member_info'][str(self.simulation_id)].item()
        #self.print_member_info = str2bool(str(print_member_info))