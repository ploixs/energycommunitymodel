import random
from typing import List, Tuple

import numpy as np
from scipy.stats import truncnorm

from energy_community.core.CommunityMember import CommunityMember


class ResidentialCommunityMember(CommunityMember):
    """Community Member Class definition. """

    def __init__(self, unique_id, model, agent_type, est_consumption_total):
        super().__init__(unique_id=unique_id, 
                         model=model,
                         agent_type=agent_type, 
                         est_consumption_total=est_consumption_total)

        self.probability_to_change = 1  # probability to modify the consumption, based on the received recommendation
        self.probability_to_consume_same = 0  # probability to consume as estimated, based on the received recommendation

        self.set_probabilities_for_willingness_to_change_consumption(agent_type)


    def make_decision_based_on_member_type(self) -> int:
        decision = 0
        decision_changed_flag = False
        # Random decision: to follow recommendation or not to follow
        if self.type == 'normal':
            decision = self.random.randint(0, 1)
            decision_changed_flag = True
        if self.type == 'good':
            decision = np.random.choice(np.arange(0, 2), p=[0.7, 0.3])
            decision_changed_flag = True
        if self.type == 'bad':
            decision = np.random.choice(np.arange(0, 2), p=[0.3, 0.7])
            decision_changed_flag = True
        if self.type == 'ideal':
            decision = 0
            decision_changed_flag = True
        if not decision_changed_flag:
            raise ValueError("Decision not set!")
        return decision

    def set_probabilities_for_willingness_to_change_consumption(self, agent_type):
        if agent_type == 'ideal':
            self.probability_to_change = 1
            self.probability_to_consume_same = 0
            if self.com_model.sim_params.print_member_info:
                print("Probabilities for willingness to change for " + agent_type + "agent " + str(self.unique_id) + " set!")
        elif agent_type == 'good':
            self.probability_to_change = 0.7
            self.probability_to_consume_same = (1 - self.probability_to_change)
            if self.com_model.sim_params.print_member_info:
                print("Probabilities for willingness to change for " + agent_type + "agent " + str(self.unique_id) + " set!")
        elif agent_type == 'normal':
            self.probability_to_change = 0.5
            self.probability_to_consume_same = (1 - self.probability_to_change)
            if self.com_model.sim_params.print_member_info:
                print("Probabilities for willingness to change for " + agent_type + "agent " + str(self.unique_id) + " set!")
        elif agent_type == 'bad':
            self.probability_to_change = 0.3
            self.probability_to_consume_same = (1 - self.probability_to_change)
            if self.com_model.sim_params.print_member_info:
                print("Probabilities for willingness to change for " + agent_type + "agent " + str(self.unique_id) + " set!")
        else:
            if self.com_model.sim_params.print_member_info:
                print("WARNING: Probabilities to change consumption not set!")

    def do_action_with_unknown_impact(self, decision : int) -> Tuple[float, float]:
        """Function to be called when agent does the action for the current hour, so the consumption is recorded accordingly.

            :param decision: represents whether the agent follows the optimal recommended action \
                             that the agent must do at the current hour (0) or not (1)
            :type decision: int"""
        sim_consumption : float = 0
        action : float = 0
        #exp_consumption = self.est_consumption_current_day_list[self.com_model.current_hour_index_day_relative]
        exp_consumption = self.est_member_load_day.data[self.com_model.current_hour_index_day_relative]
        if decision:  # Agent follows recommendation
            sim_agent_correctness_value = 1  # take into account that he follows the recommendation

            if self.recommendation_current_hour == -1:  # strong decrease
                # Adapt the real power profile according to the agent current decision
                sim_consumption = random.uniform(0, 1.2) * exp_consumption
                action = -1
            if self.recommendation_current_hour == -0.5:  # moderate decrease
                sim_consumption = random.uniform(0.5, 1.2) * exp_consumption
                action = -0.5
            if self.recommendation_current_hour == 1:  # strong increase
                sim_consumption = random.uniform(0.8, 3) * exp_consumption
                action = 1
            if self.recommendation_current_hour == 0.5:  # moderate increase
                sim_consumption = random.uniform(0.8, 1.5) * exp_consumption
                action = 0.5
            if self.recommendation_current_hour == 0:  # same
                sim_consumption = random.uniform(0.8, 1.2) * exp_consumption
                action = 0
            if self.recommendation_current_hour == 2:
                #sim_consumption_old = self.sim_consumption_current_day[self.com_model.current_hour_index_day_relative]
                sim_consumption_old = self.sim_member_load_day.data[self.com_model.current_hour_index_day_relative]
                sim_consumption = truncnorm.rvs(1, 2) * 1.5 * sim_consumption_old # type: ignore
                action = 2
            if self.recommendation_current_hour == - 2:
                #sim_consumption_old = self.sim_consumption_current_day[self.com_model.current_hour_index_day_relative]
                sim_consumption_old = self.sim_member_load_day.data[self.com_model.current_hour_index_day_relative]
                sim_consumption = abs(random.normalvariate(0, .5)) * sim_consumption_old # type: ignore
                action = -2
        else:
            # Agent doesnt follow recommendation
            sim_consumption = exp_consumption
            action = 0
        return action, sim_consumption

    def do_action_with_fixed_impact(self, decision):
        """Function to be called when agent does the action for the current hour, so the consumption is recorded accordingly.

            :param decision: represents whether the agent follows the optimal recommended action \
                             that the agent must do at the current hour (0) or not (1)
            :type decision: int"""
        sim_consumption = None
        action = None
        exp_consumption = self.est_consumption_current_day_list[self.model.current_hour_index_day_relative]
        if decision == 0:  # Agent follows recommendation
            sim_agent_correctness_value = 1  # take into account that he follows the recommendation

            if self.recommendation_current_hour == -1:  # strong decrease
                sim_consumption = (1 - self.com_model.community_manager.strong_load_change) * exp_consumption
                action = -1
            if self.recommendation_current_hour == -0.5:  # moderate decrease
                sim_consumption = (1 - self.aem.moderate_load_change) * exp_consumption
                action = -0.5
            if self.recommendation_current_hour == 1:  # strong increase
                sim_consumption = (1 + self.aem.strong_load_change) * exp_consumption
                action = 1
            if self.recommendation_current_hour == 0.5:  # moderate increase
                sim_consumption = (1 + self.aem.moderate_load_change) * exp_consumption
                action = 0.5
            if self.recommendation_current_hour == 0:  # same
                sim_consumption = exp_consumption
                action = 0
        else:
            # Agent doesnt follow recommendation
            sim_agent_correctness_value = 0
            sim_consumption = exp_consumption
            action = 0
        return action, sim_agent_correctness_value, sim_consumption