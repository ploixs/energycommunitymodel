import os
import random
from typing import TYPE_CHECKING, List

from energy_community.core.constants import ExpectancyDev, MType, PrivacyLevel, Strategy, TargetOfRec
from energy_community.core.energy.get_power_data import get_consumption_for_a_house_from_csv
from energy_community.helpers.scripting import get_member_type_based_on_attentive_following_prob, get_simulation_parameters, prepare_results_folder_mc, run_simulation
from energy_community.residential.core.residential_parameters import ResidentialParameters
from energy_community.residential.core.community import ResidentialCommunity
from energy_community.residential.core.profiles import OneTurnResidentialMarkovCommunityMember, OneTurnResidentialManager
from configuration import Config
import csv

from residential_realistic_monte_carlo import get_simulation_id_from_folder_id

if TYPE_CHECKING:
    from energy_community.residential.core.community import ResidentialCommunity

def main(script_config):
    
    # Set the attributes of the Config object based on the configuration data for the residential script
    # Get the folder ids
    # The simulation scenarios are the same (since it is a stochastic process)
    # However, we need folders to keep all the results
    folder_ids_scenarios_to_run = range(script_config["simulation_scenarios_to_run"]["start"],script_config["simulation_scenarios_to_run"]["end"])

    # Get the plot_in_browser flag from the configuration object
    plot_in_browser = script_config["plot_in_browser"]

    # Get the print_flag from the configuration object
    print_flag = script_config["print_flag"]

    # Get the list of house IDs from the configuration object
    HOUSE_IDS = script_config["house_ids"]
    
    # Record models
    models : List["ResidentialCommunity"] = []
    
    # Prepare the results folder for the current simulation
    results_folder_paths = prepare_results_folder_mc(list(folder_ids_scenarios_to_run), script_config["paths"])

    # Iterate through each folder id
    for folder_id in folder_ids_scenarios_to_run:
        
        # Print a message in the console (because there are many simulation scenarios)
        print(f"Running simulation {folder_id}...")

        # We het the simualtion id, consdering the segmentation of folder ids
        sim_id = get_simulation_id_from_folder_id(folder_id)

        # Get the simulation parameters for the current simulation
        sim_params = get_simulation_parameters(sim_id, script_config["paths"], community_type="residential", print_flag=print_flag)

        if type(sim_params) != ResidentialParameters:
            raise ValueError("Parameters not set appropriate")
        
        # Instantiate the ResidentialCommunity model with the simulation parameters and stages
        model = ResidentialCommunity(sim_params, results_folder_paths[folder_id], sim_params.stages)

        # Get the manager for the current simulation
        manager = get_manager(model, sim_params, script_config["pv_surface"])

        # If a manager is defined, assign it to the community
        if manager:
            model.community_manager = manager

        # Create the community members for the current simulation
        members = create_community_members(HOUSE_IDS, sim_params, model)

        # Add the community members to the community
        model.add_new_community_members(members)

        # Run the simulation on the community model
        run_simulation(model)

        # If plot_in_browser is True, plot the results in the browser
        if plot_in_browser:
            model.show_results_plot_in_browser()
        
        # Record models
        models.append(model)
    
    file_path =  os.path.join(script_config['paths']['root_folder_name'], 
                              script_config['paths']['community_folder_name'], 
                              script_config['paths']['results_folder_name'], 'learning_model_comparison.csv')
    performances = [model.community_manager.classification_system.best_performances for model in models]   
    write_to_csv(performances, file_path)



def write_to_csv(list_of_dicts, filename):
    keys = list_of_dicts[0].keys()
    with open(filename, 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(list_of_dicts)


                     
def create_community_members(house_ids: list[int], sim_params: ResidentialParameters, model: ResidentialCommunity) -> List:
    """
    Creates a list of residential community members based on the given input parameters.

    Parameters:
        house_ids (List[int]): A list of integers representing the IDs of the houses whose consumption data will be retrieved.
        sim_params (ResidentialParam): An object containing simulation parameters such as the starting and ending dates of the simulation and the simulation stage.
        model (ResidentialCommunity): An object representing the community of residential households being modeled.

    Returns:
        List: A list of ResidentialCommunityMember objects.

    """
    # Initialize an empty list to store the community members
    members = []
    
    
    # Retrieve the consumption data for the current house
    df = get_consumption_for_a_house_from_csv(house_ids[0], sim_params.starting_date, sim_params.ending_date, sim_params.root_folder_name, sim_params.community_folder_name, sim_params.data_folder_name)
    
    # Set a random probability to follow the recommendation while attentive
    emis_x = random.random()
    emis_y =  random.random()
    trans_z = random.random()
    
    # Get member true type based on this probability
    cm_true_type = get_member_type_based_on_attentive_following_prob(emis_x)
    
    desc = None
    if cm_true_type == MType.GOOD:
        desc = "Enthusiastic"
    elif cm_true_type == MType.NORMAL:
        desc = "Normal"
    else:
        desc = "Reluctant"
    
    desc_availability = "Balanced"
    if trans_z < 0.4:
        desc_availability = "Inattentive"
    elif trans_z > 0.6:
        desc_availability = "Attentive"
    
    # Check the simulation stage and create the appropriate community member object
    new_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, desc_availability + " " + desc, 
                                                     cm_true_type, model, df, 
                                                     explicit_emissions=[[emis_x, 1 - emis_x],[0.5, 0.5]],
                                                     explicit_transmission=[[trans_z, 1 - trans_z],[trans_z, 1-trans_z]])

    # Append the new community member to the list of members
    members.append(new_cm)  # type: ignore 
    
    # Generate other members for defining typical behaviour
    good_cm = OneTurnResidentialMarkovCommunityMember(house_ids[1] % 1000, 
                                                      "Balanced Enthusiastic", MType.GOOD, model, df, 
                                                      explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], 
                                                      explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 
    normal_cm = OneTurnResidentialMarkovCommunityMember(house_ids[2] % 1000, 
                                                        "Balanced Normal", MType.NORMAL, model, df, 
                                                        explicit_emissions=[[0.5, 0.5],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 
    
    bad_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000, 
                                                     "Balanced Reluctant", MType.BAD, model, df, 
                                                     explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 
    
    attentive_enthusiastic_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 1, 
                                                                        "Attentive Enthusiastic", MType.GOOD, model, df, 
                                                                        explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], 
                                                                        explicit_transmission=[[0.8, 0.2],[0.8, 0.2]])
    
    attentive_reluctant_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 2, 
                                                                    "Attentive Reluctant",  MType.BAD, model, df, 
                                                                    explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.8, 0.2],[0.8, 0.2]]) 
    
    inattentive_enthusiastic_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 3, 
                                                                        "Inattentive Enthusiastic", MType.GOOD, model, df, 
                                                                        explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], 
                                                                        explicit_transmission=[[0.2, 0.8],[0.2, 0.8]])
    
    inattentive_reluctant_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 4, 
                                                                    "Inattentive Reluctant", MType.BAD, model, df, 
                                                                    explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.2, 0.8],[0.2, 0.8]]) 
    
    attentive_normal_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 5, 
                                                                    "Attentive Normal", MType.NORMAL, model, df, 
                                                                    explicit_emissions=[[0.5, 0.5],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.8, 0.2],[0.8, 0.2]])
    
    inattentive_normal_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 6, 
                                                                    "Inattentive Normal", MType.NORMAL, model, df, 
                                                                    explicit_emissions=[[0.5, 0.5],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.2, 0.8],[0.2, 0.8]]) 
            
    members += [good_cm, normal_cm, bad_cm, attentive_enthusiastic_cm, attentive_reluctant_cm, 
               inattentive_enthusiastic_cm, inattentive_reluctant_cm, attentive_normal_cm, inattentive_normal_cm]  
    
    # Return the list of community members
    return members


def get_manager(model: ResidentialCommunity, sim_params: ResidentialParameters, pv_surface: float):
    
    # Initialize manager variable to None
    manager = None

    # Based on the simulation_id, create an instance of the appropriate manager class with the required parameters.
    # For each simulation_id, different parameters are passed to either the OneTurnResidentialManager or TwoTurnsResidentialManager classes.
    
    manager = OneTurnResidentialManager(pv_surface, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.INFORMATIVE, 
                                        PrivacyLevel.PRIVATE, sim_params.get_comfort_parameter(), is_known_community=False)

    if not manager:
        raise ValueError("Manager not set!")
    
    return manager

 
if __name__ == "__main__":
    
    config = Config()
    residential_config = config.get_script_config("residential_markov_learning_robust")
    main(residential_config)
        