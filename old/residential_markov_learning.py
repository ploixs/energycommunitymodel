import os
import random
from typing import TYPE_CHECKING, List

import numpy as np
from energy_community.core.constants import ExpectancyDev, MType, PossibleActions, PossibleStates, PrivacyLevel, Stages, Strategy, TargetOfRec
from energy_community.core.energy.get_power_data import get_consumption_for_a_house_from_csv
from energy_community.helpers.scripting import get_member_type_based_on_attentive_following_prob, get_simulation_parameters, prepare_results_folder, run_simulation
from energy_community.residential.core.residential_parameters import ResidentialParameters
from energy_community.residential.core.community import ResidentialCommunity
from energy_community.residential.core.profiles import OneTurnResidentialMarkovCommunityMember, OneTurnResidentialManager, OneTurnResidentialPresenceCommunityMember, OneTurnResidentialSimpleCommunityMember, TwoTurnsResidentialMarkovCommunityMember, TwoTurnsResidentialManager
from configuration import Config
import csv

if TYPE_CHECKING:
    from energy_community.residential.core.community import ResidentialCommunity

def main(script_config):
    
    # Set the attributes of the Config object based on the configuration data for the residential script
    
    # Get the list of simulation scenarios to run from the configuration object
    simulation_scenarios_to_run = range(script_config["simulation_scenarios_to_run"]['start'],script_config["simulation_scenarios_to_run"]['end'])

    # Get the plot_in_browser flag from the configuration object
    plot_in_browser = script_config["plot_in_browser"]

    # Get the print_flag from the configuration object
    print_flag = script_config["print_flag"]

    # Get the list of house IDs from the configuration object
    HOUSE_IDS = script_config["house_ids"]
    
    # Record models
    models : List["ResidentialCommunity"] = []

    # Iterate through each simulation scenario
    for simulation_id in simulation_scenarios_to_run:
        
        # Print a message in the console (because there are many simulation scenarios)
        print(f"Running simulation {simulation_id}...")

        # Prepare the results folder for the current simulation
        results_folder_path = prepare_results_folder(simulation_id, script_config["paths"])

        # Get the simulation parameters for the current simulation
        sim_params = get_simulation_parameters(simulation_id, script_config["paths"], community_type="residential", print_flag=print_flag)

        if type(sim_params) != ResidentialParameters:
            raise ValueError("Parameters not set appropriate")
        
        # Instantiate the ResidentialCommunity model with the simulation parameters and stages
        model = ResidentialCommunity(sim_params, results_folder_path, sim_params.stages)

        # Get the manager for the current simulation
        manager = get_manager(simulation_id, model, sim_params, script_config["pv_surface"])

        # If a manager is defined, assign it to the community
        if manager:
            model.community_manager = manager

        # Create the community members for the current simulation
        members = create_community_members(simulation_id, HOUSE_IDS, sim_params, model)

        # Add the community members to the community
        model.add_new_community_members(members)

        # Run the simulation on the community model
        run_simulation(model)

        # If plot_in_browser is True, plot the results in the browser
        if plot_in_browser:
            model.show_results_plot_in_browser()
        
        # Record models
        models.append(model)
    
    file_path =  os.path.join(script_config['paths']['root_folder_name'], 
                              script_config['paths']['community_folder_name'], 
                              script_config['paths']['results_folder_name'], 'learning_model_comparison.csv')
    performances = [model.community_manager.classification_system.best_performances for model in models]   
    write_to_csv(performances, file_path)



def write_to_csv(list_of_dicts, filename):
    keys = list_of_dicts[0].keys()
    with open(filename, 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(list_of_dicts)


                     
def create_community_members(simulation_id: int, house_ids: list[int], sim_params: ResidentialParameters, model: ResidentialCommunity) -> List:
    """
    Creates a list of residential community members based on the given input parameters.

    Parameters:
        house_ids (List[int]): A list of integers representing the IDs of the houses whose consumption data will be retrieved.
        sim_params (ResidentialParam): An object containing simulation parameters such as the starting and ending dates of the simulation and the simulation stage.
        model (ResidentialCommunity): An object representing the community of residential households being modeled.

    Returns:
        List: A list of ResidentialCommunityMember objects.

    """
    # Initialize an empty list to store the community members
    members = []
    
    
    # Retrieve the consumption data for the current house
    df = get_consumption_for_a_house_from_csv(house_ids[0], sim_params.starting_date, sim_params.ending_date, sim_params.root_folder_name, sim_params.community_folder_name, sim_params.data_folder_name)
    
     # Generate other members for defining typical behaviour
    good_cm = OneTurnResidentialMarkovCommunityMember(house_ids[1] % 1000, 
                                                      "Balanced Enthusiastic", MType.GOOD, model, df, 
                                                      explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], 
                                                      explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 
    normal_cm = OneTurnResidentialMarkovCommunityMember(house_ids[2] % 1000, 
                                                        "Balanced Normal", MType.NORMAL, model, df, 
                                                        explicit_emissions=[[0.5, 0.5],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 
    
    bad_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000, 
                                                     "Balanced Reluctant", MType.BAD, model, df, 
                                                     explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 
    
    attentive_enthusiastic_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 1, 
                                                                        "Attentive Enthusiastic", MType.GOOD, model, df, 
                                                                        explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], 
                                                                        explicit_transmission=[[0.8, 0.2],[0.8, 0.2]])
    
    attentive_reluctant_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 2, 
                                                                    "Attentive Reluctant",  MType.BAD, model, df, 
                                                                    explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.8, 0.2],[0.8, 0.2]]) 
    
    inattentive_enthusiastic_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 3, 
                                                                        "Inattentive Enthusiastic", MType.GOOD, model, df, 
                                                                        explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], 
                                                                        explicit_transmission=[[0.2, 0.8],[0.2, 0.8]])
    
    inattentive_reluctant_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 4, 
                                                                    "Inattentive Reluctant", MType.BAD, model, df, 
                                                                    explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.2, 0.8],[0.2, 0.8]]) 
    
    attentive_normal_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 5, 
                                                                    "Attentive Normal", MType.NORMAL, model, df, 
                                                                    explicit_emissions=[[0.5, 0.5],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.8, 0.2],[0.8, 0.2]])
    
    inattentive_normal_cm = OneTurnResidentialMarkovCommunityMember(house_ids[3] % 1000 + 6, 
                                                                    "Inattentive Normal", MType.NORMAL, model, df, 
                                                                    explicit_emissions=[[0.5, 0.5],[0.5, 0.5]], 
                                                                    explicit_transmission=[[0.2, 0.8],[0.2, 0.8]]) 
    
    if simulation_id == 1:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Balanced normal", MType.NORMAL, model, df, 
                                                             explicit_emissions=[[0.5, 0.5],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 

    if simulation_id == 2:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Balanced Enthusiastic", MType.GOOD, model, df, 
                                                             explicit_emissions=[[0.7, 0.3],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 

    if simulation_id == 3:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Balanced Enthusiastic", MType.GOOD, model, df, 
                                                             explicit_emissions=[[0.9, 0.1],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]])

    if simulation_id == 4:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Balanced Reluctant", MType.BAD, model, df, 
                                                             explicit_emissions=[[0.3, 0.7],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 

    if simulation_id == 5:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Balanced Reluctant", MType.BAD, model, df, 
                                                             explicit_emissions=[[0.1, 0.9],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]]) 

    if simulation_id in [6, 7, 8]:
        x = random.random()
        cm_true_type = get_member_type_based_on_attentive_following_prob(x)
        desc = None
        if cm_true_type == MType.GOOD:
            desc = "Enthusiastic"
        elif cm_true_type == MType.NORMAL:
            desc = "Normal"
        else:
            desc = "Reluctant"
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Balanced " + desc, cm_true_type, model, df, 
                                                             explicit_emissions=[[x, 1 - x],[0.5, 0.5]], explicit_transmission=[[0.5, 0.5],[0.5, 0.5]])

    if simulation_id == 9:
        unknown_cm = OneTurnResidentialSimpleCommunityMember(house_ids[0] % 1000, "Simple Ideal", MType.IDEAL, model, df)
    
    if simulation_id == 10:
        unknown_cm = OneTurnResidentialSimpleCommunityMember(house_ids[0] % 1000, "Simple Enthusiastic", MType.GOOD, model, df)
        
    if simulation_id == 11:
        unknown_cm = OneTurnResidentialSimpleCommunityMember(house_ids[0] % 1000, "Simple Normal", MType.NORMAL, model, df)
        
    if simulation_id == 12:
        unknown_cm = OneTurnResidentialSimpleCommunityMember(house_ids[0] % 1000, "Simple Reluctant", MType.BAD, model, df)
        
    if simulation_id == 13:
        unknown_cm = OneTurnResidentialPresenceCommunityMember(house_ids[0] % 1000, "Presence Ideal", MType.IDEAL, model, df)
        
    if simulation_id == 14:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Attentive Enthusiastic", MType.BAD, model, df, 
                                                             explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], explicit_transmission=[[0.8, 0.2],[0.8, 0.2]]) 
        
    if simulation_id == 15:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Attentive Reluctant", MType.BAD, model, df, 
                                                             explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], explicit_transmission=[[0.8, 0.2],[0.8, 0.2]]) 
    
    if simulation_id == 16:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Inattentive Enthusiastic", MType.BAD, model, df, 
                                                             explicit_emissions=[[0.8, 0.2],[0.5, 0.5]], explicit_transmission=[[0.2, 0.8],[0.2, 0.8]])
    
    if simulation_id == 17:
        # Check the simulation stage and create the appropriate community member object
        unknown_cm = OneTurnResidentialMarkovCommunityMember(house_ids[0] % 1000, "Inattentive Reluctant", MType.BAD, model, df, 
                                                             explicit_emissions=[[0.2, 0.8],[0.5, 0.5]], explicit_transmission=[[0.2, 0.8],[0.2, 0.8]])
    
    # Append the new community member to the list of members
    members = [unknown_cm, good_cm, normal_cm, bad_cm, attentive_enthusiastic_cm, attentive_reluctant_cm, 
               inattentive_enthusiastic_cm, inattentive_reluctant_cm, attentive_normal_cm, inattentive_normal_cm]  

    
    # Return the list of community members
    return members


def get_manager(simulation_id: int, model: ResidentialCommunity, sim_params: ResidentialParameters, pv_surface: float):
    
    # Initialize manager variable to None
    manager = None

    # Based on the simulation_id, create an instance of the appropriate manager class with the required parameters.
    # For each simulation_id, different parameters are passed to either the OneTurnResidentialManager or TwoTurnsResidentialManager classes.

    # The following dictionary maps a configuration for a manager instance to each simulation_id (1 to 15) with their respective parameters.
    
    manager = OneTurnResidentialManager(pv_surface, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.INFORMATIVE, 
                                        PrivacyLevel.PRIVATE, sim_params.get_comfort_parameter(), is_known_community=False)

    if not manager:
        raise ValueError("Manager not set!")
    
    return manager

 
if __name__ == "__main__":
    
    config = Config()
    residential_config = config.get_script_config("residential_markov_learning")
    main(residential_config)
        