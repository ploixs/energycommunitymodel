

from energy_community.ResidentialCommunityMember import ResidentialCommunityMember



class OneTurnFixedIntensity(ResidentialCommunityMember):
    """ one stage, fixed intensity, fixed willingness based on categories, always available"""

    def __init__(self, unique_id, model, automatic_energy_manager, agent_type, est_consumption_total):
        super().__init__(unique_id, model, automatic_energy_manager, agent_type, est_consumption_total)

    def first_stage(self):
        if self.aem.flexibility_period.hour_min <= self.model.time_range_simulation[
            self.model.current_hour_index].hour < self.aem.flexibility_period.hour_max:
            for period_name in self.aem.period_names_list:
                if self.aem.periods_current_day[period_name].hour_min <= self.model.current_hour_index_day_relative < \
                        self.aem.periods_current_day[period_name].hour_max:
                    self.action_to_be_done_current_hour = self.actions_to_be_done_current_day_list[
                        self.model.current_hour_index_day_relative]

            # MAKE DECISION BASED ON RECOMMENDATION FOR CURRENT HOUR
            decision = self.make_decision_based_on_member_type()
            # DO ACTION ACCORDING TO DECISION
            [action, sim_agent_correctness_value, sim_consumption_current_hour] = self.do_action_with_fixed_impact(
                decision)
            # UPDATE RANKING
            if self.model.simulation_parameters.member_willingness == 'variable':
                self.update_rating(sim_agent_correctness_value)
            # RECORD DATA
            self.record_current_hour_data(action, sim_agent_correctness_value, sim_consumption_current_hour)
            # PRINT MESSAGE
            if self.model.simulation_parameters.print_member_info:
                print(self.type + " should do action " + str(self.action_to_be_done_current_hour) + " at " + str(
                    self.model.time_range_simulation[self.model.current_time_index]))
        else:
            # NO RECOMMENDATION, CONSUME AS EXPECTED
            self.record_current_hour_data(0, 0, self.est_consumption_current_day_list[
                self.model.current_hour_index_day_relative])

    def second_stage(self):
        pass


class OneTurnRealisticIntensity(ResidentialCommunityMember):
    """one stage, realistic intensity, no model for willingness (ideal agent), always available"""

    def __init__(self, unique_id, model, agent_type, est_consumption_total):
        super().__init__(unique_id, model, agent_type, est_consumption_total)

    def first_stage(self):
        if self.com_model.community_manager.flexibility_period.hour_min <= self.com_model.timeframe_current_day[
            self.com_model.current_hour_index_day_relative].hour < self.com_model.community_manager.flexibility_period.hour_max:
            # GET ACTION TO BE DONE CURRENT HOUR FROM RECOMMENDATIONS LIST
            self.action_to_be_done_current_hour = self.actions_to_be_done_current_day_list[
                self.com_model.current_hour_index_day_relative]
            # MAKE DECISION BASED ON RECOMMENDATION FOR CURRENT HOUR
            decision = self.make_decision_based_on_member_type()
            # DO ACTION ACCORDING TO DECISION
            [action, sim_consumption_current_hour] = self.do_action_with_unknown_impact(decision)
            # RECORD DATA
            self.record_current_hour_data(action, sim_consumption_current_hour)
            # PRINT MESSAGE
            if self.com_model.sim_params.print_member_info:
                print(f'{self.type} should do action {self.action_to_be_done_current_hour} at \
                    {self.com_model.timeframe_simulation[self.com_model.current_hour_index]}')
        else:
            # NO RECOMMENDATION, CONSUME AS EXPECTED
            self.record_current_hour_data(0, self.est_member_load_day.data[self.com_model.current_hour_index_day_relative])

    def second_stage(self):
        pass


class TwoTurnsRealisticIntensity(ResidentialCommunityMember):
    """reactive, realistic intensity, no model for willingness, always available"""

    def __init__(self, unique_id, model, automatic_energy_manager, agent_type, est_consumption_total):
        super().__init__(unique_id, model, automatic_energy_manager, agent_type, est_consumption_total)

    def first_stage(self):
        if self.aem.flexibility_period.hour_min <= self.model.time_range_simulation[
            self.model.current_hour_index].hour < self.aem.flexibility_period.hour_max:
            # GET ACTION TO BE DONE CURRENT HOUR FROM RECOMMENDATIONS LIST
            self.action_to_be_done_current_hour = self.actions_to_be_done_current_day_list[
                self.model.current_hour_index_day_relative]
            # MAKE DECISION BASED ON RECOMMENDATION FOR CURRENT HOUR
            decision = self.make_decision_based_on_member_type()
            # DO ACTION ACCORDING TO DECISION
            [action, sim_agent_correctness_value, sim_consumption_current_hour] = self.do_action_with_unknown_impact(
                decision)
            if self.model.sim_params.member_willingness == 'variable':
                # UPDATE RANKING
                self.update_rating(sim_agent_correctness_value)
            # RECORD DATA
            self.record_current_hour_data(action, sim_agent_correctness_value, sim_consumption_current_hour)
            # PRINT MESSAGE
            if self.model.sim_params.print_member_info:
                print(self.type + " should do action " + str(self.action_to_be_done_current_hour) + " at " + str(
                    self.model.time_range_simulation[self.model.current_hour_index]))
        else:
            # NO RECOMMENDATION, CONSUME AS EXPECTED
            self.record_current_hour_data(0, 0, self.est_consumption_current_day_list[
                self.model.current_hour_index_day_relative])

    def second_stage(self):
        if self.aem.flexibility_period.hour_min <= self.model.time_range_simulation[
            self.model.current_hour_index].hour < self.aem.flexibility_period.hour_max:
            # GET ACTION TO BE DONE CURRENT HOUR FROM RECOMMENDATIONS LIST
            self.action_to_be_done_current_hour = self.actions_to_be_done_current_day_list[
                self.model.current_hour_index_day_relative]
            # MAKE DECISION BASED ON RECOMMENDATION FOR CURRENT HOUR
            decision = self.make_decision_based_on_member_type()
            # DO ACTION ACCORDING TO DECISION
            [action, sim_agent_correctness_value, sim_consumption_current_hour] = self.do_action_with_unknown_impact(
                decision)
            # RECORD DATA
            self.record_current_hour_data(action, sim_agent_correctness_value, sim_consumption_current_hour)
            # PRINT MESSAGE
            if self.model.sim_params.print_member_info:
                print('MORE HELP NEEDED: ' + self.type + " should do action " + str(
                    self.action_to_be_done_current_hour) + " at " + str(
                    self.model.time_range_simulation[self.model.current_hour_index]))
        else:
            # NO RECOMMENDATION, CONSUME AS EXPECTED
            self.record_current_hour_data(0, 0, self.est_consumption_current_day_list[
                self.model.current_hour_index_day_relative])

    def third_stage(self):
        pass






