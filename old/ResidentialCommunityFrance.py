import os
import sqlite3
import sys

import pandas as pd

from energy_community.residential_manager_types import OneTurnCoachingClustersFixedExpectancy
from energy_community.residential_member_types import OneTurnFixedIntensity
from energy_community.core.EnergyCommunityModel import EnergyCommunityModel
from energy_community.core.energy.get_power_data import get_consumption_for_a_house_from_database
from energy_community.helpers.time_management_functions import datetime_shift_year
from buildingenergy.timemg import epochtimems_to_datetime

from simulation.Configurator import Configuration


class ResidentialCommunityFrance(EnergyCommunityModel):

    def __init__(self, sim_params):
        super().__init__(sim_params)

        self.set_the_community_manager()

        if not self.sim_params.approach_comparison_ok:
            self.set_available_houses_ids_from_france()
        else:
            print("ERROR: residential community configuration not chosen correctly.")
            sys.exit()

    def print_members_in_community(self):
        print("Community members ids: ", [m.unique_id for m in self.community_manager.cm_list])
        
    def print_current_status_of_members(self):
        print(f"Currently, the community has: ")
        for key in self.community_manager.agents_by_type.keys():
            print(f"{len(self.community_manager.agents_by_type[key])} {key} agents")
    
    def get_agent_type_to_be_added(self) -> str:
        """Gets the next agent type to be added in the community.

         :rtype: str"""

        agent_type_to_be_added = None
        for agent_type in self.sim_params.willingness_types_in_community:
            if agent_type not in self.community_manager.cm_types_in_community:
                agent_type_to_be_added = agent_type
                break
            if len(self.community_manager.agents_by_type[agent_type]) < int(self.sim_params.agents_lim[agent_type]):
                agent_type_to_be_added = agent_type
                break
            else:
                if self.sim_params.print_member_info:
                    print("Agent limit reached for type: ", agent_type)
                continue
        if self.sim_params.print_member_info:
            print("Next agent added will be ", agent_type_to_be_added)
        return agent_type_to_be_added
    
    def remove_agents_who_left_community(self):
        """Checks if current simulation time corresponds with the last timestamp of a house consumption dataset. \
            If yes, removes the house from the model."""

        agents_to_be_removed_list = []
        for agent in self.community_manager.cm_list:
            if agent.unique_id != 999:
                # if agent.leaving_date < self.current_time:
                if agent.leaving_date < self.timeframe_simulation[self.current_hour_index]:
                    agents_to_be_removed_list.append(agent)
        for agent in agents_to_be_removed_list:
            self.community_manager.agents_by_type[agent.type].remove(agent)
            self.schedule.remove(agent)
            if self.sim_params.print_member_info:
                print(agent.type + " agent with ID " + str(agent.unique_id) + " has been removed.")
    
    def add_new_community_member(self, house_id, agent_type, consumption_df):
        new_member =  OneTurnFixedIntensity(house_id, self, self.community_manager, agent_type, consumption_df)


        self.schedule.add(new_member)  # type: ignore
        if agent_type not in self.community_manager.cm_types_in_community:
            self.community_manager.update_agent_types_in_community(agent_type)
        self.community_manager.agents_by_type[agent_type].append(new_member)
        self.community_manager.cm_list.append(new_member)
        if self.sim_params.print_member_info:
            print("Agent " + str(new_member.unique_id) + " added to the community as a " + agent_type + " agent")

    def set_available_houses_ids_from_france(self):
        db_path = Configuration.get_consumption_data_config('source_folder')
        db_path = os.path.join(db_path, 'irise.sqlite3')
        db = sqlite3.connect(os.path.normpath(db_path))
        # Extract houses data from the database
        df = pd.read_sql_query("SELECT ID, StartingEpochTime, EndingEpochTime  FROM HOUSE", db)
        starting_dates = []
        ending_dates = []
        house_id_list = df['ID'].to_list()
        starting_epochtimes = df['StartingEpochTime'].to_list()
        ending_epochtimes = df['EndingEpochTime'].to_list()
        for i in range(len(df)):
            new_start_datetime = datetime_shift_year(epochtimems_to_datetime(starting_epochtimes[i] * 1000), 17)
            starting_dates.append(new_start_datetime)
            new_end_datetime = datetime_shift_year(epochtimems_to_datetime(ending_epochtimes[i] * 1000), 17)
            ending_dates.append(new_end_datetime)

        available_houses = []
        for i in range(len(house_id_list)):
            if starting_dates[i] <= self.sim_params.starting_date and ending_dates[
                i] >= self.sim_params.ending_date and house_id_list[
                i] != 2000902:
                print(house_id_list[i])
                available_houses.append(house_id_list[i])
                print("HOUSE " + str(house_id_list[i]) + " available from " + str(starting_dates[i]) + " to " + str(
                    ending_dates[i]))

        print("AVAILABLE HOUSES: ", available_houses)
        self.available_houses_id_list = available_houses

    def check_and_add_new_community_members(self):
        """Checks if current simulation time corresponds with the first timestamp of a house consumption dataset. \
        If yes, adds the house as a community member, depending on the required community configuration."""

        ids_of_added_houses = [x.unique_id for x in self.community_manager.cm_list]
        print("Added houses until now:", ids_of_added_houses)
        # At each step, add more agents to the model, according ot their joining dates
        for house_id in self.available_houses_id_list:
            print(self.available_houses_id_list)
            # get consumption data for each house
            # df, ok = get_power_data_1_house_from_pickle(house_id)
            df, ok = get_consumption_for_a_house_from_database(house_id, self.sim_params.starting_date,
                                                               self.sim_params.ending_date, True)

            if ok == 0 or len(df) != len(self.timeframe_simulation):
                print('Error at consumption processing')
            else:
                agent_type_to_be_added = self.get_agent_type_to_be_added()
                if house_id not in ids_of_added_houses and agent_type_to_be_added:
                    print("Community member found. ID :" + str(house_id))
                    self.add_new_community_member(house_id, agent_type_to_be_added, df)

    def step(self):
        """Advances the model by one step."""

        if self.current_hour_index == self.start_of_day_hour_indexes[0]:
            print("- Removing old community members -")
            self.remove_agents_who_left_community()  # Check and remove agents who left the community
            print("- Searching for new community members -")
            self.check_and_add_new_community_members()  # Check and add new agents according to current time and agent joining time

        # Record data externally to files and figures
        if self.current_hour_index in self.start_of_day_hour_indexes:
            self.current_hour_index_day_relative = 0
            self.set_timeframe_current_day()
            self.days_passed += 1
            print("DAYS PASSED: " + str(self.days_passed) + " in scenario " + str(
                self.sim_params.simulation_id))

        # Make a model step and update the hour
        print("------------------------------------")
        print(
            "Current Hour: " + str(self.timeframe_simulation[self.current_hour_index]) + " Current index: " + str(
                self.current_hour_index) +
            " Current day-relative index: " + str(self.current_hour_index_day_relative))
        print("------------------------------------")
        self.schedule.step() # type: ignore

        # Update time
        self.current_hour_index += 1
        self.current_hour_index_day_relative += 1

    def set_the_community_manager(self):
        """Sets the community manager according to the specification in the scenario_description file."""

        manager = None
        if self.sim_params.manager_stages == 'one_stage' and \
                self.sim_params.expectancy_development_approach == 'fixed' and \
                self.sim_params.recommendation_strategy == 'coaching':
            manager = OneTurnCoachingClustersFixedExpectancy(999, self)
        else:
            print("ERROR")
            sys.exit()

        self.schedule.add(manager)  # type: ignore
        self.community_manager = manager

