import ast
import os
import sys
from typing import List

import matplotlib.dates as mdates
import numpy as np
import pandas as pd
import math

from sklearn import preprocessing
from energy_community.core.data import Data
from energy_community.core.simulation.Period import Period
from energy_community.helpers.constants import AsignType, DSType, DType, MType, Rec



def get_member_model_based_on_features(cm_models_of_member : List[Data], data_source_type : DSType, data_type: DType) -> Data:
    for data_model in cm_models_of_member:
        if data_model.data_type == data_type and \
           data_model.data_source_type == data_source_type and \
           data_model.asignee_type == AsignType.MEMBER:
               return data_model
    raise ValueError("Could not generate member model!")

def topsis_analysis(topsis_dataset, scenarios):
    similiarity_to_best = topsis_method(topsis_dataset)
    best_value = max(similiarity_to_best)
    for i in range(len(similiarity_to_best)):
        if similiarity_to_best[i] == best_value:
            best_index = i
    print(f"***Topsis analysis between: {[key for key in topsis_dataset.keys()]}")
    print(f"***Scenario with highest similarity to best value: {scenarios[best_index].short_description}\n")


def normalize(x):
    norm_vector = []
    squared_x = [val ** 2 for val in x]
    for val in x:
        # norm_val = val / math.sqrt(sum(squared_x))
        norm_val = np.tanh(val)
        norm_vector.append(norm_val)
    return norm_vector


def topsis_method(data):
    print('STEP 1: Topsis analysis dataset:')
    for key in data.keys():
        print(f'{key}:', data[key])  # check if dataset it formatted correctly
        alternative_length = len(data[key])  # get the length of the alternatives in the dataset

    weights = [1 / len(data.keys())] * len(data.keys())  # set up the weights equally for each criterion
    print(f'Weights: {weights}')

    norm_data = {}
    for key in data.keys():
        # norm_data[key] = preprocessing.normalize([np.array(data[key])])[0]  # normalize data for each criterion
        norm_data[key] = normalize(data[key])  # normalize data for each criterion

    print('STEP 2: Normalized values:')
    for key in norm_data.keys():
        print(f'{key}:', norm_data[key])

    weighted_norm_data = {}
    for key in norm_data.keys():
        weighted_norm_data[key] = []

    # Calculate weighted normalised values for each criterion
    for j, key in enumerate(data.keys()):
        for i in range(len(norm_data[key])):
            weighted_norm_data[key].append(weights[j] * norm_data[key][i])

    print('STEP 3: Normalised weighted values:')
    for key in weighted_norm_data.keys():
        print(f'{key}:', weighted_norm_data[key])

    # For each criterion, get the best and worst alternative
    best_i_list = []
    worst_i_list = []
    for key in weighted_norm_data.keys():
        best_i_list.append(min(weighted_norm_data[key]))
        worst_i_list.append(max(weighted_norm_data[key]))
    best_i = tuple(best_i_list)
    worst_i = tuple(worst_i_list)

    print('STEP 4: Best/worst alternatives for each criterion:')
    print("BEST ", best_i)
    print("WORST", worst_i)

    dist_crit = {'best': [], 'worst': []}

    for i in range(alternative_length):
        val_best, val_worst = 0, 0
        for j, key in enumerate(weighted_norm_data.keys()):
            val_best += (weighted_norm_data[key][i] - best_i[j]) ** 2
            val_worst += (weighted_norm_data[key][i] - worst_i[j]) ** 2
        dist_crit['best'].append(val_best)
        dist_crit['worst'].append(val_worst)

    print('STEP 5: Distance to best/worst criterion:')
    print(dist_crit)

    similarity = []
    for i in range(alternative_length):
        val = dist_crit['worst'][i] / (dist_crit['best'][i] + dist_crit['worst'][i])
        similarity.append(val)

    print('STEP 6: Similarity vector:')
    print(similarity)

    return similarity


def get_agent_types_in_simulation(cfg):
    simulation_id = cfg.Configuration.get_simulations_parameters_set()
    dynamic_ranking_flag = cfg.Configuration.get_simulation_dynamic_ranking_parameter(simulation_id)
    agent_types = []
    if not dynamic_ranking_flag:
        for a_type in ['ideal', 'normal', 'good', 'bad']:
            agents_lim = cfg.Configuration.get_simulation_agents_count(simulation_id, a_type)
            if agents_lim > 0:
                agent_types.append(a_type)
    return agent_types


def convert_dataframe_in_plotting_format(df, flexibility_periods):
    #new_list = [int(Recommendations(x).value) if x else np.nan for x in df]
    new_list  = []
    for value in df:
        if value:
            if Rec(str(value)).value == Rec.BLINKING_RED:
                new_list.append(1)
            if Rec(str(value)).value == Rec.STRONG_RED:
                new_list.append(2)
            if Rec(str(value)).value == Rec.RED:
                new_list.append(3)
            if Rec(str(value)).value == Rec.WHITE:
                new_list.append(4)
            if Rec(str(value)).value == Rec.GREEN:
                new_list.append(5)
            if Rec(str(value)).value == Rec.STRONG_GREEN:
                new_list.append(6)
            if Rec(str(value)).value == Rec.BLINKING_GREEN:
                new_list.append(7)
        else:
            new_list.append(0)
        
    #for value in df:
    #    if value in Recommendations:
    #        new_list.append(value.value)
    #    else:
    #        new_list.append(np.nan)
            
    if flexibility_periods:
        df.loc[(df.index.hour < flexibility_periods[0].hour_min)] = np.nan
        df.loc[(df.index.hour > flexibility_periods[0].hour_max) &
               (df.index.hour < flexibility_periods[1].hour_min)] = np.nan
        df.loc[(df.index.hour > flexibility_periods[1].hour_max)] = np.nan
    x = mdates.date2num(df.index)

    
    '''
    if value == Recommendations.WHITE:
        new_list.append(0)
    if value == -2:
        new_list.append(1)
    if value == -1:
        new_list.append(2)
    if value == -0.5:
        new_list.append(3)
    if value == 0:
        new_list.append(4)
    if value == 0.5:
        new_list.append(5)
    if value == 1:
        new_list.append(6)
    if value == 2:
        new_list.append(7)'''

    y = pd.Series(new_list, index=df.index)
    return x, y


def set_case_based_on_scenario(df):
    agent_type = ['normal', 'good', 'bad', 'ideal']
    agent_categories_list = []

    for type in agent_type:
        if 'first_' + type + '_agent_recommended_actions_total' in df:
            agent_categories_list.append(type)

    return agent_categories_list


def set_community_member_type_based_on_agent_type(agent_type):
    community_member_type = 'ideal'
    if agent_type == 'enthusiastic':
        community_member_type = 'Enthusiastic'
    if agent_type == 'normal':
        community_member_type = 'Normal'
    if agent_type == 'reluctant':
        community_member_type = "Reluctant"
    return community_member_type


def set_plot_properties_power_profile_comparison_general(plt, scenario, index, title_font_size, axis_font_size,
                                                         legend_font_size):
    inf_date = pd.to_datetime(scenario.spec.plot_intervals[index][0], format="%d-%m-%Y").tz_localize(scenario.local_tz)
    sup_date = pd.to_datetime(scenario.spec.plot_intervals[index][1], format="%d-%m-%Y").tz_localize(scenario.local_tz)
    title_inf_date = f'{inf_date.day} {month_mask[inf_date.month - 1]}'
    title_sup_date = f'{sup_date.day} {month_mask[sup_date.month - 1]}'
    delta = sup_date - inf_date

    plt.title(f"Power profile comparison for the community with "
              fr"$\tau=$ {round(scenario.params.no_alert_threshold)} W between {title_inf_date} and {title_sup_date} ",
              fontsize=title_font_size)
    plt.ylabel('Power [kW]', fontsize=axis_font_size)
    plt.xlabel('Time', fontsize=axis_font_size)
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=axis_font_size)
    plt.gca().xaxis_date(tz=scenario.local_tz)
    
    if delta.days > 2 and delta.days < 10:
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=5))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d-%m %H'))
    else:
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=1))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%H'))
    
    plt.xticks(rotation=90)
    plt.legend(framealpha=0.8, fontsize=legend_font_size, loc='center left', bbox_to_anchor=(1, 0.5))
    # plt.legend(framealpha=0.8, fontsize=legend_font_size, loc='best')


def set_plot_properties_actions_comparison(plt, agent_type, scenario, index, title_font_size, axis_font_size,
                                           legend_font_size):
    # SET GENERAL PLOT PROPERTIES
    inf_date = pd.to_datetime(scenario.spec.plot_intervals[index][0], format="%d-%m-%Y").tz_localize(scenario.local_tz)
    sup_date = pd.to_datetime(scenario.spec.plot_intervals[index][1], format="%d-%m-%Y").tz_localize(scenario.local_tz)
    delta = sup_date - inf_date
    plt.grid(True)
    plt.gca().xaxis_date(tz=scenario.local_tz)
    if delta.days > 2 and delta.days < 10:
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=5))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d-%m %H'))
    else:
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=1))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%H'))
    plt.xlabel('Time', fontsize=axis_font_size)
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=axis_font_size)
    plt.xticks(rotation=90)

    # GET THE COMMUNITY MEMBER TYPE
    community_member_type = set_community_member_type_based_on_agent_type(agent_type)

    # SET THE TITLE ACCORDING TO COMMNUITY MEMBER TYPE
    # plt.title(community_member_type + " member 1 simulated actions - month: " + str(scenario.month_list[month_index]),
    #          fontsize=title_font_size)
    plt.title("Recommendations given for the simulation period",
              fontsize=title_font_size)

    # SET THE Y AXIS ACCORDING TO THE RECOMMENDATIONS GIVEN
    if scenario.manager_archetype == 'two_turns_informative_everyone_adaptive_expectancy' or \
            scenario.manager_archetype == 'two_turns_informative_everyone_basic_expectancy':
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0.5, 7)
        plt.gca().set_yticklabels(['BLINKING RED', 'STRONG RED', 'RED', 'WHITE', 'GREEN', 'STRONG GREEN', 'BLINKING GREEN'])

    elif scenario.manager_archetype == 'one_turn_informative_everyone_adaptive_expectancy' or \
            scenario.manager_archetype == 'one_turn_informative_everyone_basic_expectancy':
        plt.yticks([3, 4, 5])
        plt.ylim(2.5, 5)
        plt.gca().set_yticklabels(['RED', 'WHITE', 'GREEN'])

    else:
        plt.yticks([2, 3, 4, 5, 6])
        plt.ylim(1.5, 6)
        plt.gca().set_yticklabels(['STRONG RED', 'RED', 'WHITE', 'GREEN', 'STRONG GREEN'])

    # PLOT THE LEGEND
    # plt.legend(['Recommendation', 'Behaviour of  Agent 1'], framealpha=0.3, fontsize=legend_font_size,
    #           loc='center left', bbox_to_anchor=(1, 0.5))


def set_plot_properties_power_profile_comparison_for_specific_agent_category(plt, scenario, month_index,
                                                                             title_font_size, axis_font_size,
                                                                             legend_font_size,
                                                                             agent_type):
    community_member_type = set_community_member_type_based_on_agent_type(agent_type)
    plt.title("Power Profile Comparison - " + community_member_type + " members - month: " + str(
        scenario.month_list[month_index]), fontsize=title_font_size)
    plt.ylabel('Power [kW]', fontsize=axis_font_size)
    plt.xlabel('Time', fontsize=axis_font_size)
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=axis_font_size)
    if scenario.day_lim_sup - scenario.day_lim_inf > 2:
        plt.gca().xaxis.set_major_locator(mdates.DayLocator(interval=1))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d-%m'))
    else:
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=1))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%H'))
    plt.xticks(rotation=90)
    plt.legend(framealpha=0.8, fontsize=legend_font_size, loc='center left', bbox_to_anchor=(1, 0.5))


def plot_power_profile_comparison_general(plt, scenario, index):
    #df = scenario.hourly_com_results_df
    df = pd.DataFrame({'est_load': scenario.est_com_load_total.data, 
                       'est_prod': scenario.est_com_prod_total.data,
                       'sim_load': scenario.sim_com_load_total.data}, index=scenario.est_com_load_total.time_frame)
    inf_datetime = pd.to_datetime(scenario.spec.plot_intervals[index][0], format="%d-%m-%Y")
    sup_datetime = pd.to_datetime(scenario.spec.plot_intervals[index][1], format="%d-%m-%Y")
    # Plot power profile comparison - general
    if scenario.params.subway_station_manager:
        #plt.plot(df[inf_datetime:sup_datetime][f'{DSType.ESTIMATED}_com_{EType.CONSUMPTION}_total'] / 1000, color='blue',
        #        linestyle='dashed', label='$p^{expected}_{commuting,Load}$')
        #plt.plot(df[inf_datetime:sup_datetime][f'{DSType.SIMULATED}_com_{EType.CONSUMPTION}_total'] / 1000, color='blue',
        #        label='$p^{simulated}_{commuting,Load}$')
        #plt.plot(df[inf_datetime:sup_datetime][f'{DSType.ESTIMATED}_com_{EType.PRODUCTION}_total'] / 1000, color='red',
        #        linestyle='dashed', label='$p^{simulated}_{PV}$')
        plt.plot(df[inf_datetime:sup_datetime]['est_load'] / 1000, color='blue', linestyle='dashed', label='$p^{expected}_{commuting,Load}$')
        plt.plot(df[inf_datetime:sup_datetime]['sim_load'] / 1000, color='blue', label='$p^{simulated}_{commuting,Load}$')
        plt.plot(df[inf_datetime:sup_datetime]['est_prod'] / 1000, color='red', linestyle='dashed', label='$p^{simulated}_{PV}$')
        print(df[inf_datetime:sup_datetime]['est_load'])
        print(df[inf_datetime:sup_datetime]['sim_load'])
        #print(df[inf_datetime:sup_datetime]['est_prod'])
        #print("a")
    else:
        plt.plot(df[inf_datetime:sup_datetime][f'{DSType.ESTIMATED}_com_{EType.CONSUMPTION}_total'] / 1000, color='blue',
                linestyle='dashed', label='$p^{estimated}_{Load}$')
        plt.plot(df[inf_datetime:sup_datetime][f'{DSType.SIMULATED}_com_{EType.CONSUMPTION}_total'] / 1000, color='blue',
                label='$p^{simulated}_{Load}$')
        plt.plot(df[inf_datetime:sup_datetime][f'{DSType.ESTIMATED}_com_{EType.PRODUCTION}_total'] / 1000, color='red',
                linestyle='dashed', label='$p^{simulated}_{PV}$')


def plot_actions_comparison_for_specific_agent_category(plt,
                                                        scenario,
                                                        index: int,
                                                        agent_type: MType):
    inf_date = pd.to_datetime(scenario.spec.plot_intervals[index][0], format="%d-%m-%Y")
    sup_date = pd.to_datetime(scenario.spec.plot_intervals[index][1], format="%d-%m-%Y")
    
    first_cm = scenario.manager.cm_list[0] # IF WE WANT TO PLOT THE ACTIONS OF THE FIST AGENT, WE EXTRACT THEM FROM THE DATA
    if scenario.spec.plot_real_behaviour_of_first_agent_flag:
        #actions_model : Data = get_member_model_based_on_features(scenario.cm_models[id_of_first_agent],DSType.SIMULATED,DType.ACTIONS)
        #if actions_model:
        df = pd.DataFrame({'actions': first_cm.sim_member_actions_total.data}, index=first_cm.sim_member_actions_total.time_frame)
        df_temp2 = df[inf_date:sup_date][f'actions'].copy()
        x2, y2 = convert_dataframe_in_plotting_format(df_temp2, scenario.manager.flexibility_periods)

    #recs_model : Data | None = get_member_model_based_on_features(scenario.cm_models[id_of_first_agent],DSType.SIMULATED,DType.RECOMMENDATIONS)
    #if recs_model:
    df2 = pd.DataFrame({'recommendations': first_cm.sim_member_rec_total.data}, index= first_cm.sim_member_rec_total.time_frame)
    df_temp = df2[inf_date:sup_date][f'recommendations'].copy()
    x1, y1 = convert_dataframe_in_plotting_format(df_temp, scenario.manager.flexibility_periods)

    w = 0.02  # width for stacked plot

    if scenario.spec.plot_real_behaviour_of_first_agent_flag:
        plt.bar(x1 - w, y1, align='edge', width=0.02, color='green')
        plt.bar(x2, y2, align='edge', width=0.02, color='blue')
    else:
        plt.bar(x1, y1, align='edge', width=0.02, color='green')


def plot_power_profile_comparison_for_specific_agent_category(plt, scenario, interval_index, agent_type):
    df = scenario.hourly_results_df
    plt.plot(df.loc[(df.index.month == scenario.month_list[interval_index]) & (
        df.index.day.isin(range(scenario.day_lim_inf, scenario.day_lim_sup)))]
             ['expected_' + agent_type + '_agents_consumption_total'] / 1000, color='blue', linestyle='dashed',
             label='$p^{expected}_{Load}$')
    plt.plot(df.loc[(df.index.month == scenario.month_list[interval_index]) & (
        df.index.day.isin(range(scenario.day_lim_inf, scenario.day_lim_sup)))]
             ['simulated_' + agent_type + '_agents_consumption_total'] / 1000, color='blue',
             label='$p^{simulated}_{Load}$')
    plt.plot(df.loc[(df.index.month == scenario.month_list[interval_index]) & (
        df.index.day.isin(range(scenario.day_lim_inf, scenario.day_lim_sup)))]
             ['simulated_community_production_total'] / 1000, color='red', label='$p^{expected}_{PV}$')





def plot_period_vertical_lines(plt, scenario, month_index):
    # Extract period times for each day from dataframe
    ts_midday1_lim_inf = \
        scenario.community_results_df.loc[(scenario.community_results_df['month'] == scenario.month_list[month_index]) &
                                          (scenario.community_results_df['day'].isin(
                                              range(scenario.day_lim_inf, scenario.day_lim_sup)))][
            'midday1_hour_min']
    ts_midday2_lim_sup = \
        scenario.community_results_df.loc[(scenario.community_results_df['month'] == scenario.month_list[month_index]) &
                                          (scenario.community_results_df['day'].isin(
                                              range(scenario.day_lim_inf, scenario.day_lim_sup)))][
            'midday2_hour_max']
    ts_peak_lim_inf = \
        scenario.community_results_df.loc[(scenario.community_results_df['month'] == scenario.month_list[month_index]) &
                                          (scenario.community_results_df['day'].isin(
                                              range(scenario.day_lim_inf, scenario.day_lim_sup)))][
            'peak_prod_hour_min']
    ts_peak_lim_sup = \
        scenario.community_results_df.loc[(scenario.community_results_df['month'] == scenario.month_list[month_index]) &
                                          (scenario.community_results_df['day'].isin(
                                              range(scenario.day_lim_inf, scenario.day_lim_sup)))][
            'peak_prod_hour_max']

    # Get list of strings with datetime AND hour
    midday1_lim_inf_str_list = []
    for y, x in ts_midday1_lim_inf.items():
        # temp = y.replace(hour=x) - timedelta(days=1)
        temp = y.replace(hour=x)
        midday1_lim_inf_str_list.append(str(temp))

    midday2_lim_sup_str_list = []
    for y, x in ts_midday2_lim_sup.items():
        # temp = y.replace(hour=x) - timedelta(days=1)
        temp = y.replace(hour=x)
        midday2_lim_sup_str_list.append(str(temp))

    peak_lim_inf_str_list = []
    for y, x in ts_peak_lim_inf.items():
        # temp = y.replace(hour=x) - timedelta(days=1)
        temp = y.replace(hour=x)
        peak_lim_inf_str_list.append(str(temp))

    peak_lim_sup_str_list = []
    for y, x in ts_peak_lim_sup.items():
        # temp = y.replace(hour=x) - timedelta(days=1)
        temp = y.replace(hour=x)
        peak_lim_sup_str_list.append(str(temp))
    # print(peak_lim_sup_str_list)
    # Plot vertical Lines
    plt.vlines([pd.to_datetime(x) for x in midday1_lim_inf_str_list], ymin=0, ymax=plt.gca().get_ylim()[1],
               linestyles='dashed', colors='orange',
               label='Production \n limits')
    plt.vlines([pd.to_datetime(x) for x in midday2_lim_sup_str_list], ymin=0, ymax=plt.gca().get_ylim()[1],
               linestyles='dashed', colors='orange')
    plt.vlines([pd.to_datetime(x) for x in peak_lim_inf_str_list], ymin=0, ymax=plt.gca().get_ylim()[1],
               linestyles='dashed', colors='red',
               label='Peak \n production')
    plt.vlines([pd.to_datetime(x) for x in peak_lim_sup_str_list], ymin=0, ymax=plt.gca().get_ylim()[1],
               linestyles='dashed', colors='red')
    # plt.legend(["aaa"])
    # Add text to lines
    # for date in midday1_lim_inf_str_list:
    #    text(pd.to_datetime(date), plt.gca().get_ylim()[1] * 5 / 6, "$t^{PV}_{lim\_inf}$", rotation=90, verticalalignment='center', size=7)
    # for date in midday2_lim_sup_str_list:
    #    text(pd.to_datetime(date), plt.gca().get_ylim()[1] * 5 / 6, "$t^{PV}_{lim\_sup}$", rotation=90, verticalalignment='center', size=7)
    # for date in peak_lim_inf_str_list:
    #    text(pd.to_datetime(date), plt.gca().get_ylim()[1] * 5 / 6, "a", rotation=90, verticalalignment='center', size=7)
    # for date in peak_lim_sup_str_list:
    #   text(pd.to_datetime(date), plt.gca().get_ylim()[1] * 5 / 6, "b", rotation=90, verticalalignment='center', size=7)


def diff(li1, li2):
    return list(set(li1) - set(li2)) + list(set(li2) - set(li1))


def check_dominance(x_neeg, y_neeg):
    if x_neeg > y_neeg:
        return True
    else:
        return False


def get_effort_data_for_comparison_by_scenario(scenario):
    effort_contribution_by_agent_type = {}
    effort_equal_contribution_by_agent_type = {}
    id_list_by_type = {}

    for agent_type in scenario.agent_types_in_simulation:
        id_list_by_type[agent_type] = ast.literal_eval(scenario.agents_results_df[agent_type + '_agents_id_list'][0])
        effort_contribution_by_agent_type[agent_type] = []
        effort_equal_contribution_by_agent_type[agent_type] = []

    for agent_type in scenario.agent_types_in_simulation:
        for agent_id in id_list_by_type[agent_type]:
            effort_contribution_by_agent_type[agent_type].append(
                scenario.performances['simulated']['effort_contribution_' + agent_type + '_' + str(agent_id)])
            effort_equal_contribution_by_agent_type[agent_type].append(
                scenario.performances['simulated']['effort_equal_contribution_' + agent_type + '_' + str(agent_id)])

    return effort_contribution_by_agent_type, effort_equal_contribution_by_agent_type


def get_data_for_pareto_plotting_by_scenario(scenario, effort_contribution_by_agent_type):
    neeg_by_agent_type = {}
    binary_effort_equal_share_by_agent_type = {}
    binary_effort_dif_share_by_agent_type = {}

    for agent_type in scenario.agent_types_in_simulation:
        neeg_by_agent_type[agent_type] = []
        binary_effort_equal_share_by_agent_type[agent_type] = []
        binary_effort_dif_share_by_agent_type[agent_type] = []

    # for agent_type in scenario.agent_types_in_simulation:
    #    for agent_id in scenario.agents_ids_by_type[agent_type]:
    #        neeg_by_agent_type[agent_type].append(
    #            scenario.performances['simulated']['individual_neeg_' + agent_type + '_' + str(agent_id)])
    #        binary_effort_equal_share_by_agent_type[agent_type].append(
    #            scenario.performances['simulated']['binary_effort_equal_share_' + agent_type + '_' + str(agent_id)])
    #        binary_effort_dif_share_by_agent_type[agent_type].append(
    #            scenario.performances['simulated']['binary_effort_dif_share_' + agent_type + '_' + str(agent_id)])

    # non_dominated_values = get_pareto_front(scenario, individual_neeg_list_by_agent_type, effort_list_by_agent_type, 10)
    non_dominated_values = get_pareto_front(scenario, 10)
    effort_binary_pareto_values = [x[0] for x in non_dominated_values]
    effort_general_pareto_values = [x[1] for x in non_dominated_values]
    return binary_effort_equal_share_by_agent_type, binary_effort_dif_share_by_agent_type, neeg_by_agent_type, effort_binary_pareto_values, effort_general_pareto_values


def get_pareto_front(pareto_length, effort_list, neeg_list):
    temp_tuple_list = []
    for i in range(len(effort_list)):
        if effort_list[i] >= 0:
            temp_tuple_list.append((effort_list[i], neeg_list[i]))

    # sorted_data = sorted(temp_tuple_list, key=lambda tup: tup[1])
    non_dominated_values = []
    i = 0
    temp_tuple_list = sorted(temp_tuple_list, key=lambda tup: tup[0])
    # print("SORTED LIST:",temp_tuple_list)

    while i < len(temp_tuple_list) and len(non_dominated_values) < pareto_length:
        # print("LEN",len(temp_tuple_list))
        current_element = temp_tuple_list[i]
        non_dominated_values.append(current_element)
        for j in range(i + 1, len(temp_tuple_list)):
            next_element = temp_tuple_list[j]
            flag = 0
            if check_dominance(current_element[1], next_element[1]):
                i = j
                break

    sorted_data = sorted(non_dominated_values, key=lambda tup: tup[1])
    effort_pareto_values = [x[0] for x in sorted_data]
    neeg_pareto_values = [x[1] for x in sorted_data]
    return effort_pareto_values, neeg_pareto_values


def plot_profile_comparison_specific_months_estimation_without_observable_days(plt, df_estimated, df_original,
                                                                               month_list, month_index,
                                                                               non_observable_days_list,
                                                                               axis_font_size):
    # (non_observable_days)
    # plt.plot(df_estimated.loc[(df_estimated.index.month == month_list[month_index]) & (df_estimated.index.day.isin(non_observable_days_list))]
    #         ['expected_community_consumption_total'] / 1000, color='blue', linestyle='dashed', label='$p^{expected}_{Load}$')
    # plt.plot(df_original.loc[(df_original.index.month == month_list[month_index]) & (df_original.index.day.isin(non_observable_days_list))]
    #         ['expected_community_consumption_total'] / 1000, color='blue', label='$p^{expected}_{Load}$')

    plt.plot(df_estimated.loc[(df_estimated.index.month == month_list[month_index])]
             ['expected_community_consumption_total'] / 1000, color='blue', label='$p^{expected}_{Load}$ - estimated')

    plt.plot(df_estimated.loc[(df_estimated.index.month == month_list[month_index])]
             ['simulated_community_consumption_total'] / 1000, color='blue', linestyle='dashed',
             label='$p^{simulated}_{Load}$ - estimated')

    plt.plot(df_original.loc[(df_original.index.month == month_list[month_index])]
             ['expected_community_consumption_total'] / 1000, color='green', label='$p^{expected}_{Load}$ - original')

    plt.plot(df_original.loc[(df_original.index.month == month_list[month_index])]
             ['simulated_community_consumption_total'] / 1000, color='green', linestyle='dashed',
             label='$p^{simulated}_{Load}$ - original')

    plt.plot(df_original.loc[(df_original.index.month == month_list[month_index])]
             ['simulated_community_production_total'] / 1000, color='red', label='$p^{expected}_{PV}$')
    # print(df_estimated.columns)
    observed_dates = df_estimated.loc[
        (df_estimated['aem_observing'] == True) & (df_estimated.index.hour == 0) & (
                df_estimated.index.month == month_list[month_index])]
    observed_dates_list = [pd.to_datetime(x) for x in observed_dates.index.strftime('%m/%d/%Y').tolist()]
    for i in range(len(observed_dates_list)):
        plt.gca().annotate('Obs', (mdates.date2num(observed_dates_list[i]), -1), xytext=(10, -8),
                           textcoords='offset points')
        # bbox=dict(facecolor='none', edgecolor='black', pad=0.1))
        # arrowprops=dict(arrowstyle='-|>'))

    plt.title("Power profile comparison - Month " + str(month_list[month_index]))
    plt.ylabel('Power [kW]', fontsize=axis_font_size)
    plt.xlabel('Time', fontsize=axis_font_size)
    plt.legend()


def results_folder_path_generation_simulation_with_estimation(id, config):
    [root_folder, folder_for_no_estimation] = results_folder_path_generation_simulation_no_estimation(
        config.Configuration.get_reference_scenario_for_estimation_system(), config)
    root_folder = os.path.join(root_folder, 'Estimation')
    # window_length_text = ""
    # if config.Configuration.get_estimation_observation_window_length_days(id) != 0:
    window_length_text = str(config.Configuration.get_estimation_observation_window_length_days(id)) + \
                         "_day_length"

    # if config.Configuration.get_estimation_observation_days_frequency(id) != 0:
    frequency_text = str(config.Configuration.get_estimation_observation_days_frequency(id)) + \
                     "_day_interval"

    results_folder_path = os.path.join(root_folder,
                                       config.Configuration.get_estimation_observation_days_decision(id),
                                       config.Configuration.get_estimation_observation_window_type(id),
                                       window_length_text,
                                       frequency_text,
                                       config.Configuration.get_estimation_method(id))
    return results_folder_path


def get_results_file_name_with_estimation(ref_id, est_id, config):
    file_path = get_results_file_name_no_estimation(ref_id, config)

    # if config.Configuration.get_estimation_observation_window_length_days(id) != 0:
    window_length_text = str(config.Configuration.get_estimation_observation_window_length_days(est_id)) + "_day_length"

    # if config.Configuration.get_estimation_observation_days_frequency(est_id) != 0:
    frequency_text = str(config.Configuration.get_estimation_observation_days_frequency(est_id)) + \
                     "_day_int"

    file_path = file_path + "_" + \
                config.Configuration.get_estimation_observation_days_decision(est_id) + "_" + \
                config.Configuration.get_estimation_observation_window_type(est_id) + "_" + \
                window_length_text + "_" + \
                frequency_text + "_" + \
                config.Configuration.get_estimation_method(est_id)

    return file_path


def set_observable_days_list(scenario_path):
    if "10DI" in scenario_path:
        return [2, 12, 22]
    if "7DI" in scenario_path:
        return [2, 9, 16, 23, 30]
    if "5DI" in scenario_path:
        return [2, 7, 12, 17, 22, 27]
    if "3DI" in scenario_path:
        return [2, 5, 8, 11, 14, 17, 20, 23, 26, 29]
    return []
