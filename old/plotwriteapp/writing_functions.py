import configparser
import os
import sys

import pandas as pd

#from simulation.Configurator import Configuration
#from simulation.SimulationResults import Results
#from simulation.SimulationResults import Results, SubwayComResults

sys.path.append("..")

#from plotwriteapp.helpers import *
from energy_community.helpers import indicators

plotting_app_config = configparser.ConfigParser()
plotting_app_config.read("plotting_app_config_file.ini")


# spec = importlib.util.spec_from_file_location("indicators", os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')), "indicators.py"))
# indicators = importlib.util.module_from_spec(spec)
# spec.loader.exec_module(indicators)


def set_performances_by_specific_period(df, cfg, performances_by_month):
    # Set day interval limits
    day_index_inf = cfg.Configuration.get_days_index_lim_inf()
    day_index_sup = cfg.Configuration.get_days_index_lim_sup()
    month_list = cfg.Configuration.get_months_to_plot()

    consumption = {'expected': [], 'simulated': []}
    consumption_by_month = {}
    production_by_month = {}

    for month in month_list:
        consumption_by_month[month] = consumption.copy()
        production_by_month[month] = df.loc[(df.index.month == month) & (df.index.day.isin(range(day_index_inf, day_index_sup)))][
            'simulated_community_production_total'].to_list().copy()

    for month in month_list:
        for case in ['expected', 'simulated']:
            consumption_by_month[month][case] = df.loc[(df.index.month == month) &
                                                       (df.index.day.isin(range(day_index_inf, day_index_sup)))][
                case + '_community_consumption_total'].to_list().copy()

            for indicator in ['sc', 'ss']:
                performances_by_month[month][case][indicator] = round(
                    indicators.evaluate_indicator_based_on_consumption_and_production(indicator,
                                                                                      consumption_by_month[
                                                                                                                                            month][case],
                                                                                      production_by_month[
                                                                                                                                            month]), 3)


def extract_results_from_one_simulation_with_estimation(cfg, id):
    """A function to extract simulation results from files

        :param scenario_path: scenario names that are used as paths for extracting the results
        :type scenario_path: str

        :returns: results for a simulation scenario
        :rtype: list"""

    month_list = cfg.Configuration.get_months_to_plot()

    scenario_path = results_folder_path_generation_simulation_with_estimation(id, cfg)

    reference_scenario_id = cfg.Configuration.get_reference_scenario_for_estimation_system()

    [original_scenario_path_root, original_scenario_path] = results_folder_path_generation_simulation_no_estimation(reference_scenario_id, cfg)

    # print(scenario_path)
    # print(original_scenario_path)

    # Generate folder paths
    community_results_full_path = os.path.join(scenario_path,
                                               cfg.Configuration.get_results_config('community_results_file_name'))
    agent_results_full_path = os.path.join(scenario_path,
                                           cfg.Configuration.get_results_config('agent_results_file_name'))

    # Read data from csv and compute the datetime index based dataframes
    df_original = pd.read_csv(os.path.join(original_scenario_path, cfg.Configuration.get_results_config('hourly_results_file_name')))
    df_original['simulated_time_hour'] = pd.to_datetime(df_original['simulated_time_hour'])
    df_original = df_original.set_index('simulated_time_hour')

    df_estimated = pd.read_csv(os.path.join(scenario_path, cfg.Configuration.get_results_config('hourly_results_file_name')))
    df_estimated['simulated_time_hour'] = pd.to_datetime(df_estimated['simulated_time_hour'])
    df_estimated = df_estimated.set_index('simulated_time_hour')

    original_consumption = df_original['expected_community_consumption_total'].to_list()
    estimated_consumption = df_estimated['expected_community_consumption_total'].to_list()

    # Evaluate performance metrics
    mean_absolute_err_val, rmse, r2score = evaluate_estimation_performances(original_consumption, estimated_consumption)

    original_consumption_month_1 = df_original.loc[(df_original.index.month == month_list[0])]['expected_community_consumption_total'].to_list()
    estimated_consumption_month_1 = df_estimated.loc[(df_estimated.index.month == month_list[0])]['expected_community_consumption_total'].to_list()
    # Evaluate performance metrics
    mean_absolute_err_month_1_val, rmse_month_1_val, r2score_month_1 = evaluate_estimation_performances(original_consumption_month_1,
                                                                                                        estimated_consumption_month_1)

    original_consumption_month_2 = df_original.loc[(df_original.index.month == month_list[1])]['expected_community_consumption_total'].to_list()
    estimated_consumption_month_2 = df_estimated.loc[(df_estimated.index.month == month_list[1])]['expected_community_consumption_total'].to_list()
    # Evaluate performance metrics
    mean_absolute_err_month_2_val, rmse_month_2_val, r2score_month_2 = evaluate_estimation_performances(original_consumption_month_2,
                                                                                                        estimated_consumption_month_2)

    # Extract SC and SS data from the community results file
    df = pd.read_csv(community_results_full_path)

    sc_predicted_period = round(df['expected_self_consumption_period_total'].to_list()[0], 2)
    ss_predicted_period = round(df['expected_self_sufficiency_period_total'].to_list()[0], 2)
    sc_recorded_period = round(df['simulated_self_consumption_period_total'].to_list()[0], 2)
    ss_recorded_period = round(df['simulated_self_sufficiency_period_total'].to_list()[0], 2)

    # Extract agent data from csv files
    df = pd.read_csv(agent_results_full_path)
    final_no_agents = df['agents_by_day_count'].to_list()[-1]

    initial_no_good_ag = df['good_agents_by_day_count'].to_list()[3]
    initial_no_normal_ag = df['normal_agents_by_day_count'].to_list()[3]
    initial_no_bad_ag = df['bad_agents_by_day_count'].to_list()[3]
    initial_no_ideal_ag = df['ideal_agents_by_day_count'].to_list()[3]

    final_no_good_ag = df['good_agents_by_day_count'].to_list()[-1]
    final_no_normal_ag = df['normal_agents_by_day_count'].to_list()[-1]
    final_no_bad_ag = df['bad_agents_by_day_count'].to_list()[-1]
    final_no_ideal_ag = df['ideal_agents_by_day_count'].to_list()[-1]

    dr_text = 'noDR'
    if 'wDR' in scenario_path:
        dr_text = 'wDR'

    data = [get_results_file_name_with_estimation(reference_scenario_id, id, cfg),
            final_no_agents,
            initial_no_good_ag,
            initial_no_normal_ag,
            initial_no_bad_ag,
            initial_no_ideal_ag,
            final_no_good_ag,
            final_no_normal_ag,
            final_no_bad_ag,
            final_no_ideal_ag,
            dr_text,
            sc_predicted_period,
            ss_predicted_period,
            sc_recorded_period,
            ss_recorded_period,
            mean_absolute_err_val,
            r2score,
            rmse,
            mean_absolute_err_month_1_val,
            r2score_month_1,
            rmse_month_1_val,
            mean_absolute_err_month_2_val,
            r2score_month_2,
            rmse_month_2_val]
    return data




def write_results_with_estimation_comparison(configuration):
    """A function to generate a .csv file from all scenarios set in the configuration file

            :param configuration: configuration module"""

    data_all_scenarios_list = []
    for id in configuration.Configuration.get_scenario_ids_with_estimation_to_plot():
        print("SCENARIO ID", id)
        data_for_one_scenario = extract_results_from_one_simulation_with_estimation(configuration, id)
        data_all_scenarios_list.append(data_for_one_scenario)

    # Create the pandas DataFrame
    df = pd.DataFrame(data_all_scenarios_list, columns=['Scenario',
                                                        'final_no_agents',
                                                        'initial_no_good_ag',
                                                        'initial_no_normal_ag',
                                                        'initial_no_bad_ag',
                                                        'initial_no_ideal_ag',
                                                        'final_no_good_ag',
                                                        'final_no_normal_ag',
                                                        'final_no_bad_ag',
                                                        'final_no_ideal_ag',
                                                        'DR',
                                                        'SC_predicted_period',
                                                        'SS_predicted_period',
                                                        'SC_recorded_period',
                                                        'SS_recorded_period',
                                                        'MAE with original',
                                                        'R2_SCORE with original',
                                                        'RMSE with original',
                                                        'MAE m1 with original',
                                                        'R2_SCORE m1 with original',
                                                        'RMSE m1 with original',
                                                        'MAE m2 with original',
                                                        'R2_SCORE m2 with original',
                                                        'RMSE m2 with original'])

    df.to_csv(os.path.join(configuration.Configuration.get_plotting_config('results_folder'),
                           configuration.Configuration.get_plotting_config('comparison_estimation_csv_file_name')))
