import os
import sys
from os import path

import matplotlib.pyplot as plt
from matplotlib import gridspec, colors
from matplotlib.ticker import FormatStrFormatter
#from energycommunity.subway.SubwayCommunity import SubwayCommunity
from energy_community.core.simulation.Period import Period
from energy_community.helpers.constants import FLEX_MAP, STRATEGY_MAP, AsignType, DSType, IndicatorsAll, IndicatorsInd, IndicatorsLP, Rec, Strategy
#from plotwriteapp.helpers import plot_actions_comparison_for_specific_agent_category, plot_period_vertical_lines, plot_period_vertical_lines_flexibility_periods, plot_power_profile_comparison_for_specific_agent_category, plot_power_profile_comparison_general, set_plot_properties_actions_comparison, set_plot_properties_power_profile_comparison_for_specific_agent_category, set_plot_properties_power_profile_comparison_general
#from simulation.Configurator import Configuration
from energy_community.helpers.indicators import evaluate_estimation_performances
#from plotwriteapp.helpers import *
import plotly.graph_objs as go
from plotly.subplots import make_subplots
from tzlocal import get_localzone 
#from simulation.SubwayComResults import SubwayComResults

#from simulation.SimulationResults import Results, SubwayComResults

plt.rcParams["timezone"] =  str(get_localzone())


def get_plot_results_folder(scenario):
    results_dir_path = Configuration.get_plotting_config('results_folder')
    if scenario.params.solar_location == 'Grenoble':
        results_dir_path = os.path.join(results_dir_path, 'strategy_comparison')
    #if scenario.sim_params.solar_data_location == 'Bucharest' and scenario.sim_params.subway_station_manager:
    #    results_dir_path = os.path.join(results_dir_path, 'subway_station_peak_shifting')
    if not path.exists(results_dir_path):
        os.mkdir(results_dir_path)
    return results_dir_path








def plot_pareto_2_by_2_neeg_rec_deviation(simulation_scenarios, save_figures_flag=False):
    if save_figures_flag:
        plt.rc('text', usetex=True)
        plt.style.use(['science'])
        plt.grid(True)

    plt.figure(figsize=(15, 5))

    original_colors = list(colors.BASE_COLORS.keys())
    original_markers = ["o", "s", "p", "*", "X", "D", "d"]
    pareto_colors = []
    pareto_markers = []
    no_alert_thresholds = []
    strategies = []
    for scenario in simulation_scenarios:
        if scenario.params.recommendation_strategy not in strategies:
            strategies.append(scenario.params.recommendation_strategy)
        if scenario.params.no_alert_threshold not in no_alert_thresholds:
            no_alert_thresholds.append(scenario.params.no_alert_threshold)
    # print(strategies)

    threshold_colors = []
    strategy_markers = []
    for i, strategy in enumerate(strategies):
        strategy_markers.append(original_markers[i])
    for i, threshold in enumerate(no_alert_thresholds):
        threshold_colors.append(original_colors[i])

    neeg_list, rec_count_list, dev_white_list, scenario_keys, legend_text = [], [], [], [], []
    for scenario in simulation_scenarios:
        current_no_alert_threshold = scenario.params.no_alert_threshold
        color_index = no_alert_thresholds.index(current_no_alert_threshold)
        pareto_colors.append(threshold_colors[color_index])

        current_strategy = scenario.params.recommendation_strategy
        marker_index = strategies.index(current_strategy)
        pareto_markers.append(strategy_markers[marker_index])

        neeg_list.append(scenario.norm_neeg)
        rec_count_list.append(scenario.norm_rec_count_groups)
        dev_white_list.append(scenario.norm_rec_count_deviation)
        scenario_keys.append(str(scenario.description_indicator_plots))
        legend_text.append(scenario.short_description)

    topsis_dataset = {'neeg': neeg_list, 'rec_count': rec_count_list, 'dev_white': dev_white_list}
    topsis_analysis(topsis_dataset, simulation_scenarios)

    topsis_dataset = {'neeg': neeg_list, 'rec_count': rec_count_list}
    topsis_analysis(topsis_dataset, simulation_scenarios)

    topsis_dataset = {'neeg': neeg_list, 'dev_white': dev_white_list}
    topsis_analysis(topsis_dataset, simulation_scenarios)

    dist_list = []
    plt.subplot(1, 2, 1)
    for i, (rec, neeg) in enumerate(zip(rec_count_list, neeg_list)):
        dist_list.append(rec + neeg)
        plt.scatter(rec, neeg, label=legend_text[i],
                    marker=pareto_markers[i], color=pareto_colors[i])

    effort_pareto_values, neeg_pareto_values = get_pareto_front(30, rec_count_list, neeg_list)
    plt.plot(effort_pareto_values, neeg_pareto_values)
    # plt.title(title_list[i - 1], fontsize=18)
    plt.xlabel('normalised recommendation count', fontsize=14)
    plt.ylabel('normalised NEEG', fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

    for distance in dist_list:
        if distance == min(dist_list):
            best_index = dist_list.index(distance)
            print("Scenario with min distance between rec count and neeg:",
                  simulation_scenarios[best_index].short_description)

    dist_list = []
    plt.subplot(1, 2, 2)
    for i, (dev, neeg) in enumerate(zip(dev_white_list, neeg_list)):
        dist_list.append(dev + neeg)
        plt.scatter(dev, neeg, label=legend_text[i],
                    marker=pareto_markers[i], color=pareto_colors[i])

    dev_pareto_values, neeg_pareto_values = get_pareto_front(30, dev_white_list, neeg_list)
    plt.plot(dev_pareto_values, neeg_pareto_values)
    # plt.title(title_list[i - 1], fontsize=18)
    plt.xlabel('normalised deviation from white color', fontsize=14)
    plt.ylabel('normalised NEEG ', fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

    ax = plt.gca()
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    for distance in dist_list:
        if distance == min(dist_list):
            best_index = dist_list.index(distance)
            print("Scenario with min distance between dev and neeg:",
                  simulation_scenarios[best_index].short_description)

    # plt.legend(loc='lower left', bbox_to_anchor=(-0.15, -0.42))
    f = plt.gcf()
    f.tight_layout()

    if save_figures_flag:
        folder_path = get_plot_results_folder(simulation_scenarios[0])
        path = os.path.join(folder_path, "Figure_indicators_pareto_2_by_2.pdf")
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()


def plot_pareto_neeg_recommendations_count(simulation_scenarios, save_figures_flag=False):
    if save_figures_flag:
        plt.rc('text', usetex=True)
        plt.style.use(['science'])
        plt.grid(True)

    plt.figure(figsize=(10, 7))
    # plt.figure()
    neeg_list = []
    rec_count_list = []
    scenario_keys = []
    legend_text = []
    strategies = []
    no_alert_thresholds = []
    pareto_colors = []
    pareto_markers = []
    for scenario in simulation_scenarios:
        neeg_list.append(scenario.performances['simulated']['neeg'])
        rec_count_list.append(scenario.norm_rec_count_deviation)
        scenario_keys.append(str(scenario.description_indicator_plots))
        legend_text.append(scenario.short_description)

    for scenario in simulation_scenarios:
        if scenario.params.recommendation_strategy not in strategies:
            strategies.append(scenario.params.recommendation_strategy)
        if scenario.params.no_alert_threshold not in no_alert_thresholds:
            no_alert_thresholds.append(scenario.params.no_alert_threshold)

    original_colors = list(colors.BASE_COLORS.keys())
    original_markers = ["o", "s", "p", "*", "X", "D", "d"]

    threshold_colors = []
    strategy_markers = []
    for i, strategy in enumerate(strategies):
        strategy_markers.append(original_markers[i])
    for i, threshold in enumerate(no_alert_thresholds):
        threshold_colors.append(original_colors[i])

    for scenario in simulation_scenarios:
        current_no_alert_threshold = scenario.params.no_alert_threshold
        color_index = no_alert_thresholds.index(current_no_alert_threshold)
        pareto_colors.append(threshold_colors[color_index])

        current_strategy = scenario.params.recommendation_strategy
        marker_index = strategies.index(current_strategy)
        pareto_markers.append(strategy_markers[marker_index])

    for i, (rec, neeg) in enumerate(zip(rec_count_list, neeg_list)):
        plt.scatter(rec, neeg, label=legend_text[i],
                    marker=pareto_markers[i], color=pareto_colors[i])

    effort_pareto_values, neeg_pareto_values = get_pareto_front(10, rec_count_list, neeg_list)
    plt.plot(effort_pareto_values, neeg_pareto_values)
    # plt.title(title_list[i - 1], fontsize=18)
    plt.ylabel('NEEG per day [kWh]', fontsize=14)
    plt.xlabel('Deviation from white signal [kWh]', fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax = plt.gca()
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    # plt.legend(loc='lower left', bbox_to_anchor=(-0.15, -0.42))
    f = plt.gcf()
    f.tight_layout()

    if save_figures_flag:
        folder_path = get_plot_results_folder(simulation_scenarios[0])
        path = os.path.join(folder_path, "Figure_indicators_pareto.pdf")
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()


def plot_indicators_comparison(simulation_scenarios, save_figures_flag=False):
    if save_figures_flag:
        plt.rc('text', usetex=True)
        plt.style.use(['science'])
        plt.grid(True)

    plt.figure(figsize=(25, 20))

    scenario_keys = []

    indicator_list = ['neeg', 'effort', 'renunciation', 'rec_count_simple', 'rec_count_deviation', 'rec_count_grouping']
    title_list = ['Normalised NEEG comparison', 'Effort comparison', 'Renunciation comparison',
                  'Recommendations count simple', 'Normalised deviation from white comparison ',
                  'Normalised recommendation count by groups']
    # x_label_list = ['Energy [kWh]', ]
    bar_colors = []

    indicator_data = {}

    for indicator in indicator_list:
        indicator_data[indicator] = []

    original_colors = list(colors.BASE_COLORS.keys())
    bar_colors = []
    old_no_alert_threshold = simulation_scenarios[0].params.no_alert_threshold
    color_index = 0
    for i, scenario in enumerate(simulation_scenarios):
        scenario_keys.append(str(scenario.short_description))
        current_no_alert_threshold = scenario.params.no_alert_threshold

        if current_no_alert_threshold != old_no_alert_threshold:
            color_index += 1
            old_no_alert_threshold = current_no_alert_threshold
        bar_colors.append(original_colors[color_index])

        indicator_data['neeg'].append(scenario.norm_neeg)
        indicator_data['effort'].append(scenario.performances['simulated']['effort'])
        indicator_data['renunciation'].append(scenario.performances['simulated']['renunciation'])
        indicator_data['rec_count_simple'].append(scenario.rec_count_simple)
        indicator_data['rec_count_deviation'].append(scenario.norm_rec_count_deviation)
        indicator_data['rec_count_grouping'].append(scenario.norm_rec_count_groups)

    for i in range(1, 7):
        plt.subplot(3, 2, i)
        plt.barh(scenario_keys, indicator_data[indicator_list[i - 1]],
                 color=bar_colors)
        plt.title(title_list[i - 1], fontsize=18)
        plt.ylabel('Scenario', fontsize=16)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)

        ax = plt.gca()
        for bars in ax.containers:
            ax.bar_label(bars, fmt='%.2f', label_type='center')

    f = plt.gcf()
    f.tight_layout()

    if save_figures_flag:
        folder_path = get_plot_results_folder(simulation_scenarios[0])
        path = os.path.join(folder_path, "Figure_indicators_comparison.pdf")
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()


def plot_effort_comparison(simulation_scenarios, save_figures_flag=False):
    if save_figures_flag:
        plt.rc('text', usetex=True)
        plt.style.use(['science'])
        plt.grid(True)

    plt.figure(figsize=(16, 5))
    plot_legend_effort = []
    scenario_description = {}
    markers = {}
    markers_effort_contribution = {}
    markers_effort_equal_contribution = {}
    possible_markers = ["o", "v", "^", "<", ">", "s", "p", "P", "*", "h", "x", "1", "2", "3", "4"]
    colors = {'good': 'blue', 'normal': 'orange', 'bad': 'red', 'ideal': 'green'}

    for scenario in simulation_scenarios:
        description = scenario.short_description
        scenario_description[scenario] = description
        markers[scenario] = possible_markers[0]
        markers_effort_contribution[scenario] = possible_markers[0]
        markers_effort_equal_contribution[scenario] = possible_markers[0]
        possible_markers[:] = possible_markers[1:]

    max_value_binary_effort = round(
        scenario.total_number_of_recommendations / len(scenario.hourly_results_df['hour'].to_list()), 2)

    for scenario in simulation_scenarios:
        bin_effort_dif_sh = {}
        bin_effort_eq_sh = {}
        gen_effort_dif_sh = {}
        gen_effort_eq_sh = {}
        indiv_neeg = {}

        for a_type in scenario.agent_types_in_simulation:
            bin_effort_dif_sh[a_type] = []
            bin_effort_eq_sh[a_type] = []
            gen_effort_dif_sh[a_type] = []
            gen_effort_eq_sh[a_type] = []
            indiv_neeg[a_type] = []
            plot_legend_effort.append(f"{a_type} agents in {scenario_description[scenario]} "
                                      f"Community NEEG/day = {scenario.performances['simulated']['neeg']} kWh")

        for a_type in scenario.agent_types_in_simulation:
            for a_id in scenario.agents_ids_by_type[a_type]:
                effort_3 = scenario.performances['simulated']['gen_effort_dif_contrib_' + a_type + '_' + str(a_id)]
                gen_effort_dif_sh[a_type].append(effort_3)
                effort_4 = scenario.performances['simulated']['gen_effort_eq_contrib_' + a_type + '_' + str(a_id)]
                gen_effort_eq_sh[a_type].append(effort_4)
                neeg_1 = scenario.performances['simulated']['neeg_' + a_type + '_' + str(a_id)]
                indiv_neeg[a_type].append(neeg_1)

        plot_data = [gen_effort_dif_sh, gen_effort_eq_sh, indiv_neeg]
        title_data = ["Involvement metric with proportional contribution", "Involvement metric with equal contribution"]
        y_axis_data = ['Energy [kWh]', 'Energy [kWh]']

        for k in range(2):
            plt.subplot(1, 2, k + 1)
            plt.grid(True)
            for a_type in scenario.agent_types_in_simulation:
                plt.plot(plot_data[k][a_type], marker=markers[scenario], color=colors[a_type])
                if k == 2:
                    plt.plot(indiv_neeg[a_type], marker=markers[scenario], color='blue')
            plt.title(title_data[k])
            plt.ylabel(y_axis_data[k])
            plt.xlabel('Member')
            plt.xticks(range(len(scenario.agents_id_list)), [
                fr"ID: {x} \\ $p^{{exp}}_{{load,day}}$ = {scenario.performances['expected'][f'exp_p_load_per_day_agent_{x}']}"
                for x in scenario.agents_id_list])
    plt.subplot(1, 2, 1)
    # plt.legend(plot_legend_effort, loc='center left', bbox_to_anchor=(1, 0.5))
    ax = plt.gca()
    ax.legend(plot_legend_effort, loc='lower left', bbox_to_anchor=(0, -0.25))

    f = plt.gcf()
    if save_figures_flag:
        folder_path = get_plot_results_folder(simulation_scenarios[0])
        path = os.path.join(folder_path, "Fig_effort_comparison.pdf")
        f.savefig(path, bbox_inches='tight')
    else:
        plt.show()


def plot_pareto_all_simulations(simulation_scenarios):
    plt.figure(figsize=(10, 10))
    plot_legend = []
    scenario_description = {}
    markers = {}
    possible_markers = ["o", "v", "^", "<", ">", "s", "p", "P", "*", "h", "x", "1", "2", "3", "4"]
    colors = {'good': 'blue', 'normal': 'orange', 'bad': 'red', 'ideal': 'green'}

    for scenario in simulation_scenarios:
        description = scenario.short_description
        scenario_description[scenario] = description
        markers[scenario] = possible_markers[0]
        possible_markers[:] = possible_markers[1:]

    # for scenario in simulation_scenarios:
    #    plt.plot(scenario_data_for_pareto_plots[scenario]['pareto_effort_general'],
    #             scenario_data_for_pareto_plots[scenario]['pareto_effort_binary'], label='_nolegend_')

    for scenario in simulation_scenarios:

        neeg = {}
        effort = {}
        for agent_type in scenario.agent_types_in_simulation:
            neeg[agent_type] = []
            effort[agent_type] = []
        for agent_type in scenario.agent_types_in_simulation:
            for agent_id in scenario.agents_ids_by_type[agent_type]:
                # neeg[agent_type].append(scenario.performances['simulated']['neeg']
                effort[agent_type].append(
                    scenario.performances['simulated']['gen_effort_dif_contrib_' + agent_type + '_' + str(agent_id)])

        for agent_type in scenario.agent_types_in_simulation:
            # plt.scatter(scenario_data_for_pareto_plots[scenario]['effort'][agent_type],
            # scenario_data_for_pareto_plots[scenario]['neeg'][agent_type], marker = markers[scenario],
            print(len(effort[agent_type]))
            print(len(scenario.performances['simulated']['neeg']))

            plt.scatter(effort[agent_type], scenario.performances['simulated']['neeg'], marker=markers[scenario],
                        color=colors[agent_type])
            for i, agent_id in enumerate(scenario.agents_ids_by_type[agent_type]):
                plt.annotate(str(agent_id), (effort[agent_type][i], scenario.performances['simulated']['neeg'][i]))
                # scenario_data_for_pareto_plots[scenario]['neeg'][agent_type][i]))
            plot_legend.append(agent_type + " agents in " + scenario_description[scenario])

    plt.title("Comparison between scenarios - private recommendation strategies")
    plt.ylabel('Effort binary')
    plt.xlabel('Effort general')

    # plt.legend(plot_legend, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.legend(plot_legend, loc='upper center', bbox_to_anchor=(0.5, -0.05))
    plt.grid()
    f = plt.gcf()
    if simulation_scenarios[0].params.approach_comparison_ok:
        path = os.path.join(Configuration.get_plotting_config('results_folder'),
                            "Fig_PARETO_all_approach_comparison.pdf")
    else:
        path = os.path.join(Configuration.get_plotting_config('results_folder'), "Fig_PARETO_all.pdf")
    f.savefig(path, bbox_inches='tight')


def plot_pareto_one_simulation(scenario, binary_effort_equal_share_list_by_agent_type,
                               individual_neeg_list_by_agent_type,
                               effort_pareto_values, neeg_pareto_values):
    plt.figure(figsize=(10, 10))

    for agent_type in scenario.agent_types_in_simulation:
        plt.scatter(binary_effort_equal_share_list_by_agent_type[agent_type],
                    [x for x in individual_neeg_list_by_agent_type[agent_type]])

    plt.plot(effort_pareto_values, neeg_pareto_values)
    scenario_desc = scenario.results_plot_file_name
    plt.title(scenario_desc.replace('_', ' '))
    plt.ylabel('Individual NEEG per day [kWh]')
    plt.xlabel('Effort')
    plt.legend(scenario.agent_types_in_simulation)
    f = plt.gcf()
    path = os.path.join(Configuration.get_plotting_config('results_folder'),
                        "Fig_PARETO_" + str(scenario.scenario_id) + ".pdf")
    f.savefig(path, bbox_inches='tight')






def plot_hourly_evolution_comparison_specific_months_and_days(scenario,
                                                              estimation_ok: bool = False,
                                                              plot_vertical_axes: bool = False,
                                                              vertical_axes_flex_periods: bool = False,
                                                              save_figures_flag: bool = False):
    if save_figures_flag:
        plt.rc('text', usetex=True)
        plt.style.use(['science'])
        plt.grid(True)

    plt.figure(figsize=(30, 20))
    # Set font sizes for figures
    axis_font_size = 18
    title_font_size = 18
    legend_font_size = 12
    if len(scenario.spec.plot_intervals) == 1:
        axis_font_size = 14
        title_font_size = 14
        legend_font_size = 12
        
    agent_types_in_sim = list(set(cm.type for cm in scenario.manager.cm_list))

    if len(agent_types_in_sim) > 1:
        specific_plots_grid_spaces = 2 * len(agent_types_in_sim)
    else:
        specific_plots_grid_spaces = 1
    grid_row_length = (1 + specific_plots_grid_spaces) * len(scenario.spec.plot_intervals)
    gs = gridspec.GridSpec(grid_row_length, 1)
    grid_index, interval_index = 0, 0

    if scenario.params.subway_flag:
        flexibility_period_1 = Period('flexibility_period_1', scenario.params.flexibility_period_1_start.hour,
                                      scenario.params.flexibility_period_1_end.hour)
        flexibility_period_2 = Period('flexibility_period_2', scenario.params.flexibility_period_2_start.hour,
                                      scenario.params.flexibility_period_2_end.hour)
        flexibility_periods = (flexibility_period_1, flexibility_period_2)
    else:
        flexibility_periods = None

    while interval_index < len(scenario.spec.plot_intervals):
        # --------------------------------------------------------------- #
        # PLOT POWER PROFILE COMPARISON GENERAL
        # --------------------------------------------------------------- #
        plt.subplot(gs[grid_index, 0])
        plot_power_profile_comparison_general(plt, scenario, interval_index)
        if vertical_axes_flex_periods:
            plot_period_vertical_lines_flexibility_periods(plt, interval_index, scenario, flexibility_periods)
        if plot_vertical_axes:
            plot_period_vertical_lines(plt, scenario, interval_index)
        set_plot_properties_power_profile_comparison_general(plt, scenario, interval_index, title_font_size,
                                                             axis_font_size, legend_font_size)
        # --------------------------------------------------------------- #
        grid_index += 1
        category_index = 0

        if len(scenario.agent_types_in_simulation) > 1:
            while category_index < len(scenario.agent_types_in_simulation):
                # --------------------------------------------------------------- #
                # PLOT POWER PROFILE COMPARISON FOR CATEGORY category_index
                # --------------------------------------------------------------- #
                agent_type = scenario.agent_types_in_simulation[category_index]
                # Set position in grid
                plt.subplot(gs[grid_index, 0])
                plot_power_profile_comparison_for_specific_agent_category(plt, scenario, interval_index, agent_type)
                plot_period_vertical_lines(plt, scenario, interval_index)
                set_plot_properties_power_profile_comparison_for_specific_agent_category(plt, scenario, interval_index,
                                                                                         title_font_size,
                                                                                         axis_font_size,
                                                                                         legend_font_size, agent_type)

                grid_index += 1
                # --------------------------------------------------------------- #
                # PLOT ACTIONS COMPARISON
                # --------------------------------------------------------------- #
                plt.subplot(gs[grid_index, 0])
                plot_actions_comparison_for_specific_agent_category(plt, scenario, interval_index, agent_type,
                                                                    flexibility_periods)
                set_plot_properties_actions_comparison(plt, agent_type, scenario, interval_index, title_font_size,
                                                       axis_font_size, legend_font_size)
                grid_index += 1
                # --------------------------------------------------------------- #

                category_index += 1
        else:
            agent_type = scenario.agent_types_in_simulation[0]
            plt.subplot(gs[grid_index, 0])
            plot_actions_comparison_for_specific_agent_category(plt, scenario, interval_index, agent_type,
                                                                flexibility_periods)
            set_plot_properties_actions_comparison(plt, agent_type, scenario, interval_index, title_font_size,
                                                   axis_font_size, legend_font_size)
            grid_index += 1

        interval_index += 1

    f = plt.gcf()

    # --------------------------------------------------------------- #

    f = plt.gcf()
    if len(scenario.agent_types_in_simulation) == 1:
        plt.subplots_adjust(wspace=0.1, hspace=0.6)
        f.set_size_inches(10, 5) if len(scenario.spec.plot_intervals) == 1 else f
        f.set_size_inches(15, 10) if len(scenario.spec.plot_intervals) == 2 else f
        f.set_size_inches(20, 15) if len(scenario.spec.plot_intervals) == 3 else f

        f.tight_layout()
    if len(scenario.agent_types_in_simulation) == 2:
        plt.subplots_adjust(wspace=0.1, hspace=1)
        f.set_size_inches(20, 25)
    if len(scenario.agent_types_in_simulation) == 3:
        f.set_size_inches(20, 30)
        plt.subplots_adjust(wspace=0.1, hspace=1)

    print("---------------------------------------------")
    print("Plotted monthly comparison - Scenario " + scenario.results_plot_file_name)
    print("---------------------------------------------")
    if estimation_ok:
        reference_scenario_id = cfg.Configuration.get_reference_scenario_for_estimation_system()
        path = os.path.join(cfg.Configuration.get_plotting_config('results_folder'),
                            "Figure_results_" + get_results_file_name_with_estimation(reference_scenario_id, id, cfg) + ".pdf")
        print(path)
        f.savefig(path, bbox_inches='tight')
    else:
        if save_figures_flag:
            folder_path = get_plot_results_folder(scenario)
            path = os.path.join(folder_path, f"Figure_results_{scenario.results_plot_file_name}.pdf")
            print(path)
            f.savefig(path, bbox_inches='tight')
        else:
            plt.show()


def plot_consumption_estimation_comparison(cfg):
    """A function to plot a comparison between the estimated consumption profile and the original consumption profile from the database.

    :param cfg: config module"""

    axis_font_size = 14
    month_list = cfg.Configuration.get_months_to_plot()

    reference_scenario_id = cfg.Configuration.get_reference_scenario_for_estimation_system()

    [original_scenario_path_root, original_scenario_path] = results_folder_path_generation_simulation_no_estimation(
        reference_scenario_id, cfg)
    # original_scenario_path = os.path.join(original_scenario_path, 'No_estimation')

    # Read the consumption data for the original reference simulation scenario
    df_original = pd.read_csv(os.path.join(original_scenario_path,
                                           cfg.Configuration.get_results_config('hourly_results_file_name')))
    df_original['simulated_time_hour'] = pd.to_datetime(df_original['simulated_time_hour'])
    df_original = df_original.set_index('simulated_time_hour')

    # print(df_original.columns)
    for simulation_id in cfg.Configuration.get_scenario_ids_with_estimation_to_plot():
        plt.clf()
        scenario_path = results_folder_path_generation_simulation_with_estimation(simulation_id, cfg)
        # Read estimate consumption data
        df_estimated = pd.read_csv(os.path.join(scenario_path,
                                                cfg.Configuration.get_results_config('hourly_results_file_name')))
        df_estimated['simulated_time_hour'] = pd.to_datetime(df_estimated['simulated_time_hour'])
        df_estimated = df_estimated.set_index('simulated_time_hour')

        observable_days_list = set_observable_days_list(scenario_path)
        non_observable_days_list = diff(list(range(1, 32)), observable_days_list)

        for i in range(len(month_list)):
            plt.subplot(2, 1, i + 1)
            plot_profile_comparison_specific_months_estimation_without_observable_days(plt, df_estimated, df_original,
                                                                                       month_list, i,
                                                                                       non_observable_days_list,
                                                                                       axis_font_size)

        original_consumption = df_original['expected_community_consumption_total'].to_list()
        estimated_consumption = df_estimated['expected_community_consumption_total'].to_list()

        # Evaluate performance metrics
        mean_absolute_err_val, mean_absolute_per_err_val, r2score = evaluate_estimation_performances(
            original_consumption, estimated_consumption)

        f = plt.gcf()
        f.suptitle("MAE ERROR whole year: " + str(round(mean_absolute_err_val, 2)) + "| R2 score whole year:" + str(
            round(r2score, 2)))

        f.set_size_inches(20, 10)
        print("---------------------------------------------")
        print("Plotted estimation comparison - " + scenario_path)
        print("---------------------------------------------")
        path = os.path.join(cfg.Configuration.get_plotting_config('results_folder'),
                            "EC_" + get_results_file_name_with_estimation(reference_scenario_id, simulation_id,
                                                                          cfg) + ".pdf")
        # print(path)
        f.savefig(path, bbox_inches='tight')
