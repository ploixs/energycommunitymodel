from typing import Dict, List
import pulp
from pulp import PULP_CBC_CMD







def optimisation_problem_coaching_private_no_adaptive_expectancy(p_com_load_period, p_pv_period, aem):
    recommendations_for_period = [0] * len(p_com_load_period)

    for hour in range(len(p_com_load_period)):
        if p_com_load_period[hour] > p_pv_period[hour] + aem.model.sim_params.no_alert_threshold:
            recommendations_for_period[hour] = -0.5
        elif p_pv_period[hour] > p_com_load_period[hour] + aem.model.sim_params.no_alert_threshold:
            recommendations_for_period[hour] = 0.5

    best_recommendation = most_frequent(recommendations_for_period)
    recommendations_for_period = [best_recommendation] * len(p_com_load_period)
    return recommendations_for_period


def optimisation_problem_coaching_private_with_adaptive_expectancy(p_com_load, p_pv, delta_power_rec_dict):
    best_match = None
    for recommendation in delta_power_rec_dict:
        neeg = 0
        for hour in range(len(p_com_load)):
            neeg += abs(p_com_load[hour] + delta_power_rec_dict[recommendation] - p_pv[hour])
        if best_match is None or neeg < best_match[1]:
            best_match = (recommendation, neeg)
    recommendations_for_period = [best_match[0]] * len(p_com_load)
    return recommendations_for_period


def optimisation_problem_general(p_load_dict: Dict[str, List[float]], p_pv : List[float], aem, print_info: bool = False) -> Dict[str, Dict[str, pulp.LpVariable]]:
    
    cm_types = list(set([cm.type for cm in aem.cm_list]))
    cms_by_type = {type: [cm for cm in aem.cm_list if type == cm.type] for type in cm_types }
    #print("Agent types in the community: ",cm_types)
    model = pulp.LpProblem("Problem_Optimal_Decision", pulp.LpMinimize)
    error_index = []
    action_index = []
    for i in range(len(cm_types)):
        action_index.extend([f'cat_{cm_types[i]}_same', f'cat_{cm_types[i]}_mod_inc', f'cat_{cm_types[i]}_mod_dec',
                             f'cat_{cm_types[i]}_strong_inc', f'cat_{cm_types[i]}_strong_dec'])

    # We generate the error index of length of the optimisation horizon
    for i in range(len(p_load_dict[str(cms_by_type[cm_types[0]][0].unique_id)])):
        error_index.append(str(i))

    error_dictionary = pulp.LpVariable.dicts("e", (i for i in error_index), cat='Continuous')
    action_dictionary = pulp.LpVariable.dicts("a", (i for i in action_index), cat='Binary')
    act_all_agents_dict = {}

    for agent in aem.cm_list:
        act_all_agents_dict[agent.unique_id] = action_dictionary

    model += pulp.lpSum([error_dictionary[i] for i in error_index])

    for hour in range(len(p_load_dict[str(cms_by_type[cm_types[0]][0].unique_id)])):
        temp = 0
        for agent in aem.cm_list:
            temp += (act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_same'] * (p_load_dict[str(agent.unique_id)][hour]) + 
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_inc'] * (agent.probability_to_change * (1 + agent.moderate_load_change) * p_load_dict[str(agent.unique_id)][hour] + agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour]) +
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_dec'] * (agent.probability_to_change * (1 - agent.moderate_load_change) * p_load_dict[str(agent.unique_id)][hour] + agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour]) +
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_inc'] * (agent.probability_to_change * (1 + agent.strong_load_change) * p_load_dict[str(agent.unique_id)][hour] + agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour]) +
                     act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_dec'] * (agent.probability_to_change * (1 - agent.strong_load_change) * p_load_dict[str(agent.unique_id)][hour] + agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour]))
            model += (act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_same'] + act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_inc'] +
                      act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_mod_dec'] + act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_inc'] +
                      act_all_agents_dict[agent.unique_id][f'cat_{agent.type}_strong_dec']) == 1

        model += (error_dictionary[str(hour)] + (temp - p_pv[hour])) >= 0
        model += (error_dictionary[str(hour)] - (temp - p_pv[hour])) >= 0

    model.solve(PULP_CBC_CMD(msg=0)) # type: ignore

    if print_info:
        print("------------------------------------")
        print(f'--- OPTIMISATION - CASE with {len(cm_types)} agent types -----')
        for agent_type in cm_types:
            print(f"{agent_type} agents actions:")
            print(f"Action 0 (same cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_same'].varValue}")
            print(f"Action 1 (+ moderate cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_mod_inc'].varValue}")
            print(f"Action 2 (- moderate cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_mod_dec'].varValue}")
            print(f"Action 3 (+ strong cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_strong_inc'].varValue}")
            print(f"Action 4 (- strong cons): {act_all_agents_dict[cms_by_type[agent_type][0].unique_id][f'cat_{agent_type}_strong_dec'].varValue}")
        print("------------------------------------")
    return act_all_agents_dict



"""

def optimisation_problem_general(p_load_dict: Dict[int, List[float]], p_pv, aem):
    
    print("Agent types in the community: ", aem.cm_types_in_community)
    model = pulp.LpProblem("Problem_Optimal_Decision", pulp.LpMinimize)
    error_index = []
    action_index = []
    for i in range(len(aem.cm_types_in_community)):
        action_index.extend(['cat_' + str(aem.cm_types_in_community[i]) + '_same',
                            'cat_' + str(aem.cm_types_in_community[i]) + '_mod_inc',
                            'cat_' + str(aem.cm_types_in_community[i]) + '_mod_dec',
                            'cat_' + str(aem.cm_types_in_community[i]) + '_strong_inc',
                            'cat_' + str(aem.cm_types_in_community[i]) + '_strong_dec'])

    # We generate the error index of length of the optimisation horizon
    for i in range(len(p_load_dict[str(aem.agents_by_type[aem.cm_types_in_community[0]][0].unique_id)])):
        error_index.append(str(i))

    error_dictionary = pulp.LpVariable.dicts("e", (i for i in error_index), cat='Continuous')
    action_dictionary = pulp.LpVariable.dicts("a", (i for i in action_index), cat='Binary')
    act_all_agents_dict = {}

    for agent in aem.agents_list:
        act_all_agents_dict[agent.unique_id] = action_dictionary

    model += pulp.lpSum([error_dictionary[i] for i in error_index])

    for hour in range(len(p_load_dict[str(aem.agents_by_type[aem.cm_types_in_community[0]][0].unique_id)])):
        temp = 0
        for agent in aem.agents_list:
            temp += (act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_same'] * (
            p_load_dict[str(agent.unique_id)][hour])
                    + act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_mod_inc'] *
                    (agent.probability_to_change * (1 + aem.moderate_load_change) * p_load_dict[str(agent.unique_id)][
                        hour] +
                    agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour])
                    + act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_mod_dec'] *
                    (agent.probability_to_change * (1 - aem.moderate_load_change) * p_load_dict[str(agent.unique_id)][
                        hour] +
                    agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour])
                    + act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_strong_inc'] *
                    (agent.probability_to_change * (1 + aem.strong_load_change) * p_load_dict[str(agent.unique_id)][hour] +
                    agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour])
                    + act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_strong_dec'] *
                    (agent.probability_to_change * (1 - aem.strong_load_change) * p_load_dict[str(agent.unique_id)][hour] +
                    agent.probability_to_consume_same * p_load_dict[str(agent.unique_id)][hour]))
            model += (act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_same'] +
                    act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_mod_inc'] +
                    act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_mod_dec'] +
                    act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_strong_inc'] +
                    act_all_agents_dict[agent.unique_id]['cat_' + agent.type + '_strong_dec']) == 1

        model += (error_dictionary[str(hour)] + (temp - p_pv[hour])) >= 0
        model += (error_dictionary[str(hour)] - (temp - p_pv[hour])) >= 0

    model.solve(PULP_CBC_CMD(msg=0))

    print("------------------------------------")
    print('--- OPTIMISATION - CASE with ' + str(len(aem.cm_types_in_community)) + ' agent types -----')
    for agent_type in aem.cm_types_in_community:
        print(agent_type + " agents actions:")
        print("Action 0 (same cons):",
            act_all_agents_dict[aem.agents_by_type[agent_type][0].unique_id]['cat_' + agent_type + '_same'].varValue)
        print("Action 1 (+ moderate cons):",
            act_all_agents_dict[aem.agents_by_type[agent_type][0].unique_id]['cat_' + agent_type + '_mod_inc'].varValue)
        print("Action 2 (- moderate cons):",
            act_all_agents_dict[aem.agents_by_type[agent_type][0].unique_id]['cat_' + agent_type + '_mod_dec'].varValue)
        print("Action 3 (+ strong cons):",
            act_all_agents_dict[aem.agents_by_type[agent_type][0].unique_id]['cat_' + agent_type + '_strong_inc'].varValue)
        print("Action 4 (- strong cons):",
            act_all_agents_dict[aem.agents_by_type[agent_type][0].unique_id]['cat_' + agent_type + '_strong_dec'].varValue)
    print("------------------------------------")
    return act_all_agents_dict

"""