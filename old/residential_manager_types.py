
from energy_community.ResidentialCommunityManager import ResidentialCommunityManager
from energy_community.core.EnergyCommunityModel import EnergyCommunityModel


class OneTurnCoachingClustersFixedExpectancy(ResidentialCommunityManager):
    """one stage, recommendation to clusters, coaching approach, fixed and known expectancy based on clusters """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.set_optimal_recommendations_current_day()
            self.communicate_actions_to_agents()

    def second_stage(self):
        if self.model.current_hour_index in self.model.end_of_day_hour_index_list and self.cm_list:
            # self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.model.current_hour_index == self.model.start_of_day_hour_index_list[
            -1]:  # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()


class OneTurnInformativeEveryoneBasicExpectancy(ResidentialCommunityManager):
    """one stage, same recommendation to everyone, informative approach, no expectancy """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.set_basic_recommendations_current_day()
            self.communicate_actions_to_agents()

    def second_stage(self):
        if self.model.current_hour_index in self.model.end_of_day_hour_index_list and self.cm_list:
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.model.current_hour_index == self.model.start_of_day_hour_index_list[
            -1]:  # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()


class OneTurnInformativeEveryoneAdaptiveExpectancy(ResidentialCommunityManager):
    """one stage, same recommendation to everyone, informative approach, adaptive expectancy """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.set_delta_power_by_recommendation_from_recorded_data()
            self.set_recommendations_current_day_informative_approach()
            self.communicate_actions_to_agents()

    def second_stage(self):
        if self.model.current_hour_index in self.model.end_of_day_hour_index_list and self.cm_list:
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.model.current_hour_index == self.model.start_of_day_hour_index_list[
            -1]:  # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()


class TwoTurnsInformativeEveryoneAdaptiveExpectancy(ResidentialCommunityManager):
    """two stage, same recommendation to everyone, reactive approach, adaptive expectancy """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.set_delta_power_by_recommendation_from_recorded_data()
            self.set_recommendations_current_day_first_stage_reactive_informative_approach()
            self.communicate_actions_to_agents()

    def second_stage(self):
        hour = self.model.current_hour_index_day_relative
        if self.cm_list and self.recommendations_current_day_list[hour] != 0:
            production = self.virtual_pv.exp_community_production_current_day_list[hour]
            sim_community_consumption = self.get_simulated_community_consumption_current_hour()
            if self.recommendations_current_day_list[
                hour] == -0.5 and production + self.model.sim_params.no_alert_threshold < sim_community_consumption:
                self.recommendations_current_day_list[hour] = -1
            elif self.recommendations_current_day_list[
                hour] == 0.5 and sim_community_consumption + self.model.sim_params.no_alert_threshold < production:
                self.recommendations_current_day_list[hour] = 1
            elif self.recommendations_current_day_list[
                hour] == -1 and production + self.model.sim_params.no_alert_threshold < sim_community_consumption:
                self.recommendations_current_day_list[hour] = -2
            elif self.recommendations_current_day_list[
                hour] == 1 and sim_community_consumption + self.model.sim_params.no_alert_threshold < production:
                self.recommendations_current_day_list[hour] = 2
            self.communicate_actions_to_agents()

    def third_stage(self):
        if self.model.current_hour_index in self.model.end_of_day_hour_index_list and self.cm_list:
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.model.current_hour_index == self.model.start_of_day_hour_index_list[
            -1]:  # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()


class OneTurnCoachingEveryoneAdaptiveExpectancy(ResidentialCommunityManager):
    """one stage, same recommendation to everyone, coaching approach, adaptive expectancy """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.set_delta_power_by_recommendation_from_recorded_data()
            self.set_coaching_private_recommendations_current_day()
            self.communicate_actions_to_agents()

    def second_stage(self):
        if self.model.current_hour_index in self.model.end_of_day_hour_index_list and self.cm_list:
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.model.current_hour_index == self.model.start_of_day_hour_index_list[
            -1]:  # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()


class OneTurnCoachingEveryoneBasicExpectancy(ResidentialCommunityManager):
    """one stage, same recommendation to everyone, coaching approach, no expectancy """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.set_coaching_private_recommendations_current_day()
            self.communicate_actions_to_agents()

    def second_stage(self):
        if self.model.current_hour_index in self.model.end_of_day_hour_index_list and self.cm_list:
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.model.current_hour_index == self.model.start_of_day_hour_index_list[
            -1]:  # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()


class TwoTurnsInformativeEveryoneBasicExpectancy(ResidentialCommunityManager):
    """two stage, same recommendation to everyone, reactive approach, no expectancy """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.set_recommendations_current_day_first_stage_reactive_approach_no_expectancy()
            self.communicate_actions_to_agents()

    def second_stage(self):
        hour = self.model.current_hour_index_day_relative
        if self.cm_list and self.recommendations_current_day_list[hour] != 0:
            production = self.virtual_pv.exp_community_production_current_day_list[hour]
            sim_community_consumption = self.get_simulated_community_consumption_current_hour()
            if self.recommendations_current_day_list[
                hour] == -1 and production + self.com_model.sim_params.no_alert_threshold < sim_community_consumption:
                self.recommendations_current_day_list[hour] = -2
            elif self.recommendations_current_day_list[
                hour] == 1 and sim_community_consumption + self.com_model.sim_params.no_alert_threshold < production:
                self.recommendations_current_day_list[hour] = 2
            self.communicate_actions_to_agents()

    def third_stage(self):
        if self.com_model.current_hour_index in self.com_model.end_of_day_hour_indexes and self.cm_list:
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.com_model.current_hour_index == self.com_model.start_of_day_hour_indexes[
            -1]:  # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()


class OneTurnDummyBasicExpectancy(ResidentialCommunityManager):
    """one stage, same recommendation to everyone, DUMMY recommendation, no expectancy """

    def __init__(self, unique_id, model: EnergyCommunityModel):
        super().__init__(unique_id, model)

    def first_stage(self):
        if self.com_model.current_hour_index in self.com_model.start_of_day_hour_indexes and self.cm_list:
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            self.initialize_residential_community_manager()
            self.set_recommendations_current_day_dummy_approach()
            self.communicate_actions_to_agents()

    def second_stage(self):
        if self.com_model.current_hour_index in self.com_model.end_of_day_hour_indexes and self.cm_list:
            
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.com_model.current_hour_index == self.com_model.start_of_day_hour_indexes[-1]:  
            # in the last day of the simulation, write data to files
            for member in self.cm_list:
                member.update_estimated_consumption_total()
            self.write_residential_community_with_coaching_manager_data_to_files()


class OneTurnDynamicEveryoneBasicExpectancy(ResidentialCommunityManager):
    """one stage, dynamic recommendation to everyone, basic expectancy"""

    def __init__(self, unique_id, model, rec_ratios=(.05, .2, .5, .2, .05)):
        super().__init__(unique_id, model, rec_ratios)

    def first_stage(self):
        if self.model.current_hour_index in self.model.start_of_day_hour_index_list and self.cm_list:
            self.initialize_residential_community_manager()
            for agent in self.cm_list:
                agent.initialize_agent_start_day()
            # self.set_delta_power_by_recommendation_from_recorded_data()
            color_index_delta_powers = self.get_color_index_delta_powers()
            self.set_recommendations_current_day_dynamic_approach(color_index_delta_powers)
            self.communicate_actions_to_agents()

    def second_stage(self):
        if self.model.current_hour_index in self.model.end_of_day_hour_index_list and self.cm_list:
            self.set_agents_requested_consumption_levels()
            self.record_residential_community_with_coaching_manager_data_from_agents_current_day()
        if self.model.current_hour_index == self.model.start_of_day_hour_index_list[-1]:
            # in the last day of the simulation, write data to files
            self.write_residential_community_with_coaching_manager_data_to_files()





