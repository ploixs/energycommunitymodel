import os
import random
from energy_community.core.simulation.BaseParameters import BaseParameters
from energy_community.helpers.constants import ExpectancyDev, MType, PossibleActions, PossibleStates, PrivacyLevel, Stages, Strategy, TargetOfRec
from energy_community.core.energy.get_power_data import get_consumption_for_a_house_from_csv
from energy_community.residential.core.residential_parameters import ResidentialParameters
from energy_community.residential.core import community
from energy_community.residential.core.profiles import OneTurnResidentialMarkovCommunityMember, OneTurnResidentialManager, TwoTurnsResidentialMarkovCommunityMember, TwoTurnsResidentialManager


if __name__ == "__main__":
    
    #simulation_scenarios_to_run = range(1,10)
    simulation_scenarios_to_run = [9]
    
    plot_in_browser = False # plot results in browser for each simulation using plotly
    print_flag = True # print messages during simulation
    
    HOUSE_IDS = [2000900, 2000903, 2000908, 2000910, 2000911]
    intention_range = [(MType.BAD, 0.1), (MType.BAD, 0.2), (MType.BAD, 0.3), (MType.NORMAL,0.4), (MType.NORMAL, 0.5), (MType.NORMAL,0.6), (MType.GOOD,0.7), (MType.GOOD,0.8), (MType.GOOD,0.9)]
    
    for simulation_id in simulation_scenarios_to_run:
        
        results_folder_path = os.path.join("energycommunity", "residential", "results_hmm", f"Scenario_{simulation_id}")
        if not os.path.exists(results_folder_path):
            os.makedirs(results_folder_path)
            
        sim_params = ResidentialParameters(os.path.join("energy_community_models", "residential", "scenarios_hmm.csv"), simulation_id, print_flag) # define parameters
        if sim_params.stages is None:
            raise ValueError("Stages not set from scenario file!")
        model = community(sim_params, results_folder_path, sim_params.stages)
        
        manager = TwoTurnsResidentialManager(99999, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.INFORMATIVE, PrivacyLevel.PRIVATE, 0) 
        
        if manager:
            model.community_manager = manager # assign the manager to the community
        
        members = []
        
        for house_id in HOUSE_IDS:
            df = get_consumption_for_a_house_from_csv(house_id, sim_params.starting_date, sim_params.ending_date)
            new_cm = TwoTurnsResidentialMarkovCommunityMember(house_id % 1000, intention_range[simulation_id - 1][0], 
                                                        model, df, explicit_emissions= {(PossibleStates.ATTENTIVE, PossibleActions.FOLLOW): intention_range[simulation_id - 1][1], 
                                                                                        (PossibleStates.ATTENTIVE, PossibleActions.NOT_FOLLOW): 1 - intention_range[simulation_id - 1][1] }) 
            
            members.append(new_cm)

        model.add_new_community_members(members) # assigne the members to the community
    
        for i in range(model.sim_params.simulation_steps):
            model.step() # run the simulation
        
        if plot_in_browser:
            model.show_results_plot_in_browser()


        
        