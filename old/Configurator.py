import configparser


'''
class Configuration:
    config = configparser.ConfigParser()

    def __init__(self, file_path):
        self.config.read(file_path)
    #config.read(r"/Users/mirceastefansimoiu/Documents/P6_Energy_Community_Mac_Local/mac_config_file.ini")
    #config.read(r"/Users/mirceastefansimoiu/Documents/P6_Energy_Community_Mac_Local/sohoma22_config_file.ini")
    #config.read(r"/Users/mirceastefansimoiu/Library/CloudStorage/OneDrive-UniversitateaPolitehnicaBucuresti/_Cercetare/_Data_and_results/energy_community_input_data/_configuration_files/willigness_model_and_strategy_comparison/config.ini")
    #config.read(r"/Users/mirceastefansimoiu/Library/CloudStorage/OneDrive-UniversitateaPolitehnicaBucuresti/_Cercetare/_Data_and_results/energy_community_input_data/_configuration_files/subway_station_peak_shifting/config.ini")

    @staticmethod
    def get_first_time_usage_bool(parameter):
        return Configuration.config.getboolean('FIRST_TIME_USAGE', parameter)

    @staticmethod
    def get_plot_bool_parameter(parameter):
        return Configuration.config.getboolean('PLOTTING_AND_WRITING_OPTIONS', parameter)


    @staticmethod
    def get_simulation_description_file():
        return Configuration.config['SCENARIOS']['scenario_description_file_path']

    @staticmethod
    def get_simulations_no_estimation_results_folder():
        return Configuration.config['RESULTS_DATA']['no_estimation_results_folder']

    # ------------------- DATA -----------------------------#
    @staticmethod
    def get_consumption_data_config(parameter):
        return Configuration.config['CONSUMPTION'][parameter]

    @staticmethod
    def get_weather_data_config_general_parameter(parameter):
        return Configuration.config['WEATHER'][parameter]

    # ------------------- RESULTS AND PLOTS -----------------------------#
    @staticmethod
    def get_results_config(parameter):
        return Configuration.config['RESULTS_DATA'][parameter]

    @staticmethod
    def get_plotting_config_bool(parameter):
        return Configuration.config.getboolean('PLOTTING_AND_WRITING_OPTIONS', parameter)

    @staticmethod
    def get_plotting_config(parameter):
        return Configuration.config['PLOTTING_AND_WRITING_OPTIONS'][parameter]

    @staticmethod
    def get_reference_scenario_for_estimation_system():
        id = Configuration.config.getint('PLOTTING_AND_WRITING_OPTIONS', 'id_of_reference_scenario_for_estimation_system')
        return id

    @staticmethod
    def get_months_to_plot():
        month_list = Configuration.config.get('PLOTTING_AND_WRITING_OPTIONS', 'months_to_plot').split(',')
        month_list = [int(x) for x in month_list]
        return month_list

    @staticmethod
    def get_days_index_lim_inf():
        day = Configuration.config.getint('PLOTTING_AND_WRITING_OPTIONS', 'days_index_lim_inf')
        return day

    @staticmethod
    def get_days_index_lim_sup():
        day = Configuration.config.getint('PLOTTING_AND_WRITING_OPTIONS', 'days_index_lim_sup')
        return day

    @staticmethod
    def get_scenario_ids_without_estimation_to_plot():
        ids = Configuration.config.get('PLOTTING_AND_WRITING_OPTIONS', 'simulation_ids_without_estimation_to_plot').split(',')
        ids = [int(x) for x in ids]
        return ids

    @staticmethod
    def get_scenario_ids_with_estimation_to_plot():
        ids = Configuration.config.get('PLOTTING_AND_WRITING_OPTIONS', 'simulation_ids_with_estimation_to_plot').split(',')
        ids = [int(x) for x in ids]
        return ids

    # ------------------- ESTIMATION SYSTEM -----------------------------#

    @staticmethod
    def get_estimation_number_of_simulation_scenarios():
        return Configuration.config.getint('ESTIMATION_SYSTEM_GENERAL_PARAMETERS', 'number_of_simulation_scenarios')

    @staticmethod
    def get_estimation_parameters_set(parameter_id):
        return Configuration.config.getint('SIMULATION_PARAMETERS_SET_' + str(parameter_id), 'estimation_parameters_set_to_use')

    @staticmethod
    def get_estimation_observation_window_type(parameter_set_id):
        return Configuration.config['ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id)]['observation_window_type']

    @staticmethod
    def get_estimation_observation_window_length_days(parameter_set_id):
        return Configuration.config.getint('ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id), 'observation_window_length_days')

    @staticmethod
    def get_estimation_observation_window_length_weeks(parameter_set_id):
        return Configuration.config.getint('ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id), 'observation_window_length_weeks')

    @staticmethod
    def get_estimation_method(parameter_set_id):
        return Configuration.config['ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id)]['estimation_method']

    @staticmethod
    def get_estimation_observation_days_frequency(parameter_set_id):
        return Configuration.config.getint('ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id), 'observation_days_frequency')

    @staticmethod
    def get_estimation_observation_days_decision(parameter_set_id):
        return Configuration.config['ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id)]['observation_days_decision']

    @staticmethod
    def get_estimation_forgetting_factor_decision(parameter_set_id):
        return Configuration.config.getboolean('ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id), 'use_forgetting_factor')

    @staticmethod
    def get_estimation_forgetting_factor(parameter_set_id):
        return Configuration.config.getfloat('ESTIMATION_PARAMETERS_SET_' + str(parameter_set_id), 'forgetting_factor')

    @staticmethod
    def get_estimation_results_folder():
        return Configuration.config['RESULTS_DATA']['estimation_results_folder']

'''