from typing import Dict, List

from energy_community.core.CommunityManager import CommunityManager
from energy_community.core.EnergyCommunityModel import EnergyCommunityModel
from energy_community.core.simulation.Period import *
import itertools
import pandas as pd
from energy_community.helpers.optimisation_problems import optimisation_problem_general, \
    optimisation_problem_coaching_private_with_adaptive_expectancy, \
    optimisation_problem_coaching_private_no_adaptive_expectancy


class ResidentialCommunityManager(CommunityManager):
    """A community manager in a typical residential community. """

    def __init__(self, unique_id, model: EnergyCommunityModel, rec_ratios=None):
        super().__init__(unique_id, model)

        # Changes in consumption for agents
        self.moderate_load_change = self.com_model.sim_params.rec_mod_change
        self.strong_load_change = self.com_model.sim_params.rec_strong_change

        self.delta_power_by_recommendation = {-1: -600, -0.5: -300, 0: 0, 0.5: 300, 1: 600}
        self.delta_power_by_recommendation_total = {-1: [], -0.5: [], 0: [], 0.5: [], 1: []}

        self.rec_ratios = rec_ratios
        self.possible_recs = (-1, -0.5, 0, 0.5, 1)

        self.peak_production_current_day = False

        # Thresholds for defining periods during a day
        self.periods_current_day = {}
        self.recommendations_current_day_by_period = {}
        self.recommendations_current_day_list = []
        self.sim_periods_total = {}
        self.aem_observing_list = []

        self.flexibility_period = Period('flexibility_period', 7, 23)
        self.period_names_list = ['morning', 'midday1', 'peak_prod', 'midday2', 'evening']
        for period_name in self.period_names_list:
            self.sim_periods_total[period_name] = []

    def set_delta_power_by_recommendation_from_recorded_data(self):
        """ Sets the average variation in power by recommendation based on historic data."""

        tail_ratio = 0.1
        recommendations_until_now = self.cm_list[0].actions_to_be_done_total_list
        power_variations_based_on_recommendations = {}

        for i in range(len(self.est_community_consumption_total)):
            if recommendations_until_now[i] not in power_variations_based_on_recommendations:
                power_variations_based_on_recommendations[recommendations_until_now[i]] = list()
            power_variations_based_on_recommendations[recommendations_until_now[i]].append(
                self.sim_community_consumption_total[i] - self.est_community_consumption_total[i])
        #print('Recommendation variations')
        #for key, value in power_variations_based_on_recommendations.items():
        #    print(key)
        thresholds_by_recommendation = {recommendation: [] for recommendation in
                                        power_variations_based_on_recommendations}
        average_values_by_recommendation = {recommendation: [] for recommendation in
                                            power_variations_based_on_recommendations}

        for recommendation in power_variations_based_on_recommendations:
            power_variations_based_on_recommendations[recommendation].sort()
            number_of_hours_per_recommendation = len(power_variations_based_on_recommendations[recommendation])
            number_of_hours_per_recommendation_tail = int(number_of_hours_per_recommendation * tail_ratio)
            if number_of_hours_per_recommendation_tail == 0:
                power_left_threshold = None
                power_left_avg_value = None
                power_right_threshold = None
                power_right_avg_value = None
                if number_of_hours_per_recommendation == 0:
                    power_central_avg_value = None
                else:
                    power_central_avg_value = sum(
                        power_variations_based_on_recommendations[recommendation]) / number_of_hours_per_recommendation
            else:
                recommendation_left_index = number_of_hours_per_recommendation_tail
                recommendation_right_index = number_of_hours_per_recommendation - number_of_hours_per_recommendation_tail - 1

                power_left_threshold = power_variations_based_on_recommendations[recommendation][
                    recommendation_left_index]
                power_right_threshold = power_variations_based_on_recommendations[recommendation][
                    recommendation_right_index]
                power_left_avg_value = sum([power_variations_based_on_recommendations[recommendation][i] for i in
                                            range(0, recommendation_left_index)]) / \
                                       number_of_hours_per_recommendation_tail
                power_right_avg_value = sum([power_variations_based_on_recommendations[recommendation][i] for i in
                                             range(recommendation_right_index, number_of_hours_per_recommendation)]) / \
                                        number_of_hours_per_recommendation_tail
                power_central_avg_value = sum(
                    [power_variations_based_on_recommendations[recommendation][i] for i in
                     range(recommendation_left_index, recommendation_right_index)]) / \
                                          (
                                                  number_of_hours_per_recommendation - 2 * number_of_hours_per_recommendation_tail)

            thresholds_by_recommendation[recommendation] = (power_left_threshold, power_right_threshold)
            average_values_by_recommendation[recommendation] = (
                power_left_avg_value, power_central_avg_value, power_right_avg_value)

        for recommendation in average_values_by_recommendation:
            if average_values_by_recommendation[recommendation][1]:
                self.delta_power_by_recommendation[recommendation] = average_values_by_recommendation[recommendation][1]

    def get_peak_production_period(self) -> List[int]:
        """Returns the starting time and ending time for the peak production period of the current day

         :returns: Two datetime indexes of the current day PV production dataframe (starting timestamp and ending timestamp)
         :rtype: list"""
        peak_production_indexes_list = []
        peak_production_int_index : int = 0
        # Get production peak in the day
        production = self.virtual_pv.est_com_prod_day.data
        peak_production_value = max(production)
        if peak_production_value:
            # Get index value of production peak
            for i in range(len(production)):
                if production[i] == peak_production_value:
                    peak_production_int_index = i

            # Check if production from previous sample is bigger then 80% of peak value
            if production[peak_production_int_index - 1] > 0.8 * peak_production_value:
                peak_production_indexes_list.append(peak_production_int_index - 1)

            peak_production_indexes_list.append(peak_production_int_index)
            # Check if production from next sample is bigger then 80% of peak value
            if production[peak_production_int_index + 1] > 0.8 * peak_production_value:
                peak_production_indexes_list.append(peak_production_int_index + 1)

        return peak_production_indexes_list

    def set_periods_for_current_day(self, non_zero_production_indexes_list: List[int], peak_production_indexes_list: List[int]):
        """Sets the periods in the day according to the peak production indexes."""

        # PERIOD SETTING
        # We make the assumption here that the relative index for the day is equivalent to the hour
        # Because peak production may not interfere with daylight changing time
        if self.com_model.sim_params.member_availability == 'permanent':
            self.periods_current_day['morning'] = Period('morning', 0, non_zero_production_indexes_list[0])
            self.periods_current_day['evening'] = Period('evening', non_zero_production_indexes_list[-1], 24)
            self.flexibility_period.hour_min = 0
            self.flexibility_period.hour_max = 24
        if self.com_model.sim_params.member_availability == 'day':
            self.periods_current_day['morning'] = Period('morning', 7, non_zero_production_indexes_list[0])
            self.periods_current_day['evening'] = Period('evening', non_zero_production_indexes_list[-1], 23)
            self.flexibility_period.hour_min = 7
            self.flexibility_period.hour_max = 23

        if non_zero_production_indexes_list[0] < self.flexibility_period.hour_min:
            self.periods_current_day['midday1'] = Period('midday1', 7, peak_production_indexes_list[0])
        else:
            self.periods_current_day['midday1'] = Period('midday1', non_zero_production_indexes_list[0],
                                                         peak_production_indexes_list[0])

        self.periods_current_day['peak_prod'] = Period('peak_prod', peak_production_indexes_list[0],
                                                       peak_production_indexes_list[-1])
        self.periods_current_day['midday2'] = Period('midday2', peak_production_indexes_list[-1],
                                                     non_zero_production_indexes_list[-1])

        for period_name in self.period_names_list:
            self.periods_current_day[period_name].update_length()

    def initialize_residential_community_manager(self):
        """AEM initialization function that resets data buffers and allocates
            production limits; the function is called at the beginning of each day"""

        self.initialize_pv_plant_for_next_day()
        member_consumption_models = [member.est_member_load_day for member in self.cm_list]
        time_frame = [pd.Timestamp(date) for date in self.com_model.timeframe_current_day]
        self.est_com_load_day.set_time_frame(time_frame)
        self.est_com_load_day.set_data(member_consumption_models)
        
        production = self.virtual_pv.est_com_prod_day.data

        # Adapt the midday period to take into account only the PV production period
        non_zero_production_indexes_list = [index for index in range(len(production)) if production[index] > 0]

        peak_production_indexes_list = self.get_peak_production_period()
        # If the peak period is represented by only one sample, we ignore it
        if len(peak_production_indexes_list) > 0:
            self.peak_production_current_day = True
            if self.com_model.sim_params.print_member_info:
                print("Peak production period:", peak_production_indexes_list)
        else:
            peak_production_indexes_list = []
            self.peak_production_current_day = False
            if self.com_model.sim_params.print_member_info:
                print("No peak production period")

        self.set_periods_for_current_day(non_zero_production_indexes_list, peak_production_indexes_list)

        # Print some data
        if self.peak_production_current_day and self.model.sim_params.print_member_info:
            print("AEM Ready (current day production data from " +
                  str(non_zero_production_indexes_list[0]) + " to " + str(non_zero_production_indexes_list[-1]) + ")")

        if self.model.sim_params.print_member_info:
            print("------------------------------------")
            print("AEM HAS SET THE PERIODS OF THE DAY:")
            for name in self.period_names_list:
                print(self.period_names_list)
                print(self.periods_current_day[name].name + " : h_min = " + str(
                    self.periods_current_day[name].hour_min) + " h_max = " + str(
                    self.periods_current_day[name].hour_max))
            print("------------------------------------")

    def get_estimated_community_consumption_for_period(self, period) -> list:
        """Computes the expected load profile of the community for the current day

         :returns: A list with 24 values representing the total expected consumption of the community
         :rtype: list"""

        hours = list(range(period.hour_min, period.hour_max))
        load_profile = [0] * len(hours)
        for i, agent in itertools.product(range(len(hours)), self.cm_list):
            load_profile[i] += agent.est_consumption_current_day_list[hours[i]]
        return load_profile

    def get_load_profiles_dict_for_optimisation(self) -> Dict:
        """Generates a dictionary with load expected profiles for each agent during a specific period.

         :param: the predefined period of the day for which the profiles are computed
         :type: str
         :returns: A dictionary with agent ids as keys, where each value represents the expected consumption list of the respective agent during the period
         :rtype: dict"""

        # Dict key is the agent unique ID
        consumption_by_period_dict = {}
        for period_name in self.period_names_list:
            load_profiles_dict = {}
            for agent in self.cm_list:
                load_profiles_dict[str(agent.unique_id)] = \
                    agent.est_consumption_current_day_df.loc[
                        (agent.est_consumption_current_day_df.index.hour >= self.periods_current_day[
                            period_name].hour_min) &
                        (agent.est_consumption_current_day_df.index.hour < self.periods_current_day[
                            period_name].hour_max)][
                        'Consumption'].to_list()
            consumption_by_period_dict[period_name] = load_profiles_dict

        return consumption_by_period_dict

    def get_production_profiles_for_optimisation(self) -> Dict:
        """Generates a list the expected production values for the given period.

         :param: the predefined period of the day for which the profiles are computed
         :type: str
         :returns: A dict with periods as keys and production values for each period
         :rtype: Dict"""

        """ Prepare production power profile for each period for the optimisation problem """
        # Power profile is a list
        production_by_period_dict = {}
        for period_name in self.period_names_list:
            production_by_period_dict[period_name] = self.virtual_pv.exp_community_production_current_day_df.loc[(
                    (self.virtual_pv.exp_community_production_current_day_df['hour'] >= self.periods_current_day[
                        period_name].hour_min) &
                    (self.virtual_pv.exp_community_production_current_day_df['hour'] < self.periods_current_day[
                        period_name].hour_max))][
                'Production'].to_list()

        return production_by_period_dict

    def communicate_actions_to_agents(self):
        """Sends the recommendations for the current day to each agent in the simulation."""

        if self.model.sim_params.recommendation_strategy == 'coaching' and \
                self.model.sim_params.manager_target_of_recommendations == 'categories':
            for agent in self.cm_list:
                for i in range(0, self.flexibility_period.hour_min):
                    agent.actions_to_be_done_current_day_list[i] = 0
                    agent.req_consumption_equal_share_current_day[i] = agent.est_consumption_current_day_list[i]
                for period_name in self.period_names_list:
                    hours_in_period_list = list(range(self.periods_current_day[period_name].hour_min,
                                                      self.periods_current_day[period_name].hour_max))
                    for hour in hours_in_period_list:
                        if self.recommendations_current_day_by_period[period_name][agent.unique_id][
                            'cat_' + agent.type + '_same'].varValue == 1.0:
                            agent.actions_to_be_done_current_day_list[hour] = 0
                            agent.req_consumption_equal_share_current_day[hour] = \
                                agent.est_consumption_current_day_list[hour]
                        if self.recommendations_current_day_by_period[period_name][agent.unique_id][
                            'cat_' + agent.type + '_mod_inc'].varValue == 1.0:
                            agent.actions_to_be_done_current_day_list[hour] = 0.5
                            agent.req_consumption_equal_share_current_day[hour] = (1 + self.moderate_load_change) * \
                                                                                  agent.est_consumption_current_day_list[
                                                                                      hour]
                        if self.recommendations_current_day_by_period[period_name][agent.unique_id][
                            'cat_' + agent.type + '_mod_dec'].varValue == 1.0:
                            agent.actions_to_be_done_current_day_list[hour] = -0.5
                            agent.req_consumption_equal_share_current_day[hour] = (1 - self.moderate_load_change) * \
                                                                                  agent.est_consumption_current_day_list[
                                                                                      hour]
                        if self.recommendations_current_day_by_period[period_name][agent.unique_id][
                            'cat_' + agent.type + '_strong_inc'].varValue == 1.0:
                            agent.actions_to_be_done_current_day_list[hour] = 1
                            agent.req_consumption_equal_share_current_day[hour] = (1 + self.strong_load_change) * \
                                                                                  agent.est_consumption_current_day_list[
                                                                                      hour]
                        if self.recommendations_current_day_by_period[period_name][agent.unique_id][
                            'cat_' + agent.type + '_strong_dec'].varValue == 1.0:
                            agent.actions_to_be_done_current_day_list[hour] = -1
                            agent.req_consumption_equal_share_current_day[hour] = (1 - self.strong_load_change) * \
                                                                                  agent.est_consumption_current_day_list[
                                                                                      hour]
                for hour in range(self.flexibility_period.hour_max, len(self.model.time_range_current_day)):
                    agent.actions_to_be_done_current_day_list[hour] = 0
                    agent.req_consumption_equal_share_current_day[hour] = agent.est_consumption_current_day_list[hour]
        else:
            for agent in self.cm_list:
                agent.actions_to_be_done_current_day_list = self.recommendations_current_day_list

        if self.model.sim_params.print_member_info:
            for agent_type in self.cm_types_in_community:
                print(agent_type + " agents have the following recommendations: " + str(
                    self.members_by_type[agent_type][0].actions_to_be_done_current_day_list))
        return

    def set_agents_requested_consumption_levels(self):
        load_profile_current_day = self.est_com_load_day.data
        production_current_day = self.virtual_pv.est_com_prod_day.data
        for agent in self.cm_list:
            for hour in range(0, len(self.com_model.timeframe_current_day)):
                agent.req_consumption_dif_share_current_day[hour] = (agent.est_member_load_day.data[hour] /
                                                                     load_profile_current_day[hour]) * \
                                                                    production_current_day[hour]
                agent.req_consumption_equal_share_current_day[hour] = agent.est_member_load_day.data[hour] + \
                                                                      0.2 * (production_current_day[hour] -
                                                                             load_profile_current_day[hour])

    def set_optimal_recommendations_current_day(self):
        """Computes the optimal recommendations for the coaching approach."""

        consumption_by_period_dict = self.get_load_profiles_dict_for_optimisation()
        production_by_period_dict = self.get_production_profiles_for_optimisation()
        actions = {}
        for period_name in self.period_names_list:
            actions[period_name] = optimisation_problem_general(consumption_by_period_dict[period_name],
                                                                production_by_period_dict[period_name], self)
            if self.model.sim_params.print_member_info:
                print("-- " + period_name + " RECOMMENDED ACTIONS --")
        self.recommendations_current_day_by_period = actions

    def set_recommendations_current_day_dummy_approach(self):
        """Computes the recommendations for the dummy approach."""

        self.recommendations_current_day_list = [0 for x in range(len(self.com_model.timeframe_current_day))]

    def set_recommendations_current_day_first_stage_reactive_approach_no_expectancy(self):
        """Computes recommendations for the two-turn informative approach with no expectancy"""

        consumption = self.get_estimated_community_consumption_current_day()
        production = self.virtual_pv.exp_community_production_current_day_list
        recommendations = []

        for hour in range(len(self.com_model.timeframe_current_day)):
            recommendation = 0
            if self.flexibility_period.hour_min < hour < self.flexibility_period.hour_max:
                if consumption[hour] > production[hour] + self.model.sim_params.no_alert_threshold:
                    recommendation = -1
                elif production[hour] > consumption[hour] + self.model.sim_params.no_alert_threshold:
                    recommendation = 1
            recommendations.append(recommendation)

        self.recommendations_current_day_list = recommendations

    def set_recommendations_current_day_first_stage_reactive_informative_approach(self):
        """Computes recommendations for the two-turn informative approach with adaptive expectancy"""

        consumption = self.get_estimated_community_consumption_current_day()
        production = self.virtual_pv.exp_community_production_current_day_list
        recommendations = []
        possible_recommendations = [-1, -0.5, 0, 0.5, 1]
        match = None
        for hour in range(len(self.model.time_range_current_day)):
            recommendation = 0
            recommendation_match = None
            if self.flexibility_period.hour_min < hour < self.flexibility_period.hour_max:
                for rec in possible_recommendations:
                    match = abs(consumption[hour] - production[hour] + self.delta_power_by_recommendation[
                        rec])  # delta power is with + as same as consumption in the neeg equation
                    if recommendation_match is None or match < recommendation_match[1]:
                        recommendation_match = (rec, match)
                if match > self.model.sim_params.no_alert_threshold:
                    recommendation = recommendation_match[0]
                if self.model.sim_params.print_member_info:
                    print(str(production[hour]) + " " + str(consumption[hour]) + " " + str(hour) + str(recommendation))
            recommendations.append(recommendation)

        self.recommendations_current_day_list = recommendations

    def set_recommendations_current_day_informative_approach(self):
        """Computes recommendations for the one turn informative approach with adaptive expectancy"""

        if self.model.sim_params.print_member_info:
            print('PREVIOUS IMPACT: ', self.delta_power_by_recommendation)
        consumption = self.get_estimated_community_consumption_current_day()
        production = self.virtual_pv.exp_community_production_current_day_list
        recommendations = []
        match = None
        for hour in range(len(self.model.time_range_current_day)):
            recommendation = 0
            recommendation_match = None
            if self.flexibility_period.hour_min < hour < self.flexibility_period.hour_max:
                for rec in self.delta_power_by_recommendation:
                    match = abs(consumption[hour] - production[hour] + self.delta_power_by_recommendation[
                        rec])  # delta power is with + as same as consumption in the neeg equation
                    if recommendation_match is None or match < recommendation_match[1]:
                        recommendation_match = (rec, match)
                if match > self.model.sim_params.no_alert_threshold:
                    recommendation = recommendation_match[0]
                if self.model.sim_params.print_member_info:
                    print(str(production[hour]) + " " + str(consumption[hour]) + " " + str(hour) + str(recommendation))
            recommendations.append(recommendation)

        self.recommendations_current_day_list = recommendations

    def get_color_index_delta_powers(self):
        """Computes recommendations for the one turn dynamic approach with basic expectancy"""

        # Get required data
        consumption_until_now = self.sim_community_consumption_total
        production_until_now = self.sim_community_production_total

        # Set delta powers from the simulation until now
        delta_powers = [production_until_now[i] - consumption_until_now[i] for i in range(len(consumption_until_now))]
        delta_powers.extend([-d for d in delta_powers[::-1]])
        delta_powers.sort()

        if self.model.days_passed > 7:

            # Based on ratios, determine the number of delt powers per recommendation
            number_of_delta_powers_per_rec = [int(color_ratio * len(delta_powers)) for color_ratio in self.rec_ratios]

            number_of_delta_powers_per_rec[int((2 * len(self.possible_recs) - 1) / 2)] += len(delta_powers) - sum(
                number_of_delta_powers_per_rec)

            # Define a dictionary with indexes starting from strong decrease in consumption (-1) and going to strong increase (+1)
            rec_index_deltas_powers = {i: [] for i in range(len(self.possible_recs))}
            color_index = 0
            rec_index_deltas_powers[0] = list()
            for i in range(len(delta_powers)):
                # Assign delta powers according to the recommendation and previous ratios
                if len(rec_index_deltas_powers[color_index]) < number_of_delta_powers_per_rec[color_index]:
                    rec_index_deltas_powers[color_index].append(delta_powers[i])
                else:
                    color_index += 1
                    rec_index_deltas_powers[color_index].append(delta_powers[i])

            return rec_index_deltas_powers
        else:
            return None

    def set_recommendations_current_day_dynamic_approach(self, color_index_deltas_powers):

        consumption_current_day = self.get_estimated_community_consumption_current_day()
        production_current_day = self.virtual_pv.exp_community_production_current_day_list
        # Set recommendations
        recommendations = []
        for hour in range(len(self.model.time_range_current_day)):
            if self.model.days_passed > 7:
                recommendation = 0
                if abs(consumption_current_day[hour] - production_current_day[
                    hour]) > self.model.sim_params.no_alert_threshold:
                    delta_power = production_current_day[hour] - consumption_current_day[hour]
                    returned_color = 0
                    # print(color_index_deltas_powers)
                    if len(color_index_deltas_powers[0]) > 0 and delta_power < color_index_deltas_powers[0][-1]:
                        returned_color = self.possible_recs[0]
                    elif len(color_index_deltas_powers[len(self.possible_recs) - 1]) > 0 and delta_power > \
                            color_index_deltas_powers[len(self.possible_recs) - 1][0]:
                        returned_color = self.possible_recs[-1]
                    else:
                        for i in range(1, len(self.possible_recs) - 1, 1):
                            if len(color_index_deltas_powers[i]) > 0 and color_index_deltas_powers[i][
                                0] <= delta_power <= color_index_deltas_powers[i][-1]:
                                returned_color = self.possible_recs[i]
                    if returned_color is None:
                        recommendation = 0
                    else:
                        recommendation = returned_color
            else:
                recommendation = 0

            recommendations.append(recommendation)

        if self.model.sim_params.print_member_info:
            print('RECOMENDATIONS', recommendations)
        # if self.model.days_passed == 9:
        #    x = production-consumption
        self.recommendations_current_day_list = recommendations

    def get_residential_community_with_coaching_manager_daily_results_total(self):
        """Generates a dataframe with community data to be written in files."""

        community_results_data = self.get_community_core_daily_results_total_dict()

        for period_name in self.period_names_list:
            community_results_data[period_name + '_hour_min'] = [period.hour_min for period in
                                                                 self.sim_periods_total[period_name]]
            community_results_data[period_name + '_hour_max'] = [period.hour_max for period in
                                                                 self.sim_periods_total[period_name]]
        return community_results_data

    def write_residential_community_with_coaching_manager_data_to_files(self):
        """Write recorded data to files, according to the type of community.
        We need the periods in files in the coaching approach."""

        # Extract and write community and agent daily data to csv
        self.get_agent_daily_results_total().to_csv(self.locator.agents_results_path)
        community_results = self.get_residential_community_with_coaching_manager_daily_results_total()
        community_results_df = pd.DataFrame(community_results)
        community_results_df.to_csv(self.locator.com_hourly_results_path)
        self.get_community_hourly_results_total().to_csv(self.locator.cm_hourly_results_path)
        if self.model.sim_params.print_member_info:
            print("-- Writing residential community data (with coaching manager) to files --")

    def record_residential_community_with_coaching_manager_data_from_agents_current_day(self):
        """Records data for the coaching approach, including the periods."""

        self.record_core_community_data_from_agents_current_day()
        for period_name in self.period_names_list:
            self.sim_periods_total[period_name].append(self.periods_current_day[period_name])
        if self.com_model.sim_params.print_member_info:
            print("Residential Coaching Community Manager has recorded community data")

    def set_basic_recommendations_current_day(self):
        """Sets basic informative recommendations for the current day."""

        if self.model.sim_params.print_member_info:
            print("SETTING RECOMMENDATIONS BASIC APPROACH")
        consumption = self.get_estimated_community_consumption_current_day()
        production = self.virtual_pv.exp_community_production_current_day_list
        recommendations = []

        for hour in range(len(self.model.time_range_current_day)):
            recommendation = 0
            if self.flexibility_period.hour_min < hour < self.flexibility_period.hour_max:
                if production[hour] > consumption[hour] + self.model.sim_params.no_alert_threshold:
                    recommendation = 0.5
                elif consumption[hour] > production[hour] + self.model.sim_params.no_alert_threshold:
                    recommendation = -0.5
            recommendations.append(recommendation)

        self.recommendations_current_day_list = recommendations

    def set_coaching_private_recommendations_current_day(self):
        """Sets coaching recommendations for the current day considering the willigness of the members."""

        production_by_period_dict = self.get_production_profiles_for_optimisation()

        rec_current_day_list = [0] * self.flexibility_period.hour_min

        for period in self.period_names_list:
            p_com_load = self.get_estimated_community_consumption_for_period(self.periods_current_day[period])
            if self.model.sim_params.expectancy_development_approach == 'adaptive':
                if self.model.sim_params.print_member_info:
                    print("SETTING RECOMMENDATIONS PRIVATE COACHING APPROACH WITH ADAPTIVE EXPECTANCY")
                recommendations = optimisation_problem_coaching_private_with_adaptive_expectancy(p_com_load,
                                                                                                 production_by_period_dict[
                                                                                                     period],
                                                                                                 self.delta_power_by_recommendation)
            else:
                if self.model.sim_params.print_member_info:
                    print("SETTING RECOMMENDATIONS PRIVATE COACHING APPROACH NO ADAPTIVE EXPECTANCY")
                if p_com_load:
                    recommendations = optimisation_problem_coaching_private_no_adaptive_expectancy(p_com_load,
                                                                                                   production_by_period_dict[
                                                                                                       period], self)
                else:
                    recommendations = []
            rec_current_day_list.extend(recommendations)

        rec_current_day_list.extend([0] * (len(self.model.time_range_current_day) - self.flexibility_period.hour_max))

        self.recommendations_current_day_list = rec_current_day_list
