import os
from energy_community.helpers.Spec import Spec
from plotwriteapp.functions import plot_hourly_evolution_comparison_specific_months_and_days, plot_effort_comparison, \
    plot_indicators_comparison, plot_pareto_neeg_recommendations_count, plot_pareto_2_by_2_neeg_rec_deviation, plot_results_in_browser
from plotwriteapp.writing_functions import write_results_no_estimation_comparison
from simulation.Configurator import Configuration
from energy_community.helpers.Results import Results, get_simulation_results_from_files
from simulation.params import Parms
from energy_community.ResidentialCommunityGrenoble import ResidentialCommunityGrenoble

cfg = Configuration('setup.ini')
#os.system("jupyter nbconvert --clear-output --inplace management_strategies_comparison.ipynb")

simulation_scenarios_to_run = [1,2,3,4,5,6,7,8]  # choose what simulation scenarios you want to run from scenarios.xlsx
# Warning: don't plot more then 10 scenarios in a notebook
#simulation_scenarios_to_run = [6, 7, 13, 14, 20, 21, 27, 28]  # dynamic strategy variants
write_consumption_data_first_time = False  # do not change
plot_flag = True
print_flag = True


for simulation_id in simulation_scenarios_to_run:
    sim_params = Parms(simulation_id, print_flag)
    sim_params.print_simulation_parameters()

    model = ResidentialCommunityGrenoble(sim_params, write_consumption_data_first_time)

    for i in range(model.sim_params.simulation_steps):
        model.step()

    if plot_flag:
        plot_results_in_browser(model)

    print(" ---- SIMULATION DONE ---- ")