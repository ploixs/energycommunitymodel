from mesa.time import StagedActivation
from energy_community.ResidentialCommunityManager import ResidentialCommunityManager

from energy_community.residential_manager_types import OneTurnInformativeEveryoneBasicExpectancy, \
    OneTurnInformativeEveryoneAdaptiveExpectancy, OneTurnCoachingEveryoneAdaptiveExpectancy, \
    OneTurnCoachingEveryoneBasicExpectancy, OneTurnDummyBasicExpectancy, OneTurnDynamicEveryoneBasicExpectancy, \
    TwoTurnsInformativeEveryoneAdaptiveExpectancy, TwoTurnsInformativeEveryoneBasicExpectancy
from energy_community.residential_member_types import OneTurnRealisticIntensity, TwoTurnsRealisticIntensity
from energy_community.core.EnergyCommunityModel import EnergyCommunityModel
from energy_community.core.energy.get_power_data import write_total_house_consumption_to_csv, get_consumption_for_a_house_from_csv


class ResidentialCommunityGrenoble(EnergyCommunityModel):

    def __init__(self, sim_params, write_consumption_data=False):
        super().__init__(sim_params)
        self.community_manager : ResidentialCommunityManager | None = None

        if self.sim_params.manager_stages == 'two_turns':
            self.schedule = StagedActivation(self, ['first_stage', 'second_stage', 'third_stage'])
        else:
            self.schedule = StagedActivation(self, ['first_stage', 'second_stage'])

        self.set_community_manager()

        self.set_available_houses_ids_from_grenoble()

        if write_consumption_data:
            write_total_house_consumption_to_csv(self.available_houses_id_list, self.sim_params.starting_date,
                                                 self.sim_params.ending_date)

    def print_members_in_community(self):
        print("Community members ids: ", [m.unique_id for m in self.community_manager.cm_list])
    
    def print_current_status_of_members(self):
        print(f"Currently, the community has: ")
        for key in self.community_manager.members_by_type.keys():
            print(f"{len(self.community_manager.members_by_type[key])} {key} agents")
    
    def get_agent_type_to_be_added(self) -> str:
        """Gets the next agent type to be added in the community.

         :rtype: str"""

        agent_type_to_be_added = None
        for agent_type in self.sim_params.willingness_types_in_community:
            if agent_type not in self.community_manager.cm_types_in_community:
                agent_type_to_be_added = agent_type
                break
            if len(self.community_manager.members_by_type[agent_type]) < int(self.sim_params.agents_lim[agent_type]):
                agent_type_to_be_added = agent_type
                break
            else:
                if self.sim_params.print_member_info:
                    print("Agent limit reached for type: ", agent_type)
                continue
        if self.sim_params.print_member_info:
            print("Next agent added will be ", agent_type_to_be_added)
        return agent_type_to_be_added
    
    def remove_agents_who_left_community(self):
        """Checks if current simulation time corresponds with the last timestamp of a house consumption dataset. \
            If yes, removes the house from the model."""

        agents_to_be_removed_list = []
        for agent in self.community_manager.cm_list:
            if agent.unique_id != 999:
                # if agent.leaving_date < self.current_time:
                if agent.leaving_date < self.timeframe_simulation[self.current_hour_index]:
                    agents_to_be_removed_list.append(agent)
        for agent in agents_to_be_removed_list:
            self.community_manager.members_by_type[agent.type].remove(agent)
            self.schedule.remove(agent)
            if self.sim_params.print_member_info:
                print(agent.type + " agent with ID " + str(agent.unique_id) + " has been removed.")
    
    def add_new_community_member(self, house_id, agent_type, consumption_df):
        if not house_id:
            print('ERROR: house id not set!')
            return
        if not agent_type:
            print('ERROR: agent type not set!')
            return
        if consumption_df is None:
            print('ERROR: expected consumption data not set!')
            return

        # Create a member object according to the simulation configuration
        new_member = None
        if self.sim_params.member_stages == 'two_turns':
            new_member = TwoTurnsRealisticIntensity(house_id, self, agent_type, consumption_df)
        elif self.sim_params.member_stages == 'one_turn':
            new_member = OneTurnRealisticIntensity(house_id, self, agent_type, consumption_df)

        # Assign the member to the community
        self.schedule.add(new_member)
        if agent_type not in self.community_manager.cm_types_in_community:
            self.community_manager.update_agent_types_in_community(agent_type)
        self.community_manager.members_by_type[agent_type].append(new_member)
        self.community_manager.cm_list.append(new_member)

        if not new_member:
            print("WARNING: configuration not found. member not assigned.")
            return
        if self.sim_params.print_member_info:
            print(f"Agent {new_member.unique_id} added to the community as a {agent_type} agent")

    def set_available_houses_ids_from_grenoble(self):

        '''
        WORKING, BUT DB NEEDED
        db_path = Configuration.get_consumption_data_config('source_folder')
        db_path = os.path.join(db_path, 'irise.sqlite3')
        db = sqlite3.connect(os.path.normpath(db_path))
        # Extract houses data from the database
        df = pd.read_sql_query("SELECT ID, ZIPcode, Location  FROM HOUSE WHERE ZIPcode LIKE '381%'", db)
        self.available_houses_id_list = df['ID'].to_list()
        '''
        self.available_houses_id_list = ['2000900', '2000903', '2000908', '2000910', '2000911']
        if self.sim_params.print_member_info:
            print(f"House ids successfully set!")

    def check_and_add_new_community_members(self):
        """Checks if current simulation time corresponds with the first timestamp of a house consumption dataset. \
        If yes, adds the house as a community member, depending on the required community configuration."""

        ids_of_added_houses = [x.unique_id for x in self.community_manager.cm_list]
        if self.sim_params.print_member_info:
            print("Added houses until now:", ids_of_added_houses)
        # At each step, add more agents to the model, according ot their joining dates
        for house_id in self.available_houses_id_list:
            # df, ok = get_consumption_for_a_house_from_database(house_id, self.sim_params.starting_date,
            #                                                   self.sim_params.ending_date, True)
            df, ok = get_consumption_for_a_house_from_csv(house_id, self.sim_params.starting_date,
                                                          self.sim_params.ending_date)
            if ok == 0 or len(df) != len(self.timeframe_simulation):
                if self.sim_params.print_member_info:
                    print('Error at consumption processing')
                return

            agent_type_to_be_added = self.get_agent_type_to_be_added()
            if house_id not in ids_of_added_houses and agent_type_to_be_added:
                if self.sim_params.print_member_info:
                    print(f"Community member found. ID : {house_id}")
                self.add_new_community_member(house_id, agent_type_to_be_added, df)

    def set_community_manager(self):
        """Sets the community manager according to the specification in the scenario_description file."""
        manager = None

        if self.sim_params.manager_stages == 'one_turn':
            if self.sim_params.expectancy_development_approach == 'basic' and \
                    self.sim_params.recommendation_strategy == 'informative':
                manager = OneTurnInformativeEveryoneBasicExpectancy(999, self)

            if self.sim_params.expectancy_development_approach == 'adaptive' and \
                    self.sim_params.recommendation_strategy == 'informative':
                manager = OneTurnInformativeEveryoneAdaptiveExpectancy(999, self)

            if self.sim_params.expectancy_development_approach == 'adaptive' and \
                    self.sim_params.recommendation_strategy == 'coaching':
                manager = OneTurnCoachingEveryoneAdaptiveExpectancy(999, self)

            if self.sim_params.expectancy_development_approach == 'basic' and \
                    self.sim_params.recommendation_strategy == 'coaching':
                manager = OneTurnCoachingEveryoneBasicExpectancy(999, self)

            if self.sim_params.expectancy_development_approach == 'basic' and \
                    self.sim_params.recommendation_strategy == 'dummy':
                manager = OneTurnDummyBasicExpectancy(999, self)

            if self.sim_params.expectancy_development_approach == 'basic' and \
                    self.sim_params.recommendation_strategy == 'dynamic':
                manager = OneTurnDynamicEveryoneBasicExpectancy(999, self)

            if self.sim_params.expectancy_development_approach == 'basic' and \
                    self.sim_params.recommendation_strategy == 'performant_dynamic':
                manager = OneTurnDynamicEveryoneBasicExpectancy(999, self, (0.2, 0.2, 0.2, 0.2, 0.2))

            if self.sim_params.expectancy_development_approach == 'basic' and \
                    self.sim_params.recommendation_strategy == 'relaxed_dynamic':
                manager = OneTurnDynamicEveryoneBasicExpectancy(999, self, (0.05, 0.1, 0.7, 0.1, 0.05))

        elif self.sim_params.manager_stages == 'two_turns':
            if self.sim_params.expectancy_development_approach == 'adaptive' and \
                    self.sim_params.recommendation_strategy == 'informative':
                manager = TwoTurnsInformativeEveryoneAdaptiveExpectancy(999, self)
            if self.sim_params.expectancy_development_approach == 'basic' and \
                    self.sim_params.recommendation_strategy == 'informative':
                manager = TwoTurnsInformativeEveryoneBasicExpectancy(999, self)

        self.schedule.add(manager)  # He must be the first agent added to schedule

        self.community_manager = manager

    def step(self):
        """Advances the model by one step."""

        if self.current_hour_index == self.start_of_day_hour_indexes[0]:
            if self.sim_params.print_member_info:
                print("- Removing old community members -")
            self.remove_agents_who_left_community()  # Check and remove agents who left the community
            if self.sim_params.print_member_info:
                print("- Searching for new community members -")
            self.check_and_add_new_community_members()  # Check and add new agents according to current time and agent joining time

            if self.sim_params.print_member_info:
                self.print_members_in_community()
                self.print_current_status_of_members()

        # Record data externally to files and figures
        if self.current_hour_index in self.start_of_day_hour_indexes:
            self.current_hour_index_day_relative = 0

            self.set_timeframe_current_day()
            self.days_passed += 1
            if self.sim_params.print_member_info:
                print(f"DAYS PASSED: {self.days_passed} in scenario {self.sim_params.simulation_id}")


        if self.sim_params.print_member_info:
            # Make a model step and update the hour
            print("------------------------------------")
            print(f"Current simulation datetime: {self.timeframe_simulation[self.current_hour_index]}")
            print(f"Current Hour: {self.timeframe_simulation[self.current_hour_index]} "
                  f"Current index: {self.current_hour_index} "
                  f"Current day-relative index:  {self.current_hour_index_day_relative}")
            print("------------------------------------")

        self.schedule.step()

        # Update time
        self.current_hour_index += 1
        self.current_hour_index_day_relative += 1
