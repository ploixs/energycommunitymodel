import logging
import os
import random
import sys
from typing import List

from energy_community.core.constants import ExpectancyDev, PrivacyLevel, Strategy, TargetOfRec
from energy_community.core.energy.get_power_data import get_mean_icp_values_from_files
from energy_community.subway.SubwayParameters import SubwayParameters
from energy_community.subway.core import SubwayCommunity
from energy_community.subway.profiles import OneTurnNoFlexibilitySubwayMember, OneTurnSemiFlexibleSubwayMember, OneTurnSubwayManager, OneTurnFullFlexibilitySubwayMember


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("energycommunity/subway/results/debug.log", mode='w'),
        logging.StreamHandler(sys.stdout),
    ]
)

def get_pref_hr_list(pref_hrs : List[int], distribution: List[float], cm_count: int) -> List[int]:
    """A function to propose a list of preferred hours to commute for all the members, according to the given distribution.

    Args:
        pref_hrs (List[int]): possible preferred hours to commute
        distribution (List[float]): the distribution between preferred hours
        cm_count (int): total number of community members

    Returns:
        List[int]: a list of preferred hours to commute, with the same size as the cm_count
    """
    pref_hours_dist = {pref_hour : int(distribution[i] * cm_count)  for i,pref_hour in enumerate(pref_hrs)}
    temp =  list([pref_hour] * times for pref_hour, times in pref_hours_dist.items())
    pref_hr_list = [item for sublist in temp for item in sublist]   
    return pref_hr_list

if __name__ == "__main__":
    
    # PROFILING TOOL
    # yappi -f pstat  -o profiling.out subway_station_community.py
    # snakeviz profiling.out

    #simulation_scenarios_to_run = [9,10,11,12,13,14,15] # paper scenarios
    simulation_scenarios_to_run = [3] # DEMO SCENARIO
    #simulation_scenarios_to_run = [13] 
    
    plot_in_browser = True # plot results in browser for each simulation using plotly
    print_flag = True # print messages during simulation
    

    for simulation_id in simulation_scenarios_to_run:
        sim_params = SubwayParameters(os.path.join("energy_community", "subway", "scenarios.csv"), simulation_id, print_flag) # define parameters
        est_mean_icp_df = get_mean_icp_values_from_files(sim_params.starting_date, sim_params.ending_date) # get consumption data
        
        #results_folder_path = f"/Users/mirceastefansimoiu/Library/CloudStorage/OneDrive-UniversitateaPolitehnicaBucuresti/_Cercetare/_Data_and_results/energy_community_input_data/results/subway_station/Scenario_{sim_params.simulation_id}"
        results_folder_path = os.path.join("energy_community","subway","results",f'Scenario_{sim_params.simulation_id}') #  default results folder - you may change it to desired folder on local machine
        model = SubwayCommunity(sim_params, results_folder_path) # define the model
        manager = None
        if simulation_id in [1, 4, 7]:
            manager = OneTurnSubwayManager(999, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.1, weight_dis=0.9)
        if simulation_id in [2, 5, 8]:
            manager = OneTurnSubwayManager(999, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.5, weight_dis=0.5)
        if simulation_id in [3, 6]:
            manager = OneTurnSubwayManager(999, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.9, weight_dis=0.1)
        if simulation_id in [9,10,11]:
            manager = OneTurnSubwayManager(10000, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=1, weight_dis=0)
        if simulation_id == 12:
            manager = OneTurnSubwayManager(10000, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.MULTI_OBJECTIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0.5, weight_dis=0.5)
        if simulation_id in [13,14,15]:
            manager = OneTurnSubwayManager(10000, model, TargetOfRec.EVERYONE, ExpectancyDev.BASIC, Strategy.INFORMATIVE, PrivacyLevel.PRIVATE, 0, weight_neeg=0, weight_dis=0)
        if manager is None:
            raise ValueError("Manager not set!")
        model.community_manager = manager

        members = []
        if simulation_id in [1,2,3]:
            cm_count = 20
            pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
            for i in range(cm_count):
                new_cm = OneTurnFullFlexibilitySubwayMember(i,model, est_mean_icp_df, pref_hr_list[i] )
                members.append(new_cm)
        
        if simulation_id in [4,5,6]:
            cm_count = 20
            pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
            for i in range(cm_count):
                new_cm = OneTurnSemiFlexibleSubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
                members.append(new_cm)
        
        if simulation_id in [7]:
            cm_count = 20
            pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
            for i in range(cm_count):
                new_cm = OneTurnNoFlexibilitySubwayMember(i,model, est_mean_icp_df,pref_hr_list[i])
                members.append(new_cm)
                
        if simulation_id in [9,13]:
            cm_count = 4000
            pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
            for i in range(cm_count):
                new_cm = OneTurnFullFlexibilitySubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
                members.append(new_cm)
                
        if simulation_id in [10,14]:
            cm_count = 4000
            pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
            for i in range(cm_count):
                new_cm = OneTurnSemiFlexibleSubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
                members.append(new_cm)
                
        if simulation_id == 11:
            cm_count = 4000
            pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count)
            for i in range(cm_count):
                new_cm = OneTurnNoFlexibilitySubwayMember(i,model, est_mean_icp_df, pref_hr_list[i])
                members.append(new_cm)
                
        if simulation_id in [12,15]:
            cm_count = 4000
            pref_hr_list = get_pref_hr_list([7,8,9,10],[0.1, 0.3, 0.4, 0.2],cm_count) # set pref hours for all members
            random.shuffle(pref_hr_list) # shuffle prefered hours between members
            cm_count_flexible = 0.2 * cm_count # 20% are fully flexible members
            cm_flexible_ids = list(range(int(cm_count_flexible)))
            cm_count_semi_flexible = 0.6 * cm_count # 60% are semi-fully flexible members
            cm_semi_flexible_ids = list(range(cm_flexible_ids[-1] + 1,cm_flexible_ids[-1] + int(cm_count_semi_flexible) + 1))
            cm_count_no_flexibility = 0.2 * cm_count # 20% have no flexibility
            cm_no_flexibility_ids = list(range(cm_semi_flexible_ids[-1] + 1,cm_semi_flexible_ids[-1] + int(cm_count_no_flexibility) + 1))
            
            for i in range(int(cm_count_flexible)):
                new_cm = OneTurnFullFlexibilitySubwayMember(cm_flexible_ids[i] ,model, est_mean_icp_df, pref_hr_list[i])
                members.append(new_cm)
            
            for i in range(int(cm_count_semi_flexible)):
                new_cm = OneTurnSemiFlexibleSubwayMember(cm_semi_flexible_ids[i],model, est_mean_icp_df, pref_hr_list[i + int(cm_count_flexible)])
                members.append(new_cm)
                
            for i in range(int(cm_count_no_flexibility)):
                new_cm = OneTurnNoFlexibilitySubwayMember(cm_no_flexibility_ids[i],model, est_mean_icp_df, pref_hr_list[i + int(cm_count_flexible + cm_count_semi_flexible)])
                members.append(new_cm)
        
        model.add_new_community_members(members)
        
        for i in range(model.sim_params.simulation_steps):
            model.step()
        
        if plot_in_browser:
            model.plot_results_in_browser()

        logger = logging.getLogger(__name__)
        logger.info(f" ---- SIMULATION {simulation_id} DONE ---- ")
        

