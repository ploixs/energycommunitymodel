

import os

from energy_community.helpers.Spec import Spec
from energy_community.helpers.analysis import plot_indicators_comparison, plot_pareto_neeg_deviation, plot_scenario_radar_plot, write_community_results_comparison
from energy_community.residential.results.base import ResidentialResults
from configuration import Config


if __name__ == "__main__":
    
    config = Config()
    script_config = config.get_script_config("residential_realistic_adaptive_coaching")
    
    # Set up LaTeX for plotting (optional, comment out if not using LaTeX)
    os.environ['PATH'] = os.environ['PATH'] + ':/Users/mirceastefansimoiu/bin'  # For Mac os, add Latex to plots
    use_tex = True  # Set this to False if you don't want to use LaTeX for plots
    
    pareto_file_name = "Figure_realistic_adaptive_coach_indicators_pareto.pdf"
    radar_plot_file_name = "Figure_realistic_adaptive_coach_indicators_radar.pdf"
    scenarios_file_name = "scenarios_realistic_adaptive_coaching.yaml"
    results_file_name = "results_realistic_adaptive_coaching.csv"
    output_folder_name = "output_realistic_adaptive_coaching"
    
    # Create a list to store the simulation scenarios
    simulation_scenarios = []

    # Set the range of scenarios to plot
    scenarios_to_plot = range(1, 11)

    # Set a flag to control whether to plot hourly comparison or not
    plot_hourly_comp_flag = True

    # Define the results folder path
    results_folder_path = os.path.join(script_config["paths"]["root_folder_name"], 
                                       script_config["paths"]["community_folder_name"], 
                                       output_folder_name)

    # Define the analysis specification
    analysis_spec = Spec(print_member_perf=False,
                        plot_intervals=[('16-03-2015', '21-03-2015'), 
                                        ('19-06-2015', '24-06-2015')],
                        plot_beh_agent_1_flag=True, 
                        get_individual_indicators_flag=False)

    # Iterate over the scenarios_to_plot and calculate performances for each scenario
    for simulation_id in scenarios_to_plot:
        scenario = ResidentialResults(simulation_id, 
                                      simulation_id, 
                                      analysis_spec, 
                                      script_config["paths"], 
                                      scenarios_file_name)
        simulation_scenarios.append(scenario)
        
    plot_pareto_neeg_deviation(simulation_scenarios, 
                               results_folder_path, 
                               save_figures_flag=True, 
                               use_scenario_description=True, 
                               filename=pareto_file_name)

    # Generate and save a radar plot for the scenarios
    plot_scenario_radar_plot(simulation_scenarios, 
                             results_folder_path, 
                             save_figures_flag=True, 
                             default_file_name=radar_plot_file_name)

    # Write community results comparison to a file
    write_community_results_comparison(simulation_scenarios, 
                                       results_folder_path, 
                                       use_scenario_description=True,
                                       default_file_name=results_file_name)

    # Plot the indicators comparison
    plot_indicators_comparison(simulation_scenarios, 
                               results_folder_path, 
                               True, 
                               use_scenario_description=True,
                               filename= "Figure_realistic_adaptive_coach_indicators_comparison.pdf")

    # Plot the hourly evolution comparison if the flag is set
    if plot_hourly_comp_flag:
        for scenario in simulation_scenarios:
            scenario.plot_hourly_evolution_comparison_specific_months_and_days(use_tex_flag=use_tex)


